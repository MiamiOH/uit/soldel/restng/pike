# Pike Library
This library contains a collection of Banner Business Objects with relations. It is currently under development. More objects will be added into this library.

## Installation
Make sure that the satis repo has been added to your composer.json file:

```json
    {
        "repositories": [
            {
                "type": "composer",
                "url": "https://satis.itapps.miamioh.edu/miamioh"
            }
        ]
    }
```

Afterwards, run this in your app directory:

```bash
$ composer require miamioh/pike
```

### For RESTng project

Pike package should be installed as an addon of RESTng instance

#### RESTng Instance Configuration
1. use composer to install pike in `$RESTNG_HOME/../addons`
2. add code block below into `$RESTNG_HOME/../conf/services.php`

```php
$app->addService(array(
    'name' => 'PikeServiceFactory',
    'class' => 'MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory',
    'description' => 'Injectable shim for getting a shared class entry',
    'set' => array(
        'dataSourceFactory' => array('name' => 'APIDataSourceFactory'),
    )
));
```

#### Web Service Project Configuration

Follow [RESTng Documentation: Dependency Injection](http://wsdev.miamioh.edu/api/docs/build_service/dependency_injection/)
to setup dependency injection of pike package into web service class.

Here’s an example of setter dependency declarations for injecting the PikeServiceFactory service:

The service declaration in your RESTconfig file should look like:

```php
$app->addService([
  'name' => 'ServiceName',
  'class' => 'RESTng\Service\Service\ServiceName',
  'set' => [
    'pikeServiceFactory' => [,
      'type' => 'service',
      'name' => 'PikeServiceFactory'
    ],
  ]
]);
```

The methods in your class would be written as:

```php
public function setPikeServiceFactory($pikeServiceFactory) {
    $this->pikeServiceFactory = $pikeServiceFactory;
}
```

### For Laravel Project (need to be fixed later)
Open ```config/app.php``` 

add this to your providers key:

```
MiamiOH\Pike\App\Framework\Laravel\PikeServiceProvider::class,
```

add this to your aliases key:

```
'Pike' => MiamiOH\Pike\App\Framework\Laravel\PikeFacade::class,
```

Publish the config file by running:

```
php artisan vendor:publish 
```
## Testing

The Pike package is heavily dependent on Illuminate packages. In most cases, the Illuminate package maintainers are able to provide forward and backward compatibility. Pike is known to work with Illuminate 6 through 11. This range covers different versions of PHP and requires a matrix approach to testing. Manually executing the tests is possibly using the includes `tests.sh` shell script in conjunction with our PHP build images.

The prerequisites for running the tests is being able to run the necessary containers. Once you have installed Docker Desktop (or the container platform of your choice), make sure you can run:

```bash
docker run -it --rm -v $(pwd):/opt/project -v ~/.composer:/root/.composer miamioh/php:7.3-devtools bash
```

This will bring to a shell in the PHP 7.3 image which is suitable for running tests. Exit the shell and run the same command with the PHP 8.2 image.

Now use the appropriate version of PHP to run the package tests with different versions of Illuminate:

```bash
# PHP 7.3

bash tests.sh --illuminate 6.0
bash tests.sh --illuminate 7.0
bash tests.sh --illuminate 8.0

# PHP 8.2

bash tests.sh --illuminate 8.0
bash tests.sh --illuminate 9.0
bash tests.sh --illuminate 10.0
bash tests.sh --illuminate 11.0
```

**!!IMPORTANT!!** The test script alters the `composer.json` file as it executes tests with different versions. The script backs up the original and restores it after composer runs, but you must be careful not to commit changes which may leak. Carefully review any changes to the `composer.json` file before committing them.
