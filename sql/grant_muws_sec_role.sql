-- DB user MUWS_SEC has both MUWS_SEC_RL and MUWS_GEN_RL roles
-- Any privilege granted to MUWS_GEN_RL does not need to grant
-- to MUWS_SEC_RL again!

-- select
grant select on sfrstcr to muws_sec_rl;

-- update
grant update on sfrstcr to muws_sec_rl;

-- execute
grant execute on fz_midterm_grde_reqd_ind to muws_sec_rl;