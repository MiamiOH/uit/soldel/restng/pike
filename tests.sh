set -e

VERSION=8.0

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
  --illuminate)
    VERSION="$2"
    shift # past argument
    shift # past value
    ;;
  *)                   # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift              # past argument
    ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

echo Running tests with illuminate $VERSION

if [ -e vendor ]; then
  rm -r vendor
fi

if [ -e composer.lock ]; then
  rm composer.lock
fi

cp composer.json composer.json.bak

composer require \
  illuminate/support:^$VERSION \
  illuminate/database:^$VERSION \
  yajra/laravel-oci8:^$VERSION \
  illuminate/cache:^$VERSION \
  illuminate/redis:^$VERSION \
  illuminate/filesystem:^$VERSION \
  -W

mv composer.json.bak composer.json

tools/phpunit tests/Unit
