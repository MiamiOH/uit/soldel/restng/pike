FROM miamioh/php:7.3-cli

# git: required by composer to install packages from Miami's Gitlab
# zip: recommend by composer to unzip package
RUN rm -rf /var/lib/apt/lists/* && apt-get update
RUN apt-get install -y git zip

# install xdebug for generating PHPUnit code coverage
RUN pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)\n\
xdebug.remote_enable=1\n\
xdebug.idekey=phpstorm-xdebug\n\
xdebug.profiler_enable=0\n\
xdebug.profiler_enable_trigger=1\n\
xdebug.profiler_output_dir=/tmp/xdebug_profiles" > /usr/local/etc/php/xdebug.ini

# a script to turn on/off xdebug
RUN echo '#!/bin/bash\n\
\n\
case $1 in\n\
    enable|on)\n\
        ln -sf /usr/local/etc/php/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini\n\
        ;;\n\
    disable|off)\n\
        rm -rf /usr/local/etc/php/conf.d/xdebug.ini\n\
        ;;\n\
    *)\n\
        echo -e "Usage: \n 1. xdebug enable\n 2. xdebug disable"\n\
        ;;\n\
esac\n\
' > /usr/local/bin/xdebug \
    && chmod 755 /usr/local/bin/xdebug \
    && ln -sf /usr/local/etc/php/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

# install for resolving php project dependencies
RUN curl -sS https://getcomposer.org/installer | php \
        && mv composer.phar /usr/local/bin/ \
        && ln -s /usr/local/bin/composer.phar /usr/local/bin/composer
