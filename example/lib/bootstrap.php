<?php

require_once '../vendor/autoload.php';

$ef = new \MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentFactory();

$envFilePath = __DIR__ . '/../.env';
if(!file_exists($envFilePath)) {
    throw new \Exception('Missing .env file');
}
$env = parse_ini_file($envFilePath);

$dbConfig = [
    'driver' => $env['DB_DRIVER'],
    'host' => $env['DB_HOST'],
    'port' => $env['DB_PORT'],
    'tns' => $env['DB_TNS'],
    'database' => $env['DB_DATABASE'],
    'username' => $env['DB_USERNAME'],
    'password' => $env['DB_PASSWORD'],
];

$ef->addConnection($dbConfig);
$elh = $ef->getEloquentHandler();

$appMapperFactory = new \MiamiOH\Pike\App\Mapper\AppMapperFactory();
$app = $appMapperFactory->createAppMapper([
    \MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler::class => $elh
]);
