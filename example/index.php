<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 6:38 PM
 */

require_once __DIR__ . '/lib/bootstrap.php';

function getTitle(string $path): ?string
{
    $content = file_get_contents($path);
    $matches = [];
    preg_match(
        '/\\/\\*\\*[\\s\\S]+?\\*[ ]*@title(.+)[\\s\\S]+?\\* \\*\\//',
        $content,
        $matches
    );
    if(count($matches) === 0) {
        return null;
    }

    return htmlentities(trim($matches[1]));
}

function listDemos(string $folder, array $blacklist = []): array
{
    $demos = [];
    $allFiles = scandir($folder);

    foreach ($allFiles as $file) {
        $path = $folder . '/' . $file;
        if (isset(pathinfo($path)['extension']) &&
            pathinfo($path)['extension'] === 'php' &&
            !in_array($file, $blacklist)
        ) {
            $demos[] = [
                'fileName' => $file,
                'path' => $path,
                'title' => getTitle($path)
            ];
        }
    }

    return $demos;
}

$matches = [];
preg_match('/\\/example\\/([^?\s]+)/', $_SERVER['REQUEST_URI'], $matches);
if (count($matches) === 0) {
    $allFiles = scandir(__DIR__);
    $phpFiles = [];
    $blacklist = [
        'index.php',
    ];

    echo "<h2>Custom Examples:</h2>";
    $customDemos = listDemos(__DIR__, ['index.php']);
    if (count($customDemos) === 0) {
        echo "<p>Currently, there is no custom examples. Please follow
        instruction below to create your own example.
        </p>";
    } else {
        foreach ($customDemos as $demo) {
            $title = $demo['title'] ?? 'Untitled Demo';
            echo "<h3><a href='{$demo['fileName']}'>{$title} ({$demo['fileName']})</a></h3>";
        }
    }

    echo "<h2>Available Demos:</h2>";
    $releasedDemos = listDemos(__DIR__ . '/demo');

    if (count($releasedDemos) === 0) {
        echo "<p>Currently, there is no released demo.
        </p>";
    } else {
        foreach ($releasedDemos as $demo) {
            $title = $demo['title'] ?? 'Untitled Demo';
            echo "<h3><a href='demo/{$demo['fileName']}'>{$title} (demo/{$demo['fileName']})</a></h3>";
        }
    }

    if (true) {
        echo "
        <h2>Instruction: Run Released Demo</h2>
        <ol>
          <li>make a copy of <strong>.env.example</strong> and name it <strong>.env</strong></li>
          <li>open <strong>.env</strong> file and change necessary db configuration (e.g. filling password)</li>
        </ol>
        <h2>Instruction: Create Custom Example</h2>
        <ol>
          <li>
            create a php file inside the <strong>example</strong> folder. (e.g. <strong>myDemo.php</strong>)
          </li>
          <li>put following code to the newly created file:
            <figure style='border: 1px solid #DDDDDD; padding: 0 15px;'>
              <pre>
                <code>
&lt;?php
/**
 * @title My First Demo
 * @var \MiamiOH\Pike\App\Mapper\AppMapper \$appMapper
 * */
\$appMapper = \$app;

// See example below
// \$result = \$appMapper->getViewMajorService()->getByCode('APET');
// print_r(\$result);
                </code>
              </pre>
            </figure>
          </li>
          <li><strong>\$appMapper</strong> is the entry point of this package. 
          You can start to write code to build custom example.
          </li>
        </ol>
        <p style='color: red;'>Note:</p>
        <ul style=''>
          <li>Print result to screen by using <string>print_r()</string> or <string>var_dump()</string></li>
          <li>Custom example page is helpful to debug during developing this package.</li>
        </ul>
        ";
    }
} else {
    echo "<h2><a href='javascript:history.back()'>Go Back</a></h2>";
    echo "<pre>";
    $time_start = microtime(true);
    require_once __DIR__ . '/' . $matches[1];
    echo "</pre>";
    echo 'Total execution time: ' . (microtime(true) - $time_start) . "s";
}