<?php

/**
 * @title Course Section Demo
 * @var \MiamiOH\Pike\App\Mapper\AppMapper $appMapper
 * */

use MiamiOH\Pike\Domain\Collection\CourseSectionAttributeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionAttribute;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentCount;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;

$appMapper = $app;


$viewCourseSectionService = $appMapper->getViewCourseSectionService();
$viewScheduleService = $appMapper->getViewCourseSectionScheduleService();
$viewAttributeService = $appMapper->getViewCourseSectionAttributeService();
$viewInstructorService = $appMapper->getViewInstructorAssignmentService();

function transformSchedule(
    CourseSectionSchedule $schedule
): array {
    return [
        'startDate' => $schedule->getStartDate()->format('Y-m-d'),
        'endDate' => $schedule->getEndDate()->format('Y-m-d'),
        'startTime' => $schedule->getStartTime()->format('H:i') ?? null,
        'endTime' => $schedule->getEndTime()->format('H:i') ?? null,
        'days' => (string)$schedule->getDays() ?? null,
        'buildingCode' => $schedule->getBuildingCode()->getValue(),
        'buildingName' => $schedule->getBuildingName(),
        'typeCode' => $schedule->getTypeCode()->getValue(),
        'typeDescription' => $schedule->getTypeDescription(),
    ];
}

function transformInstructor(InstructorAssignment $instructorAssignment): array
{
    return [
        'uniqueId' => $instructorAssignment->getUniqueId()->getValue()
    ];
}

function transformInstructorCollection(
    InstructorAssignmentCollection $instructorAssignmentCollection
) {
    $rtn = [];
    foreach ($instructorAssignmentCollection as $item) {
        $rtn[] = transformInstructor($item);
    }

    return $rtn;
}

function transformScheduleCollection(CourseSectionScheduleCollection $collection
): array {
    $rtn = [];
    foreach ($collection as $item) {
        $rtn[] = transformSchedule($item);
    }

    return $rtn;
}

function transformAttribute(
    CourseSectionAttribute $attribute
): array {
    return [
        'code' => $attribute->getCode()->getValue(),
        'description' => $attribute->getDescription()
    ];
}

function transformAttributeCollection(CourseSectionAttributeCollection $collection)
{
    $rtn = [];
    foreach ($collection as $item) {
        $rtn[] = transformAttribute($item);
    }

    return $rtn;
}

function transformCount(
    CourseSectionEnrollmentCount $count
): array {
    return [
        'numOfMaxEnrollments' => $count->getNumOfMaxEnrollments(),
        'numOfCurrentEnrollments' => $count->getNumOfCurrentEnrollments(),
        'numOfActiveEnrollments' => $count->getNumOfActiveEnrollments(),
        'numOfAvailableEnrollments' => $count->getNumOfAvailableEnrollments(),
    ];
}

function transformSection(
    CourseSection $courseSection,
    CourseSectionScheduleCollection $schedules,
    CourseSectionAttributeCollection $attributes,
    CourseSectionEnrollmentCount $count,
    InstructorAssignmentCollection $instructorAssignmentCollection
): array {
    return [
        'academicTerm' => $courseSection->getTermCode()->getValue(),
        'academicTermDesc' => $courseSection->getTermDescription(),
        'courseGuid' => $courseSection->getCourse()->getGuid()->getValue(),
        'courseCode' => $courseSection->getCourse()->getSubjectCode()->getValue() .
            ' ' .
            $courseSection->getCourse()->getNumber()->getValue() .
            ' ' .
            $courseSection->getSectionCode(),
        'schoolCode' => $courseSection->getCourse()->getSchoolCode()->getValue(),
        'schoolName' => $courseSection->getCourse()->getSchoolName(),
        'deptName' => $courseSection->getCourse()->getDepartmentCode()->getValue(),
        'deptCode' => $courseSection->getCourse()->getDepartmentName(),
        'standardizedDivisionCode' => (string)$courseSection->getStandardizedDivisionCode(),
        'standardizedDivisionName' => $courseSection->getStandardizedDivisionName(),
        'standardizedDeptCode' => (string)$courseSection->getStandardizedDepartmentCode(),
        'standardizedDeptName' => $courseSection->getStandardizedDepartmentName(),
        'traditionalStandardizedDeptCode' => (string)$courseSection->getLegacyStandardizedDepartmentCode(),
        'traditionalStandardizedDeptName' => $courseSection->getLegacyStandardizedDepartmentName(),
        'courseTitle' => $courseSection->getCourse()->getTitle(),
        'instructionalType' => $courseSection->getInstructionalTypeCode()->getValue(),
        'instructionalTypeDescription' => $courseSection->getInstructionalTypeDescription(),
        'courseSubjectCode' => $courseSection->getCourse()->getSubjectCode()->getValue(),
        'courseSubjectDesc' => $courseSection->getCourse()->getSubjectDescription(),
        'courseNumber' => $courseSection->getCourse()->getNumber()->getValue(),
        'courseSectionCode' => $courseSection->getSectionCode(),
        'courseStatus' => $courseSection->getCourseSectionStatusCode()->getValue(),
        'campusCode' => $courseSection->getCampusCode()->getValue(),
        'campusName' => $courseSection->getCampusDescription(),
        'creditHoursDesc' => $courseSection->getCreditHoursDescription(),
        'creditHoursHigh' => $courseSection->getCourse()->getCreditHoursHigh(),
        'creditHoursLow' => $courseSection->getCourse()->getCreditHoursLow(),
        'lectureHoursDesc' => $courseSection->getCourse()->getLectureHoursDescription(),
        'lectureHoursLow' => $courseSection->getCourse()->getLabHoursLow(),
        'lectureHoursHigh' => $courseSection->getCourse()->getLectureHoursHigh(),
        'labHoursDesc' => $courseSection->getCourse()->getLabHoursDescription(),
        'labHoursLow' => $courseSection->getCourse()->getLabHoursLow(),
        'labHoursHigh' => $courseSection->getCourse()->getLabHoursHigh(),
        'enrollmentCountMax' => $count->getNumOfMaxEnrollments(),
        'enrollmentCountCurrent' => $count->getNumOfCurrentEnrollments(),
        'enrollmentCountActive' => $count->getNumOfActiveEnrollments(),
        'enrollmentCountAvailable' => $count->getNumOfAvailableEnrollments(),
        'partOfTermCode' => $courseSection->getPartOfTerm()->getCode()->getValue(),
        'partOfTermName' => $courseSection->getPartOfTerm()->getDescription(),
        'partOfTermStartDate' => $courseSection->getPartOfTerm()->getStartDate()->format('Y-m-d'),
        'partOfTermEndDate' => $courseSection->getPartOfTerm()->getEndDate()->format('Y-m-d'),
        'midtermGradeSubmissionAvailable' => $courseSection->isMidtermGradeSubmissionAvailable(),
        'finalGradeSubmissionAvailable' => $courseSection->isFinalGradeSubmissionAvailable(),
        'gradeRequiredFinal' => $courseSection->isFinalGradeRequired(),
        'courseDescription' => $courseSection->getCourse()->getDescription(),
        'prntInd' => $courseSection->isDisplayed(),
        'courseSchedules' => transformScheduleCollection($schedules),
        'instructors' => transformInstructorCollection($instructorAssignmentCollection),
        'attributes' => transformAttributeCollection($attributes),
        'crossListedCourses' => null
    ];
}

$sectionGuids = [
    '8c49914f-8c84-4475-b01c-347f7ffbaaeb'
];

$data = [];
$scheduleMap = [];
$attributeMap = [];
$enrollmentCountMap = [];
$instructorMap = [];

$sections = $viewCourseSectionService->getByGuids($sectionGuids);
$schedules = $viewScheduleService
    ->getCollectionByCourseSectionGuidCollection($sectionGuids);
$attributes = $viewAttributeService
    ->getCollectionByCourseSectionGuids($sectionGuids);
$enrollmentCounts = $viewCourseSectionService->getEnrollmentCountBysectionGuids($sectionGuids);
$instructors = $viewInstructorService->getCollectionByCourseSectionGuids($sectionGuids);

foreach ($schedules as $schedule) {
    $sectionGuid = $schedule->getCourseSectionGuid()->getValue();
    if (!isset($scheduleMap[$sectionGuid])) {
        $scheduleMap[$sectionGuid] = new CourseSectionScheduleCollection();
    }
    $scheduleMap[$sectionGuid]->push($schedule);
}

foreach ($attributes as $attribute) {
    $sectionGuid = $attribute->getCourseSectionGuid()->getValue();
    if (!isset($attributeMap[$sectionGuid])) {
        $attributeMap[$sectionGuid] = new CourseSectionAttributeCollection();
    }
    $attributeMap[$sectionGuid]->push($attribute);
}

foreach ($instructors as $instructor) {
    $sectionGuid = $instructor->getCourseSectionGuid()->getValue();
    if (!isset($instructorMap[$sectionGuid])) {
        $instructorMap[$sectionGuid] = new InstructorAssignmentCollection();
    }
    $instructorMap[$sectionGuid]->push($instructor);
}

foreach ($enrollmentCounts as $enrollmentCount) {
    $sectionGuid = $enrollmentCount->getCourseSectionGuid()->getValue();
    $enrollmentCountMap[$sectionGuid] = $enrollmentCount;
}

foreach ($sections as $section) {
    $guid = $section->getGuid()->getValue();
    $data[] = transformSection(
        $section,
        $scheduleMap[$guid] ?? new CourseSectionScheduleCollection(),
        $attributeMap[$guid] ?? new CourseSectionAttributeCollection(),
        $enrollmentCountMap[$guid],
        $instructorMap[$guid] ?? new InstructorAssignmentCollection()
    );
}

print_r(json_encode($data, JSON_PRETTY_PRINT));
