<?php
/**
 * Author: liaom
 * Date: 5/15/18
 * Time: 3:27 PM
 */

namespace MiamiOH\Pike\Tests\Unit;


class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function setUp() :void
    {
        parent::setUp();

        if (method_exists($this, 'bootMockEloquentHelperTrait')) {
            call_user_func([$this, 'bootMockEloquentHelperTrait']);
        }
    }
}