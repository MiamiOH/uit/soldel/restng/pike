<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 1:06 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Common;


use MiamiOH\Pike\Common\Converter;
use PHPUnit\Framework\TestCase;

class ConverterTest extends TestCase
{
    /**
     * @var Converter
     */
    protected $converter;

    public function testStringToFloat()
    {
        $this->assertEquals(1.3, $this->converter->stringToFloat('1.3'));
        $this->assertEquals(1, $this->converter->stringToFloat('1'));
    }

    public function testStringToFloatWithDefaultValue()
    {
        $this->assertEquals(3, $this->converter->stringToFloat(null, 3));
    }

    public function testStringToInt()
    {
        $this->assertEquals(11, $this->converter->stringToInt('11'));
        $this->assertEquals(2, $this->converter->stringToInt('2'));
    }

    public function testStringToIntWithDefaultValue()
    {
        $this->assertEquals(3, $this->converter->stringToInt(null, 3));
    }

    public function testYesNoIndicatorToBool()
    {
        $this->assertEquals(true, $this->converter->yesNoIndicatorToBool('Y'));
        $this->assertEquals(false, $this->converter->yesNoIndicatorToBool('N'));
        $this->assertEquals(true,
            $this->converter->yesNoIndicatorToBool(null, true));
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->converter = new Converter();
    }
}