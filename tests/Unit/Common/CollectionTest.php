<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 11/29/17
 * Time: 3:13 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Common;

use MiamiOH\Pike\Common\Collection;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    protected $element1;
    protected $element2;
    protected $element3;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->element1 = new TestObject(1);
        $this->element2 = new TestObject(2);
        $this->element3 = new TestObject(3);

    }//end __construct()

    public function testCanRemoveElementFromCollection()
    {
        $collection = new TestObjectCollection(
            [
                $this->element1,
                $this->element2,
                $this->element3,
            ]
        );
        unset($collection[0]);

        self::assertEquals(
            [
                $this->element2,
                $this->element3,
            ],
            $collection->values()->toArray()
        );

    }//end testCanRemoveElementFromCollection()

    public function testArgumentParsedToConstructorIsNotArray()
    {
        $this->expectException(InvalidArgumentException::class);
        new TestObjectCollection(1);
    }

    public function testTotalIsZeroWhenPaginationIsDisabled()
    {
        $collection = new TestObjectCollection();
        $this->assertFalse($collection->isPageable());
        $this->assertEquals(0, $collection->getTotalNumOfItems());
    }

    public function testFlagIsPageableToTrueWhenSetTotal()
    {
        $collection = new TestObjectCollection();
        $collection->setTotalNumOfItems(100);
        $this->assertTrue($collection->isPageable());
        $this->assertEquals(100, $collection->getTotalNumOfItems());
    }
}//end class

class TestObject
{
    private $value;


    function __construct(int $value)
    {
        $this->value = $value;

    }//end __construct()


    public function getValue(): int
    {
        return $this->value;

    }//end getValue()


}//end class

class TestObjectCollection extends Collection
{

}