<?php

/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:45 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\AbstractBaseRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use PHPUnit\Framework\TestCase;

class AbstractBaseRepositoryTest extends TestCase
{
    /**
     * @var EloquentHandler
     */
    private $eloquentHandler;
    /**
     * @var TestAbstractBaseRepository
     */
    private $testBaseEloquentImplementation;

    public function testCanGetEloquentHandler()
    {
        $this->assertInstanceOf(EloquentHandler::class,
            $this->testBaseEloquentImplementation->getEloquentHandler());
        self::assertEquals($this->eloquentHandler,
            $this->testBaseEloquentImplementation->getEloquentHandler());
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->createMock(EloquentHandler::class);
        $this->testBaseEloquentImplementation = new TestAbstractBaseRepository($this->eloquentHandler);
    }
}

class TestAbstractBaseRepository extends AbstractBaseRepository
{

}