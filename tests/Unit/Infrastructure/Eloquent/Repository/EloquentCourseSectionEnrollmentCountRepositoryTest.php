<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentCount;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionEnrollmentCountRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\CourseSectionEnrollmentCountEntity;
use MiamiOH\Pike\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class EloquentCourseSectionEnrollmentCountRepositoryTest extends TestCase
{
    use MockEloquentHelperTrait;

    /**
     * @var EloquentCourseSectionEnrollmentCountRepository
     */
    private $courseSectionEnrollmentCountRepository;

    /**
     * @var MockObject
     */
    private $entity;

    /**
     * @var array
     */
    private $courseSectionEnrollmentCountData1;

    /**
     * @var CourseSectionEnrollmentCount
     */
    private $courseSectionEnrollmentCount1;

    /**
     * @var MockObject
     */
    private $entity1;

    public function testConstructCourseSectionEnrollmentCount()
    {
        $mirror = new \ReflectionClass($this->courseSectionEnrollmentCountRepository);
        $func = $mirror->getMethod('constructCourseSectionEnrollmentCount');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionEnrollmentCountRepository,
            $this->entity1);
        $this->assertEquals($this->courseSectionEnrollmentCount1, $actual);
    }

    public function testConstructCourseSectionEnrollmentCountCollection()
    {
        $mirror = new \ReflectionClass($this->courseSectionEnrollmentCountRepository);
        $func = $mirror->getMethod('constructCourseSectionEnrollmentCountCollection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionEnrollmentCountRepository,
            new EloquentCollection([$this->entity1]));
        $this->assertEquals(new CourseSectionEnrollmentCountCollection([$this->courseSectionEnrollmentCount1]),
            $actual);
    }

    public function testGetCollectionByCourseSectionGuidCollection()
    {
        $this->entity->method('get')->willReturn(new EloquentCollection([
            $this->entity1
        ]));
        $actual = $this->courseSectionEnrollmentCountRepository
            ->getCollectionByCourseSectionGuidCollection(
                new CourseSectionGuidCollection([$this->courseSectionEnrollmentCount1->getCourseSectionGuid()])
            );
        $this->assertEquals(
            new CourseSectionEnrollmentCountCollection([$this->courseSectionEnrollmentCount1]),
            $actual
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionEnrollmentCountRepository = new EloquentCourseSectionEnrollmentCountRepository(
            $this->eloquentHandler,
            new Converter()
        );

        $this->entity = $this->mockEloquentBuilder(CourseSectionEnrollmentCountEntity::class);

        $this->eloquentHandler->method('model')
            ->willreturn($this->entity);

        $this->courseSectionEnrollmentCountData1 = [
            'courseSectionGuid' => CourseSectionGuid::create(),
            'numOfMaxEnrollments' => 4,
            'numOfCurrentEnrollments' => 0,
            'numOfActiveEnrollments' => 3,
            'numOfAvailableEnrollments' => 1

        ];

        $this->courseSectionEnrollmentCount1 = new CourseSectionEnrollmentCount(
            $this->courseSectionEnrollmentCountData1['courseSectionGuid'],
            $this->courseSectionEnrollmentCountData1['numOfMaxEnrollments'],
            $this->courseSectionEnrollmentCountData1['numOfCurrentEnrollments'],
            $this->courseSectionEnrollmentCountData1['numOfActiveEnrollments'],
            $this->courseSectionEnrollmentCountData1['numOfAvailableEnrollments']

        );

        $this->entity1 = $this->mockEloquentModel(CourseSectionEnrollmentCountEntity::class,
            [
                'getNumOfMaxEnrollment' => (string)$this->courseSectionEnrollmentCountData1['numOfMaxEnrollments'],
                'getNumOfCurrentEnrollment' => null,
                'getNumOfActiveEnrollment' => (string)$this->courseSectionEnrollmentCountData1['numOfActiveEnrollments'],
                'getNumOfAvailableEnrollment' => (string)$this->courseSectionEnrollmentCountData1['numOfAvailableEnrollments'],
                'getCourseSectionGuid' => (string)$this->courseSectionEnrollmentCountData1['courseSectionGuid']
            ]);


    }
}