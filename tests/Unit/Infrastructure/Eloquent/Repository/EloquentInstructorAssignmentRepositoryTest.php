<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/14/18
 * Time: 1:53 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\InstructorAssignmentNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentInstructorAssignmentRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Sirasgn;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;


class EloquentInstructorAssignmentRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var EloquentInstructorAssignmentRepository
     */
    private $instructorAssignmentRepository;

    /**
     * @var MockObject
     */
    private $sirasgn;

    /**
     * @var MockObject
     */
    private $sirasgn1;

    /**
     * @var InstructorAssignmentCollection
     */
    private $instructorAssignmentCollection;

    /**
     * @var InstructorAssignment
     */
    private $instructorAssignment;
    /**
     * @var array
     */
    private $instructorAssignmentData;
    /**
     * @var EloquentCollection
     */
    private $sirasgnCollection;

    public function testGetCollectionByTermCodesCrnsUniqueIds()
    {
        $this->sirasgn->method('get')->willReturn($this->sirasgnCollection);

        $actualResult = $this->instructorAssignmentRepository->getCollectionByTermCodesCrnsUniqueIds(
            new TermCodeCollection(['201720']),
            new CrnCollection(["123"]),
            new UniqueIdCollection(['xiaw'])
        );
        $this->assertEquals($this->instructorAssignmentCollection, $actualResult);
    }

    public function testGetCollectionByUniqueIdTermCode()
    {
        $this->sirasgn->method('get')->willReturn($this->sirasgnCollection);

        $actualResult = $this->instructorAssignmentRepository->getCollectionByUniqueIdTermCode(
            new UniqueId('xiaw'),
            new TermCode('201720')
        );
        $this->assertEquals($this->instructorAssignmentCollection, $actualResult);
    }

    public function testGetCollectionByTermCodeCrn()
    {
        $this->sirasgn->method('get')->willReturn($this->sirasgnCollection);

        $actualResult = $this->instructorAssignmentRepository->getCollectionByTermCodeCrn(
            new TermCode('201720'),
            new Crn('12345')
        );
        $this->assertEquals($this->instructorAssignmentCollection, $actualResult);
    }

    public function testGetByUniqueIdTermCodeCrn()
    {
        $this->sirasgn->method('first')->willReturn($this->sirasgn1);

        $actualResult = $this->instructorAssignmentRepository->getByUniqueIdTermCodeCrn(
            new UniqueId('xiaw'),
            new TermCode('201720'),
            new Crn('12345')
        );
        $this->assertEquals($this->instructorAssignment, $actualResult);
    }

    public function testGetByUniqueIdTermCodeCrnWithInvalidInstructor()
    {
        $this->sirasgn->method('first')->willReturn(null);
        $this->expectException(InstructorAssignmentNotFoundException::class);

        $actualResult = $this->instructorAssignmentRepository->getByUniqueIdTermCodeCrn(
            new UniqueId('xiaw'),
            new TermCode('201720'),
            new Crn('12345')
        );
    }

    public function testGetByGuid()
    {
        $this->sirasgn->method('first')->willReturn($this->sirasgn1);

        $actualResult = $this->instructorAssignmentRepository->getByGuid(
            InstructorAssignmentGuid::create()
        );
        $this->assertEquals($this->instructorAssignment, $actualResult);
    }

    public function testGetByGuidWithInvalidInstructor()
    {
        $this->sirasgn->method('first')->willReturn(null);
        $this->expectException(InstructorAssignmentNotFoundException::class);
        $actualResult = $this->instructorAssignmentRepository->getByGuid(
            InstructorAssignmentGuid::create()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model'])
            ->getMock();

        $this->instructorAssignmentRepository = new EloquentInstructorAssignmentRepository(
            $this->eloquentHandler,
            new Converter()
        );

        $this->sirasgn = $this->getMockBuilder(Sirasgn::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'joinSsbsect',
                'joinSzbuniq',
                'byTermCodes',
                'byUniqueIds',
                'byCrns',
                'get',
                'first',
                'byUniqueIdTermCode',
                'byTermCodeCrn',
                'byUniqueIdTermCodeCrn',
                'byGuid'
            ])
            ->getMock();


        $this->sirasgn->method('joinSsbsect')
            ->willReturnSelf();
        $this->sirasgn->method('joinSzbuniq')
            ->willReturnSelf();
        $this->sirasgn->method('byTermCodes')
            ->willReturnSelf();
        $this->sirasgn->method('byUniqueIds')
            ->willReturnSelf();
        $this->sirasgn->method('byCrns')
            ->willReturnSelf();
        $this->sirasgn->method('byUniqueIdTermCode')
            ->willReturnSelf();
        $this->sirasgn->method('byTermCodeCrn')
            ->willReturnSelf();
        $this->sirasgn->method('byUniqueIdTermCodeCrn')
            ->willReturnSelf();
        $this->sirasgn->method('byGuid')
            ->willReturnSelf();

        $this->eloquentHandler->method('model')
            ->willreturn($this->sirasgn);

        $this->instructorAssignmentCollection = new InstructorAssignmentCollection();
        $this->instructorAssignmentData = [
            'guid' => (string)InstructorAssignmentGuid::create(),
            'uniqueId' => 'xiaw',
            'termCode' => '201720',
            'crn' => '12345',
            'courseSectionGuid' => (string)CourseSectionGuid::create(),
            'isPrimary' => true
        ];

        $this->instructorAssignment = new InstructorAssignment(
            new InstructorAssignmentGuid($this->instructorAssignmentData['guid']),
            new UniqueId($this->instructorAssignmentData['uniqueId']),
            new TermCode($this->instructorAssignmentData['termCode']),
            new Crn($this->instructorAssignmentData['crn']),
            new CourseSectionGuid($this->instructorAssignmentData['courseSectionGuid']),
            $this->instructorAssignmentData['isPrimary']
        );
        $this->instructorAssignmentCollection->push($this->instructorAssignment);

        $this->sirasgn1 = $this->createMock(Sirasgn::class);

        $this->sirasgn1->method('getGuid')->willReturn($this->instructorAssignmentData['guid']);
        $this->sirasgn1->method('getUniqueId')->willReturn($this->instructorAssignmentData['uniqueId']);
        $this->sirasgn1->method('getTermCode')->willReturn($this->instructorAssignmentData['termCode']);
        $this->sirasgn1->method('getCrn')->willReturn($this->instructorAssignmentData['crn']);
        $this->sirasgn1->method('getIsPrimaryFlag')->willReturn($this->instructorAssignmentData['isPrimary'] === true ? 'Y' : 'N');
        $this->sirasgn1->method('getCourseSectionGuid')->willReturn($this->instructorAssignmentData['courseSectionGuid']);


        $this->sirasgnCollection = new EloquentCollection();
        $this->sirasgnCollection->push($this->sirasgn1);

    }


}