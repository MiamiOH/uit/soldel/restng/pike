<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\BannerIdCollection;
use MiamiOH\Pike\Domain\Collection\PersonCollection;
use MiamiOH\Pike\Domain\Collection\PidmCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\PersonNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentPersonRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Spriden;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EloquentPersonRepositoryTest extends TestCase
{
    /**
     * @var EloquentPersonRepository
     */
    private $personRepository;
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var MockObject
     */
    private $spriden;

    /**
     * @var array
     */
    private $personData1;
    /**
     * @var array
     */
    private $personData2;

    /**
     * @var Person
     */
    private $person1;
    /**
     * @var Person
     */
    private $person2;

    /**
     * @var MockObject
     */
    private $spriden1;
    /**
     * @var MockObject
     */
    private $spriden2;

    public function testConstructPerson()
    {
        $mirror = new \ReflectionClass($this->personRepository);
        $func = $mirror->getMethod('constructPerson');
        $func->setAccessible(true);

        $actual = $func->invoke($this->personRepository, $this->spriden1);
        $this->assertEquals($this->person1, $actual);
    }

    public function testConstructPersonCollection()
    {
        $mirror = new \ReflectionClass($this->personRepository);
        $func = $mirror->getMethod('constructPersonCollection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->personRepository,
            new EloquentCollection([$this->spriden1]));
        $this->assertEquals(new PersonCollection([$this->person1]), $actual);
    }

    public function testInvalidUniqueIdWillCauseException()
    {
        $this->spriden->method('first')->willReturn(null);

        $this->expectException(PersonNotFoundException::class);
        $this->personRepository->getByUniqueId(new UniqueId('alsk'));
    }

    public function testGetPersonByUniqueId()
    {
        $this->spriden->method('first')->willReturn($this->spriden1);

        $actualResult = $this->personRepository->getByUniqueId($this->person1->getUniqueId());

        $this->assertEquals($this->person1, $actualResult);
    }

    public function testGetPersonByPidm()
    {
        $this->spriden->method('first')->willReturn($this->spriden1);

        $actualResult = $this->personRepository->getByPidm($this->person1->getPidm());

        $this->assertEquals($this->person1, $actualResult);
    }

    public function testCouldNotFindPersonByPidm()
    {
        $this->spriden->method('first')->willReturn(null);

        $this->expectException(PersonNotFoundException::class);
        $this->personRepository->getByPidm(new Pidm('1234567'));
    }

    public function testGetPersonByBannerId()
    {
        $this->spriden->method('first')->willReturn($this->spriden1);

        $actualResult = $this->personRepository->getByBannerId($this->person1->getBannerId());

        $this->assertEquals($this->person1, $actualResult);
    }

    /**
     * @throws PersonNotFoundException
     */
    public function testCouldNotFindPersonByBannerId()
    {
        $this->spriden->method('first')->willReturn(null);

        $this->expectException(PersonNotFoundException::class);
        $this->personRepository->getByBannerId(new BannerId('+10293023'));
    }

    public function testGetCollectionByPidmCollection()
    {
        $this->spriden->method('get')
            ->willReturn(
                new EloquentCollection([
                    $this->spriden1
                ])
            );

        $actualResult = $this->personRepository
            ->getCollectionByPidmCollection(
                new PidmCollection([
                    $this->person1->getPidm()
                ])
            );

        $this->assertEquals(
            new PersonCollection([
                $this->person1
            ]),
            $actualResult
        );
    }

    public function testGetCollectionByBannerIdCollection()
    {
        $this->spriden->method('get')
            ->willReturn(
                new EloquentCollection([
                    $this->spriden1
                ])
            );

        $actualResult = $this->personRepository
            ->getCollectionByBannerIdCollection(
                new BannerIdCollection([
                    $this->person1->getBannerId()
                ])
            );

        $this->assertEquals(
            new PersonCollection([
                $this->person1
            ]),
            $actualResult
        );
    }

    public function testGetCollectionByUniqueIdCollection()
    {
        $this->spriden->method('get')
            ->willReturn(
                new EloquentCollection([
                    $this->spriden1
                ])
            );

        $actualResult = $this->personRepository
            ->getCollectionByUniqueIdCollection(
                new UniqueIdCollection([
                    $this->person1->getUniqueId()
                ])
            );

        $this->assertEquals(
            new PersonCollection([
                $this->person1
            ]),
            $actualResult
        );
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model'])
            ->getMock();

        $this->personRepository = new EloquentPersonRepository($this->eloquentHandler);

        $this->spriden = $this->getMockBuilder(Spriden::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'first',
                'byPidm',
                'byBannerId',
                'byUniqueId',
                'byUniqueIds',
                'byPidms',
                'byBannerIds',
                'get'
            ])
            ->getMock();

        $this->spriden->method('byUniqueId')->willReturnSelf();
        $this->spriden->method('byPidm')->willReturnSelf();
        $this->spriden->method('byBannerId')->willReturnSelf();
        $this->spriden->method('byUniqueIds')->willReturnSelf();
        $this->spriden->method('byPidms')->willReturnSelf();
        $this->spriden->method('byBannerIds')->willReturnSelf();


        $this->eloquentHandler->method('model')
            ->willreturn($this->spriden);

        $this->personData1 = [
            'uniqueId' => new UniqueId('kajdk'),
            'pidm' => new Pidm('1734283'),
            'bannerId' => new BannerId('+928394832'),
            'lastName' => 'asdf',
            'firstName' => 'gadsf',
            'middleName' => null,
            'namePrefix' => null,
            'nameSuffix' => null,
            'preferredFirstName' => 'asdf'
        ];

        $this->personData2 = [
            'uniqueId' => new UniqueId('asd'),
            'pidm' => new Pidm('1728372'),
            'bannerId' => new BannerId('+02938492'),
            'lastName' => 'aaaa',
            'firstName' => 'bbbb',
            'middleName' => null,
            'namePrefix' => 'dd',
            'nameSuffix' => null,
            'preferredFirstName' => 'asdf'
        ];

        $this->person1 = new Person(
            $this->personData1['uniqueId'],
            $this->personData1['pidm'],
            $this->personData1['bannerId'],
            $this->personData1['lastName'],
            $this->personData1['firstName'],
            $this->personData1['middleName'],
            $this->personData1['namePrefix'],
            $this->personData1['nameSuffix'],
            $this->personData1['preferredFirstName']
        );

        $this->person2 = new Person(
            $this->personData2['uniqueId'],
            $this->personData2['pidm'],
            $this->personData2['bannerId'],
            $this->personData2['lastName'],
            $this->personData2['firstName'],
            $this->personData2['middleName'],
            $this->personData2['namePrefix'],
            $this->personData2['nameSuffix'],
            $this->personData2['preferredFirstName']
        );

        $this->spriden1 = $this->createMock(Spriden::class);
        $this->spriden1->method('getPidm')->willReturn((string)$this->personData1['pidm']);
        $this->spriden1->method('getBannerId')->willReturn((string)$this->personData1['bannerId']);
        $this->spriden1->method('getUniqueId')->willReturn((string) $this->personData1['uniqueId']);
        $this->spriden1->method('getLastName')->willReturn((string)$this->personData1['lastName']);
        $this->spriden1->method('getFirstName')->willReturn((string)$this->personData1['firstName']);
        $this->spriden1->method('getMiddleName')->willReturn((string)$this->personData1['middleName']);
        $this->spriden1->method('getNamePrefix')->willReturn((string)$this->personData1['namePrefix']);
        $this->spriden1->method('getNameSuffix')->willReturn((string)$this->personData1['nameSuffix']);
        $this->spriden1->method('getPreferredFirstName')->willReturn((string)$this->personData1['preferredFirstName']);


        $this->spriden2 = $this->createMock(Spriden::class);
        $this->spriden2->method('getPidm')->willReturn((string)$this->personData2['pidm']);
        $this->spriden2->method('getBannerId')->willReturn((string)$this->personData2['bannerId']);
        $this->spriden2->method('getUniqueId')->willReturn((string)$this->personData2['uniqueId']);
        $this->spriden2->method('getLastName')->willReturn((string)$this->personData2['lastName']);
        $this->spriden2->method('getFirstName')->willReturn((string)$this->personData2['firstName']);
        $this->spriden2->method('getMiddleName')->willReturn((string)$this->personData2['middleName']);
        $this->spriden2->method('getNamePrefix')->willReturn((string)$this->personData2['namePrefix']);
        $this->spriden2->method('getNameSuffix')->willReturn((string)$this->personData2['nameSuffix']);
        $this->spriden2->method('getPreferredFirstName')->willReturn((string)$this->personData2['preferredFirstName']);

    }
}