<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\ThematicSequenceCollection;
use MiamiOH\Pike\Domain\Model\ThematicSequence;
use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;
use MiamiOH\Pike\Exception\ThematicSequenceNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentThematicSequenceRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Spriden;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\ThematicSequence as ThematicSequenceEntity;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EloquentThematicSequenceRepositoryTest extends TestCase
{
    /**
     * @var EloquentThematicSequenceRepository
     */
    private $thematicSequenceRepository;
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var MockObject
     */
    private $stvmajr;

    /**
     * @var array
     */
    private $thematicSequenceData1;

    /**
     * @var ThematicSequence
     */
    private $thematicSequence1;

    /**
     * @var MockObject
     */
    private $stvmajr1;

    public function testConstructThematicSequence()
    {
        $mirror = new \ReflectionClass($this->thematicSequenceRepository);
        $func = $mirror->getMethod('constructThematicSequence');
        $func->setAccessible(true);

        $actual = $func->invoke($this->thematicSequenceRepository, $this->stvmajr1);
        $this->assertEquals($this->thematicSequence1, $actual);
    }

    public function testConstructThematicSequenceCollection()
    {
        $mirror = new \ReflectionClass($this->thematicSequenceRepository);
        $func = $mirror->getMethod('constructThematicSequenceCollection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->thematicSequenceRepository,
            new EloquentCollection([$this->stvmajr1]));
        $this->assertEquals(new ThematicSequenceCollection([$this->thematicSequence1]),
            $actual);
    }

    public function testCouldNotFindThematicSequenceByCode()
    {
        $this->stvmajr->method('first')->willReturn(null);

        $this->expectException(ThematicSequenceNotFoundException::class);
        $this->thematicSequenceRepository->getByCode(new ThematicSequenceCode('DSDS'));
    }

    public function testGetThematicSequenceByCode()
    {
        $this->stvmajr->method('first')->willReturn($this->stvmajr1);

        $actualResult = $this->thematicSequenceRepository->getByCode($this->thematicSequence1->getCode());

        $this->assertEquals($this->thematicSequence1, $actualResult);
    }

    public function testGetAllThematicSequence()
    {
        $this->stvmajr->method('get')->willReturn(new EloquentCollection([$this->stvmajr1]));

        $actualResult = $this->thematicSequenceRepository->getAll();

        $this->assertEquals(new ThematicSequenceCollection([$this->thematicSequence1]),
            $actualResult);
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model'])
            ->getMock();

        $this->thematicSequenceRepository = new EloquentThematicSequenceRepository($this->eloquentHandler);

        $this->stvmajr = $this->getMockBuilder(Spriden::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'orderByCode',
                'get',
                'codeEquals',
                'first',
            ])
            ->getMock();

        $this->stvmajr->method('orderByCode')->willReturnSelf();
        $this->stvmajr->method('codeEquals')->willReturnSelf();


        $this->eloquentHandler->method('model')
            ->willreturn($this->stvmajr);

        $this->thematicSequenceData1 = [
            'code' => new ThematicSequenceCode('ABCD'),
            'description' => 'asdfasdf'
        ];

        $this->thematicSequence1 = new ThematicSequence(
            $this->thematicSequenceData1['code'],
            $this->thematicSequenceData1['description']
        );

        $this->stvmajr1 = $this->createMock(ThematicSequenceEntity::class);
        $this->stvmajr1->method('getCode')->willReturn((string)$this->thematicSequenceData1['code']);
        $this->stvmajr1->method('getDescription')->willReturn($this->thematicSequenceData1['description']);

    }
}