<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\PartOfTerm;
use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Exception\CourseSectionNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssbsect;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Stvptrm;
use MiamiOH\Pike\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class EloquentCourseSectionRepositoryTest extends TestCase
{
    use MockEloquentHelperTrait;

    /**
     * @var EloquentCourseSectionRepository
     */
    private $courseSectionRepository;

    /**
     * @var MockObject
     */
    private $ssbsect;

    /**
     * @var array
     */
    private $courseSectionData1;

    /**
     * @var CourseSection
     */
    private $courseSection1;

    /**
     * @var MockObject
     */
    private $ssbsect1;

    /**
     * @var MockObject
     */
    private $courseRepository;
    /**
     * @var MockObject
     */
    private $course;
    /**
     * @var MockObject
     */
    private $partOfTerm;

    public function testConstructCourseSection()
    {
        $mirror = new \ReflectionClass($this->courseSectionRepository);
        $func = $mirror->getMethod('constructCourseSection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionRepository, $this->ssbsect1,
            $this->course);
        $this->assertEquals($this->courseSection1, $actual);
    }

    public function testConstructCourseSectionCollection()
    {
        $mirror = new \ReflectionClass($this->courseSectionRepository);
        $func = $mirror->getMethod('constructCourseSectionCollection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionRepository,
            new EloquentCollection([$this->ssbsect1]));
        $this->assertEquals(new CourseSectionCollection([$this->courseSection1]),
            $actual);
    }

    public function testGetByGuid()
    {
        $this->ssbsect->method('first')->willReturn($this->ssbsect1);
        $actual = $this->courseSectionRepository->getByGuid($this->courseSectionData1['guid']);
        $this->assertEquals(
            $this->courseSection1,
            $actual
        );
    }

    public function testCouldNotFindCoursebyGuid()
    {
        $this->ssbsect->method('first')->willReturn(null);
        $this->expectException(CourseSectionNotFoundException::class);
        $this->courseSectionRepository->getByGuid($this->courseSectionData1['guid']);
    }

    public function testGetByGuids()
    {
        $this->ssbsect->method('get')->willReturn(new EloquentCollection([$this->ssbsect1]));
        $actual = $this->courseSectionRepository->getByGuids(new CourseSectionGuidCollection([$this->courseSectionData1['guid']]));
        $this->assertEquals(
            new CourseSectionCollection([$this->courseSection1]),
            $actual
        );
    }

    public function testGetByTermCodeCrn()
    {
        $this->ssbsect->method('first')->willReturn($this->ssbsect1);
        $actual = $this->courseSectionRepository->getByTermCodeCrn(
            new TermCode('201710'),
            new Crn('12345')
        );
        $this->assertEquals(
            $this->courseSection1,
            $actual
        );
    }

    public function testCouldNotFindSectionByTermCodeCrn()
    {
        $this->ssbsect->method('first')->willReturn(null);
        $this->expectException(CourseSectionNotFoundException::class);
        $this->courseSectionRepository->getByTermCodeCrn(
            new TermCode('201710'),
            new Crn('12345')
        );
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->course = $this->createMock(Course::class);
        $this->partOfTerm = $this->createMock(PartOfTerm::class);
        $this->courseRepository = $this->createMock(EloquentCourseRepository::class);
        $this->courseSectionRepository = new EloquentCourseSectionRepository(
            $this->eloquentHandler,
            $this->courseRepository,
            new Converter()
        );

        $this->ssbsect = $this->mockEloquentBuilder(Ssbsect::class);

        $this->eloquentHandler->method('model')
            ->willreturn($this->ssbsect);

        $this->courseSectionData1 = [
            'guid' => CourseSectionGuid::create(),
            'course' => $this->course,
            'termCode' => new TermCode('201810'),
            'title' => 'CR Reading',
            'crn' => new Crn('12345'),
            'termDescription' => 'asdf',
            'sectionCode' => 'A',
            'creditHours' => 3,
            'creditHoursLow' => 3,
            'creditHoursHigh' => 3,
            'creditHoursAvailable' => [3],
            'instructionalTypeCode' => new InstructionalTypeCode('A'),
            'instructionalTypeDescription' => 'asdf',
            'campusCode' => new CampusCode('O'),
            'campusDescription' => 'asdf',
            'courseSectionStatusCode' => new CourseSectionStatusCode('A'),
            'courseSectionStatusDescription' => 'asdf',
            'maxNumberOfEnrollment' => 32,
            'isMidtermGradeSubmissionAvailable' => true,
            'isFinalGradeSubmissionAvailable' => false,
            'isFinalGradeRequired' => true,
            'isDisplayed' => false,
            'partOfTermCode' => new PartOfTermCode('1'),
            'partOfTermDescription' => 'asdfasdf',
            'partOfTermStartDate' => Carbon::create(2017, 1, 2, 0, 0, 0),
            'partOfTermEndDate' => Carbon::create(2017, 2, 2, 0, 0, 0),
            'courseGuid' => CourseGuid::create(),
            'standardizedDivisionCode' => new DivisionCode('CAS'),
            'standardizedDivisionName' => 'asdfasd',
            'standardizedDepartmentCode' => new DepartmentCode('CSE'),
            'standardizedDepartmentName' => 'agfdfg',
            'legacyStandardizedDepartmentCode' => new DepartmentCode('CSE'),
            'legacyStandardizedDepartmentName' => 'asdfew',
        ];

        $this->courseSection1 = new CourseSection(
            $this->courseSectionData1['guid'],
            $this->courseSectionData1['course'],
            $this->courseSectionData1['title'],
            $this->courseSectionData1['termCode'],
            $this->courseSectionData1['crn'],
            $this->courseSectionData1['termDescription'],
            $this->courseSectionData1['sectionCode'],
            $this->courseSectionData1['creditHours'],
            $this->courseSectionData1['creditHoursLow'],
            $this->courseSectionData1['creditHoursHigh'],
            $this->courseSectionData1['creditHoursAvailable'],
            $this->courseSectionData1['instructionalTypeCode'],
            $this->courseSectionData1['instructionalTypeDescription'],
            $this->courseSectionData1['campusCode'],
            $this->courseSectionData1['campusDescription'],
            $this->courseSectionData1['courseSectionStatusCode'],
            $this->courseSectionData1['courseSectionStatusDescription'],
            $this->courseSectionData1['maxNumberOfEnrollment'],
            new PartOfTerm(
                $this->courseSectionData1['partOfTermCode'],
                $this->courseSectionData1['partOfTermDescription'],
                $this->courseSectionData1['partOfTermStartDate'],
                $this->courseSectionData1['partOfTermEndDate']
            ),
            $this->courseSectionData1['isMidtermGradeSubmissionAvailable'],
            $this->courseSectionData1['isFinalGradeSubmissionAvailable'],
            $this->courseSectionData1['isFinalGradeRequired'],
            $this->courseSectionData1['isDisplayed'],
            $this->courseSectionData1['standardizedDivisionCode'],
            $this->courseSectionData1['standardizedDivisionName'],
            $this->courseSectionData1['standardizedDepartmentCode'],
            $this->courseSectionData1['standardizedDepartmentName'],
            $this->courseSectionData1['legacyStandardizedDepartmentCode'],
            $this->courseSectionData1['legacyStandardizedDepartmentName']
        );

        $stvptrm = $this->mockEloquentModel(Stvptrm::class, [
            'getCode' => (string)$this->courseSectionData1['partOfTermCode'],
            'getDescription' => $this->courseSectionData1['partOfTermDescription']
        ]);

        $courseGuid = $this->courseSectionData1['courseGuid'];
        $this->course->method('getGuid')->willReturn($courseGuid);
        $this->courseRepository->method('getCollectionByCourseGuidTermCodeCollection')->willReturn(new CourseCollection([$this->course]));

        $this->ssbsect1 = $this->mockEloquentModel(Ssbsect::class, [
            'getCrn' => (string)$this->courseSectionData1['crn'],
            'getTermCode' => (string)$this->courseSectionData1['termCode'],
            'getTitle' => $this->courseSectionData1['title'],
            'getGuid' => (string)$this->courseSectionData1['guid'],
            'getInstructionalTypeCode' => (string)$this->courseSectionData1['instructionalTypeCode'],
            'getInstructionalTypeDescription' => $this->courseSectionData1['instructionalTypeDescription'],
            'getCampusCode' => (string)$this->courseSectionData1['campusCode'],
            'getCampusDescription' => $this->courseSectionData1['campusDescription'],
            'getCourseSectionStatusCode' => (string)$this->courseSectionData1['courseSectionStatusCode'],
            'getCourseSectionStatusDescription' => $this->courseSectionData1['courseSectionStatusDescription'],
            'getCreditHours' => (string)$this->courseSectionData1['creditHours'],
            'getTermDescription' => $this->courseSectionData1['termDescription'],
            'getSectionCode' => (string)$this->courseSectionData1['sectionCode'],
            'getMaxNumberOfEnrollment' => (string)$this->courseSectionData1['maxNumberOfEnrollment'],
            'getCourseGuid' => (string)$this->courseSectionData1['courseGuid'],
            'getPartOfTerm' => $stvptrm,
            'getPartOfTermStartDate' => $this->courseSectionData1['partOfTermStartDate'],
            'getPartOfTermEndDate' => $this->courseSectionData1['partOfTermEndDate'],
            'getMidtermGradeSubmissionAvailableIndicator' => 'Y',
            'getFinalGradeSubmissionAvailableIndicator' => 'N',
            'getFinalGradeRequiredIndicator' => 'Y',
            'getPrintIndicator' => 'N',
            'getStandardizedDivisionCode' => (string)$this->courseSectionData1['standardizedDivisionCode'],
            'getStandardizedDivisionName' => $this->courseSectionData1['standardizedDivisionName'],
            'getStandardizedDepartmentCode' => (string)$this->courseSectionData1['standardizedDepartmentCode'],
            'getStandardizedDepartmentName' => $this->courseSectionData1['standardizedDepartmentName'],
            'getLegacyStandardizedDepartmentCode' => (string)$this->courseSectionData1['legacyStandardizedDepartmentCode'],
            'getLegacyStandardizedDepartmentName' => $this->courseSectionData1['legacyStandardizedDepartmentName'],
        ]);


    }
}