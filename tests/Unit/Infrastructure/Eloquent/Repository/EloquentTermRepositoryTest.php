<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\TermCollection;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentTermRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Term as EloquentTerm;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use function DI\string;

class EloquentTermRepositoryTest extends TestCase
{
    /**
     * @var EloquentTermRepository
     */
    private $termRepository;
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var MockObject
     */
    private $eloquentTerm;

    /**
     * @var array
     */
    private $termData1;
    /**
     * @var array
     */
    private $termData2;

    /**
     * @var Term
     */
    private $term1;
    /**
     * @var Term
     */
    private $term2;

    /**
     * @var MockObject
     */
    private $eloquentTerm1;
    /**
     * @var MockObject
     */
    private $eloquentTerm2;

    public function testCanGetAllTerm()
    {
        $eloquentCollection = new EloquentCollection();
        $eloquentCollection->push($this->eloquentTerm1);

        $this->eloquentTerm->method('get')
            ->willReturn($eloquentCollection);

        $termCollection = $this->termRepository->getAll();

        $expectResult = new TermCollection([$this->term1]);

        $this->assertEquals($expectResult, $termCollection);
    }

    public function testCanGetByCode()
    {
        $this->eloquentTerm->method('first')
            ->willReturn($this->eloquentTerm1);

        $actualResult = $this->termRepository->getByCode($this->termData1['code']);

        $this->assertEquals($this->term1, $actualResult);
    }

    public function testThrowExceptionWhenTermDoesNotFoundByCode()
    {
        $this->eloquentTerm->method('first')
            ->willReturn(null);

        $this->expectException(\Exception::class);

        $actualResult = $this->termRepository->getByCode(new TermCode('100010'));
    }

    public function testCanGetCurrentTerm()
    {
        $this->eloquentTerm->method('first')
            ->willReturn($this->eloquentTerm1);

        $actualResult = $this->termRepository->getCurrentTerm();

        $this->assertEquals($this->term1, $actualResult);
    }

    public function testCurrentTermNotFound()
    {
        $this->eloquentTerm->method('first')
            ->willReturn(null);

        $this->expectException(\Exception::class);
        $this->termRepository->getCurrentTerm();
    }

    public function testCanGetNextTerm()
    {
        $this->eloquentTerm->method('first')
            ->willReturn($this->eloquentTerm1);

        $actualResult = $this->termRepository->getNextTerm();

        $this->assertEquals($this->term1, $actualResult);
    }

    public function testNextTermNotFound()
    {
        $this->eloquentTerm->method('first')
            ->willReturn(null);

        $this->expectException(\Exception::class);
        $this->termRepository->getNextTerm();
    }

    public function testCanGetPreviousTerm()
    {
        $this->eloquentTerm->method('first')
            ->willReturn($this->eloquentTerm1);

        $actualResult = $this->termRepository->getPrevTerm();

        $this->assertEquals($this->term1, $actualResult);
    }

    public function testPreviousTermNotFound()
    {
        $this->eloquentTerm->method('first')
            ->willReturn(null);

        $this->expectException(\Exception::class);
        $this->termRepository->getPrevTerm();
    }

    public function testCanGetMultipleTermsByCodes()
    {
        $eloquentCollection = new EloquentCollection();
        $eloquentCollection->push($this->eloquentTerm1);
        $eloquentCollection->push($this->eloquentTerm2);

        $this->eloquentTerm->method('get')
            ->willReturn($eloquentCollection);

        $termCollection = $this->termRepository->getTermCollectionByCodes(
            new TermCodeCollection(
                [
                    $this->termData1['code'],
                    $this->termData2['code']
                ]
            )
        );

        $expectResult = new TermCollection([$this->term1, $this->term2]);

        $this->assertEquals($expectResult, $termCollection);
    }

    public function testCanGetNextMultipleTerms()
    {
        $eloquentCollection = new EloquentCollection();
        $eloquentCollection->push($this->eloquentTerm1);
        $eloquentCollection->push($this->eloquentTerm2);

        $this->eloquentTerm->method('first')
            ->willReturn($this->eloquentTerm1);

        $this->eloquentTerm->method('get')
            ->willReturn($eloquentCollection);

        $termCollection = $this->termRepository->getNextTerms(2);
        $expectedResult = new TermCollection([$this->term1, $this->term2]);
        $this->assertEquals($expectedResult, $termCollection);
    }

    public function testNumOfTermInGetNextTermsMustBeAPositiveInteger()
    {
        $this->expectException(\Exception::class);
        $this->termRepository->getNextTerms(-3);
    }

    public function testCanGetPreviousMultipleTerms()
    {
        $eloquentCollection = new EloquentCollection();
        $eloquentCollection->push($this->eloquentTerm1);
        $eloquentCollection->push($this->eloquentTerm2);

        $this->eloquentTerm->method('first')
            ->willReturn($this->eloquentTerm1);

        $this->eloquentTerm->method('get')
            ->willReturn($eloquentCollection);

        $termCollection = $this->termRepository->getPrevTerms(2);

        $expectResult = new TermCollection([$this->term2, $this->term1]);

        $this->assertEquals($expectResult, $termCollection->values());
    }

    public function testNumOfTermInGetPrevTermsMustBeAPositiveInteger()
    {
        $this->expectException(\Exception::class);
        $this->termRepository->getPrevTerms(-3);
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model'])
            ->getMock();

        $this->termRepository = new EloquentTermRepository($this->eloquentHandler);

        $this->eloquentTerm = $this->getMockBuilder(EloquentTerm::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'get',
                'first',
                'currentTerm',
                'nextTerm',
                'prevTerm',
                'codeIn',
                'startDateAfter',
                'startDateBefore',
                'take',
                'orderByDesc',
                'CodeEquals'
            ])
            ->getMock();

        $this->eloquentTerm->method('currentTerm')->willReturnSelf();
        $this->eloquentTerm->method('prevTerm')->willReturnSelf();
        $this->eloquentTerm->method('nextTerm')->willReturnSelf();
        $this->eloquentTerm->method('codeIn')->willReturnSelf();
        $this->eloquentTerm->method('startDateAfter')->willReturnSelf();
        $this->eloquentTerm->method('startDateBefore')->willReturnSelf();
        $this->eloquentTerm->method('take')->willReturnSelf();
        $this->eloquentTerm->method('orderByDesc')->willReturnSelf();
        $this->eloquentTerm->method('CodeEquals')->willReturnSelf();


        $this->eloquentHandler->method('model')
            ->willreturn($this->eloquentTerm);

        $this->termData1 = [
            'code' => new TermCode('201810'),
            'description' => 'sd',
            'startDate' => Carbon::create(2018, 1, 2, 0, 0, 0),
            'endDate' => Carbon::create(2018, 1, 3, 0, 0, 0),
            'isDisplayed' => true
        ];

        $this->termData2 = [
            'code' => new TermCode('201710'),
            'description' => 'aaa',
            'startDate' => Carbon::create(2018, 1, 8, 0, 0, 0),
            'endDate' => Carbon::create(2018, 2, 2, 0, 0, 0),
            'isDisplayed' => false
        ];

        $this->term1 = new Term(
            $this->termData1['code'],
            $this->termData1['description'],
            $this->termData1['startDate'],
            $this->termData1['endDate'],
            $this->termData1['isDisplayed']
        );

        $this->term2 = new Term(
            $this->termData2['code'],
            $this->termData2['description'],
            $this->termData2['startDate'],
            $this->termData2['endDate'],
            $this->termData2['isDisplayed']
        );

        $this->eloquentTerm1 = $this->createMock(EloquentTerm::class);
        $this->eloquentTerm1->method('getCode')->willReturn((string)$this->termData1['code']);
        $this->eloquentTerm1->method('getDescription')->willReturn($this->termData1['description']);
        $this->eloquentTerm1->method('getStartDate')->willReturn($this->termData1['startDate']);
        $this->eloquentTerm1->method('getEndDate')->willReturn($this->termData1['endDate']);
        $this->eloquentTerm1->method('isDisplayed')->willReturn($this->termData1['isDisplayed']);

        $this->eloquentTerm2 = $this->createMock(EloquentTerm::class);
        $this->eloquentTerm2->method('getCode')->willReturn((string)$this->termData2['code']);
        $this->eloquentTerm2->method('getDescription')->willReturn($this->termData2['description']);
        $this->eloquentTerm2->method('getStartDate')->willReturn($this->termData2['startDate']);
        $this->eloquentTerm2->method('getEndDate')->willReturn($this->termData2['endDate']);
        $this->eloquentTerm2->method('isDisplayed')->willReturn($this->termData2['isDisplayed']);
    }
}