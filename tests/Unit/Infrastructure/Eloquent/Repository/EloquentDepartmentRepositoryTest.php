<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\DepartmentCollection;
use MiamiOH\Pike\Domain\Model\Department;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Exception\DepartmentNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentDepartmentRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Department as DepartmentEntity;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Spriden;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EloquentDepartmentRepositoryTest extends TestCase
{
    /**
     * @var EloquentDepartmentRepository
     */
    private $departmentRepository;
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var MockObject
     */
    private $stvmajr;

    /**
     * @var array
     */
    private $departmentData1;

    /**
     * @var Department
     */
    private $department1;

    /**
     * @var MockObject
     */
    private $stvmajr1;

    public function testConstructDepartment()
    {
        $mirror = new \ReflectionClass($this->departmentRepository);
        $func = $mirror->getMethod('constructDepartment');
        $func->setAccessible(true);

        $actual = $func->invoke($this->departmentRepository, $this->stvmajr1);
        $this->assertEquals($this->department1, $actual);
    }

    public function testConstructDepartmentCollection()
    {
        $mirror = new \ReflectionClass($this->departmentRepository);
        $func = $mirror->getMethod('constructDepartmentCollection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->departmentRepository,
            new EloquentCollection([$this->stvmajr1]));
        $this->assertEquals(new DepartmentCollection([$this->department1]), $actual);
    }

    public function testCouldNotFindDepartmentByCode()
    {
        $this->stvmajr->method('first')->willReturn(null);

        $this->expectException(DepartmentNotFoundException::class);
        $this->departmentRepository->getByCode(new DepartmentCode('EVD'));
    }

    public function testGetDepartmentByCode()
    {
        $this->stvmajr->method('first')->willReturn($this->stvmajr1);

        $actualResult = $this->departmentRepository->getByCode($this->department1->getCode());

        $this->assertEquals($this->department1, $actualResult);
    }

    public function testGetAllDepartment()
    {
        $this->stvmajr->method('get')->willReturn(new EloquentCollection([$this->stvmajr1]));

        $actualResult = $this->departmentRepository->getAll();

        $this->assertEquals(new DepartmentCollection([$this->department1]),
            $actualResult);
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model'])
            ->getMock();

        $this->departmentRepository = new EloquentDepartmentRepository($this->eloquentHandler);

        $this->stvmajr = $this->getMockBuilder(Spriden::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'orderByCode',
                'get',
                'codeEquals',
                'first',
            ])
            ->getMock();

        $this->stvmajr->method('orderByCode')->willReturnSelf();
        $this->stvmajr->method('codeEquals')->willReturnSelf();


        $this->eloquentHandler->method('model')
            ->willreturn($this->stvmajr);

        $this->departmentData1 = [
            'code' => new DepartmentCode('CSE'),
            'description' => 'asdfasdf'
        ];

        $this->department1 = new Department(
            $this->departmentData1['code'],
            $this->departmentData1['description']
        );

        $this->stvmajr1 = $this->createMock(DepartmentEntity::class);
        $this->stvmajr1->method('getCode')->willReturn((string)$this->departmentData1['code']);
        $this->stvmajr1->method('getDescription')->willReturn($this->departmentData1['description']);

    }
}