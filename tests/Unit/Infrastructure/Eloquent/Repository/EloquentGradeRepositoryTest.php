<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\GradeTypeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentGradeRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Grade as EloquentGrade;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;


class EloquentGradeRepositoryTest extends TestCase
{
    /**
     * @var EloquentGradeRepository
     */
    private $gradeRepository;
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var MockObject
     */
    private $eloquentGrade;

    /**
     * @var array
     */
    private $gradeData1;
    /**
     * @var GradeCollection
     */
    private $grade1Collection;

    /**
     * @var Grade
     */
    private $gradeFinal;

    /**
     * @var Grade
     */
    private $gradeMidterm;

    /**
     * @var MockObject
     */
    private $eloquentGrade1;

    /**
     * @var MockObject
     */
    private $eloquentGrade2;

    /**
     * @var EloquentCollection
     */
    private $eloquentGradeCollection;


    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model'])
            ->getMock();

        $this->gradeRepository = new EloquentGradeRepository($this->eloquentHandler);

        $this->eloquentGrade = $this->getMockBuilder(EloquentGrade::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'first',
                'byCourseSectionGuidAndUniqueId',
                'get',
                'byTermCodesCrnsUniqueIds',
                'JoinSsbsgidAndszbuniq',
                'byTermCodes',
                'byCrns',
                'byUniqueIds',
                'byCourseSectionGuids'
            ])
            ->getMock();

        $this->eloquentGrade->method('byCourseSectionGuidAndUniqueId')->willReturnSelf();
        $this->eloquentGrade->method('byTermCodesCrnsUniqueIds')->willReturnSelf();
        $this->eloquentGrade->method('JoinSsbsgidAndszbuniq')->willReturnSelf();
        $this->eloquentGrade->method('byTermCodes')->willReturnSelf();
        $this->eloquentGrade->method('byCrns')->willReturnSelf();
        $this->eloquentGrade->method('byUniqueIds')->willReturnSelf();
        $this->eloquentGrade->method('byCourseSectionGuids')->willReturnSelf();


        $this->eloquentHandler->method('model')
            ->willreturn($this->eloquentGrade);


        $this->gradeData1 = [
            'midtermValue' => 'A',
            'finalValue' => 'B',
            'modeCode' => 'C',
            'courseSectionGuid' => (string)CourseSectionGuid::create(),
            'crn' => '123',
            'termCode' => '201710',
            'uniqueId' => 'xiaw'

        ];

        $this->grade1Collection = new GradeCollection();


        $this->gradeMidterm = new Grade(
            new GradeValue($this->gradeData1['midtermValue']),
            new GradeModeCode($this->gradeData1['modeCode']),
            new GradeType('midterm'),
            new CourseSectionGuid($this->gradeData1['courseSectionGuid']),
            new Crn($this->gradeData1['crn']),
            new TermCode($this->gradeData1['termCode']),
            new UniqueId($this->gradeData1['uniqueId'])
        );

        $this->gradeFinal = new Grade(
            new GradeValue($this->gradeData1['finalValue']),
            new GradeModeCode($this->gradeData1['modeCode']),
            new GradeType('final'),
            new CourseSectionGuid($this->gradeData1['courseSectionGuid']),
            new Crn($this->gradeData1['crn']),
            new TermCode($this->gradeData1['termCode']),
            new UniqueId($this->gradeData1['uniqueId'])
        );
        $this->grade1Collection->push($this->gradeMidterm);
        $this->grade1Collection->push($this->gradeFinal);


        $this->eloquentGrade1 = $this->createMock(EloquentGrade::class);
        $this->eloquentGrade1->method('getMidtermValue')->willReturn($this->gradeData1['midtermValue']);
        $this->eloquentGrade1->method('getFinalValue')->willReturn($this->gradeData1['finalValue']);
        $this->eloquentGrade1->method('getGradeModeCode')->willReturn($this->gradeData1['modeCode']);
        $this->eloquentGrade1->method('getCrn')->willReturn($this->gradeData1['crn']);
        $this->eloquentGrade1->method('getTermCode')->willReturn($this->gradeData1['termCode']);
        $this->eloquentGrade1->method('getUniqueId')->willReturn($this->gradeData1['uniqueId']);
        $this->eloquentGrade1->method('getCourseSectionGuid')->willReturn($this->gradeData1['courseSectionGuid']);
        $this->eloquentGradeCollection = new EloquentCollection();
        $this->eloquentGradeCollection->push($this->eloquentGrade1);

        $this->eloquentGrade2 = $this->createMock(EloquentGrade::class);
        $this->eloquentGrade2->method('getMidtermValue')->willReturn(null);
        $this->eloquentGrade2->method('getFinalValue')->willReturn(null);

    }

    public function testInvalidUniqueIdWillCauseException()
    {
        $this->eloquentGrade->method('first')->willReturn($this->eloquentGrade2);

        $actualResult = $this->gradeRepository->getCollectionByCourseSectionGuidAndUniqueId(
            CourseSectionGuid::create(),
            new UniqueId("xiaw1"));
        $this->assertEquals(0, $actualResult->count());

    }

    public function testGetCollectionByCourseSectionGuidAndUniqueId()
    {
        $this->eloquentGrade->method('first')->willReturn($this->eloquentGrade1);
        $actualResult = $this->gradeRepository->getCollectionByCourseSectionGuidAndUniqueId(
            CourseSectionGuid::create(),
            new UniqueId("xiaw")
        );
        $this->assertEquals($this->grade1Collection, $actualResult);
    }

    public function testGetCollectionByTermCodesCrnsUniqueIdsTypes()
    {
        $this->eloquentGrade->method('get')->willReturn($this->eloquentGradeCollection);
        $actualResult = $this->gradeRepository->getCollectionByTermCodesCrnsUniqueIdsTypes(
            new TermCodeCollection(['123']),
            new CrnCollection(['123456']),
            new UniqueIdCollection(['xiaw']),
            new GradeTypeCollection([
                new GradeType('final'),
                new GradeType('midterm')
            ])
        );
        $this->assertEquals($this->grade1Collection, $actualResult);
    }

    public function testGetCollectionByCourseSectionGuidsUniqueIdsTypes()
    {
        $this->eloquentGrade->method('get')->willReturn($this->eloquentGradeCollection);
        $actualResult = $this->gradeRepository->getCollectionByCourseSectionGuidsUniqueIdsTypes(
            new CourseSectionGuidCollection([
                (string)CourseSectionGuid::create()
            ]),
            new UniqueIdCollection(['xiaw']),
            new GradeTypeCollection([
                new GradeType('final'),
                new GradeType('midterm')
            ])
        );
        $this->assertEquals($this->grade1Collection, $actualResult);

    }
}