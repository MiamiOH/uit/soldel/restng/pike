<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionAttributeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionAttribute;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionAttributeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionAttributeRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssrattr;
use MiamiOH\Pike\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class EloquentCourseSectionAttributeRepositoryTest extends TestCase
{
    use MockEloquentHelperTrait;

    /**
     * @var EloquentCourseSectionAttributeRepository
     */
    private $courseSectionAttributeRepository;

    /**
     * @var MockObject
     */
    private $ssrattr;

    /**
     * @var array
     */
    private $courseSectionAttributeData1;

    /**
     * @var CourseSectionAttribute
     */
    private $courseSectionAttribute1;

    /**
     * @var MockObject
     */
    private $ssrattr1;

    public function testConstructCourseSectionAttribute()
    {
        $mirror = new \ReflectionClass($this->courseSectionAttributeRepository);
        $func = $mirror->getMethod('constructCourseSectionAttribute');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionAttributeRepository,
            $this->ssrattr1);
        $this->assertEquals($this->courseSectionAttribute1, $actual);
    }

    public function testConstructCourseSectionAttributeCollection()
    {
        $mirror = new \ReflectionClass($this->courseSectionAttributeRepository);
        $func = $mirror->getMethod('constructCourseSectionAttributeCollection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionAttributeRepository,
            new EloquentCollection([$this->ssrattr1]));
        $this->assertEquals(new CourseSectionAttributeCollection([$this->courseSectionAttribute1]),
            $actual);
    }

    public function testGetCollectionByCourseSectionGuidCollection()
    {
        $this->ssrattr->method('get')->willReturn(new EloquentCollection([$this->ssrattr1]));
        $actual = $this->courseSectionAttributeRepository->getCollectionByCourseSectionGuidCollection(
            new CourseSectionGuidCollection([$this->courseSectionAttribute1->getCourseSectionGuid()])
        );
        $this->assertEquals(
            new CourseSectionAttributeCollection([$this->courseSectionAttribute1]),
            $actual
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionAttributeRepository = new EloquentCourseSectionAttributeRepository($this->eloquentHandler);

        $this->ssrattr = $this->mockEloquentBuilder(Ssrattr::class);

        $this->eloquentHandler->method('model')
            ->willreturn($this->ssrattr);

        $this->courseSectionAttributeData1 = [
            'code' => new CourseSectionAttributeCode('IIS'),
            'description' => 'adsfa',
            'courseSectionGuid' => CourseSectionGuid::create()
        ];

        $this->courseSectionAttribute1 = new CourseSectionAttribute(
            $this->courseSectionAttributeData1['code'],
            $this->courseSectionAttributeData1['description'],
            $this->courseSectionAttributeData1['courseSectionGuid']
        );

        $this->ssrattr1 = $this->mockEloquentModel(Ssrattr::class, [
            'getCode' => (string)$this->courseSectionAttributeData1['code'],
            'getDescription' => (string)$this->courseSectionAttributeData1['description'],
            'getCourseSectionGuid' => (string)$this->courseSectionAttributeData1['courseSectionGuid'],
        ]);

    }
}