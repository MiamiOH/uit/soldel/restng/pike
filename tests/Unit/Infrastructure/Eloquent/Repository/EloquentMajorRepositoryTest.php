<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\MajorCodeCollection;
use MiamiOH\Pike\Domain\Collection\MajorCollection;
use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentMajorRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Major as EloquentMajor;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EloquentMajorRepositoryTest extends TestCase
{
    /**
     * @var EloquentMajorRepository
     */
    private $majorRepository;
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var MockObject
     */
    private $eloquentMajor;

    /**
     * @var array
     */
    private $majorData1;
    /**
     * @var array
     */
    private $majorData2;

    /**
     * @var Major
     */
    private $major1;
    /**
     * @var Major
     */
    private $major2;

    /**
     * @var MockObject
     */
    private $eloquentMajor1;
    /**
     * @var MockObject
     */
    private $eloquentMajor2;

    public function testCanGetAllTerm()
    {
        $this->eloquentMajor->method('get')->willReturn(new EloquentCollection([
            $this->eloquentMajor1,
            $this->eloquentMajor2
        ]));

        $expectedResult = new MajorCollection([$this->major1, $this->major2]);
        $actualResult = $this->majorRepository->getAll();

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testThrowExceptionWhenCodeIsInvalid()
    {
        $this->eloquentMajor->method('first')->willReturn(null);

        $this->expectException(\Exception::class);
        $this->majorRepository->getByCode(new MajorCode('sadf'));
    }

    public function testCanGetSingleMajor()
    {
        $this->eloquentMajor->method('first')->willReturn($this->eloquentMajor1);

        $actualResult = $this->majorRepository->getByCode($this->major1->getCode());

        $this->assertEquals($this->major1, $actualResult);
    }

    public function testCanGetMultipleMajorByCodes()
    {
        $this->eloquentMajor->method('get')->willReturn(new EloquentCollection([
            $this->eloquentMajor1,
            $this->eloquentMajor2
        ]));

        $expectedResult = new MajorCollection([$this->major1, $this->major2]);
        $actualResult = $this->majorRepository->getMajorCollectionByCodes(
            new MajorCodeCollection([
                $this->major1->getCode(),
                $this->major2->getCode()
            ])
        );

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testConstructMajor()
    {
        $reflectionMajorRepository = new \ReflectionClass($this->majorRepository);
        $reflectionConstructMajor = $reflectionMajorRepository->getMethod('constructMajor');
        $reflectionConstructMajor->setAccessible(true);

        $response = $reflectionConstructMajor->invoke($this->majorRepository,
            $this->eloquentMajor1);

        $this->assertEquals($this->major1, $response);
    }

    public function testConstructMajorCollection()
    {
        $reflectionMajorCollectionRepository = new \ReflectionClass($this->majorRepository);
        $reflectionConstructMajorCollection = $reflectionMajorCollectionRepository->getMethod('constructMajorCollection');
        $reflectionConstructMajorCollection->setAccessible(true);

        $response = $reflectionConstructMajorCollection->invoke($this->majorRepository,
            new EloquentCollection([$this->eloquentMajor1, $this->eloquentMajor2]));

        $this->assertEquals(new MajorCollection([$this->major1, $this->major2]),
            $response);
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model'])
            ->getMock();

        $this->majorRepository = new EloquentMajorRepository($this->eloquentHandler);

        $this->eloquentMajor = $this->getMockBuilder(EloquentMajor::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'orderByMajorCode',
                'first',
                'get',
                'codeIn',
                'codeEquals'
            ])
            ->getMock();

        $this->eloquentMajor->method('orderByMajorCode')->willReturnSelf();
        $this->eloquentMajor->method('codeIn')->willReturnSelf();
        $this->eloquentMajor->method('codeEquals')->willReturnSelf();


        $this->eloquentHandler->method('model')
            ->willreturn($this->eloquentMajor);

        $this->majorData1 = [
            'code' => new MajorCode('APET'),
            'description' => 'some description',
        ];

        $this->majorData2 = [
            'code' => new MajorCode('KSLD'),
            'description' => 'lorem dksl',
        ];

        $this->major1 = new Major(
            $this->majorData1['code'],
            $this->majorData1['description']
        );

        $this->major2 = new Major(
            $this->majorData2['code'],
            $this->majorData2['description']
        );

        $this->eloquentMajor1 = $this->createMock(EloquentMajor::class);
        $this->eloquentMajor1->method('getCode')->willReturn((string)$this->majorData1['code']);
        $this->eloquentMajor1->method('getDescription')->willReturn($this->majorData1['description']);

        $this->eloquentMajor2 = $this->createMock(EloquentMajor::class);
        $this->eloquentMajor2->method('getCode')->willReturn((string)$this->majorData2['code']);
        $this->eloquentMajor2->method('getDescription')->willReturn($this->majorData2['description']);

    }
}