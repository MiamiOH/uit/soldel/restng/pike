<?php
/**
 * Author: liaom
 * Date: 5/15/18
 * Time: 3:47 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use PHPUnit\Framework\MockObject\MockObject;

trait MockEloquentHelperTrait
{
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    public function bootMockEloquentHelperTrait()
    {
        $this->eloquentHandler = $this->createMock(EloquentHandler::class);
    }

    public function mockEloquentBuilder(
        string $modelName,
        array $setMethods = [],
        array $whitelist = []
    ): MockObject {
        $eloquentBuilderMethods = get_class_methods(EloquentBuilder::class);
        foreach ($eloquentBuilderMethods as $index => $method) {
            if (preg_match('/^__/', $method)) {
                unset($eloquentBuilderMethods[$index]);
            }
        }
        $modelMethods = get_class_methods($modelName);
        foreach ($modelMethods as $index => $method) {
            if (preg_match('/^__/', $method)) {
                unset($modelMethods[$index]);
            }
        }

        $scopeMethods = [];
        foreach ($modelMethods as $method) {
            $scopeMatches = [];
            if (preg_match('/^scope(.+)$/', $method, $scopeMatches) &&
                !in_array($method, $eloquentBuilderMethods) &&
                $scopeMatches[1] !== '') {
                $scopeMethods[] = lcfirst($scopeMatches[1]);
            }
        }

        $defaultSetMethods = ['all', 'with'];
        $defaultWhitelist = ['all', 'get', 'first'];
        $setMethods = array_unique(array_merge(
            $setMethods,
            $defaultSetMethods,
            $scopeMethods,
            $modelMethods,
            $eloquentBuilderMethods
        ));

        $eloquentBuilder = $this->getMockBuilder(EloquentBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods($setMethods)
            ->getMock();


        $fluentMethods = array_unique(array_diff(
            $setMethods,
            $whitelist,
            $defaultWhitelist
        ));

        foreach ($fluentMethods as $fluentMethod) {
            $eloquentBuilder->method($fluentMethod)->willReturnSelf();
        }

        return $eloquentBuilder;
    }

    public function mockEloquentCollection(string $modelName, array $data)
    {
        $collection = new EloquentCollection();

        foreach ($data as $singleObjectData) {
            $collection->push($this->mockEloquentModel(
                $modelName,
                $singleObjectData
            ));
        }

        return $collection;
    }

    public function mockEloquentModel(string $modelName, array $data)
    {
        $model = $this->createMock($modelName);

        foreach ($data as $methodName => $returnValue) {
            $model->method($methodName)->willReturn($returnValue);
        }

        return $model;
    }
}