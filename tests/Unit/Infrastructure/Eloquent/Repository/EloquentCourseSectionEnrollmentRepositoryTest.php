<?php
/**
 * Author: xiaw
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Query\Builder;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentStatus;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Repository\CourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionEnrollmentRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Sfrstcr;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionClass;


class EloquentCourseSectionEnrollmentRepositoryTest extends TestCase
{
    /**
     * @var EloquentCourseSectionEnrollmentRepository
     */
    private $courseSectionEnrollmentRepository;
    /**
     * @var MockObject
     */
    private $eloquentHandler;

    /**
     * @var MockObject
     */
    private $sfrstcr;
    /**
     * @var MockObject
     */
    private $converter;

    /**
     * @var MockObject
     */
    private $courseSectionRepository;

    /**
     * @var MockObject
     */
    private $courseRepository;
    /**
     * @var MockObject
     */
    private $grade;
    /**
     * @var MockObject
     */
    private $courseSectionEnrollmentStatus;
    /**
     * @var array
     */
    private $courseSectionEnrollmentData;
    /**
     * @var CourseSectionEnrollment
     */
    private $courseSectionEnrollment1;
    /**
     * @var CourseSectionEnrollmentCollection
     */
    private $courseSectionEnrollmentCollection;
    /**
     * @var MockObject
     */
    private $sfrstcr1;
    /**
     * @var EloquentCollection
     */
    private $sfrstcrCollection;
    /**
     * @var MockObject
     */
    private $builder;

    /**
     * @var ReflectionClass
     */
    private $reflectionClass;

    public function testGetCollectionByTermCodesCrnsUniqueIds()
    {
        $this->sfrstcr->method('get')->willReturn($this->sfrstcrCollection);
        $this->sfrstcr->method('orderByGuidPidm')->willReturnSelf();
        $courseSectionMock = $this->createMock(CourseSection::class);
        $courseSectionMock->method('getGuid')->willReturn(new CourseSectionGuid($this->courseSectionEnrollmentData['courseSectionGuid']));
        $courseSectionMock->method('isFinalGradeRequired')->willReturn(true);
        $this->courseSectionRepository->method('getByGuids')
            ->willReturn(new CourseSectionCollection([$courseSectionMock]));

        $actualResult = $this->courseSectionEnrollmentRepository->getCollectionByTermCodesCrnsUniqueIds(
            new TermCodeCollection(['201720']),
            new CrnCollection([]),
            new UniqueIdCollection(['xiaw'])
        );
        $this->assertEquals($this->courseSectionEnrollmentCollection, $actualResult);
    }

    public function testParseGradeSubmissionEligible()
    {
        $method = $this->reflectionClass->getMethod('parseGradeSubmissionEligible');
        $method->setAccessible(true);

        $this->assertTrue($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('RE'),
            null
        ));

        $this->assertTrue($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('RW'),
            null
        ));

        $this->assertFalse($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('AA'),
            null
        ));

        $this->assertFalse($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('RE'),
            Carbon::now()
        ));

        $this->assertFalse($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('RW'),
            Carbon::now()
        ));
    }

    public function testParseGradeSubmissionEligibleComment()
    {
        $method = $this->reflectionClass->getMethod('parseGradeSubmissionEligibleComment');
        $method->setAccessible(true);

        $this->assertEquals(
            'Student Registered(RE) or Web-Registered(RW) in this course',
            $method->invoke(
                $this->courseSectionEnrollmentRepository,
                true,
                new CourseSectionEnrollmentStatusCode('RE'),
                'some description',
                null
            ));

        $this->assertEquals(
            'Grade has already been rolled into academic history',
            $method->invoke(
                $this->courseSectionEnrollmentRepository,
                false,
                new CourseSectionEnrollmentStatusCode('RE'),
                'some description',
                Carbon::now()
            ));

        $this->assertEquals(
            'Grade has already been rolled into academic history',
            $method->invoke(
                $this->courseSectionEnrollmentRepository,
                false,
                new CourseSectionEnrollmentStatusCode('RW'),
                'some description',
                Carbon::now()
            ));

        $this->assertEquals(
            'Registration status some description',
            $method->invoke(
                $this->courseSectionEnrollmentRepository,
                false,
                new CourseSectionEnrollmentStatusCode('AA'),
                'some description',
                null
            ));
    }

    public function testHasAttended()
    {
        $method = $this->reflectionClass->getMethod('hasAttended');
        $method->setAccessible(true);

        $this->assertTrue($method->invoke(
            $this->courseSectionEnrollmentRepository,
            null
        ));

        $this->assertTrue($method->invoke(
            $this->courseSectionEnrollmentRepository,
            '3'
        ));

        $this->assertFalse($method->invoke(
            $this->courseSectionEnrollmentRepository,
            '0'
        ));
    }

    public function testIsEnrollmentActive()
    {
        $method = $this->reflectionClass->getMethod('isEnrollmentActive');
        $method->setAccessible(true);

        $this->assertTrue($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('RE')
        ));

        $this->assertTrue($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('RE')
        ));

        $this->assertTrue($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('AU')
        ));

        $this->assertFalse($method->invoke(
            $this->courseSectionEnrollmentRepository,
            new CourseSectionEnrollmentStatusCode('AA')
        ));
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->eloquentHandler = $this->getMockBuilder(EloquentHandler::class)
            ->disableOriginalConstructor()
            ->setMethods(['model', 'table', 'selectRaw', 'addBinding', 'first'])
            ->getMock();

        $this->builder = $this->getMockBuilder(builder::class)
            ->disableOriginalConstructor()
            ->setMethods(['selectRaw', 'addBinding', 'first'])
            ->getMock();

        $this->eloquentHandler->method('table')
            ->willreturn($this->builder);
        $this->builder->method('selectRaw')
            ->willReturnSelf();
        $this->builder->method('addBinding')
            ->willReturnSelf();
        $this->builder->method('first')
            ->willreturn((object)[
                "indicator" => "Y",
            ]);


        $this->converter = $this->getMockBuilder(Converter::class)
            ->disableOriginalConstructor()
            ->setMethods(['stringToFloat', 'stringToInt'])
            ->getMock();

        $this->courseRepository = new EloquentCourseRepository($this->eloquentHandler,
            $this->converter);


        $this->courseSectionRepository = $this->createMock(CourseSectionRepositoryInterface::class);
        $this->courseSectionEnrollmentRepository = new EloquentCourseSectionEnrollmentRepository(
            $this->eloquentHandler,
            $this->courseSectionRepository,
            new Converter()
        );

        $this->sfrstcr = $this->getMockBuilder(Sfrstcr::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'get',
                'byTermCodes',
                'byUniqueIds',
                'byCrns',
                'joinSzbuniqAndStvrsts',
                'orderByGuidPidm'
            ])
            ->getMock();

        $this->sfrstcr->method('byTermCodes')->willReturnSelf();
        $this->sfrstcr->method('byUniqueIds')->willReturnSelf();
        $this->sfrstcr->method('byCrns')->willReturnSelf();
        $this->sfrstcr->method('joinSzbuniqAndStvrsts')->willReturnSelf();


        $this->eloquentHandler->method('model')
            ->willreturn($this->sfrstcr);


        $this->grade = $this->createMock(Grade::class);


        $this->courseSectionEnrollmentStatus = $this->createMock(CourseSectionEnrollmentStatus::class);


        $this->courseSectionEnrollmentData = [
            'hasAttended' => true,
            'isEnrollmentActive' => false,
            'isGradeSubmissionEligible' => false,
            'gradeSubmissionEligibleComment' => "Registration status status Description",
            'isFinalGradeRequired' => true,
            'isMidtermGradeRequired' => true,
            'gradeMidtermValue' => 'A',
            'gradeFinalValue' => 'A',
            'gradeModeCode' => 'C',
            'gradeType' => 'midterm',
            'uniqueId' => 'xiaw',
            'status' => $this->courseSectionEnrollmentStatus,
            'statusCode' => "A1",
            'statusDescription' => 'status Description',
            'courseSectionGuid' => (string)CourseSectionGuid::create(),
            'creditHours' => 3,
            'studentLevelCode' => 'UG',
            'termCode' => 'AR2000',
            'crn' => '1234'
        ];

        $this->grade->method("getValue")->willReturn(new GradeValue('A'));
        $this->grade->method("getGradeModeCode")->willReturn(new GradeModeCode('A'));
        $this->grade->method("getType")->willReturn(new GradeType('final'));

        $this->courseSectionEnrollment1 = new CourseSectionEnrollment(
            $this->courseSectionEnrollmentData['hasAttended'],
            $this->courseSectionEnrollmentData['isEnrollmentActive'],
            $this->courseSectionEnrollmentData['isGradeSubmissionEligible'],
            $this->courseSectionEnrollmentData['gradeSubmissionEligibleComment'],
            $this->courseSectionEnrollmentData['isFinalGradeRequired'],
            $this->courseSectionEnrollmentData['isMidtermGradeRequired'],
            new GradeCollection([
                new Grade(
                    new GradeValue($this->courseSectionEnrollmentData['gradeMidtermValue']),
                    new GradeModeCode($this->courseSectionEnrollmentData['gradeModeCode']),
                    new GradeType("midterm"),
                    new CourseSectionGuid($this->courseSectionEnrollmentData['courseSectionGuid']),
                    new Crn($this->courseSectionEnrollmentData['crn']),
                    new TermCode($this->courseSectionEnrollmentData['termCode']),
                    new UniqueId($this->courseSectionEnrollmentData['uniqueId'])

                ),
                new Grade(
                    new GradeValue($this->courseSectionEnrollmentData['gradeFinalValue']),
                    new GradeModeCode($this->courseSectionEnrollmentData['gradeModeCode']),
                    new GradeType("final"),
                    new CourseSectionGuid($this->courseSectionEnrollmentData['courseSectionGuid']),
                    new Crn($this->courseSectionEnrollmentData['crn']),
                    new TermCode($this->courseSectionEnrollmentData['termCode']),
                    new UniqueId($this->courseSectionEnrollmentData['uniqueId'])
                ),
            ]),
            new UniqueId($this->courseSectionEnrollmentData['uniqueId']),
            new CourseSectionEnrollmentStatus(
                new CourseSectionEnrollmentStatusCode(
                    $this->courseSectionEnrollmentData['statusCode']
                ),
                $this->courseSectionEnrollmentData['statusDescription']
            ),
            new CourseSectionGuid($this->courseSectionEnrollmentData['courseSectionGuid']),
            $this->courseSectionEnrollmentData['creditHours'],
            new StudentLevelCode($this->courseSectionEnrollmentData['studentLevelCode']),
            new TermCode($this->courseSectionEnrollmentData['termCode']),
            new GradeModeCode($this->courseSectionEnrollmentData['gradeModeCode'])
        );

        $this->courseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection();
        $this->courseSectionEnrollmentCollection->push($this->courseSectionEnrollment1);
        $this->sfrstcr1 = $this->createMock(Sfrstcr::class);
        $this->sfrstcr1->method('getUniqueId')->willReturn($this->courseSectionEnrollmentData['uniqueId']);
        $this->sfrstcr1->method('getTermCode')->willReturn($this->courseSectionEnrollmentData['termCode']);
        $this->sfrstcr1->method('getStatusCode')->willReturn($this->courseSectionEnrollmentData['statusCode']);
        $this->sfrstcr1->method('getStatusDescription')->willReturn($this->courseSectionEnrollmentData['statusDescription']);
        $this->sfrstcr1->method('getMidtermGradeString')->willReturn($this->courseSectionEnrollmentData['gradeMidtermValue']);
        $this->sfrstcr1->method('getFinalGradeString')->willReturn($this->courseSectionEnrollmentData['gradeFinalValue']);
        $this->sfrstcr1->method('getCreditHours')->willReturn($this->courseSectionEnrollmentData['creditHours']);
        $this->sfrstcr1->method('getLevelCode')->willReturn($this->courseSectionEnrollmentData['studentLevelCode']);
        $this->sfrstcr1->method('getCourseSectionGuid')->willReturn($this->courseSectionEnrollmentData['courseSectionGuid']);
        $this->sfrstcr1->method('getGuid')->willReturn($this->courseSectionEnrollmentData['uniqueId']);
        $this->sfrstcr1->method('getGradeModeCode')->willReturn($this->courseSectionEnrollmentData['gradeModeCode']);
        $this->sfrstcr1->method('getCrn')->willReturn("1234");
        $this->sfrstcr1->method('getPidm')->willReturn("32121");


        $this->sfrstcrCollection = new EloquentCollection();
        $this->sfrstcrCollection->push($this->sfrstcr1);
        $this->reflectionClass = new ReflectionClass($this->courseSectionEnrollmentRepository);
    }

}