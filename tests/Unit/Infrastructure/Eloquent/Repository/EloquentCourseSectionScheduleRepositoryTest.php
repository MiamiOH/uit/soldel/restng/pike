<?php
/**
 * Author: liaom
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionScheduleRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssrmeet;
use MiamiOH\Pike\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class EloquentCourseSectionScheduleRepositoryTest extends TestCase
{
    use MockEloquentHelperTrait;

    /**
     * @var EloquentCourseSectionScheduleRepository
     */
    private $courseSectionScheduleRepository;

    /**
     * @var MockObject
     */
    private $ssrmeet;

    /**
     * @var array
     */
    private $courseSectionScheduleData1;

    /**
     * @var CourseSectionSchedule
     */
    private $courseSectionSchedule1;

    /**
     * @var MockObject
     */
    private $ssrmeet1;

    public function testConstructDays()
    {
        $mirror = new \ReflectionClass($this->courseSectionScheduleRepository);
        $func = $mirror->getMethod('constructDays');
        $func->setAccessible(true);

        $this->assertEquals(
            new CourseSectionScheduleDays('TR'),
            $func->invoke(
                $this->courseSectionScheduleRepository,
                null,
                'T',
                null,
                'R',
                null,
                null,
                null
            )
        );

        $this->assertEquals(
            null,
            $func->invoke(
                $this->courseSectionScheduleRepository,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            )
        );
    }

    public function testConstructTime()
    {
        $mirror = new \ReflectionClass($this->courseSectionScheduleRepository);
        $func = $mirror->getMethod('constructTime');
        $func->setAccessible(true);

        $this->assertEquals(
            Carbon::createFromFormat('Hi', '1200'),
            $func->invoke(
                $this->courseSectionScheduleRepository,
                '1200'
            )
        );

        $this->assertEquals(
            null,
            $func->invoke(
                $this->courseSectionScheduleRepository,
                null
            )
        );
    }

    public function testConstructCourseSectionSchedule()
    {
        $mirror = new \ReflectionClass($this->courseSectionScheduleRepository);
        $func = $mirror->getMethod('constructCourseSectionSchedule');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionScheduleRepository,
            $this->ssrmeet1);
        $this->assertEquals($this->courseSectionSchedule1, $actual);
    }

    public function testConstructCourseSectionScheduleCollection()
    {
        $mirror = new \ReflectionClass($this->courseSectionScheduleRepository);
        $func = $mirror->getMethod('constructCourseSectionScheduleCollection');
        $func->setAccessible(true);

        $actual = $func->invoke($this->courseSectionScheduleRepository,
            new EloquentCollection([$this->ssrmeet1]));
        $this->assertEquals(new CourseSectionScheduleCollection([$this->courseSectionSchedule1]),
            $actual);
    }

    public function testGetCollectionByCourseSectionGuids()
    {
        $this->ssrmeet->method('get')->willReturn(new EloquentCollection([$this->ssrmeet1]));
        $actual = $this->courseSectionScheduleRepository
            ->getCollectionByCourseSectionGuidCollection(
                new CourseSectionGuidCollection([
                    $this->courseSectionScheduleData1['courseSectionGuid']
                ])
            );
        $this->assertEquals(
            new CourseSectionScheduleCollection([$this->courseSectionSchedule1]),
            $actual
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionScheduleRepository = new EloquentCourseSectionScheduleRepository($this->eloquentHandler);

        $this->ssrmeet = $this->mockEloquentBuilder(Ssrmeet::class);

        $this->eloquentHandler->method('model')
            ->willreturn($this->ssrmeet);

        $this->courseSectionScheduleData1 = [
            'courseSectionGuid' => CourseSectionGuid::create(),
            'startDate' => Carbon::create(2017, 1, 2, 0, 0, 0),
            'endDate' => Carbon::create(2017, 4, 2, 0, 0, 0),
            'startTime' => Carbon::createFromFormat('Hi', '1200'),
            'endTime' => Carbon::createFromFormat('Hi', '1430'),
            'days' => new CourseSectionScheduleDays('TF'),
            'room' => '123',
            'buildingCode' => new BuildingCode('ECE'),
            'buildingName' => 'adf',
            'typeCode' => new CourseSectionScheduleTypeCode('CLAS'),
            'typeDescription' => 'Class',
        ];

        $this->courseSectionSchedule1 = new CourseSectionSchedule(
            $this->courseSectionScheduleData1['courseSectionGuid'],
            $this->courseSectionScheduleData1['startDate'],
            $this->courseSectionScheduleData1['endDate'],
            $this->courseSectionScheduleData1['startTime'],
            $this->courseSectionScheduleData1['endTime'],
            $this->courseSectionScheduleData1['days'],
            $this->courseSectionScheduleData1['room'],
            $this->courseSectionScheduleData1['buildingCode'],
            $this->courseSectionScheduleData1['buildingName'],
            $this->courseSectionScheduleData1['typeCode'],
            $this->courseSectionScheduleData1['typeDescription']
        );

        $this->ssrmeet1 = $this->mockEloquentModel(Ssrmeet::class, [
            'getStartDate' => $this->courseSectionScheduleData1['startDate'],
            'getEndDate' => $this->courseSectionScheduleData1['endDate'],
            'getStartTime' => '1200',
            'getEndTime' => '1430',
            'getRoom' => $this->courseSectionScheduleData1['room'],
            'getBuildingCode' => (string)$this->courseSectionScheduleData1['buildingCode'],
            'getBuildingName' => $this->courseSectionScheduleData1['buildingName'],
            'getMondayDay' => null,
            'getTuesdayDay' => 'T',
            'getWednesdayDay' => null,
            'getThursdayDay' => null,
            'getFridayDay' => 'F',
            'getSaturdayDay' => null,
            'getTypeCode' => $this->courseSectionScheduleData1['typeCode'],
            'getTypeDescription' => $this->courseSectionScheduleData1['typeDescription'],
            'getCourseSectionGuid' => (string)$this->courseSectionScheduleData1['courseSectionGuid'],

        ]);


    }
}