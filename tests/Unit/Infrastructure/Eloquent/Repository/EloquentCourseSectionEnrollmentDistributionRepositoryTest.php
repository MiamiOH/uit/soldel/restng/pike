<?php
/**
 * Author: xiaw
 * Date: 4/19/18
 * Time: 9:51 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Infrastructure\Eloquent\Repository;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCreditHoursDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionLevelDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionSummaryCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionCreditHoursDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionLevelDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\Model\CourseSectionSummary;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionEnrollmentDistributionRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionScheduleRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssrmeet;
use MiamiOH\Pike\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use stdClass;

class EloquentCourseSectionEnrollmentDistributionRepositoryTest extends TestCase
{
    use MockEloquentHelperTrait;

    /**
     * @var EloquentCourseSectionEnrollmentDistributionRepository
     */
    private $courseSectionLevelDistributionRepository;

    /**
     * @var array
     */
    private $courseSectionLevelDistributionData1;

    /**
     * @var CourseSectionLevelDistribution
     */
    private $courseSectionLevelDistribution1;

    /**
     * @var array
     */
    private $selectCurrentData;

    /**
     * @var CourseSectionCreditHoursDistribution
     */
    private $courseSectionCreditDistribution1;

    /**
     * @var CourseSectionSummary
     */
    private $courseSectionSummary1;

    /**
     * @var CourseSectionSummary
     */
    private $courseSectionSummary2;


    public function testGetCollectionByCourseSectionGuids()
    {
        //$this->ssrmeet->method('get')->willReturn(new EloquentCollection([$this->ssrmeet1]));

        $this->eloquentHandler->method('select')
            ->willreturn([$this->selectCurrentData]);
        $actual = $this->courseSectionLevelDistributionRepository
            ->getCollectionByCourseSectionGuidCollection(
                new CourseSectionGuidCollection([
                    $this->courseSectionLevelDistributionData1['courseSectionGuid']
                ])
            );

            $enrollmentDistribution = new CourseSectionEnrollmentDistribution(
                $this->courseSectionLevelDistributionData1['courseSectionGuid'],
                new CourseSectionCreditHoursDistributionCollection([$this->courseSectionCreditDistribution1]),
                new CourseSectionLevelDistributionCollection([$this->courseSectionLevelDistribution1]),
                new CourseSectionSummaryCollection([
                    $this->courseSectionSummary1,
                    $this->courseSectionSummary2,
             ])
            );

        $this->assertEquals(
            new CourseSectionEnrollmentDistributionCollection([$enrollmentDistribution]),
            $actual
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionLevelDistributionRepository = new EloquentCourseSectionEnrollmentDistributionRepository($this->eloquentHandler);

        $this->courseSectionLevelDistributionData1 = [
            'courseSectionGuid' => CourseSectionGuid::create(),
            'code' => '01',
            'description' => '123',
            'numberOfCurrentEnrollment' => 12,
            'numberOfActiveEnrollment' => 12,
            'credit' => 3
        ];

        $this->courseSectionLevelDistribution1 = new CourseSectionLevelDistribution(
            $this->courseSectionLevelDistributionData1['code'],
            $this->courseSectionLevelDistributionData1['description'],
            $this->courseSectionLevelDistributionData1['numberOfCurrentEnrollment'],
            $this->courseSectionLevelDistributionData1['numberOfActiveEnrollment']
        );

        $this->courseSectionCreditDistribution1 = new CourseSectionCreditHoursDistribution(
            $this->courseSectionLevelDistributionData1['credit'],
            $this->courseSectionLevelDistributionData1['numberOfCurrentEnrollment'],
            $this->courseSectionLevelDistributionData1['numberOfActiveEnrollment']
        );

        $this->courseSectionSummary1 = new CourseSectionSummary(
            'undergraduate',
            $this->courseSectionLevelDistributionData1['numberOfCurrentEnrollment'],
            $this->courseSectionLevelDistributionData1['numberOfActiveEnrollment']
        );

        $this->courseSectionSummary2 = new CourseSectionSummary(
            'underclassman',
            $this->courseSectionLevelDistributionData1['numberOfCurrentEnrollment'],
            $this->courseSectionLevelDistributionData1['numberOfActiveEnrollment']
        );

        $this->selectCurrentData = new stdClass();;
        $this->selectCurrentData->termcrn = 1;
        $this->selectCurrentData->level_code = '01';
        $this->selectCurrentData->level_desc = '123';
        $this->selectCurrentData->level_count = 12;
        $this->selectCurrentData->credit_hours = 3;


    }
}