<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 1:16 PM
 */

namespace MiamiOH\Pike\Tests\Unit\App\Mapper;


use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Mapper\AppMapperFactory;
use MiamiOH\Pike\App\Service\UpdateGradeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionAttributeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentService;
use MiamiOH\Pike\App\Service\ViewCourseSectionScheduleService;
use MiamiOH\Pike\App\Service\ViewCourseSectionService;
use MiamiOH\Pike\App\Service\ViewCourseService;
use MiamiOH\Pike\App\Service\ViewCrossListedCourseSectionService;
use MiamiOH\Pike\App\Service\ViewDepartmentService;
use MiamiOH\Pike\App\Service\ViewGradeService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\App\Service\ViewMajorService;
use MiamiOH\Pike\App\Service\ViewPersonService;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\App\Service\ViewThematicSequenceService;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use PHPUnit\Framework\TestCase;

class AppMapperTest extends TestCase
{
    /**
     * @var AppMapper
     */
    private $appMapper;

    public function testGetViewCourseSectionEnrollmentService()
    {
        $this->assertInstanceOf(
            ViewCourseSectionEnrollmentService::class,
            $this->appMapper->getViewCourseSectionEnrollmentService()
        );
    }

    public function testGetViewCourseSectionService()
    {
        $this->assertInstanceOf(
            ViewCourseSectionService::class,
            $this->appMapper->getViewCourseSectionService()
        );
    }

    public function testGetViewCourseService()
    {
        $this->assertInstanceOf(
            ViewCourseService::class,
            $this->appMapper->getViewCourseService()
        );
    }

    public function testGetViewDepartmentService()
    {
        $this->assertInstanceOf(
            ViewDepartmentService::class,
            $this->appMapper->getViewDepartmentService()
        );
    }

    public function testGetViewInstructorAssignmentService()
    {
        $this->assertInstanceOf(
            ViewInstructorAssignmentService::class,
            $this->appMapper->getViewInstructorAssignmentService()
        );
    }

    public function testGetViewMajorService()
    {
        $this->assertInstanceOf(
            ViewMajorService::class,
            $this->appMapper->getViewMajorService()
        );
    }

    public function testGetViewPersonService()
    {
        $this->assertInstanceOf(
            ViewPersonService::class,
            $this->appMapper->getViewPersonService()
        );
    }

    public function testGetViewTermService()
    {
        $this->assertInstanceOf(
            ViewTermService::class,
            $this->appMapper->getViewTermService()
        );
    }

    public function testGetViewThematicSequenceService()
    {
        $this->assertInstanceOf(
            ViewThematicSequenceService::class,
            $this->appMapper->getViewThematicSequenceService()
        );
    }

    public function testGetViewGradeService()
    {
        $this->assertInstanceOf(
            ViewGradeService::class,
            $this->appMapper->getViewGradeService()
        );
    }

    public function testGetViewCourseSectionScheduleService()
    {
        $this->assertInstanceOf(
            ViewCourseSectionScheduleService::class,
            $this->appMapper->getViewCourseSectionScheduleService()
        );
    }

    public function testGetViewCourseSectionAttributeService()
    {
        $this->assertInstanceOf(
            ViewCourseSectionAttributeService::class,
            $this->appMapper->getViewCourseSectionAttributeService()
        );
    }

    public function testGetViewCrossListedCourseSectionService()
    {
        $this->assertInstanceOf(
            ViewCrossListedCourseSectionService::class,
            $this->appMapper->getViewCrossListedCourseSectionService()
        );
    }

    public function testGetUpdateGradeService()
    {
        $this->assertInstanceOf(
            UpdateGradeService::class,
            $this->appMapper->getUpdateGradeService()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();
        $eloquentHandler = $this->createMock(EloquentHandler::class);
        $appMapperFactory = new AppMapperFactory();
        $this->appMapper = $appMapperFactory->createAppMapper([
            EloquentHandler::class => $eloquentHandler
        ]);
    }

}