<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 1:12 PM
 */

namespace MiamiOH\Pike\Tests\Unit\App\Mapper;


use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Mapper\AppMapperFactory;
use PHPUnit\Framework\TestCase;

class AppMapperFactoryTest extends TestCase
{
    public function testCreateAppMapper()
    {
        $appMapperFactory = new AppMapperFactory();
        $this->assertInstanceOf(AppMapper::class,
            $appMapperFactory->createAppMapper());
    }
}