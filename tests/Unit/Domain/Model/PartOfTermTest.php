<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:43 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\Model\PartOfTerm;
use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;
use PHPUnit\Framework\TestCase;

class PartOfTermTest extends TestCase
{
    /**
     * @var array
     */
    private $partOfTermData;
    /**
     * @var PartOfTerm
     */
    private $partOfTerm;

    public function testGetCode()
    {
        $this->assertEquals(
            $this->partOfTermData['code'],
            $this->partOfTerm->getCode()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->partOfTermData['description'],
            $this->partOfTerm->getDescription()
        );
    }

    public function testGetStartDate()
    {
        $this->assertEquals(
            $this->partOfTermData['startDate'],
            $this->partOfTerm->getStartDate()
        );
    }

    public function testGetEndDate()
    {
        $this->assertEquals(
            $this->partOfTermData['endDate'],
            $this->partOfTerm->getEndDate()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->partOfTermData = [
            'code' => new PartOfTermCode('1'),
            'description' => 'asdf',
            'startDate' => Carbon::create(2017, 1, 2, 0, 0, 0),
            'endDate' => Carbon::create(2017, 2, 2, 0, 0, 0),
        ];

        $this->partOfTerm = new PartOfTerm(
            $this->partOfTermData['code'],
            $this->partOfTermData['description'],
            $this->partOfTermData['startDate'],
            $this->partOfTermData['endDate']
        );
    }
}