<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:43 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\SchoolCode;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use PHPUnit\Framework\TestCase;

class CourseTest extends TestCase
{
    /**
     * @var array
     */
    private $courseData;
    /**
     * @var Course
     */
    private $course;

    public function testGetGuid()
    {
        $this->assertEquals(
            $this->courseData['guid'],
            $this->course->getGuid()
        );
    }

    public function testGetTermCode()
    {
        $this->assertEquals(
            $this->courseData['termCode'],
            $this->course->getTermCode()
        );
    }

    public function testGetSubjectCode()
    {
        $this->assertEquals(
            $this->courseData['subjectCode'],
            $this->course->getSubjectCode()
        );
    }

    public function testGetNumber()
    {
        $this->assertEquals(
            $this->courseData['number'],
            $this->course->getNumber()
        );
    }

    public function testGetTermEffectedCode()
    {
        $this->assertEquals(
            $this->courseData['termEffectedCode'],
            $this->course->getTermEffectedCode()
        );
    }

    public function testGetTitle()
    {
        $this->assertEquals(
            $this->courseData['title'],
            $this->course->getTitle()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->courseData['description'],
            $this->course->getDescription()
        );
    }

    public function testGetSchoolCode()
    {
        $this->assertEquals(
            $this->courseData['schoolCode'],
            $this->course->getSchoolCode()
        );
    }

    public function testGetSchoolName()
    {
        $this->assertEquals(
            $this->courseData['schoolName'],
            $this->course->getSchoolName()
        );
    }

    public function testGetDepartmentCode()
    {
        $this->assertEquals(
            $this->courseData['departmentCode'],
            $this->course->getDepartmentCode()
        );
    }

    public function testGetDepartmentName()
    {
        $this->assertEquals(
            $this->courseData['departmentName'],
            $this->course->getDepartmentName()
        );
    }

    public function testGetSubjectDescription()
    {
        $this->assertEquals(
            $this->courseData['subjectDescription'],
            $this->course->getSubjectDescription()
        );
    }

    public function testGetCreditHoursLow()
    {
        $this->assertEquals(
            $this->courseData['creditHoursLow'],
            $this->course->getCreditHoursLow()
        );
    }

    public function testGetCreditHoursHigh()
    {
        $this->assertEquals(
            $this->courseData['creditHoursHigh'],
            $this->course->getCreditHoursHigh()
        );
    }

    public function testGetLectureHoursLow()
    {
        $this->assertEquals(
            $this->courseData['lectureHoursLow'],
            $this->course->getLectureHoursLow()
        );
    }

    public function testGetLectureHoursHigh()
    {
        $this->assertEquals(
            $this->courseData['lectureHoursHigh'],
            $this->course->getLectureHoursHigh()
        );
    }


    public function testGetLabHoursLow()
    {
        $this->assertEquals(
            $this->courseData['labHoursLow'],
            $this->course->getLabHoursLow()
        );
    }

    public function testGetLabHoursHigh()
    {
        $this->assertEquals(
            $this->courseData['labHoursHigh'],
            $this->course->getLabHoursHigh()
        );
    }

    public function testGetLabHourDescription()
    {
        $this->assertEquals(
            $this->courseData['labHourDescription'],
            $this->course->getLabHoursDescription()
        );
    }

    public function testGetLectureDescription()
    {
        $this->assertEquals(
            $this->courseData['lectureDescription'],
            $this->course->getLectureHoursDescription()
        );
    }

    public function testGetCreditHourDescription()
    {
        $this->assertEquals(
            $this->courseData['creditHourDescription'],
            $this->course->getCreditHoursDescription()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseData = [
            'guid' => CourseGuid::create(),
            'termCode' => new TermCode('201710'),
            'subjectCode' => new SubjectCode('CSE'),
            'number' => new CourseNumber('123'),
            'termEffectedCode' => new TermCode('201810'),
            'title' => 'asdfasd',
            'description' => 'fdsgads',
            'schoolCode' => new SchoolCode('AO'),
            'schoolName' => 'asdfas',
            'departmentCode' => new DepartmentCode('CSE'),
            'departmentName' => 'asdfadsf',
            'subjectDescription' => 'asdkjfdj',
            'creditHoursLow' => 3,
            'creditHoursHigh' => 0,
            'lectureHoursLow' => 2,
            'lectureHoursHigh' => null,
            'labHoursLow' => 1,
            'labHoursHigh' => 2,
            'creditHourDescription' => 'low high',
            'labHourDescription' => 'low high',
            'lectureDescription' => 'low high',
            'creditHoursAvailable' => [1, 2]
        ];

        $this->course = new Course(
            $this->courseData['guid'],
            $this->courseData['termCode'],
            $this->courseData['subjectCode'],
            $this->courseData['number'],
            $this->courseData['termEffectedCode'],
            $this->courseData['title'],
            $this->courseData['description'],
            $this->courseData['schoolCode'],
            $this->courseData['schoolName'],
            $this->courseData['departmentCode'],
            $this->courseData['departmentName'],
            $this->courseData['subjectDescription'],
            $this->courseData['creditHoursLow'],
            $this->courseData['creditHoursHigh'],
            $this->courseData['lectureHoursLow'],
            $this->courseData['lectureHoursHigh'],
            $this->courseData['labHoursLow'],
            $this->courseData['labHoursHigh'],
            $this->courseData['creditHourDescription'],
            $this->courseData['labHourDescription'],
            $this->courseData['lectureDescription'],
            $this->courseData['creditHoursAvailable']
        );
    }
}