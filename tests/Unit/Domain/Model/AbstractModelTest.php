<?php
/**
 * Author: liaom
 * Date: 4/23/18
 * Time: 1:50 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\AbstractModel;
use PHPUnit\Framework\TestCase;

class AbstractModelTest extends TestCase
{
    public function testModelCanTransformToArray()
    {
        $testModel = new TestModel();

        $this->assertEquals([
            'attr1' => 3,
            'attr2' => 'str'
        ], $testModel->toArray());
    }
}

class TestModel extends AbstractModel
{
    protected $attr1 = 3;
    protected $attr2 = 'str';
}