<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;
use PHPUnit\Framework\TestCase;

class MajorTest extends TestCase
{
    /**
     * @var array
     */
    private $majorDate;
    /**
     * @var Major
     */
    private $major;

    public function testGetCode()
    {
        $this->assertEquals(
            $this->majorDate['code'],
            $this->major->getCode()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->majorDate['description'],
            $this->major->getDescription()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->majorDate = [
            'code' => new MajorCode('KDLS'),
            'description' => 'adsfa'
        ];

        $this->major = new Major(
            $this->majorDate['code'],
            $this->majorDate['description']
        );
    }
}//end class
