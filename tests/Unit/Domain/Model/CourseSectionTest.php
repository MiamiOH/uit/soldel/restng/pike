<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\PartOfTerm;
use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CourseSectionTest extends TestCase
{
    /**
     * @var array
     */
    private $courseSectionDate;
    /**
     * @var CourseSection
     */
    private $courseSection;
    /**
     * @var MockObject
     */
    private $course;
    /**
     * @var MockObject
     */
    private $partOfTerm;

    /**
     * @var array
     */
    private $courseData;

    public function testGetGuid()
    {
        $this->assertEquals(
            $this->courseSectionDate['guid'],
            $this->courseSection->getGuid()
        );
    }

    public function testGetCourse()
    {
        $this->assertEquals(
            $this->courseSectionDate['course'],
            $this->courseSection->getCourse()
        );
    }

    public function testGetTermCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['termCode'],
            $this->courseSection->getTermCode()
        );
    }

    public function testGetTitle()
    {
        $this->assertEquals(
            $this->courseSectionDate['title'],
            $this->courseSection->getTitle()
        );
    }

    public function testGetCrn()
    {
        $this->assertEquals(
            $this->courseSectionDate['crn'],
            $this->courseSection->getCrn()
        );
    }

    public function testGetTermDescription()
    {
        $this->assertEquals(
            $this->courseSectionDate['termDescription'],
            $this->courseSection->getTermDescription()
        );
    }

    public function testGetSectionCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['sectionCode'],
            $this->courseSection->getSectionCode()
        );
    }

    public function testGetCreditHours()
    {
        $this->assertEquals(
            $this->courseSectionDate['creditHours'],
            $this->courseSection->getCreditHoursDescription()
        );
    }

    public function testGetInstructionalTypeCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['instructionalTypeCode'],
            $this->courseSection->getInstructionalTypeCode()
        );
    }

    public function testGetInstructionalTypeDescription()
    {
        $this->assertEquals(
            $this->courseSectionDate['instructionalTypeDescription'],
            $this->courseSection->getInstructionalTypeDescription()
        );
    }

    public function testGetCampusCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['campusCode'],
            $this->courseSection->getCampusCode()
        );
    }

    public function testGetCampusDescription()
    {
        $this->assertEquals(
            $this->courseSectionDate['campusDescription'],
            $this->courseSection->getCampusDescription()
        );
    }

    public function testGetCourseSectionStatusCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['courseSectionStatusCode'],
            $this->courseSection->getCourseSectionStatusCode()
        );
    }

    public function testGetCourseSectionStatusDescription()
    {
        $this->assertEquals(
            $this->courseSectionDate['courseSectionStatusDescription'],
            $this->courseSection->getCourseSectionStatusDescription()
        );
    }

    public function testGetMaxNumberOfEnrollment()
    {
        $this->assertEquals(
            $this->courseSectionDate['maxNumberOfEnrollment'],
            $this->courseSection->getMaxNumberOfEnrollment()
        );
    }

    public function testGetCourseTitle()
    {
        $this->course->method('getTitle')->willReturn('abc');
        $this->assertEquals(
            'abc',
            $this->courseSection->getCourseTitle()
        );
    }

    public function testGetPartOfTerm()
    {
        $this->assertEquals(
            $this->partOfTerm,
            $this->courseSection->getPartOfTerm()
        );
    }

    public function testIsMidtermGradeSubmissionAvailable()
    {
        $this->assertEquals(
            $this->courseSectionDate['isMidtermGradeSubmissionAvailable'],
            $this->courseSection->isMidtermGradeSubmissionAvailable()
        );
    }

    public function testIsFinalGradeSubmissionAvailable()
    {
        $this->assertEquals(
            $this->courseSectionDate['isFinalGradeSubmissionAvailable'],
            $this->courseSection->isFinalGradeSubmissionAvailable()
        );
    }

    public function testIsFinalGradeRequired()
    {
        $this->assertEquals(
            $this->courseSectionDate['isFinalGradeRequired'],
            $this->courseSection->isFinalGradeRequired()
        );
    }

    public function testIsDisplayed()
    {
        $this->assertEquals(
            $this->courseSectionDate['isDisplayed'],
            $this->courseSection->isDisplayed()
        );
    }

    public function testSectionName()
    {
        $this->course->method("getSubjectCode")->willReturn(new SubjectCode($this->courseData['subjectCode']));
        $this->course->method("getNumber")->willReturn(new CourseNumber($this->courseData['number']));

        $this->assertEquals(
            'ACC 201 A',
            $this->courseSection->getSectionName()
        );
    }

    public function testGetStandardizedDivisionCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['standardizedDivisionCode'],
            $this->courseSection->getStandardizedDivisionCode()
        );
    }

    public function testGetStandardizedDivisionName()
    {
        $this->assertEquals(
            $this->courseSectionDate['standardizedDivisionName'],
            $this->courseSection->getStandardizedDivisionName()
        );
    }

    public function testGetStandardizedDepartmentCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['standardizedDepartmentCode'],
            $this->courseSection->getStandardizedDepartmentCode()
        );
    }

    public function testGetStandardizedDepartmentName()
    {
        $this->assertEquals(
            $this->courseSectionDate['standardizedDepartmentName'],
            $this->courseSection->getStandardizedDepartmentName()
        );
    }

    public function testGetLegacyStandardizedDepartmentCode()
    {
        $this->assertEquals(
            $this->courseSectionDate['legacyStandardizedDepartmentCode'],
            $this->courseSection->getLegacyStandardizedDepartmentCode()
        );
    }

    public function testGetLegacyStandardizedDepartmentName()
    {
        $this->assertEquals(
            $this->courseSectionDate['legacyStandardizedDepartmentName'],
            $this->courseSection->getLegacyStandardizedDepartmentName()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->course = $this->createMock(Course::class);
        $this->partOfTerm = $this->createMock(PartOfTerm::class);

        $this->courseData = [
            'subjectCode' => "ACC",
            'number' => "201",
        ];

        $this->courseSectionDate = [
            'guid' => CourseSectionGuid::create(),
            'course' => $this->course,
            'termCode' => new TermCode('201810'),
            'crn' => new Crn('12345'),
            'title' => 'CR Reading',
            'termDescription' => 'asdf',
            'sectionCode' => 'A',
            'creditHours' => '3',
            'creditHoursLow' => '3',
            'creditHoursHigh' => '3',
            'creditHoursAvailable' => ['3'],
            'instructionalTypeCode' => new InstructionalTypeCode('A'),
            'instructionalTypeDescription' => 'asdf',
            'campusCode' => new CampusCode('O'),
            'campusDescription' => 'asdf',
            'courseSectionStatusCode' => new CourseSectionStatusCode('A'),
            'courseSectionStatusDescription' => 'asdf',
            'maxNumberOfEnrollment' => 32,
            'isMidtermGradeSubmissionAvailable' => true,
            'isFinalGradeSubmissionAvailable' => true,
            'isFinalGradeRequired' => true,
            'isDisplayed' => false,
            'standardizedDivisionCode' => new DivisionCode('CAS'),
            'standardizedDivisionName' => 'asdfasd',
            'standardizedDepartmentCode' => new DepartmentCode('CSE'),
            'standardizedDepartmentName' => 'agfdfg',
            'legacyStandardizedDepartmentCode' => new DepartmentCode('CSE'),
            'legacyStandardizedDepartmentName' => 'asdfew',
        ];

        $this->courseSection = new CourseSection(
            $this->courseSectionDate['guid'],
            $this->courseSectionDate['course'],
            $this->courseSectionDate['title'],
            $this->courseSectionDate['termCode'],
            $this->courseSectionDate['crn'],
            $this->courseSectionDate['termDescription'],
            $this->courseSectionDate['sectionCode'],
            $this->courseSectionDate['creditHours'],
            $this->courseSectionDate['creditHoursLow'],
            $this->courseSectionDate['creditHoursHigh'],
            $this->courseSectionDate['creditHoursAvailable'],
            $this->courseSectionDate['instructionalTypeCode'],
            $this->courseSectionDate['instructionalTypeDescription'],
            $this->courseSectionDate['campusCode'],
            $this->courseSectionDate['campusDescription'],
            $this->courseSectionDate['courseSectionStatusCode'],
            $this->courseSectionDate['courseSectionStatusDescription'],
            $this->courseSectionDate['maxNumberOfEnrollment'],
            $this->partOfTerm,
            $this->courseSectionDate['isMidtermGradeSubmissionAvailable'],
            $this->courseSectionDate['isFinalGradeSubmissionAvailable'],
            $this->courseSectionDate['isFinalGradeRequired'],
            $this->courseSectionDate['isDisplayed'],
            $this->courseSectionDate['standardizedDivisionCode'],
            $this->courseSectionDate['standardizedDivisionName'],
            $this->courseSectionDate['standardizedDepartmentCode'],
            $this->courseSectionDate['standardizedDepartmentName'],
            $this->courseSectionDate['legacyStandardizedDepartmentCode'],
            $this->courseSectionDate['legacyStandardizedDepartmentName']
        );
    }
}//end class
