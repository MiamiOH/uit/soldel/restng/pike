<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:43 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;
use PHPUnit\Framework\TestCase;

class CourseSectionScheduleTest extends TestCase
{
    /**
     * @var array
     */
    private $courseSectionScheduleData;
    /**
     * @var CourseSectionSchedule
     */
    private $courseSectionSchedule;

    public function testCourseSectionGuid()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['courseSectionGuid'],
            $this->courseSectionSchedule->getCourseSectionGuid()
        );
    }

    public function testStartDate()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['startDate'],
            $this->courseSectionSchedule->getStartDate()
        );
    }

    public function testEndDate()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['endDate'],
            $this->courseSectionSchedule->getEndDate()
        );
    }

    public function testStartTime()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['startTime'],
            $this->courseSectionSchedule->getStartTime()
        );
    }

    public function testEndTime()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['endTime'],
            $this->courseSectionSchedule->getEndTime()
        );
    }

    public function testDays()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['days'],
            $this->courseSectionSchedule->getDays()
        );
    }

    public function testBuildingCode()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['buildingCode'],
            $this->courseSectionSchedule->getBuildingCode()
        );
    }

    public function testBuildingName()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['buildingName'],
            $this->courseSectionSchedule->getBuildingName()
        );
    }

    public function testTypeCode()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['typeCode'],
            $this->courseSectionSchedule->getTypeCode()
        );
    }

    public function testTypeDescription()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['typeDescription'],
            $this->courseSectionSchedule->getTypeDescription()
        );
    }

    public function testGetRoom()
    {
        $this->assertEquals(
            $this->courseSectionScheduleData['room'],
            $this->courseSectionSchedule->getRoom()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionScheduleData = [
            'courseSectionGuid' => CourseSectionGuid::create(),
            'startDate' => Carbon::create(2017, 1, 2, 0, 0, 0),
            'endDate' => Carbon::create(2017, 4, 2, 0, 0, 0),
            'startTime' => Carbon::create(0, 0, 0, 12, 0, 0),
            'endTime' => Carbon::create(0, 0, 0, 14, 30, 0),
            'days' => new CourseSectionScheduleDays('TF'),
            'room' => '123',
            'buildingCode' => new BuildingCode('ECE'),
            'buildingName' => 'adf',
            'typeCode' => new CourseSectionScheduleTypeCode('CLAS'),
            'typeDescription' => 'Class',
        ];

        $this->courseSectionSchedule = new CourseSectionSchedule(
            $this->courseSectionScheduleData['courseSectionGuid'],
            $this->courseSectionScheduleData['startDate'],
            $this->courseSectionScheduleData['endDate'],
            $this->courseSectionScheduleData['startTime'],
            $this->courseSectionScheduleData['endTime'],
            $this->courseSectionScheduleData['days'],
            $this->courseSectionScheduleData['room'],
            $this->courseSectionScheduleData['buildingCode'],
            $this->courseSectionScheduleData['buildingName'],
            $this->courseSectionScheduleData['typeCode'],
            $this->courseSectionScheduleData['typeDescription']
        );
    }

}