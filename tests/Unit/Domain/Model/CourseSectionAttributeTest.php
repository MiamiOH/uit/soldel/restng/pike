<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\CourseSectionAttribute;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionAttributeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use PHPUnit\Framework\TestCase;

class CourseSectionAttributeTest extends TestCase
{
    /**
     * @var array
     */
    private $courseSectionAttributeDate;
    /**
     * @var CourseSectionAttribute
     */
    private $courseSectionAttribute;

    public function testGetCode()
    {
        $this->assertEquals(
            $this->courseSectionAttributeDate['code'],
            $this->courseSectionAttribute->getCode()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->courseSectionAttributeDate['description'],
            $this->courseSectionAttribute->getDescription()
        );
    }

    public function testGetCourseSectionGuid()
    {
        $this->assertEquals(
            $this->courseSectionAttributeDate['courseSectionGuid'],
            $this->courseSectionAttribute->getCourseSectionGuid()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionAttributeDate = [
            'code' => new CourseSectionAttributeCode('IIS'),
            'description' => 'adsfa',
            'courseSectionGuid' => CourseSectionGuid::create()
        ];

        $this->courseSectionAttribute = new CourseSectionAttribute(
            $this->courseSectionAttributeDate['code'],
            $this->courseSectionAttributeDate['description'],
            $this->courseSectionAttributeDate['courseSectionGuid']
        );
    }
}//end class
