<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:43 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use PHPUnit\Framework\TestCase;

class GradeTest extends TestCase
{
    /**
     * @var array
     */
    private $gradeData;
    /**
     * @var Grade
     */
    private $grade;

    protected function setUp() :void
    {
        parent::setUp();

        $this->gradeData = [
            'value' => new GradeValue('A'),
            'type' => new GradeType('midterm'),
            'modeCode' => new GradeModeCode('X'),
            'courseSectionGuid' => CourseSectionGuid::create(),
            'termCode' => new TermCode('201710'),
            'crn' => new Crn('12345'),
            'uniqueId' => new UniqueId('xiaw')

        ];

        $this->grade = new Grade(
            $this->gradeData['value'],
            $this->gradeData['modeCode'],
            $this->gradeData['type'],
            $this->gradeData['courseSectionGuid'],
            $this->gradeData['crn'],
            $this->gradeData['termCode'],
            $this->gradeData['uniqueId']
        );
    }

    public function testGetCourseSectionGuid()
    {
        $this->assertEquals(
            $this->gradeData['courseSectionGuid'],
            $this->grade->getCourseSectionGuid()
        );
    }

    public function testGetTermCode()
    {
        $this->assertEquals(
            $this->gradeData['termCode'],
            $this->grade->getTermCode()
        );
    }

    public function testGetUniqueId()
    {
        $this->assertEquals(
            $this->gradeData['uniqueId'],
            $this->grade->getUniqueId()
        );
    }

    public function testGetCrn()
    {
        $this->assertEquals(
            $this->gradeData['crn'],
            $this->grade->getCrn()
        );
    }

    public function testGetModeCode()
    {
        $this->assertEquals(
            $this->gradeData['modeCode'],
            $this->grade->getGradeModeCode()
        );
    }

    public function testGetType()
    {
        $this->assertEquals(
            $this->gradeData['type'],
            $this->grade->getType()
        );
    }

    public function testGetValue()
    {
        $this->assertEquals(
            $this->gradeData['value'],
            $this->grade->getValue()
        );
    }

}