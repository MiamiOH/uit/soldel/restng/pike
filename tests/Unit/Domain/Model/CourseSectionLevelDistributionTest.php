<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\CourseSectionLevelDistribution;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use PHPUnit\Framework\TestCase;

class CourseSectionLevelDistributionTest extends TestCase
{

    /**
     * @var CourseSectionLevelDistribution
     */
    private $courseSectionLevelDistribution;

    /**
     * @var array
     */
    private $courseSectionLevelDistributionData1;

    public function testGetCode()
    {
        $this->assertEquals(
            $this->courseSectionLevelDistributionData1['code'],
            $this->courseSectionLevelDistribution->getCode()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->courseSectionLevelDistributionData1['description'],
            $this->courseSectionLevelDistribution->getDescription()
        );
    }

    public function testGetNumberOfCurrentEnrollment()
    {
        $this->assertEquals(
            $this->courseSectionLevelDistributionData1['numberOfCurrentEnrollment'],
            $this->courseSectionLevelDistribution->getNumberOfCurrentEnrollment()
        );
    }

    public function testGetNumberOfActiveEnrollment()
    {
        $this->assertEquals(
            $this->courseSectionLevelDistributionData1['numberOfActiveEnrollment'],
            $this->courseSectionLevelDistribution->getNumberOfActiveEnrollment()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionLevelDistributionData1 = [
            'code' => 1,
            'description' => '123',
            'numberOfCurrentEnrollment' => 12,
            'numberOfActiveEnrollment' => 12,
        ];

        $this->courseSectionLevelDistribution = new CourseSectionLevelDistribution(
            $this->courseSectionLevelDistributionData1['code'],
            $this->courseSectionLevelDistributionData1['description'],
            $this->courseSectionLevelDistributionData1['numberOfCurrentEnrollment'],
            $this->courseSectionLevelDistributionData1['numberOfActiveEnrollment']
        );

    }
}//end class
