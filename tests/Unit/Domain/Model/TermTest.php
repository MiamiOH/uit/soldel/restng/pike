<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use Carbon\Carbon;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use PHPUnit\Framework\TestCase;

class TermTest extends TestCase
{
    /**
     * @var TermCode
     */
    private $code;
    /**
     * @var string
     */
    private $description;
    /**
     * @var Carbon
     */
    private $startDate;
    /**
     * @var Carbon
     */
    private $endDate;
    /**
     * @var bool
     */
    private $isDisplayed;

    /**
     * @var Term
     */
    private $term;

    public function testCanGetCode()
    {
        self::assertEquals($this->code, $this->term->getCode());
    }

    public function testCanGetDescription()
    {
        self::assertEquals($this->description, $this->term->getDescription());
    }

    public function testCanGetStartDate()
    {
        self::assertEquals($this->startDate, $this->term->getStartDate());
    }

    public function testCanGetEndDate()
    {
        self::assertEquals($this->endDate, $this->term->getEndDate());
    }

    public function testCanGetIsDisplayed()
    {
        self::assertEquals($this->isDisplayed, $this->term->isDisplayed());
    }

    protected function setUp() :void
    {
        parent::setUp();
        $this->code = new TermCode('201810');
        $this->description = 'asdf';
        $this->startDate = Carbon::yesterday();
        $this->endDate = Carbon::tomorrow();
        $this->isDisplayed = true;

        $this->term = new Term(
            $this->code,
            $this->description,
            $this->startDate,
            $this->endDate,
            $this->isDisplayed);
    }


}//end class
