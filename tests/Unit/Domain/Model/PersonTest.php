<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:43 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    /**
     * @var Person
     */
    private $person;

    /**
     * @var array
     */
    private $personData;

    public function testGetUniqueId()
    {
        $this->assertEquals(
            $this->personData['uniqueId'],
            $this->person->getUniqueId()
        );
    }

    public function testGetPidm()
    {
        $this->assertEquals(
            $this->personData['pidm'],
            $this->person->getPidm()
        );
    }

    public function testGetBannerId()
    {
        $this->assertEquals(
            $this->personData['bannerId'],
            $this->person->getBannerId()
        );
    }

    public function testGetLastName()
    {
        $this->assertEquals(
            $this->personData['lastName'],
            $this->person->getLastName()
        );
    }

    public function testGetFirstName()
    {
        $this->assertEquals(
            $this->personData['firstName'],
            $this->person->getFirstName()
        );
    }

    public function testGetMiddleName()
    {
        $this->assertEquals(
            $this->personData['middleName'],
            $this->person->getMiddleName()
        );
    }

    public function testGetPrefix()
    {
        $this->assertEquals(
            $this->personData['prefix'],
            $this->person->getPrefix()
        );
    }

    public function testGetSuffix()
    {
        $this->assertEquals(
            $this->personData['suffix'],
            $this->person->getSuffix()
        );
    }

    public function testGetPreferredFirstName()
    {
        $this->assertEquals(
            $this->personData['preferredFirstName'],
            $this->person->getPreferredFirstName()
        );
    }

    public function testGetInformalDisplayedName()
    {
        $this->assertEquals('Jan Ellen Eighme',
            $this->person->getInformalDisplayedName());
    }

    public function testGetFormalDisplayedName()
    {
        $this->assertEquals('Dr. Jan Ellen Eighme',
            $this->person->getFormalDisplayedName());
    }

    public function testGetInformalSortedName()
    {
        $this->assertEquals('Eighme, Jan Ellen',
            $this->person->getInformalSortedName());
    }

    public function testGetFormalSortedName()
    {
        $this->assertEquals('Eighme, Jan Ellen Dr.',
            $this->person->getFormalSortedName());
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->personData = [
            'uniqueId' => new UniqueId('liaom'),
            'pidm' => new Pidm('1048293'),
            'bannerId' => new BannerId('+01928394'),
            'lastName' => 'Eighme',
            'firstName' => 'Jan',
            'middleName' => 'Ellen',
            'prefix' => 'Dr.',
            'suffix' => null,
            'preferredFirstName' => null,
        ];

        $this->person = new Person(
            $this->personData['uniqueId'],
            $this->personData['pidm'],
            $this->personData['bannerId'],
            $this->personData['lastName'],
            $this->personData['firstName'],
            $this->personData['middleName'],
            $this->personData['prefix'],
            $this->personData['suffix'],
            $this->personData['preferredFirstName']
        );
    }
}