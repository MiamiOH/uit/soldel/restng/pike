<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 7:33 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use PHPUnit\Framework\TestCase;

class InstructorAssignmentTest extends TestCase
{
    /**
     * @var array
     */
    private $instructorAssignmentData;
    /**
     * @var InstructorAssignment
     */
    private $instructorAssignment;

    public function testGetUniqueId()
    {
        $this->assertEquals(
            $this->instructorAssignmentData['uniqueId'],
            $this->instructorAssignment->getUniqueId()
        );
    }

    public function testGetTermCode()
    {
        $this->assertEquals(
            $this->instructorAssignmentData['termCode'],
            $this->instructorAssignment->getTermCode()
        );
    }

    public function testGetCrn()
    {
        $this->assertEquals(
            $this->instructorAssignmentData['crn'],
            $this->instructorAssignment->getCrn()
        );
    }

    public function testGetGuid()
    {
        $this->assertEquals(
            $this->instructorAssignmentData['guid'],
            $this->instructorAssignment->getGuid()
        );
    }

    public function testGetCourseSectionGuid()
    {
        $this->assertEquals(
            $this->instructorAssignmentData['courseSectionGuid'],
            $this->instructorAssignment->getCourseSectionGuid()
        );
    }

    public function testIsPrimary()
    {
        $this->assertEquals(
            $this->instructorAssignmentData['isPrimary'],
            $this->instructorAssignment->isPrimary()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->instructorAssignmentData = [
            'guid' => InstructorAssignmentGuid::create(),
            'uniqueId' => new UniqueId('liaom'),
            'termCode' => new TermCode('201810'),
            'crn' => new Crn('12345'),
            'courseSectionGuid' => CourseSectionGuid::create(),
            'isPrimary' => true
        ];

        $this->instructorAssignment = new InstructorAssignment(
            $this->instructorAssignmentData['guid'],
            $this->instructorAssignmentData['uniqueId'],
            $this->instructorAssignmentData['termCode'],
            $this->instructorAssignmentData['crn'],
            $this->instructorAssignmentData['courseSectionGuid'],
            $this->instructorAssignmentData['isPrimary']
        );
    }
}