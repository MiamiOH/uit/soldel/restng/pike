<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\ThematicSequence;
use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;
use PHPUnit\Framework\TestCase;

class ThematicSequenceTest extends TestCase
{
    /**
     * @var array
     */
    private $thematicSequenceDate;
    /**
     * @var ThematicSequence
     */
    private $thematicSequence;

    public function testGetCode()
    {
        $this->assertEquals(
            $this->thematicSequenceDate['code'],
            $this->thematicSequence->getCode()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->thematicSequenceDate['description'],
            $this->thematicSequence->getDescription()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->thematicSequenceDate = [
            'code' => new ThematicSequenceCode('KDJS'),
            'description' => 'adsfa'
        ];

        $this->thematicSequence = new ThematicSequence(
            $this->thematicSequenceDate['code'],
            $this->thematicSequenceDate['description']
        );
    }
}//end class
