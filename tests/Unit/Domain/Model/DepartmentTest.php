<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\Department;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use PHPUnit\Framework\TestCase;

class DepartmentTest extends TestCase
{
    /**
     * @var array
     */
    private $departmentDate;
    /**
     * @var Department
     */
    private $department;

    public function testGetCode()
    {
        $this->assertEquals(
            $this->departmentDate['code'],
            $this->department->getCode()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->departmentDate['description'],
            $this->department->getDescription()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->departmentDate = [
            'code' => new DepartmentCode('SKDL'),
            'description' => 'adsfa'
        ];

        $this->department = new Department(
            $this->departmentDate['code'],
            $this->departmentDate['description']
        );
    }
}//end class
