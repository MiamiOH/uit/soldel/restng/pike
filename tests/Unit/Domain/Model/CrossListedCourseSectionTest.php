<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use MiamiOH\Pike\Domain\Model\CrossListedCourseSection;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use PHPUnit\Framework\TestCase;

class CrossListedCourseSectionTest extends TestCase
{
    /**
     * @var array
     */
    private $crossListedCourseSectionData;
    /**
     * @var CrossListedCourseSection
     */
    private $crossListedCourseSection;

    public function testGetCrossListedCourseSectionGuid()
    {
        $this->assertEquals(
            $this->crossListedCourseSectionData['courseSectionGuid'],
            $this->crossListedCourseSection->getCourseSectionGuid()
        );
    }

    public function testGetHostCrossListedCouseSectionGuid()
    {
        $this->assertEquals(
            $this->crossListedCourseSectionData['hostCourseSectionGuid'],
            $this->crossListedCourseSection->getHostCourseSectionGuid()
        );
    }

    public function testGetCrn()
    {
        $this->assertEquals(
            $this->crossListedCourseSectionData['crn'],
            $this->crossListedCourseSection->getCrn()
        );
    }

    public function testGetSubjectCode()
    {
        $this->assertEquals(
            $this->crossListedCourseSectionData['subjectCode'],
            $this->crossListedCourseSection->getSubjectCode()
        );
    }

    public function testGetCourseNumber()
    {
        $this->assertEquals(
            $this->crossListedCourseSectionData['courseNumber'],
            $this->crossListedCourseSection->getCourseNumber()
        );
    }

    public function testSectionName()
    {
        $this->assertEquals(
            'ABC 123 A',
            $this->crossListedCourseSection->getSectionName()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->crossListedCourseSectionData = [
            'crn' => new Crn('12345'),
            'subjectCode' => new SubjectCode('ABC'),
            'sectionCode' => 'A',
            'courseNumber' => new CourseNumber('123'),
            'courseSectionGuid' => CourseSectionGuid::create(),
            'hostCourseSectionGuid' => CourseSectionGuid::create(),
        ];

        $this->crossListedCourseSection = new crossListedCourseSection(
            $this->crossListedCourseSectionData['crn'],
            $this->crossListedCourseSectionData['subjectCode'],
            $this->crossListedCourseSectionData['sectionCode'],
            $this->crossListedCourseSectionData['courseNumber'],
            $this->crossListedCourseSectionData['courseSectionGuid'],
            $this->crossListedCourseSectionData['hostCourseSectionGuid']
        );
    }
}//end class
