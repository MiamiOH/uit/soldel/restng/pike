<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentStatus;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use PHPUnit\Framework\TestCase;

class CourseSectionEnrollmentStatusTest extends TestCase
{
    /**
     * @var array
     */
    private $courseSectionEnrollmentStatusDate;
    /**
     * @var CourseSectionEnrollmentStatus
     */
    private $courseSectionEnrollmentStatus;

    public function testGetCode()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentStatusDate['code'],
            $this->courseSectionEnrollmentStatus->getCode()
        );
    }

    public function testGetDescription()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentStatusDate['description'],
            $this->courseSectionEnrollmentStatus->getDescription()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionEnrollmentStatusDate = [
            'code' => new CourseSectionEnrollmentStatusCode('AU'),
            'description' => 'adsfa'
        ];

        $this->courseSectionEnrollmentStatus = new CourseSectionEnrollmentStatus(
            $this->courseSectionEnrollmentStatusDate['code'],
            $this->courseSectionEnrollmentStatusDate['description']
        );
    }

}//end class
