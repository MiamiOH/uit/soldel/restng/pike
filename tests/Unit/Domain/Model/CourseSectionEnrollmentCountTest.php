<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;


use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentCount;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use PHPUnit\Framework\TestCase;

class CourseSectionEnrollmentCountTest extends TestCase
{
    /**
     * @var array
     */
    private $courseSectionEnrollmentCountDate;
    /**
     * @var CourseSectionEnrollmentCount
     */
    private $courseSectionEnrollmentCount;

    public function testGetCourseSectionGuid()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentCountDate['courseSectionGuid'],
            $this->courseSectionEnrollmentCount->getCourseSectionGuid()
        );
    }

    public function testGetNumOfMaxEnrollments()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentCountDate['numOfMaxEnrollments'],
            $this->courseSectionEnrollmentCount->getNumOfMaxEnrollments()
        );
    }

    public function testGetNumOfCurrentEnrollments()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentCountDate['numOfCurrentEnrollments'],
            $this->courseSectionEnrollmentCount->getNumOfCurrentEnrollments()
        );
    }

    public function testGetNumOfActiveEnrollments()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentCountDate['numOfActiveEnrollments'],
            $this->courseSectionEnrollmentCount->getNumOfActiveEnrollments()
        );
    }

    public function testGetNumOfAvailableEnrollments()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentCountDate['numOfAvailableEnrollments'],
            $this->courseSectionEnrollmentCount->getNumOfAvailableEnrollments()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionEnrollmentCountDate = [
            'courseSectionGuid' => CourseSectionGuid::create(),
            'numOfMaxEnrollments' => 4,
            'numOfCurrentEnrollments' => 4,
            'numOfActiveEnrollments' => 3,
            'numOfAvailableEnrollments' => 1
        ];

        $this->courseSectionEnrollmentCount = new CourseSectionEnrollmentCount(
            $this->courseSectionEnrollmentCountDate['courseSectionGuid'],
            $this->courseSectionEnrollmentCountDate['numOfMaxEnrollments'],
            $this->courseSectionEnrollmentCountDate['numOfCurrentEnrollments'],
            $this->courseSectionEnrollmentCountDate['numOfActiveEnrollments'],
            $this->courseSectionEnrollmentCountDate['numOfAvailableEnrollments']
        );
    }

}//end class
