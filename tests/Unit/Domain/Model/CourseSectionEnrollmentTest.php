<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 10:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentStatus;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CourseSectionEnrollmentTest extends TestCase
{
    /**
     * @var array
     */
    private $courseSectionEnrollmentDate;
    /**
     * @var CourseSectionEnrollment
     */
    private $courseSectionEnrollment;
    /**
     * @var MockObject
     */
    private $courseSectionEnrollmentStatus;
    /**
     * @var MockObject
     */
    private $midtermGrade;
    /**
     * @var MockObject
     */
    private $finalGrade;

    public function testHasAttended()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['hasAttended'],
            $this->courseSectionEnrollment->hasAttended()
        );
    }

    public function testLastAttendDate()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['lastAttendDate'],
            $this->courseSectionEnrollment->lastAttendDate()
        );
    }

    public function testIsEnrollmentActive()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['isEnrollmentActive'],
            $this->courseSectionEnrollment->isEnrollmentActive()
        );
    }

    public function testIsGradeSubmissionEligible()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['isGradeSubmissionEligible'],
            $this->courseSectionEnrollment->isGradeSubmissionEligible()
        );
    }

    public function testGetGradeSubmissionEligibleComment()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['gradeSubmissionEligibleComment'],
            $this->courseSectionEnrollment->getGradeSubmissionEligibleComment()
        );
    }

    public function testIsFinalGradeRequired()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['isFinalGradeRequired'],
            $this->courseSectionEnrollment->isFinalGradeRequired()
        );
    }

    public function testIsMidtermGradeRequired()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['isMidtermGradeRequired'],
            $this->courseSectionEnrollment->isMidtermGradeRequired()
        );
    }

    public function testGetStatusCode()
    {
        $code = new CourseSectionEnrollmentStatusCode('AS');
        $this->courseSectionEnrollmentStatus->method('getCode')->willReturn($code);
        $this->assertEquals(
            $code,
            $this->courseSectionEnrollment->getStatusCode()
        );
    }

    public function testGetStatusDescription()
    {
        $this->courseSectionEnrollmentStatus->method('getDescription')->willReturn('aaa');
        $this->assertEquals(
            'aaa',
            $this->courseSectionEnrollment->getStatusDescription()
        );
    }

    public function testGetCourseSectionGuid()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['courseSectionGuid'],
            $this->courseSectionEnrollment->getCourseSectionGuid()
        );
    }

    public function testGetStudentLevelCode()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['studentLevelCode'],
            $this->courseSectionEnrollment->getStudentLevelCode()
        );
    }

    public function testGetUniqueId()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['uniqueId'],
            $this->courseSectionEnrollment->getUniqueId()
        );
    }

    public function testGetCreditHours()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['creditHours'],
            $this->courseSectionEnrollment->getCreditHours()
        );
    }

    public function testGetTermCode()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['termCode'],
            $this->courseSectionEnrollment->getTermCode()
        );
    }

    public function testGetGrade()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['grades'],
            $this->courseSectionEnrollment->getGrades()
        );
    }

    public function testGradeModeCode()
    {
        $this->assertEquals(
            $this->courseSectionEnrollmentDate['gradeModeCode'],
            $this->courseSectionEnrollment->getGradeModeCode()
        );
    }

    protected function setUp() :void
    {
        parent::setUp();

        $this->courseSectionEnrollmentStatus = $this->createMock(CourseSectionEnrollmentStatus::class);
        $this->midtermGrade = $this->createMock(Grade::class);
        $this->finalGrade = $this->createMock(Grade::class);

        $this->courseSectionEnrollmentDate = [
            'hasAttended' => true,
            'lastAttendDate' => Carbon::parse('2020-01-15'),
            'isEnrollmentActive' => false,
            'isGradeSubmissionEligible' => true,
            'gradeSubmissionEligibleComment' => 'asdf',
            'isFinalGradeRequired' => true,
            'isMidtermGradeRequired' => false,
            'grades' => new GradeCollection([
                $this->midtermGrade,
                $this->finalGrade
            ]),
            'uniqueId' => new UniqueId('liaom'),
            'status' => $this->courseSectionEnrollmentStatus,
            'courseSectionGuid' => CourseSectionGuid::create(),
            'creditHours' => 3,
            'studentLevelCode' => new StudentLevelCode('UG'),
            'termCode' => new TermCode('201710'),
            'gradeModeCode' => new GradeModeCode('S')
        ];

        $this->courseSectionEnrollment = new CourseSectionEnrollment(
            $this->courseSectionEnrollmentDate['hasAttended'],
            $this->courseSectionEnrollmentDate['isEnrollmentActive'],
            $this->courseSectionEnrollmentDate['isGradeSubmissionEligible'],
            $this->courseSectionEnrollmentDate['gradeSubmissionEligibleComment'],
            $this->courseSectionEnrollmentDate['isFinalGradeRequired'],
            $this->courseSectionEnrollmentDate['isMidtermGradeRequired'],
            $this->courseSectionEnrollmentDate['grades'],
            $this->courseSectionEnrollmentDate['uniqueId'],
            $this->courseSectionEnrollmentDate['status'],
            $this->courseSectionEnrollmentDate['courseSectionGuid'],
            $this->courseSectionEnrollmentDate['creditHours'],
            $this->courseSectionEnrollmentDate['studentLevelCode'],
            $this->courseSectionEnrollmentDate['termCode'],
            $this->courseSectionEnrollmentDate['gradeModeCode'],
            $this->courseSectionEnrollmentDate['lastAttendDate']
        );
    }
}//end class
