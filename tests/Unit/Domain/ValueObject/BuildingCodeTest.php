<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Exception\InvalidBuildingCodeException;

class BuildingCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = BuildingCode::class;
    protected $expectedException = InvalidBuildingCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('ABC');
        $this->assertPass('ABCDEF');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('ABCDEFGHI');
    }
}