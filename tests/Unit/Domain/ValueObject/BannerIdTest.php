<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Exception\InvalidBannerIdException;

class BannerIdTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = BannerId::class;
    protected $expectedException = InvalidBannerIdException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('+01422364');
        $this->assertPass('+014283932');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('+01234');
        $this->assertFail('01422364');
        $this->assertFail('liaom');
        $this->assertFail('1440394');
    }
}