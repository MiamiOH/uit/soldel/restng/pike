<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:31 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Exception\InvalidCourseNumberException;

class CourseNumberTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = CourseNumber::class;
    protected $expectedException = InvalidCourseNumberException::class;


    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('174');
        $this->assertPass('201e');
        $this->assertPass('5090');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('&$*');
        $this->assertFail('');
        $this->assertFail('1 74');
        $this->assertFail('1');
        $this->assertFail('3829378');
        $this->assertFail('900');
    }
}