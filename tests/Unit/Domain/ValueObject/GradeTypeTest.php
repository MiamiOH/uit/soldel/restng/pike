<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Exception\InvalidGradeTypeException;

class GradeTypeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = GradeType::class;
    protected $expectedException = InvalidGradeTypeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('midterm');
        $this->assertPass('final');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('&$*');
    }

    public function testIsMidterm()
    {
        $this->assertEquals(
            true,
            GradeType::ofMidterm()->isMidterm()
        );
    }

    public function testIsFinal()
    {
        $this->assertEquals(
            true,
            GradeType::ofFinal()->isFinal()
        );
    }
}