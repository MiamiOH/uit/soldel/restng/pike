<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Exception\InvalidPidmException;

class PidmTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = Pidm::class;
    protected $expectedException = InvalidPidmException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('13849');
        $this->assertPass('1728374');
        $this->assertPass('17283748');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('AAAAA');
        $this->assertFail('$D%');
        $this->assertFail('');
        $this->assertFail('1728');
        $this->assertFail('273847382');
    }
}