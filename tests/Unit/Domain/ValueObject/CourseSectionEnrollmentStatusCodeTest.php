<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use MiamiOH\Pike\Exception\InvalidCourseSectionEnrollmentStatusCodeException;

class CourseSectionEnrollmentStatusCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = CourseSectionEnrollmentStatusCode::class;
    protected $expectedException = InvalidCourseSectionEnrollmentStatusCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('rE');
        $this->assertPass('45');
        $this->assertPass('E3');
        $this->assertPass('3R');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('AAA');
        $this->assertFail('$%');
        $this->assertFail(' A');
        $this->assertFail('');
    }
}