<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Exception\InvalidInstructionalTypeCode;

class InstructionalTypeCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = InstructionalTypeCode::class;
    protected $expectedException = InvalidInstructionalTypeCode::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('A');
        $this->assertPass('B');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('');
        $this->assertFail('AAAA');
        $this->assertFail('23');
        $this->assertFail('&*');
    }
}