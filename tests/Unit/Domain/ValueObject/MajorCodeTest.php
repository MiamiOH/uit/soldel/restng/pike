<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\MajorCode;
use MiamiOH\Pike\Exception\InvalidMajorCodeException;

class MajorCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = MajorCode::class;
    protected $expectedException = InvalidMajorCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('aaa');
        $this->assertPass('AA45');
        $this->assertPass('0000');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('AAAAA');
        $this->assertFail('$D%');
        $this->assertFail('');
    }
}