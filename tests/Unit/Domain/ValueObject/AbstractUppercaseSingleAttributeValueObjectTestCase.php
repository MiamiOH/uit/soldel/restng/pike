<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:21 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

abstract class AbstractUppercaseSingleAttributeValueObjectTestCase extends AbstractSingleAttributeValueObjectTestCase
{
    public function assertPass(string $value)
    {
        $obj = new $this->className(strtolower($value));
        $this->assertEquals(strtoupper($value), $obj->getValue());
        $this->assertEquals(strtoupper($value), (string)$obj);
    }

}