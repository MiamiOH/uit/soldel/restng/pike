<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:21 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

abstract class AbstractSingleIntegerValueObjectTestCase extends TestCase
{
    protected $className = null;
    protected $expectedException = null;

    public function assertPass(int $value)
    {
        $obj = new $this->className($value);
        $this->assertSame($value, $obj->getValue());
        $this->assertSame((string)$value, (string)$obj);
    }

    public function assertFail(int $value)
    {
        if ($this->expectedException === null) {
            $this->expectException(\Exception::class);
        } else {
            $this->expectException($this->expectedException);
        }

        new $this->className($value);
    }

    abstract public function testCreateValueObjectWithValidValue();

    abstract public function testValueObjectShouldNotAcceptInvalidValue();

    protected function setUp() :void
    {
        parent::setUp();
        if ($this->className === null) {
            throw new InvalidArgumentException('Class variable className must be set');
        }
    }
}