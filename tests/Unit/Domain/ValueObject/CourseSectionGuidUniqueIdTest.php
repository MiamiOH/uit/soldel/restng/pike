<?php
/**
 * Author: liaom
 * Date: 6/15/18
 * Time: 11:22 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuidUniqueId;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Tests\Unit\TestCase;

class CourseSectionGuidUniqueIdTest extends TestCase
{
    /**
     * @var CourseSectionGuid
     */
    private $courseSectionGuid;
    /**
     * @var UniqueId
     */
    private $uniqueId;
    /**
     * @var CourseSectionGuidUniqueId
     */
    private $courseSectionGuidUniqueId;

    protected function setUp() :void
    {
        parent::setUp();
        $this->courseSectionGuid = CourseSectionGuid::create();
        $this->uniqueId = new UniqueId('liaom');
        $this->courseSectionGuidUniqueId = new CourseSectionGuidUniqueId(
            $this->courseSectionGuid,
            $this->uniqueId
        );
    }

    public function testGetCourseSectionGuid()
    {
        $this->assertEquals(
            $this->courseSectionGuid,
            $this->courseSectionGuidUniqueId->getCourseSectionGuid()
        );
    }

    public function testGetUniqueId()
    {
        $this->assertEquals(
            $this->uniqueId,
            $this->courseSectionGuidUniqueId->getUniqueId()
        );
    }
}