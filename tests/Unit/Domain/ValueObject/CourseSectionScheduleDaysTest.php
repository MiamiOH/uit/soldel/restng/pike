<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Exception\InvalidCourseSectionScheduleDaysException;

class CourseSectionScheduleDaysTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = CourseSectionScheduleDays::class;
    protected $expectedException = InvalidCourseSectionScheduleDaysException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('M');
        $this->assertPass('T');
        $this->assertPass('W');
        $this->assertPass('R');
        $this->assertPass('F');
        $this->assertPass('S');
        $this->assertPass('U');
        $this->assertPass('TR');
        $this->assertPass('MWF');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('43');
    }

    public function testHasMonday()
    {
        $days = new CourseSectionScheduleDays('M');
        $this->assertTrue($days->hasMonday());
        $this->assertFalse($days->hasTuesday());
        $this->assertFalse($days->hasWednesday());
        $this->assertFalse($days->hasThursday());
        $this->assertFalse($days->hasFriday());
        $this->assertFalse($days->hasSaturday());
        $this->assertFalse($days->hasSunday());

    }

    public function testHasTuesday()
    {
        $days = new CourseSectionScheduleDays('T');
        $this->assertFalse($days->hasMonday());
        $this->assertTrue($days->hasTuesday());
        $this->assertFalse($days->hasWednesday());
        $this->assertFalse($days->hasThursday());
        $this->assertFalse($days->hasFriday());
        $this->assertFalse($days->hasSaturday());
        $this->assertFalse($days->hasSunday());

    }

    public function testHasWednesday()
    {
        $days = new CourseSectionScheduleDays('W');
        $this->assertFalse($days->hasMonday());
        $this->assertFalse($days->hasTuesday());
        $this->assertTrue($days->hasWednesday());
        $this->assertFalse($days->hasThursday());
        $this->assertFalse($days->hasFriday());
        $this->assertFalse($days->hasSaturday());
        $this->assertFalse($days->hasSunday());

    }

    public function testHasThursday()
    {
        $days = new CourseSectionScheduleDays('R');
        $this->assertFalse($days->hasMonday());
        $this->assertFalse($days->hasTuesday());
        $this->assertFalse($days->hasWednesday());
        $this->assertTrue($days->hasThursday());
        $this->assertFalse($days->hasFriday());
        $this->assertFalse($days->hasSaturday());
        $this->assertFalse($days->hasSunday());

    }

    public function testHasFriday()
    {
        $days = new CourseSectionScheduleDays('F');
        $this->assertFalse($days->hasMonday());
        $this->assertFalse($days->hasTuesday());
        $this->assertFalse($days->hasWednesday());
        $this->assertFalse($days->hasThursday());
        $this->assertTrue($days->hasFriday());
        $this->assertFalse($days->hasSaturday());
        $this->assertFalse($days->hasSunday());

    }

    public function testHasSaturday()
    {
        $days = new CourseSectionScheduleDays('S');
        $this->assertFalse($days->hasMonday());
        $this->assertFalse($days->hasTuesday());
        $this->assertFalse($days->hasWednesday());
        $this->assertFalse($days->hasThursday());
        $this->assertFalse($days->hasFriday());
        $this->assertTrue($days->hasSaturday());
        $this->assertFalse($days->hasSunday());

    }

    public function testHasSunday()
    {
        $days = new CourseSectionScheduleDays('U');
        $this->assertFalse($days->hasMonday());
        $this->assertFalse($days->hasTuesday());
        $this->assertFalse($days->hasWednesday());
        $this->assertFalse($days->hasThursday());
        $this->assertFalse($days->hasFriday());
        $this->assertFalse($days->hasSaturday());
        $this->assertTrue($days->hasSunday());

    }
}