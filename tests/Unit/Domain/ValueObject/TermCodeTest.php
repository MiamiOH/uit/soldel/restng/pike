<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Exception\InvalidTermCodeException;

class TermCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = TermCode::class;
    protected $expectedException = InvalidTermCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('201810');
        $this->assertPass('201815');
        $this->assertPass('201820');
        $this->assertPass('201830');
        $this->assertPass('999999');
        $this->assertPass('000000');
        $this->assertPass('ar2000');
        $this->assertPass('PB2000');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('201840');
        $this->assertFail('AAAA');
        $this->assertFail('2018');
        $this->assertFail('');
        $this->assertFail('&$JK');
    }
}