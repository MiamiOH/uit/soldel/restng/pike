<?php
/**
 * Author: liaom
 * Date: 6/15/18
 * Time: 11:22 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\TermCodeCrn;
use MiamiOH\Pike\Tests\Unit\TestCase;

class TermCodeCrnTest extends TestCase
{
    /**
     * @var TermCode
     */
    private $termCode;
    /**
     * @var Crn
     */
    private $crn;
    /**
     * @var TermCodeCrn
     */
    private $termCodeCrn;

    protected function setUp() :void
    {
        parent::setUp();
        $this->termCode = new TermCode('201710');
        $this->crn = new Crn('12345');
        $this->termCodeCrn = new TermCodeCrn(
            $this->termCode,
            $this->crn
        );
    }

    public function testGetTermCode()
    {
        $this->assertEquals(
            $this->termCode,
            $this->termCodeCrn->getTermCode()
        );
    }

    public function testGetCrn()
    {
        $this->assertEquals(
            $this->crn,
            $this->termCodeCrn->getCrn()
        );
    }
}