<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Exception\InvalidOffsetException;

class OffsetTest extends AbstractSingleIntegerValueObjectTestCase
{
    protected $className = Offset::class;
    protected $expectedException = InvalidOffSetException::class;

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail(0);
        $this->assertFail(-1);
    }

    public function testCreateValueObjectWithValidValue()
    {
        $offset = new Offset(2);
        $this->assertEquals(1, $offset->getValue());
    }
}