<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;
use MiamiOH\Pike\Exception\InvalidCourseSectionScheduleTypeCodeException;

class CourseSectionScheduleTypeCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = CourseSectionScheduleTypeCode::class;
    protected $expectedException = InvalidCourseSectionScheduleTypeCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('AAAA');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('AA');
        $this->assertFail('AAA');
        $this->assertFail('AAAAA');
    }
}