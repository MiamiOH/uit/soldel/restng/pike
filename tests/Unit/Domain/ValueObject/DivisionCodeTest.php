<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:31 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Exception\InvalidDivisionCodeException;

class DivisionCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = DivisionCode::class;
    protected $expectedException = InvalidDivisionCodeException::class;


    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('CAS');
        $this->assertPass('CEC');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('&$*');
        $this->assertFail('');
        $this->assertFail('asdfasdfasdfasdfas');
    }
}