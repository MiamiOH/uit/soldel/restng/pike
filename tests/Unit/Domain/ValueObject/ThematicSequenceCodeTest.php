<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;
use MiamiOH\Pike\Exception\InvalidThematicSequenceCodeException;

class ThematicSequenceCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = ThematicSequenceCode::class;
    protected $expectedException = InvalidThematicSequenceCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('xcon');
        $this->assertPass('KDJ3');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('AAAAA');
        $this->assertFail('$D%');
        $this->assertFail('');
        $this->assertFail('AE6');
    }

}