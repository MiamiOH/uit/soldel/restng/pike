<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Exception\InvalidCourseSectionStatusCodeException;

class CourseSectionStatusCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = CourseSectionStatusCode::class;
    protected $expectedException = InvalidCourseSectionStatusCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('A');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('DS$');
        $this->assertFail('AA');

    }
}