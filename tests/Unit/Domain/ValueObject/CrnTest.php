<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Exception\InvalidCrnException;

class CrnTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = Crn::class;
    protected $expectedException = InvalidCrnException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('17283');
        $this->assertPass('172836');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('');
        $this->assertFail('skdlf');
        $this->assertFail('273');
        $this->assertFail('382947382');
        $this->assertFail('&$*');
    }
}