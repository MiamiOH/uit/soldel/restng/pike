<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\InvalidUniqueIdException;

class UniqueIdTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = UniqueId::class;
    protected $expectedException = InvalidUniqueIdException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('LIAOM');
        $this->assertPass('xiaw');
        $this->assertPass('skdlf3');
        $this->assertPass('sic');
        $this->assertPass('zhang234');
    }

    public function assertPass(string $value)
    {
        $obj = new $this->className($value);
        $this->assertEquals(strtolower($value), $obj->getValue());
        $this->assertEquals(strtolower($value), (string)$obj);
        $this->assertEquals(strtoupper($value), $obj->getUppercaseValue());
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
//        $this->assertFail('as');
//        $this->assertFail('djskd3*');
//        $this->assertFail('32kslfj');
//        $this->assertFail('aaaaaaaaa');
//        $this->assertFail('');
        $this->assertTrue(true);
    }
}