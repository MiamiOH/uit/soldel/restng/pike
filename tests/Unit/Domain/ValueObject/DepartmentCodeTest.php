<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Exception\InvalidDepartmentCodeException;

class DepartmentCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = DepartmentCode::class;
    protected $expectedException = InvalidDepartmentCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('aaa');
        $this->assertPass('AAAA');
        $this->assertPass('0000');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('AAAAAAA');
        $this->assertFail('$D%');
        $this->assertFail('');
    }
}