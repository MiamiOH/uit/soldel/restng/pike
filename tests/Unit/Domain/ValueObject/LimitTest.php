<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Exception\InvalidLimitException;

class LimitTest extends AbstractSingleIntegerValueObjectTestCase
{
    protected $className = Limit::class;
    protected $expectedException = InvalidLimitException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass(1);
        $this->assertPass(2);
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail(0);
        $this->assertFail(-1);
        $this->assertFail(201);

    }
}