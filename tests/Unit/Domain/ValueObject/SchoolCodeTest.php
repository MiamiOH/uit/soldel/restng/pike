<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\SchoolCode;
use MiamiOH\Pike\Exception\InvalidSchoolCodeException;

class SchoolCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = SchoolCode::class;
    protected $expectedException = InvalidSchoolCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('OS');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('AAAA');
        $this->assertFail('&*');
    }
}