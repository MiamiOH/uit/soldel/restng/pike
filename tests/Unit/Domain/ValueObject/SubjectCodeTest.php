<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Exception\InvalidSubjectCodeException;

class SubjectCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = SubjectCode::class;
    protected $expectedException = InvalidSubjectCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('AAA');
        $this->assertPass('aa');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('AAAA');
        $this->assertFail('$D%');
        $this->assertFail('');
        $this->assertFail('AE6');
    }
}