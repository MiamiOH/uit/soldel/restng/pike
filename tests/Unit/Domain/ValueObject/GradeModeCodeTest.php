<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Exception\InvalidGradeModeCodeException;

class GradeModeCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = GradeModeCode::class;
    protected $expectedException = InvalidGradeModeCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('A');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('AB');

    }
}