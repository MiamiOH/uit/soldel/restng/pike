<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Exception\InvalidGradeValueException;

class GradeValueTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = GradeValue::class;
    protected $expectedException = InvalidGradeValueException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('A');
        $this->assertPass('123456');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('1234567');
    }
}