<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;
use MiamiOH\Pike\Exception\InvalidPartOfTermCodeException;

class PartOfTermCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = PartOfTermCode::class;
    protected $expectedException = InvalidPartOfTermCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('ABC');
        $this->assertPass('AB');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('ABCD');
    }
}