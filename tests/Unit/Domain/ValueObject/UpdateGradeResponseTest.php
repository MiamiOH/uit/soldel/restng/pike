<?php
/**
 * Author: liaom
 * Date: 6/15/18
 * Time: 11:22 AM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\UpdateGradeResponse;
use MiamiOH\Pike\Tests\Unit\TestCase;

class UpdateGradeResponseTest extends TestCase
{
    private $requestIndex;
    private $isSuccessful;
    private $message;
    /**
     * @var UpdateGradeResponse
     */
    private $response;

    protected function setUp() :void
    {
        parent::setUp();
        $this->requestIndex = 3;
        $this->isSuccessful = true;
        $this->message = 'some message';
        $this->response = new UpdateGradeResponse(
            $this->requestIndex,
            $this->isSuccessful,
            $this->message
        );
    }

    public function testGetRequestIndex()
    {
        $this->assertEquals(
            $this->requestIndex,
            $this->response->getRequestIndex()
        );
    }

    public function testIsSuccessful()
    {
        $this->assertEquals(
            $this->isSuccessful,
            $this->response->isSuccessful()
        );
    }

    public function testGetMessage()
    {
        $this->assertEquals(
            $this->message,
            $this->response->getMessage()
        );
    }
}