<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:41 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;

use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Exception\InvalidStudentLevelCodeException;

class StudentLevelCodeTest extends AbstractUppercaseSingleAttributeValueObjectTestCase
{
    protected $className = StudentLevelCode::class;
    protected $expectedException = InvalidStudentLevelCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('CR');
        $this->assertPass('gr');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('A');
        $this->assertFail('AAA');
        $this->assertFail('$D%');
        $this->assertFail('');
        $this->assertFail('00');
    }
}