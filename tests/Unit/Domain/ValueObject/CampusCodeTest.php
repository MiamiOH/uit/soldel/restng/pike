<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Exception\InvalidCampusCodeException;

class CampusCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = CampusCode::class;
    protected $expectedException = InvalidCampusCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('O');
        $this->assertPass('H');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('');
        $this->assertFail('AAAA');
        $this->assertFail('23');
        $this->assertFail('&*');
    }
}