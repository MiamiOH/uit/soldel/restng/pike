<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:45 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\ValueObject;


use MiamiOH\Pike\Domain\ValueObject\CourseSectionAttributeCode;
use MiamiOH\Pike\Exception\InvalidCourseSectionAttributeCodeException;

class CourseSectionAttributeCodeTest extends AbstractSingleAttributeValueObjectTestCase
{
    protected $className = CourseSectionAttributeCode::class;
    protected $expectedException = InvalidCourseSectionAttributeCodeException::class;

    public function testCreateValueObjectWithValidValue()
    {
        $this->assertPass('IIA');
        $this->assertPass('AAAA');
    }

    public function testValueObjectShouldNotAcceptInvalidValue()
    {
        $this->assertFail('asdfsadf');
        $this->assertFail('$RW');
    }
}