<?php
/**
 * Author: liaom
 * Date: 6/12/18
 * Time: 3:54 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Request;


use Carbon\Carbon;
use MiamiOH\Pike\Domain\Request\UpdateGradeRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Tests\Unit\TestCase;

class UpdateGradeRequestTest extends TestCase
{
    private $goodCourseSectionGuid;
    private $badCourseSectionGuid;
    private $goodStudentUniqueId;
    private $badStudentUniqueId;
    private $goodGradeType;
    private $badGradeType;
    private $goodGradeValue;
    private $badGradeValue;
    private $goodInstructorUniqueId;
    private $badInstructorUniqueId;
    private $hasAttended;
    private $lastAttendDate;

    protected function setUp() :void
    {
        parent::setUp();
        $this->goodCourseSectionGuid = '954b6b70-a876-4a72-ac79-6b481fc22778';
        $this->badCourseSectionGuid = 'asfdasdf';
        $this->goodStudentUniqueId = 'liaom';
        $this->badStudentUniqueId = 'asdlkjlKL$$KJDjlskdjflksdjlf';
        $this->goodGradeType = 'final';
        $this->badGradeType = 'asdf';
        $this->goodGradeValue = 'A';
        $this->badGradeValue = 'ZKDLFDD';
        $this->goodInstructorUniqueId = 'liaom';
        $this->badInstructorUniqueId = 'asdfajdsljfladksjflaksdjf';
        $this->hasAttended = true;
        $this->lastAttendDate = Carbon::create(2018, 1, 1, 1, 1, 1);
    }

    public function testGoodRequest()
    {
        $request = new UpdateGradeRequest(
            $this->goodCourseSectionGuid,
            $this->goodStudentUniqueId,
            $this->goodGradeType,
            $this->goodGradeValue,
            $this->goodInstructorUniqueId,
            $this->hasAttended,
            $this->lastAttendDate
        );

        $this->assertEquals(new CourseSectionGuid($this->goodCourseSectionGuid),
            $request->getCourseSectionGuid());
        $this->assertEquals(new UniqueId($this->goodStudentUniqueId),
            $request->getStudentUniqueId());
        $this->assertEquals(GradeType::ofFinal(), $request->getGradeType());
        $this->assertEquals(new GradeValue($this->goodGradeValue),
            $request->getGradeValue());
        $this->assertEquals(new UniqueId($this->goodInstructorUniqueId),
            $request->getInstructorUniqueId());
        $this->assertEquals($this->hasAttended, $request->getHasAttended());
        $this->assertEquals($this->lastAttendDate, $request->getLastAttendDate());
    }

    public function testGoodRequestWithoutHasAttended()
    {
        $request = new UpdateGradeRequest(
            $this->goodCourseSectionGuid,
            $this->goodStudentUniqueId,
            $this->goodGradeType,
            $this->goodGradeValue,
            $this->goodInstructorUniqueId,
            null,
            $this->lastAttendDate
        );

        $this->assertEquals(null, $request->getHasAttended());
    }

    public function testGoodRequestWithoutLastAttendDate()
    {
        $request = new UpdateGradeRequest(
            $this->goodCourseSectionGuid,
            $this->goodStudentUniqueId,
            $this->goodGradeType,
            $this->goodGradeValue,
            $this->goodInstructorUniqueId,
            null,
            null
        );

        $this->assertEquals(null, $request->getLastAttendDate());
    }

    public function testRequestHasInvalidCourseSectionGuid()
    {
        $this->expectException(InvalidArgumentException::class);

        new UpdateGradeRequest(
            $this->badCourseSectionGuid,
            $this->goodStudentUniqueId,
            $this->goodGradeType,
            $this->goodGradeValue,
            $this->goodInstructorUniqueId,
            $this->hasAttended,
            $this->lastAttendDate
        );
    }

//    public function testRequestHasInvalidStudentUniqueId()
//    {
//        $this->expectException(InvalidArgumentException::class);
//
//        new UpdateGradeRequest(
//            $this->goodCourseSectionGuid,
//            $this->badStudentUniqueId,
//            $this->goodGradeType,
//            $this->goodGradeValue,
//            $this->goodInstructorUniqueId,
//            $this->hasAttended,
//            $this->lastAttendDate
//        );
//    }

    public function testRequestHasInvalidGradeType()
    {
        $this->expectException(InvalidArgumentException::class);

        new UpdateGradeRequest(
            $this->goodCourseSectionGuid,
            $this->goodStudentUniqueId,
            $this->badGradeType,
            $this->goodGradeValue,
            $this->goodInstructorUniqueId,
            $this->hasAttended,
            $this->lastAttendDate
        );
    }

    public function testRequestHasInvalidGradeValue()
    {
        $this->expectException(InvalidArgumentException::class);

        new UpdateGradeRequest(
            $this->goodCourseSectionGuid,
            $this->goodStudentUniqueId,
            $this->goodGradeType,
            $this->badGradeValue,
            $this->goodInstructorUniqueId,
            $this->hasAttended,
            $this->lastAttendDate
        );
    }

//    public function testRequestHasInvalidInstructorUniqueId()
//    {
//        $this->expectException(InvalidArgumentException::class);
//
//        new UpdateGradeRequest(
//            $this->goodCourseSectionGuid,
//            $this->goodStudentUniqueId,
//            $this->goodGradeType,
//            $this->goodGradeValue,
//            $this->badInstructorUniqueId,
//            $this->hasAttended,
//            $this->lastAttendDate
//        );
//    }
}