<?php
/**
 * Author: liaom
 * Date: 6/12/18
 * Time: 4:22 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Request;


use Carbon\Carbon;
use MiamiOH\Pike\Domain\Collection\BuildingCodeCollection;
use MiamiOH\Pike\Domain\Collection\CampusCodeCollection;
use MiamiOH\Pike\Domain\Collection\CourseNumberCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionStatusCodeCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\PartOfTermCodeCollection;
use MiamiOH\Pike\Domain\Collection\SubjectCodeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Tests\Unit\TestCase;

class SearchCourseSectionRequestTest extends TestCase
{
    private $crns;
    private $crnCollection;
    private $subjectCodes;
    private $subjectCodeCollection;
    private $courseNumbers;
    private $courseNumberCollection;
    private $courseSectionCodes;
    private $courseSectionStatusCodes;
    private $courseSectionStatusCollection;
    private $numOfMaxEnrollments;

    protected function setUp() :void
    {
        parent::setUp();
        $this->crns = ['12345', '23456'];
        $this->crnCollection = CrnCollection::createFromValueArray($this->crns);
        $this->subjectCodes = ['CSE', 'ACC'];
        $this->subjectCodeCollection = SubjectCodeCollection::createFromValueArray($this->subjectCodes);
        $this->courseNumbers = ['123', '445C'];
        $this->courseNumberCollection = CourseNumberCollection::createFromValueArray($this->courseNumbers);
        $this->courseSectionCodes = ['A', 'H A'];
        $this->courseSectionStatusCodes = ['A', 'I'];
        $this->courseSectionStatusCollection = CourseSectionStatusCodeCollection::createFromValueArray($this->courseSectionStatusCodes);
        $this->numOfMaxEnrollments = 3;

    }

    public function testCrn()
    {
        $request = new SearchCourseSectionRequest();
        $request->setCrns($this->crns);
        $this->assertEquals($this->crnCollection, $request->getCrnCollection());
        $request->setCrns(null);
        $this->assertEquals(null, $request->getCrnCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setCrns(['asdlfkladskf']);
    }

    public function testSubjectCode()
    {
        $request = new SearchCourseSectionRequest();
        $request->setSubjectCodes($this->subjectCodes);
        $this->assertEquals($this->subjectCodeCollection,
            $request->getSubjectCodeCollection());
        $request->setSubjectCodes(null);
        $this->assertEquals(null, $request->getSubjectCodeCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setSubjectCodes(['kdlsjfkdslfjdklsf']);
    }

    public function testCourseNumber()
    {
        $request = new SearchCourseSectionRequest();
        $request->setCourseNumbers($this->courseNumbers);
        $this->assertEquals($this->courseNumberCollection,
            $request->getCourseNumberCollection());
        $request->setCourseNumbers(null);
        $this->assertEquals(null, $request->getCourseNumberCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setCourseNumbers(['asdfasdfasdfadsfadsf']);
    }

    public function testCourseSectionCodes()
    {
        $request = new SearchCourseSectionRequest();
        $request->setCourseSectionCodes($this->courseSectionCodes);
        $this->assertEquals($this->courseSectionCodes,
            $request->getCourseSectionCodes());
        $request->setCourseSectionCodes(null);
        $this->assertEquals(null, $request->getCourseSectionCodes());
    }

    public function testCourseSectionStatusCode()
    {
        $request = new SearchCourseSectionRequest();
        $request->setCourseSectionStatusCodes($this->courseSectionStatusCodes);
        $this->assertEquals($this->courseSectionStatusCollection,
            $request->getCourseSectionStatusCodeCollection());
        $request->setCourseSectionStatusCodes(null);
        $this->assertEquals(null, $request->getCourseSectionStatusCodeCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setCourseSectionStatusCodes(['asdfasdfasdf']);
    }

    public function testNumOfMaxEnrollments()
    {
        $request = new SearchCourseSectionRequest();
        $request->setNumOfMaxEnrollments($this->numOfMaxEnrollments,4);
        $this->assertEquals($this->numOfMaxEnrollments, $request->getNumOfMaxEnrollments());
        $this->assertEquals(4, $request->getNumOfMaxEnrollmentsCount());

        $request->setNumOfMaxEnrollments(null, null);
        $this->assertEquals(null, $request->getNumOfMaxEnrollments());
        $this->assertEquals(null, $request->getNumOfMaxEnrollmentsCount());
        try {
            $request->setNumOfMaxEnrollments(3, -3);
        }catch (InvalidArgumentException $e) {
            $this->assertEquals('Invalid numOfMaxEnrollments', $e->getMessage());
        }
    }

    public function testNumOfCurrentEnrollments()
    {
        $request = new SearchCourseSectionRequest();
        $request->setNumOfCurrentEnrollments(3, 3);
        $this->assertEquals(3, $request->getNumOfCurrentEnrollments());
        $this->assertEquals(3, $request->getNumOfCurrentEnrollmentsCount());

        $request->setNumOfCurrentEnrollments(null, null);
        $this->assertEquals(null, $request->getNumOfCurrentEnrollments());
        $this->assertEquals(null, $request->getNumOfCurrentEnrollmentsCount());
        try {
            $request->setNumOfCurrentEnrollments(3, -3);
        }catch (InvalidArgumentException $e) {
            $this->assertEquals('Invalid numOfCurrentEnrollments', $e->getMessage());
        }
    }

    public function testNumOfActiveEnrollments()
    {
        $request = new SearchCourseSectionRequest();
        $request->setNumOfActiveEnrollments(3, 3);
        $this->assertEquals(3, $request->getNumOfActiveEnrollments());
        $this->assertEquals(3, $request->getNumOfActiveEnrollmentsCount());

        $request->setNumOfActiveEnrollments(null, null);
        $this->assertEquals(null, $request->getNumOfActiveEnrollments());
        $this->assertEquals(null, $request->getNumOfActiveEnrollmentsCount());
        try {
            $request->setNumOfActiveEnrollments(3, -3);
        }catch (InvalidArgumentException $e) {
            $this->assertEquals('Invalid numOfActiveEnrollments', $e->getMessage());
        }
    }

    public function testNumOfAvailableEnrollments()
    {
        $request = new SearchCourseSectionRequest();
        $request->setNumOfAvailableEnrollments(3, 3);
        $this->assertEquals(3, $request->getNumOfAvailableEnrollments());
        $this->assertEquals(3, $request->getNumOfAvailableEnrollmentsCount());

        $request->setNumOfAvailableEnrollments(null, null);
        $this->assertEquals(null, $request->getNumOfAvailableEnrollments());
        $this->assertEquals(null, $request->getNumOfAvailableEnrollmentsCount());
        try {
            $request->setNumOfAvailableEnrollments(3, -3);
        }catch (InvalidArgumentException $e) {
            $this->assertEquals('Invalid numOfAvailableEnrollments', $e->getMessage());
        }
    }

    public function testisMidtermGradeSubmissionAvailable()
    {
        $request = new SearchCourseSectionRequest();
        $request->setIsMidtermGradeSubmissionAvailable(true);
        $this->assertEquals(true, $request->isMidtermGradeSubmissionAvailable());
        $request->setIsMidtermGradeSubmissionAvailable(null);
        $this->assertEquals(null, $request->isMidtermGradeSubmissionAvailable());
    }

    public function testisFinalGradeSubmissionAvailable()
    {
        $request = new SearchCourseSectionRequest();
        $request->setIsFinalGradeSubmissionAvailable(true);
        $this->assertEquals(true, $request->isFinalGradeSubmissionAvailable());
        $request->setIsFinalGradeSubmissionAvailable(null);
        $this->assertEquals(null, $request->isFinalGradeSubmissionAvailable());
    }

    public function testHasSeatAvailable()
    {
        $request = new SearchCourseSectionRequest();
        $request->setHasSeatAvailable(true);
        $this->assertEquals(true, $request->getHasSeatAvailable());
        $request->setHasSeatAvailable(null);
        $this->assertEquals(null, $request->getHasSeatAvailable());
    }

    public function testPartOfTermCodeCollection()
    {
        $codes = ['1', 'A'];
        $collection = PartOfTermCodeCollection::createFromValueArray($codes);

        $request = new SearchCourseSectionRequest();
        $request->setPartOfTermCodes($codes);
        $this->assertEquals($collection, $request->getPartOfTermCodeCollection());
        $request->setPartOfTermCodes(null);
        $this->assertEquals(null, $request->getPartOfTermCodeCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setPartOfTermCodes(['AAAAAAAAAAAAAA']);
    }

    public function testPartOfTermStartDate()
    {
        $date = Carbon::create(2018, 1, 2, 2, 1, 2);
        $request = new SearchCourseSectionRequest();
        $request->setPartOfTermStartDate($date, $date);
        $this->assertEquals($date, $request->getPartOfTermStartDate());
        $this->assertEquals($date, $request->getPartOfTermStartDateEnd());

        $request->setPartOfTermStartDate(null, null);
        $this->assertEquals(null, $request->getPartOfTermStartDate());
        $this->assertEquals(null, $request->getPartOfTermStartDateEnd());
    }

    public function testPartOfTermEndDate()
    {
        $date = Carbon::create(2018, 1, 2, 2, 1, 2);
        $request = new SearchCourseSectionRequest();
        $request->setPartOfTermEndDate($date, $date);
        $this->assertEquals($date, $request->getPartOfTermEndDate());
        $this->assertEquals($date, $request->getPartOfTermEndDateEnd());

        $request->setPartOfTermEndDate(null, null);
        $this->assertEquals(null, $request->getPartOfTermEndDate());
        $this->assertEquals(null, $request->getPartOfTermEndDateEnd());
    }

    public function testScheduleStartDate()
    {
        $date = Carbon::create(2018, 1, 2, 2, 1, 2);
        $request = new SearchCourseSectionRequest();
        $request->setScheduleStartDate($date, $date);
        $this->assertEquals($date, $request->getScheduleStartDate());
        $this->assertEquals($date, $request->getScheduleStartDateEnd());

        $request->setScheduleStartDate(null,null);
        $this->assertEquals(null, $request->getScheduleStartDate());
        $this->assertEquals(null, $request->getScheduleStartDateEnd());
    }

    public function testScheduleEndDate()
    {
        $date = Carbon::create(2018, 1, 2, 2, 1, 2);
        $request = new SearchCourseSectionRequest();
        $request->setScheduleEndDate($date, $date);
        $this->assertEquals($date, $request->getScheduleEndDate());
        $this->assertEquals($date, $request->getScheduleEndDateEnd());

        $request->setScheduleEndDate(null, null);
        $this->assertEquals(null, $request->getScheduleEndDate());
        $this->assertEquals(null, $request->getScheduleEndDateEnd());
    }

    public function testScheduleStartTime()
    {
        $time = Carbon::createFromFormat('H:i', '12:00');
        $request = new SearchCourseSectionRequest();
        $request->setScheduleStartTime($time, $time);
        $this->assertEquals($time, $request->getScheduleStartTime());
        $this->assertEquals($time, $request->getScheduleStartTimeEnd());

        $request->setScheduleStartTime(null, null);
        $this->assertEquals(null, $request->getScheduleStartTime());
        $this->assertEquals(null, $request->getScheduleStartTimeEnd());
    }

    public function testScheduleEndTime()
    {
        $time = Carbon::createFromFormat('H:i', '11:00');
        $request = new SearchCourseSectionRequest();
        $request->setScheduleEndTime($time, $time);
        $this->assertEquals($time, $request->getScheduleEndTime());
        $this->assertEquals($time, $request->getScheduleEndTimeEnd());

        $request->setScheduleEndTime(null, null);
        $this->assertEquals(null, $request->getScheduleEndTime());
        $this->assertEquals(null, $request->getScheduleEndTimeEnd());
    }

    public function testScheduleBuildingCodeCollection()
    {
        $codes = ['BAC', 'BEN'];
        $collection = BuildingCodeCollection::createFromValueArray($codes);

        $request = new SearchCourseSectionRequest();
        $request->setScheduleBuildingCodes($codes);
        $this->assertEquals($collection,
            $request->getScheduleBuildingCodeCollection());
        $request->setScheduleBuildingCodes(null);
        $this->assertEquals(null, $request->getScheduleBuildingCodeCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setScheduleBuildingCodes(['AAAAAAAAAAAAAA']);
    }

    public function testScheduleRooms()
    {
        $rooms = ['101', '102'];

        $request = new SearchCourseSectionRequest();
        $request->setScheduleRooms($rooms);
        $this->assertEquals($rooms, $request->getScheduleRooms());
        $request->setScheduleRooms(null);
        $this->assertEquals(null, $request->getScheduleRooms());
    }

    public function testScheduleDays()
    {
        $days = 'MFW';

        $request = new SearchCourseSectionRequest();
        $request->setScheduleDays($days);
        $this->assertEquals(new CourseSectionScheduleDays($days),
            $request->getScheduleDays());
        $request->setScheduleDays(null);
        $this->assertEquals(null, $request->getScheduleDays());
        $this->expectException(InvalidArgumentException::class);
        $request->setScheduleDays('SDK');
    }

    public function testInstructorUniqueIdCollection()
    {
        $uniqueIds = ['aaaaa', 'bbbbbb'];
        $uniqueIdCollection = UniqueIdCollection::createFromValueArray($uniqueIds);

        $request = new SearchCourseSectionRequest();
        $request->setInstructorUniqueIds($uniqueIds);
        $this->assertEquals($uniqueIdCollection,
            $request->getInstructorUniqueIdCollection());
        $request->setInstructorUniqueIds(null);
        $this->assertEquals(null, $request->getInstructorUniqueIdCollection());
//        $this->expectException(InvalidArgumentException::class);
//        $request->setInstructorUniqueIds(['skslfjdkslf']);
    }

    public function testcourseSectionGuidCollection()
    {
        $guids = [
            '954b6b70-a876-4a72-ac79-6b481fc22778',
            '9f096d54-4de9-4d47-ad02-3ab20cbb7fa5'
        ];
        $collection = CourseSectionGuidCollection::createFromValueArray($guids);

        $request = new SearchCourseSectionRequest();
        $request->setCourseSectionGuids($guids);
        $this->assertEquals($collection, $request->getCourseSectionGuidCollection());
        $request->setCourseSectionGuids(null);
        $this->assertEquals(null, $request->getCourseSectionGuidCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setCourseSectionGuids(['asdf']);
    }

    public function testOffset()
    {
        $request = new SearchCourseSectionRequest();
        $this->assertEquals(1, $request->getOffset());
        $request->setOffset(2);
        $this->assertEquals(1, $request->getOffset());
        $this->expectException(InvalidArgumentException::class);
        $request->setOffset(0);
    }

    public function testLimit()
    {
        $request = new SearchCourseSectionRequest();
        $this->assertEquals(50, $request->getLimit());
        $request->setLimit(1);
        $this->assertEquals(1, $request->getLimit());
        $this->expectException(InvalidArgumentException::class);
        $request->setLimit(0);
    }

    public function testTermCodeCollection()
    {
        $codes = ['201710', '201720'];
        $collection = TermCodeCollection::createFromValueArray($codes);

        $request = new SearchCourseSectionRequest();
        $request->setTermCodes($codes);
        $this->assertEquals($collection, $request->getTermCodeCollection());
        $request->setTermCodes(null);
        $this->assertEquals(null, $request->getTermCodeCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setTermCodes(['AAAAAAAAAAAAAA']);
    }

    public function testCampusCodeCollection()
    {
        $codes = ['O', 'H'];
        $collection = CampusCodeCollection::createFromValueArray($codes);

        $request = new SearchCourseSectionRequest();
        $request->setCampusCodes($codes);
        $this->assertEquals($collection, $request->getCampusCodeCollection());
        $request->setCampusCodes(null);
        $this->assertEquals(null, $request->getCampusCodeCollection());
        $this->expectException(InvalidArgumentException::class);
        $request->setCampusCodes(['AAAAAAAAAAAAAA']);
    }


}