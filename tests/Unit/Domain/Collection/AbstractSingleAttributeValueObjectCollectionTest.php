<?php
/**
 * Author: liaom
 * Date: 5/17/18
 * Time: 12:35 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Collection;


use MiamiOH\Pike\Domain\Collection\AbstractSingleAttributeValueObjectCollection;
use MiamiOH\Pike\Domain\ValueObject\AbstractSingleAttributeValueObject;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Tests\Unit\TestCase;

class AbstractSingleAttributeValueObjectCollectionTest extends TestCase
{
    /**
     * @var array
     */
    private $valueArray;
    /**
     * @var array
     */
    private $test1ObjectArray;
    /**
     * @var Test1Collection
     */
    private $test1Collection;

    protected function setUp() :void
    {
        parent::setUp();
        $this->valueArray = ['1', '2', 3];
        $this->test1ObjectArray = [
            new Test1Object('1'),
            new Test1Object('2'),
            new Test1Object('3')
        ];
        $this->test1Collection = new Test1Collection($this->test1ObjectArray);
    }

    public function testValueObjectNameMustBeSet()
    {
        $this->expectException(InvalidArgumentException::class);
        BadTest1Collection::createFromValueArray($this->valueArray);
    }

    public function testCreateCollectionFromValueArray()
    {
        $this->assertEquals(
            $this->test1Collection,
            Test1Collection::createFromValueArray($this->valueArray)
        );
    }

    public function testCollectionToValueArray()
    {
        $this->assertEquals(
            $this->valueArray,
            $this->test1Collection->toValueArray()
        );
    }
}

class BadTest1Collection extends AbstractSingleAttributeValueObjectCollection
{

}

class Test1Collection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = Test1Object::class;
}

class Test1Object extends AbstractSingleAttributeValueObject
{

    protected function validate(string $value): void
    {

    }
}