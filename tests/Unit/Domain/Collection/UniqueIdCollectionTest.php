<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 3:34 PM
 */

namespace MiamiOH\Pike\Tests\Unit\Domain\Collection;


use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use PHPUnit\Framework\TestCase;

class UniqueIdCollectionTest extends TestCase
{
    public function testToUpperCaseArray()
    {
        $collection = new UniqueIdCollection([
            new UniqueId('liaom'),
            new UniqueId('Duhy')
        ]);

        $this->assertEquals(
            [
                'LIAOM',
                'DUHY'
            ],
            $collection->toUpperCaseArray()
        );
    }
}