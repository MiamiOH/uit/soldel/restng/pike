<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 10:15 PM
 */

use MiamiOH\Pike\App\Service\UpdateGradeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionAttributeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentDistributionService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentService;
use MiamiOH\Pike\App\Service\ViewCourseSectionScheduleService;
use MiamiOH\Pike\App\Service\ViewCourseSectionService;
use MiamiOH\Pike\App\Service\ViewCourseService;
use MiamiOH\Pike\App\Service\ViewCrossListedCourseSectionService;
use MiamiOH\Pike\App\Service\ViewDepartmentService;
use MiamiOH\Pike\App\Service\ViewGradeService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\App\Service\ViewMajorService;
use MiamiOH\Pike\App\Service\ViewPersonService;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\App\Service\ViewThematicSequenceService;
use MiamiOH\Pike\Domain\Repository\CourseRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionAttributeRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentCountRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentDistributionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionScheduleRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CrossListedCourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\DepartmentRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\GradeRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\InstructorAssignmentRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\MajorRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\PersonRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\SearchCourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\TermRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\ThematicSequenceRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\UpdateGradeRepositoryInterface;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionAttributeRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionEnrollmentCountRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionEnrollmentRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionEnrollmentDistributionRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCourseSectionScheduleRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentCrossListedCourseSectionRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentDepartmentRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentGradeRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentInstructorAssignmentRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentMajorRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentPersonRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentSearchCourseSectionRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentTermRepository;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentThematicSequenceRepository;
use function DI\autowire;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository\EloquentUpdateGradeRepository;

return [
    CourseRepositoryInterface::class => autowire(EloquentCourseRepository::class),
    CourseSectionEnrollmentRepositoryInterface::class => autowire(EloquentCourseSectionEnrollmentRepository::class),
    CourseSectionRepositoryInterface::class => autowire(EloquentCourseSectionRepository::class),
    DepartmentRepositoryInterface::class => autowire(EloquentDepartmentRepository::class),
    InstructorAssignmentRepositoryInterface::class => autowire(EloquentInstructorAssignmentRepository::class),
    MajorRepositoryInterface::class => autowire(EloquentMajorRepository::class),
    PersonRepositoryInterface::class => autowire(EloquentPersonRepository::class),
    TermRepositoryInterface::class => autowire(EloquentTermRepository::class),
    ThematicSequenceRepositoryInterface::class => autowire(EloquentThematicSequenceRepository::class),
    GradeRepositoryInterface::class => autowire(EloquentGradeRepository::class),
    CourseSectionScheduleRepositoryInterface::class => autowire(EloquentCourseSectionScheduleRepository::class),
    CourseSectionEnrollmentDistributionRepositoryInterface::class => autowire(EloquentCourseSectionEnrollmentDistributionRepository::class),
    CourseSectionAttributeRepositoryInterface::class => autowire(EloquentCourseSectionAttributeRepository::class),
    CourseSectionEnrollmentCountRepositoryInterface::class => autowire(EloquentCourseSectionEnrollmentCountRepository::class),
    SearchCourseSectionRepositoryInterface::class => autowire(EloquentSearchCourseSectionRepository::class),
    UpdateGradeRepositoryInterface::class => autowire(EloquentUpdateGradeRepository::class),
    CrossListedCourseSectionRepositoryInterface::class => autowire(EloquentCrossListedCourseSectionRepository::class),

    'ViewCourseSectionEnrollmentService' => autowire(ViewCourseSectionEnrollmentService::class),
    'ViewCourseSectionService' => autowire(ViewCourseSectionService::class),
    'ViewCourseService' => autowire(ViewCourseService::class),
    'ViewDepartmentService' => autowire(ViewDepartmentService::class),
    'ViewInstructorAssignmentService' => autowire(ViewInstructorAssignmentService::class),
    'ViewMajorService' => autowire(ViewMajorService::class),
    'ViewPersonService' => autowire(ViewPersonService::class),
    'ViewTermService' => autowire(ViewTermService::class),
    'ViewThematicSequenceService' => autowire(ViewThematicSequenceService::class),
    'ViewGradeService' => autowire(ViewGradeService::class),
    'ViewCourseSectionScheduleService' => autowire(ViewCourseSectionScheduleService::class),
    'ViewCourseSectionAttributeService' => autowire(ViewCourseSectionAttributeService::class),
    'UpdateGradeService' => autowire(UpdateGradeService::class),
    'ViewCrossListedCourseSectionService' => autowire(ViewCrossListedCourseSectionService::class),
    'ViewCourseSectionEnrollmentDistributionService' => autowire(ViewCourseSectionEnrollmentDistributionService::class),
];