<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 11/29/17
 * Time: 3:47 PM
 */

namespace MiamiOH\Pike\Common;

use Illuminate\Support\Collection as BaseCollection;
use MiamiOH\Pike\Exception\InvalidArgumentException;

class Collection extends BaseCollection
{
    /**
     * @var int
     */
    private $totalNumOfItems = 0;
    /**
     * @var bool
     */
    private $isPageable = false;

    public function __construct($items = [])
    {
        if (!is_array($items)) {
            throw new InvalidArgumentException(
                "Argument 2 passed to " . __METHOD__ . " must be an array"
            );
        }
        parent::__construct($items);
    }

    /**
     * @return bool
     */
    public function isPageable(): bool
    {
        return $this->isPageable;
    }

    /**
     * @return int
     */
    public function getTotalNumOfItems(): int
    {
        return $this->totalNumOfItems;
    }

    /**
     * @param int $totalNumOfItems
     */
    public function setTotalNumOfItems(int $totalNumOfItems): void
    {
        $this->isPageable = true;
        $this->totalNumOfItems = $totalNumOfItems;
    }
}//end class
