<?php
/**
 * Author: liaom
 * Date: 5/3/18
 * Time: 5:50 PM
 */

namespace MiamiOH\Pike\Common;

class Converter
{
    public function stringToFloat(?string $str, ?float $replaceNullTo = null): ?float
    {
        if ($str !== null) {
            return floatval($str);
        }

        return $replaceNullTo;
    }

    public function stringToInt(?string $str, ?int $replaceNullTo = null): ?int
    {
        if ($str !== null) {
            return intval($str);
        }

        return $replaceNullTo;
    }

    public function yesNoIndicatorToBool(
        ?string $indicator,
        ?bool $replaceNullTo = false
    ): ?bool {
        if ($indicator === 'Y') {
            return true;
        } elseif ($indicator === 'N') {
            return false;
        }

        return $replaceNullTo;
    }
}
