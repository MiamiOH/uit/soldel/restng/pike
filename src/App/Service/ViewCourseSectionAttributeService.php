<?php
/**
 * Author: liaom
 * Date: 5/10/18
 * Time: 2:29 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionAttributeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Repository\CourseSectionAttributeRepositoryInterface;

class ViewCourseSectionAttributeService
{
    /**
     * @var CourseSectionAttributeRepositoryInterface
     */
    protected $courseSectionAttributeRepository;

    /**
     * ViewCourseSectionAttributeService constructor.
     *
     * @param CourseSectionAttributeRepositoryInterface $courseSectionAttributeRepository
     */
    public function __construct(
        CourseSectionAttributeRepositoryInterface $courseSectionAttributeRepository
    ) {
        $this->courseSectionAttributeRepository = $courseSectionAttributeRepository;
    }

    public function getCollectionByCourseSectionGuids(
        array $courseSectionGuids
    ): CourseSectionAttributeCollection {
        return $this->courseSectionAttributeRepository
            ->getCollectionByCourseSectionGuidCollection(
                CourseSectionGuidCollection::createFromValueArray($courseSectionGuids)
            );
    }
}
