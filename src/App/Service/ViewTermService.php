<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\TermCollection;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\Repository\TermRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

class ViewTermService
{
    protected $termRepository;


    public function __construct(TermRepositoryInterface $TermCollectionRepository)
    {
        $this->termRepository = $TermCollectionRepository;
    }//end __construct()


    public function getAll(): TermCollection
    {
        return $this->termRepository->getAll();
    }//end getAll()

    public function isValidCode(string $code)
    {
        try {
            $this->getByCode($code);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }//end getByCode()

    public function getByCode(string $code): Term
    {
        return $this->termRepository->getByCode(new TermCode($code));
    }//end isValidCode()

    public function getCurrentTerm(): Term
    {
        return $this->termRepository->getCurrentTerm();
    }//end getCurrentTerm()


    public function getNextTerm(string $termCode = null): Term
    {
        if ($termCode !== null) {
            $termCode = new TermCode($termCode);
        }

        return $this->termRepository->getNextTerm($termCode);
    }//end getNextTerm()


    public function getPrevTerm(string $termCode = null): Term
    {
        if ($termCode !== null) {
            $termCode = new TermCode($termCode);
        }

        return $this->termRepository->getPrevTerm($termCode);
    }//end getPrevTerm()


    public function getTermCollectionByCodes(array $codes): TermCollection
    {
        return $this->termRepository
            ->getTermCollectionByCodes(
                TermCodeCollection::createFromValueArray($codes)
            );
    }//end getTermCollectionByCodes()


    public function getNextTerms(
        int $numOfTerms,
        string $termCode = null
    ): TermCollection {
        if ($termCode !== null) {
            $termCode = new TermCode($termCode);
        }

        return $this->termRepository->getNextTerms($numOfTerms, $termCode);
    }//end getNextTerms()


    public function getPrevTerms(
        int $numOfTerms,
        string $termCode = null
    ): TermCollection {
        if ($termCode !== null) {
            $termCode = new TermCode($termCode);
        }

        return $this->termRepository->getPrevTerms($numOfTerms, $termCode);
    }//end getPrevTerms()
}//end class
