<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentCountRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\SearchCourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

class ViewCourseSectionService
{
    protected $courseSectionRepository;
    private $courseSectionEnrollmentCountRepository;
    private $searchCourseSectionRepository;

    public function __construct(
        CourseSectionRepositoryInterface $courseSectionRepository,
        CourseSectionEnrollmentCountRepositoryInterface $courseSectionEnrollmentCountRepository,
        SearchCourseSectionRepositoryInterface $searchCourseSectionRepository
    ) {
        $this->courseSectionRepository = $courseSectionRepository;
        $this->courseSectionEnrollmentCountRepository = $courseSectionEnrollmentCountRepository;
        $this->searchCourseSectionRepository = $searchCourseSectionRepository;
    }//end __construct()

    public function getByTermCodeCrn(string $termCode, string $crn): CourseSection
    {
        return $this->courseSectionRepository->getByTermCodeCrn(
            new TermCode($termCode),
            new Crn($crn)
        );
    }

    public function getByGuid(string $guid)
    {
        return $this->courseSectionRepository->getByGuid(new CourseSectionGuid($guid));
    }

    public function getByGuids(array $guids)
    {
        $guids = CourseSectionGuidCollection::createFromValueArray($guids);

        return $this->courseSectionRepository
            ->getByGuids($guids);
    }

    public function getEnrollmentCountBysectionGuids(
        array $sectionGuids
    ): CourseSectionEnrollmentCountCollection {
        return $this->courseSectionEnrollmentCountRepository
            ->getCollectionByCourseSectionGuidCollection(
                CourseSectionGuidCollection::createFromValueArray($sectionGuids)
            );
    }

    public function searchCourseSection(
        SearchCourseSectionRequest $searchRequest
    ): CourseSectionCollection {
        $courseSectionGuids = $this->searchCourseSectionRepository->search($searchRequest);

        $courseSectionCollection = $this->courseSectionRepository->getByGuids($courseSectionGuids);

        if ($courseSectionGuids->isPageable()) {
            $courseSectionCollection->setTotalNumOfItems($courseSectionGuids->getTotalNumOfItems());
        }

        return $courseSectionCollection;
    }
}//end class
