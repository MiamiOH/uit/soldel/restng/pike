<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 6:17 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentDistributionCollection;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentDistributionRepositoryInterface;

class ViewCourseSectionEnrollmentDistributionService
{
    /**
     * @var CourseSectionEnrollmentDistributionRepositoryInterface
     */
    protected $courseSectionEnrollmentDistributionRepository;

    /**
     * ViewCourseSectionEnrollmentDistributionService constructor.
     *
     * @param CourseSectionEnrollmentDistributionRepositoryInterface $courseSectionEnrollmentDistributionRepository
     */
    public function __construct(
        CourseSectionEnrollmentDistributionRepositoryInterface $courseSectionEnrollmentDistributionRepository
    ) {
        $this->courseSectionEnrollmentDistributionRepository = $courseSectionEnrollmentDistributionRepository;
    }

    public function getCollectionByCourseSectionGuids(
        array $courseSectionGuids
    ): CourseSectionEnrollmentDistributionCollection {
        $courseSectionGuidCollection = CourseSectionGuidCollection::createFromValueArray($courseSectionGuids);

        return $this->courseSectionEnrollmentDistributionRepository
            ->getCollectionByCourseSectionGuidCollection(
                $courseSectionGuidCollection
            );
    }
}
