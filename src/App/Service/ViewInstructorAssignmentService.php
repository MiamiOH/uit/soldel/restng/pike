<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Repository\InstructorAssignmentRepositoryInterface;
use MiamiOH\Pike\Domain\Request\SearchInstructorAssignmentRequest;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class ViewInstructorAssignmentService
{
    protected $instructorAssignmentRepository;

    public function __construct(
        InstructorAssignmentRepositoryInterface $instructorAssignmentRepository
    ) {
        $this->instructorAssignmentRepository = $instructorAssignmentRepository;
    }//end __construct()


    /**
     * Get a collection of instructor assignment of a specified course section
     *
     * @param string $termCode
     * @param string $crn
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByTermCodeCrn(
        string $termCode,
        string $crn
    ): InstructorAssignmentCollection {
        return $this->instructorAssignmentRepository->getCollectionByTermCodeCrn(
            new TermCode($termCode),
            new Crn($crn)
        );
    }

    /**
     * Get an instructor assignment of a specified course section and instructor's
     * unique id
     *
     * @param string $uniqueId
     * @param string $termCode
     * @param string $crn
     *
     * @return InstructorAssignment
     */
    public function getByUniqueIdTermCodeCrn(
        string $uniqueId,
        string $termCode,
        string $crn
    ): InstructorAssignment {
        return $this->instructorAssignmentRepository->getByUniqueIdTermCodeCrn(
            new UniqueId($uniqueId),
            new TermCode($termCode),
            new Crn($crn)
        );
    }

    /**
     * Get a collection of instructor assignment that a specified instructor has
     * in a specified term
     *
     * @param string $uniqueId
     * @param string $termCode
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByUniqueIdTermCode(
        string $uniqueId,
        string $termCode
    ): InstructorAssignmentCollection {
        return $this->instructorAssignmentRepository->getCollectionByUniqueIdTermCode(
            new UniqueId($uniqueId),
            new TermCode($termCode)
        );
    }

    public function getByGuid(string $guid)
    {
        return $this->instructorAssignmentRepository
            ->getByGuid(new InstructorAssignmentGuid($guid));
    }

    /**
     * Get a collection of instructor assignment
     *
     * @param array $termCodes
     * @param array $crns
     * @param array $uniqueIds
     *
     * @param int|null $offset
     * @param int|null $limit
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByTermCodesCrnsUniqeIds(
        array $termCodes,
        array $crns,
        array $uniqueIds,
        int $offset = null,
        int $limit = null
    ): InstructorAssignmentCollection {
        $termCodesCollection = TermCodeCollection::createFromValueArray($termCodes);
        $crnCollection = CrnCollection::createFromValueArray($crns);
        $uniqueIdCollection = UniqueIdCollection::createFromValueArray($uniqueIds);

        return $this->instructorAssignmentRepository->getCollectionByTermCodesCrnsUniqueIds(
            $termCodesCollection,
            $crnCollection,
            $uniqueIdCollection,
            $offset === null ? null : new Offset($offset),
            $limit === null ? null : new Limit($limit)
        );
    }

    public function getCollectionByCourseSectionGuids(
        array $guids
    ) {
        return $this->instructorAssignmentRepository
            ->getCollectionByCourseSectionGuidCollection(
                CourseSectionGuidCollection::createFromValueArray($guids)
            );
    }

    public function getCollectionByCourseSectionGuidsUniqueIds(
        array $guids,
        array $uniqueIds
    ) {
        return $this->instructorAssignmentRepository
            ->getCollectionByCourseSectionGuidsUniqueIds(
                CourseSectionGuidCollection::createFromValueArray($guids),
                UniqueIdCollection::createFromValueArray($uniqueIds)
            );
    }

    public function search(
        SearchInstructorAssignmentRequest $searchRequest
    ): InstructorAssignmentCollection {
        return $this->instructorAssignmentRepository->search($searchRequest);
    }
}//end class
