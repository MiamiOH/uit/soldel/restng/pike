<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 6:17 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Repository\CourseSectionScheduleRepositoryInterface;

class ViewCourseSectionScheduleService
{
    /**
     * @var CourseSectionScheduleRepositoryInterface
     */
    protected $courseSectionScheduleRepository;

    /**
     * ViewCourseSectionScheduleService constructor.
     *
     * @param CourseSectionScheduleRepositoryInterface $courseSectionScheduleRepository
     */
    public function __construct(
        CourseSectionScheduleRepositoryInterface $courseSectionScheduleRepository
    ) {
        $this->courseSectionScheduleRepository = $courseSectionScheduleRepository;
    }

    public function getCollectionByCourseSectionGuidCollection(
        array $courseSectionGuids
    ): CourseSectionScheduleCollection {
        $courseSectionGuidCollection = CourseSectionGuidCollection::createFromValueArray($courseSectionGuids);

        return $this->courseSectionScheduleRepository
            ->getCollectionByCourseSectionGuidCollection(
                $courseSectionGuidCollection
            );
    }
}
