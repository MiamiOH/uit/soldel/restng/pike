<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentRepositoryInterface;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionEnrollmentRequest;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class ViewCourseSectionEnrollmentService
{
    protected $courseSectionEnrollmentRepository;


    public function __construct(
        CourseSectionEnrollmentRepositoryInterface $courseSectionEnrollmentRepository
    ) {
        $this->courseSectionEnrollmentRepository = $courseSectionEnrollmentRepository;
    }//end __construct()

    public function getByUniqueIdTermCodeCrn(
        string $uniqueId,
        string $termCode,
        string $crn
    ): CourseSectionEnrollment {
        return $this->courseSectionEnrollmentRepository->getByUniqueIdTermCodeCrn(
            new UniqueId($uniqueId),
            new TermCode($termCode),
            new Crn($crn)
        );
    }

    public function getCollectionByUniqueIdTermCode(
        string $uniqueId,
        string $termCode
    ): CourseSectionEnrollmentCollection {
        return $this->courseSectionEnrollmentRepository->getCollectionByUniqueIdTermCode(
            new UniqueId($uniqueId),
            new TermCode($termCode)
        );
    }

    public function getCollectionByTermCodeCrn(
        string $termCode,
        string $crn
    ): CourseSectionEnrollmentCollection {
        return $this->courseSectionEnrollmentRepository->getCollectionByTermCodeCrn(
            new TermCode($termCode),
            new Crn($crn)
        );
    }

    public function getCollectionByTermCodesCrnsUniqeIds(
        array $termCodes,
        array $crns,
        array $uniqeIds,
        int $offset = null,
        int $limit = null
    ): CourseSectionEnrollmentCollection {
        $uniqueIdCollection = UniqueIdCollection::createFromValueArray($uniqeIds);
        $crnCollection = CrnCollection::createFromValueArray($crns);
        $termCodesCollection = TermCodeCollection::createFromValueArray($termCodes);

        return $this->courseSectionEnrollmentRepository->getCollectionByTermCodesCrnsUniqueIds(
            $termCodesCollection,
            $crnCollection,
            $uniqueIdCollection,
            $offset === null ? null : new Offset($offset),
            $limit === null ? null : new Limit($limit)
        );
    }

    public function search(
        SearchCourseSectionEnrollmentRequest $searchRequest
    ): CourseSectionEnrollmentCollection {
        return $this->courseSectionEnrollmentRepository->search($searchRequest);
    }
}//end class
