<?php
/**
 * Author: liaom
 * Date: 5/22/18
 * Time: 9:01 AM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\UpdateGradeRequestCollection;
use MiamiOH\Pike\Domain\Collection\UpdateGradeResponseCollection;
use MiamiOH\Pike\Domain\Repository\UpdateGradeRepositoryInterface;

class UpdateGradeService
{
    /**
     * @var UpdateGradeRepositoryInterface
     */
    private $updateGradeRepository;

    /**
     * UpdateGradeService constructor.
     *
     * @param UpdateGradeRepositoryInterface $updateGradeRepository
     */
    public function __construct(
        UpdateGradeRepositoryInterface $updateGradeRepository
    ) {
        $this->updateGradeRepository = $updateGradeRepository;
    }

    public function bulkUpdate(
        UpdateGradeRequestCollection $requestCollection
    ): UpdateGradeResponseCollection {
        return $this->updateGradeRepository->bulkUpdate($requestCollection);
    }
}
