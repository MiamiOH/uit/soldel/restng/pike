<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\MajorCodeCollection;
use MiamiOH\Pike\Domain\Collection\MajorCollection;
use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\Pike\Domain\Repository\MajorRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;

class ViewMajorService
{
    protected $MajorCollectionRepository;


    public function __construct(MajorRepositoryInterface $MajorCollectionRepository)
    {
        $this->MajorCollectionRepository = $MajorCollectionRepository;
    }//end __construct()


    public function getAll(): MajorCollection
    {
        return $this->MajorCollectionRepository->getAll();
    }//end getAll()

    public function getMajorCollectionByCodes(array $codes): MajorCollection
    {
        return $this->MajorCollectionRepository->getMajorCollectionByCodes(
            MajorCodeCollection::createFromValueArray($codes)
        );
    }//end getByCode()

    public function isValidCode(string $code)
    {
        try {
            $this->getByCode($code);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }//end getMajorCollectionByCodes()

    public function getByCode(string $code): Major
    {
        return $this->MajorCollectionRepository->getByCode(new MajorCode($code));
    }//end isValidCode()
}//end class
