<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrossListedCourseSectionCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentCountRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CrossListedCourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\SearchCourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

class ViewCrossListedCourseSectionService
{
    /**
     * @var CrossListedCourseSectionRepositoryInterface
     */
    protected $crossListedCourseSectionRepository;

    /**
     * ViewCrossListedCourseSectionService constructor.
     * @param CrossListedCourseSectionRepositoryInterface $crossListedCourseSectionRepository
     */
    public function __construct(CrossListedCourseSectionRepositoryInterface $crossListedCourseSectionRepository)
    {
        $this->crossListedCourseSectionRepository = $crossListedCourseSectionRepository;
    }

    public function getCollectionByCourseSectionGuids(array $courseSectionGuids): CrossListedCourseSectionCollection
    {
        return $this->crossListedCourseSectionRepository
            ->getColectionByGuidCollectioin(
                CourseSectionGuidCollection::createFromValueArray($courseSectionGuids)
            );
    }
}//end class
