<?php

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\GradeTypeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Repository\GradeRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class ViewGradeService
{
    /**
     * @var GradeRepositoryInterface
     */
    protected $gradeRepository;

    /**
     * ViewDepartmentService constructor.
     *
     * @param GradeRepositoryInterface $gradeRepository
     */
    public function __construct(GradeRepositoryInterface $gradeRepository)
    {
        $this->gradeRepository = $gradeRepository;
    }


    public function getCollectionByCourseSectionGuidAndUniqueId(
        string $courseSectionGuid,
        string $uniqueId
    ): GradeCollection {
        return $this->gradeRepository->getCollectionByCourseSectionGuidAndUniqueId(
            new CourseSectionGuid($courseSectionGuid),
            new UniqueId($uniqueId)
        );
    }

    public function getCollectionByTermCodesCrnsUniqueIdsTypes(
        array $termCodes,
        array $crns,
        array $uniqueIds,
        array $types
    ): GradeCollection {
        return $this->gradeRepository->getCollectionByTermCodesCrnsUniqueIdsTypes(
            TermCodeCollection::createFromValueArray($termCodes),
            CrnCollection::createFromValueArray($crns),
            UniqueIdCollection::createFromValueArray($uniqueIds),
            GradeTypeCollection::createFromValueArray($types)
        );
    }

    public function getCollectionByCourseSectionGuidsUniqueIdsTypes(
        array $courseSectionGuids,
        array $uniqueIds,
        array $types
    ): GradeCollection {
        return $this->gradeRepository->getCollectionByCourseSectionGuidsUniqueIdsTypes(
            CourseSectionGuidCollection::createFromValueArray($courseSectionGuids),
            UniqueIdCollection::createFromValueArray($uniqueIds),
            GradeTypeCollection::createFromValueArray($types)
        );
    }
}
