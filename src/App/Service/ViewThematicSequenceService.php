<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\ThematicSequenceCollection;
use MiamiOH\Pike\Domain\Model\ThematicSequence;
use MiamiOH\Pike\Domain\Repository\ThematicSequenceRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;

class ViewThematicSequenceService
{
    protected $ThematicSequenceCollectionRepository;


    public function __construct(
        ThematicSequenceRepositoryInterface $ThematicSequenceCollectionRepository
    ) {
        $this->ThematicSequenceCollectionRepository = $ThematicSequenceCollectionRepository;
    }//end __construct()


    public function getAll(): ThematicSequenceCollection
    {
        return $this->ThematicSequenceCollectionRepository->getAll();
    }//end getAll()

    public function isValidCode(string $code)
    {
        try {
            $this->getByCode($code);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }//end getByCode()

    public function getByCode(string $code): ThematicSequence
    {
        return $this->ThematicSequenceCollectionRepository->getByCode(new ThematicSequenceCode($code));
    }//end isValidCode()
}//end class
