<?php

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\BannerIdCollection;
use MiamiOH\Pike\Domain\Collection\PersonCollection;
use MiamiOH\Pike\Domain\Collection\PidmCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\Repository\PersonRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class ViewPersonService
{
    protected $personRepository;

    public function __construct(PersonRepositoryInterface $personRepository)
    {
        $this->personRepository = $personRepository;
    }//end __construct()

    public function getByUniqueId(string $uniqueId): Person
    {
        return $this->personRepository->getByUniqueId(new UniqueId($uniqueId));
    }

    public function getByPidm(string $pidm): Person
    {
        return $this->personRepository->getByPidm(new Pidm($pidm));
    }

    public function getByBannerId(string $bannerId): Person
    {
        return $this->personRepository->getByBannerId(new BannerId($bannerId));
    }

    public function getCollectionByUniqueIds(
        array $uniqueIds
    ): PersonCollection {
        return $this->personRepository
            ->getCollectionByUniqueIdCollection(
                UniqueIdCollection::createFromValueArray($uniqueIds)
            );
    }

    public function getCollectionByPidms(
        array $pidms
    ): PersonCollection {
        return $this->personRepository
            ->getCollectionByPidmCollection(
                PidmCollection::createFromValueArray($pidms)
            );
    }

    public function getCollectionByBannerIds(
        array $bannerIds
    ): PersonCollection {
        return $this->personRepository
            ->getCollectionByBannerIdCollection(
                BannerIdCollection::createFromValueArray($bannerIds)
            );
    }
}//end class
