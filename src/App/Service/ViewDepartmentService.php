<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\DepartmentCollection;
use MiamiOH\Pike\Domain\Model\Department;
use MiamiOH\Pike\Domain\Repository\DepartmentRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;

class ViewDepartmentService
{
    protected $departmentCollectionRepository;


    public function __construct(
        DepartmentRepositoryInterface $departmentCollectionRepository
    ) {
        $this->departmentCollectionRepository = $departmentCollectionRepository;
    }//end __construct()


    public function getAll(): DepartmentCollection
    {
        return $this->departmentCollectionRepository->getAll();
    }//end getAll()

    public function isValidCode(string $code)
    {
        try {
            $this->getByCode($code);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }//end getByCode()

    public function getByCode(string $code): Department
    {
        return $this->departmentCollectionRepository->getByCode(new DepartmentCode($code));
    }//end isValidCode()
}//end class
