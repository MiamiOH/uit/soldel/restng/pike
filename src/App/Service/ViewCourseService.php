<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 4:26 PM
 */

namespace MiamiOH\Pike\App\Service;

use MiamiOH\Pike\Domain\Collection\CourseCollection;
use MiamiOH\Pike\Domain\Collection\CourseGuidCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Repository\CourseRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

class ViewCourseService
{
    protected $courseRepository;


    public function __construct(CourseRepositoryInterface $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }//end __construct()
}//end class
