<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/6/18
 * Time: 2:29 PM
 */

namespace MiamiOH\Pike\App\Framework\RESTng;

class InvalidDatasourceTypeException extends \Exception
{
    protected $message = 'Invalid datasource type';
}//end class
