<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/6/18
 * Time: 2:21 PM
 */

namespace MiamiOH\Pike\App\Framework\RESTng;

use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Mapper\AppMapperFactory;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentFactory;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

class PikeServiceFactory
{
    private $dataSourceManager;
    /**
     * @var EloquentFactory
     */
    private $eloquentFactory;

    /**
     * @var AppMapperFactory
     */
    private $appMapperFactory;


    public function setDataSourceFactory($dataSourceManager)
    {
        $this->dataSourceManager = $dataSourceManager;
    }//end setDataSourceFactory()


    public function getEloquentPikeServiceMapper(string $datasourceName): AppMapper
    {
        $datasource = $this->dataSourceManager->getDataSource($datasourceName);

        if ($datasource->getType() !== 'OCI8') {
            throw new InvalidDatasourceTypeException();
        }

        if ($this->eloquentFactory === null) {
            $this->eloquentFactory = new EloquentFactory();
        }

        $dbConfig = [
            'driver' => 'oracle',
            'host' => '',
            'database' => '',
            'username' => $datasource->getUser(),
            'password' => $datasource->getPassword(),
            'port' => '',
            'tns' => $datasource->getDatabase(),
            'options' => [\PDO::ATTR_PERSISTENT => true],
        ];

        $this->eloquentFactory->addConnection($dbConfig);
        $eloquentHandler = $this->eloquentFactory->getEloquentHandler();

        if ($this->appMapperFactory === null) {
            $this->appMapperFactory = new AppMapperFactory();
        }

        return $this->appMapperFactory->createAppMapper([
            EloquentHandler::class => $eloquentHandler
        ]);
    }//end getEloquentPikeServiceMapper()
}//end class
