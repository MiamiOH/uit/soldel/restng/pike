<?php

return [
    'driver' => env('PIKE_MAIN_DRIVER', 'database'),
    'oracle' => [
        'host' => env('PIKE_ORACLE_HOST', env('DB_HOST', '')),
        'port' => env('PIKE_ORACLE_PORT', env('DB_PORT', '')),
        'username' => env('PIKE_ORACLE_USERNAME', env('DB_USERNAME', '')),
        'password' => env('PIKE_ORACLE_PASSWORD', env('DB_PASSWORD', '')),
        'charset' => env('PIKE_ORACLE_CHARSET', 'AL32UTF8'),
        'service_name' => env('PIKE_ORACLE_SERVICE', env('DB_SERVICE_NAME', '')),
        'tns' => env('PIKE_ORACLE_TNS', env('DB_TNS', '')),
    ]
];
