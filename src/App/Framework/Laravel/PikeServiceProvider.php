<?php

namespace MiamiOH\Pike\App\Framework\Laravel;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Mapper\SqlRepositoryMapper;
use MiamiOH\Pike\Exception\Exception;
use MiamiOH\Pike\Infrastructure\Persistence\Sql\DatabaseHandler;

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 7:24 PM
 */
class PikeServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/config/pike.php' => config_path('pike.php'),
            ]
        );
    }//end boot()


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'pike',
            function (Application $app) {
                $host = config('pike.oracle.host');
                $port = config('pike.oracle.port');
                $tns = config('pike.oracle.tns');
                $serviceName = config('pike.oracle.service_name');
                $username = config('pike.oracle.username');
                $password = config('pike.oracle.password');

                $databaseHandler = null;

                if ($tns !== null && $tns !== '') {
                    $databaseHandler = new DatabaseHandler(
                        $tns,
                        $username,
                        $password
                    );
                } else {
                    $databaseHandler = DatabaseHandler::createWithoutTns(
                        $host,
                        $port,
                        $serviceName,
                        $username,
                        $password
                    );
                }

                if (config('pike.driver') === 'database') {
                    return new AppMapper(
                        new SqlRepositoryMapper($databaseHandler)
                    );
                } else {
                    throw new Exception('[Pike] Unsupported driver');
                }
            }
        );
    }//end register()
}//end class
