<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 7:39 PM
 */

namespace MiamiOH\Pike\App\Framework\Laravel;

use Illuminate\Support\Facades\Facade;

class PikeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'pike';
    }//end getFacadeAccessor()
}//end class
