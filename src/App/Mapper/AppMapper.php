<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 7:10 PM
 */

namespace MiamiOH\Pike\App\Mapper;

use DI\Container;
use DI\DependencyException;
use DI\NotFoundException;
use MiamiOH\Pike\App\Service\UpdateGradeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionAttributeService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentDistributionService;
use MiamiOH\Pike\App\Service\ViewCourseSectionEnrollmentService;
use MiamiOH\Pike\App\Service\ViewCourseSectionLevelDistributionService;
use MiamiOH\Pike\App\Service\ViewCourseSectionScheduleService;
use MiamiOH\Pike\App\Service\ViewCourseSectionService;
use MiamiOH\Pike\App\Service\ViewCourseService;
use MiamiOH\Pike\App\Service\ViewCrossListedCourseSectionService;
use MiamiOH\Pike\App\Service\ViewDepartmentService;
use MiamiOH\Pike\App\Service\ViewGradeService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\App\Service\ViewMajorService;
use MiamiOH\Pike\App\Service\ViewPersonService;
use MiamiOH\Pike\App\Service\ViewTermService;
use MiamiOH\Pike\App\Service\ViewThematicSequenceService;
use MiamiOH\Pike\Exception\Exception;

class AppMapper
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }//end __construct()


    public function getViewCourseSectionEnrollmentService(
    ): ViewCourseSectionEnrollmentService {
        return $this->getFromContainer('ViewCourseSectionEnrollmentService');
    }

    private function getFromContainer(string $key)
    {
        try {
            return $this->container->get($key);
        } catch (DependencyException $e) {
            throw new Exception($e->getMessage());
        } catch (NotFoundException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getViewCourseSectionService(): ViewCourseSectionService
    {
        return $this->getFromContainer('ViewCourseSectionService');
    }

    public function getViewCourseService(): ViewCourseService
    {
        return $this->getFromContainer('ViewCourseService');
    }

    public function getViewDepartmentService(): ViewDepartmentService
    {
        return $this->getFromContainer('ViewDepartmentService');
    }

    public function getViewInstructorAssignmentService(
    ): ViewInstructorAssignmentService {
        return $this->getFromContainer('ViewInstructorAssignmentService');
    }

    public function getViewMajorService(): ViewMajorService
    {
        return $this->getFromContainer('ViewMajorService');
    }

    public function getViewPersonService(): ViewPersonService
    {
        return $this->getFromContainer('ViewPersonService');
    }

    public function getViewTermService(): ViewTermService
    {
        return $this->getFromContainer('ViewTermService');
    }

    public function getViewThematicSequenceService(): ViewThematicSequenceService
    {
        return $this->getFromContainer('ViewThematicSequenceService');
    }

    public function getViewGradeService(): ViewGradeService
    {
        return $this->getFromContainer('ViewGradeService');
    }

    public function getViewCourseSectionScheduleService(
    ): ViewCourseSectionScheduleService {
        return $this->getFromContainer('ViewCourseSectionScheduleService');
    }

    public function getViewCourseSectionAttributeService(
    ): ViewCourseSectionAttributeService {
        return $this->getFromContainer('ViewCourseSectionAttributeService');
    }

    public function getUpdateGradeService(): UpdateGradeService
    {
        return $this->getFromContainer('UpdateGradeService');
    }

    public function getViewCrossListedCourseSectionService(): ViewCrossListedCourseSectionService
    {
        return $this->getFromContainer('ViewCrossListedCourseSectionService');
    }

    public function getViewCourseSectionEnrollmentDistributionService(): ViewCourseSectionEnrollmentDistributionService
    {
        return $this->getFromContainer('ViewCourseSectionEnrollmentDistributionService');
    }
}//end class
