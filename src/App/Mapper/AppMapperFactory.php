<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 10:56 PM
 */

namespace MiamiOH\Pike\App\Mapper;

use DI\ContainerBuilder;
use MiamiOH\Pike\Exception\Exception;

class AppMapperFactory
{
    public function createAppMapper(array $additionalDiConfigs = []): AppMapper
    {
        $diContainerBuilder = new ContainerBuilder();
        $diConfigFilePath = __DIR__ . '/../../../config/phpdi.php';
        if (!file_exists($diConfigFilePath)) {
            throw new Exception('Missing di config file');
        }
        $diContainerBuilder->addDefinitions($diConfigFilePath);
        $diContainerBuilder->addDefinitions($additionalDiConfigs);
        $diContainer = $diContainerBuilder->build();

        return new AppMapper($diContainer);
    }
}
