<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/8/18
 * Time: 10:39 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidBuildingCodeException extends InvalidArgumentException
{
    protected $message = 'Invalid building code.';
}
