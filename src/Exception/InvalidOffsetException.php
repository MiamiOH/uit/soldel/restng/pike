<?php
/**
 * Author: liaom
 * Date: 5/29/18
 * Time: 9:07 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidOffsetException extends InvalidArgumentException
{
    protected $message = 'Invalid offset: must be larger than 0';
}
