<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 9:16 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidCourseSectionStatusCodeException extends InvalidArgumentException
{
    protected $message = 'Invalid course section status code';
}
