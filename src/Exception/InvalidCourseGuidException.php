<?php
/**
 * Author: liaom
 * Date: 5/29/18
 * Time: 9:09 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidCourseGuidException extends InvalidGuidException
{
    protected $message = 'Invalid course GUID';
}
