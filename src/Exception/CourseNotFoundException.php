<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:13 PM
 */

namespace MiamiOH\Pike\Exception;

class CourseNotFoundException extends NotFoundException
{
    protected $message = 'Course not found';
}
