<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/8/18
 * Time: 10:28 AM
 */

namespace MiamiOH\Pike\Exception;

class CourseSectionNotFoundException extends NotFoundException
{
    protected $message = 'Course section not found.';
}
