<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:20 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidPidmException extends InvalidArgumentException
{
    protected $message = 'Invalid pidm';
}
