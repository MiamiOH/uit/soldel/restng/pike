<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:18 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidThematicSequenceCodeException extends InvalidArgumentException
{
    protected $message = 'Invalid thematic sequence code';
}
