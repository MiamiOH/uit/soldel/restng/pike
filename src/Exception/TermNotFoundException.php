<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 10:05 AM
 */

namespace MiamiOH\Pike\Exception;

class TermNotFoundException extends NotFoundException
{
    protected $message = 'Term not found';
}
