<?php
/**
 * Author: liaom
 * Date: 6/11/18
 * Time: 1:53 PM
 */

namespace MiamiOH\Pike\Exception;

class NotFoundException extends Exception
{
}
