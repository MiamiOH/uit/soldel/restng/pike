<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 10:02 AM
 */

namespace MiamiOH\Pike\Exception;

class CourseSectionEnrollmentNotFoundException extends NotFoundException
{
    protected $message = 'Course section enrollment not found';
}
