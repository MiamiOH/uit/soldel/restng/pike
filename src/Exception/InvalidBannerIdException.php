<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 9:16 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidBannerIdException extends InvalidArgumentException
{
    protected $message = 'Invalid banner id';
}
