<?php
/**
 * Author: liaom
 * Date: 5/29/18
 * Time: 9:07 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidGuidException extends InvalidArgumentException
{
    protected $message = 'Invalid GUID';
}
