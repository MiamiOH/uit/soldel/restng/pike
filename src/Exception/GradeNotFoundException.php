<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/8/18
 * Time: 10:28 AM
 */

namespace MiamiOH\Pike\Exception;

class GradeNotFoundException extends NotFoundException
{
    protected $message = 'Grade not found.';
}
