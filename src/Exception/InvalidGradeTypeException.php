<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/8/18
 * Time: 10:39 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidGradeTypeException extends InvalidArgumentException
{
    protected $message = 'Invalid grade type, can only accept midterm and final.';
}
