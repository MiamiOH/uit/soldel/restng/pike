<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 10:07 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidCrnException extends InvalidArgumentException
{
    protected $message = 'Invalid CRN.';
}
