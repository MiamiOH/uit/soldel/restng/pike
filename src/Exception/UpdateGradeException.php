<?php
/**
 * Author: liaom
 * Date: 5/21/18
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Exception;

class UpdateGradeException extends Exception
{
    protected $message = 'Failed to update grade';
}
