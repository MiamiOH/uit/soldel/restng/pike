<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:21 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidCourseSectionScheduleDaysException extends InvalidArgumentException
{
    protected $message = 'Invalid course section schedule days.';
}
