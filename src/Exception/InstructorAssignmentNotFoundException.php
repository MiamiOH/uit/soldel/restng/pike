<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:34 PM
 */

namespace MiamiOH\Pike\Exception;

class InstructorAssignmentNotFoundException extends NotFoundException
{
    protected $message = 'Instructor assignment not found';
}
