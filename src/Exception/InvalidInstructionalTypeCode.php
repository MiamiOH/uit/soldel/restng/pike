<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:11 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidInstructionalTypeCode extends InvalidArgumentException
{
    protected $message = 'Invalid instructional type code';
}
