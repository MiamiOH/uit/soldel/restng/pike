<?php
/**
 * Author: liaom
 * Date: 5/29/18
 * Time: 9:07 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidLimitException extends InvalidArgumentException
{
    protected $message = 'Invalid limit: must between 1 and 200 inclusive';
}
