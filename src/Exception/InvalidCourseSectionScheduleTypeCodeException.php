<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:06 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidCourseSectionScheduleTypeCodeException extends InvalidArgumentException
{
    protected $message = 'Invalid course section schedule type code';
}
