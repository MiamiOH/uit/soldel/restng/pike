<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 9:16 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidDivisionCodeException extends InvalidArgumentException
{
    protected $message = 'Invalid division code';
}
