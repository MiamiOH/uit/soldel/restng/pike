<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:22 PM
 */

namespace MiamiOH\Pike\Exception;

class PersonNotFoundException extends NotFoundException
{
    protected $message = 'Person not found';
}
