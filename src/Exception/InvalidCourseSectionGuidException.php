<?php
/**
 * Author: liaom
 * Date: 5/29/18
 * Time: 9:08 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidCourseSectionGuidException extends InvalidGuidException
{
    protected $message = 'Invalid course section GUID';
}
