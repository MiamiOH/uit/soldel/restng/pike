<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 10:04 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidTermCodeException extends InvalidArgumentException
{
    protected $message = 'Invalid term code';
}
