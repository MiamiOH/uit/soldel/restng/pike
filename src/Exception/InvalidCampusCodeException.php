<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 2:09 PM
 */

namespace MiamiOH\Pike\Exception;

class InvalidCampusCodeException extends InvalidArgumentException
{
    protected $message = 'Invalid campus code';
}
