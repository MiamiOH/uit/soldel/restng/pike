<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 10:05 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidUniqueIdException extends InvalidArgumentException
{
    protected $message = 'Invalid UniqueID.';
}
