<?php
/**
 * Author: liaom
 * Date: 5/29/18
 * Time: 9:09 AM
 */

namespace MiamiOH\Pike\Exception;

class InvalidInstructorAssignmentGuidException extends InvalidGuidException
{
    protected $message = 'Invalid instructor assignment GUID';
}
