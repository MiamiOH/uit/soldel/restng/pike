<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 10:32 AM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Query\Expression;

/**
 * Class EloquentHandler
 *
 * @package            MiamiOH\Pike\Infrastructure\Persistence\Eloquent
 * @codeCoverageIgnore
 */
class EloquentHandler
{
    /**
     * @var Capsule
     */
    private $capsule;

    private $connectionName;


    /**
     * EloquentHandler constructor.
     *
     * @param Capsule $capsule
     * @param string $connectionName
     */
    public function __construct(Capsule $capsule, string $connectionName)
    {
        $this->capsule = $capsule;
        $this->connectionName = $connectionName;
    }//end __construct()

    public static function raw(string $plainSql): Expression
    {
        return new Expression($plainSql);
    }//end table()

    public function table(string $table): QueryBuilder
    {
        return $this->capsule::connection($this->connectionName)->table($table);
    }//end model()

    public function select(string $query, array $bindings = [])
    {
        return $this->capsule::connection($this->connectionName)->select(
            $query,
            $bindings
        );
    }//end select()

    public function model(string $modelName)
    {
        /*
            * @var $model Model
         */
        $model = new $modelName;
        $model->setConnection($this->connectionName);

        return $model;
    }//end raw()
}//end class
