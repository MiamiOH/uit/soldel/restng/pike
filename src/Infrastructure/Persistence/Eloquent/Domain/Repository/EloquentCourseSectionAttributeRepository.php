<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionAttributeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionAttribute;
use MiamiOH\Pike\Domain\Repository\CourseSectionAttributeRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionAttributeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssrattr;

class EloquentCourseSectionAttributeRepository extends AbstractBaseRepository implements CourseSectionAttributeRepositoryInterface
{
    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $courseSectionGuidCollection
    ): CourseSectionAttributeCollection {
        $ssrattrCollection = $this->getEloquentHandler()
            ->model(Ssrattr::class)
            ->byCourseSectionGuids($courseSectionGuidCollection)
            ->get();

        return $this->constructCourseSectionAttributeCollection(
            $ssrattrCollection
        );
    }

    private function constructCourseSectionAttributeCollection(
        EloquentCollection $ssrattrCollection
    ): CourseSectionAttributeCollection {
        $courseSectionAttributeCollection = new CourseSectionAttributeCollection();

        foreach ($ssrattrCollection as $ssrattr) {
            $courseSectionAttributeCollection->push(
                $this->constructCourseSectionAttribute($ssrattr)
            );
        }

        return $courseSectionAttributeCollection;
    }

    private function constructCourseSectionAttribute(
        Ssrattr $ssrattr
    ): CourseSectionAttribute {
        return new CourseSectionAttribute(
            new CourseSectionAttributeCode($ssrattr->getCode()),
            $ssrattr->getDescription(),
            new CourseSectionGuid($ssrattr->getCourseSectionGuid())
        );
    }
}//end class
