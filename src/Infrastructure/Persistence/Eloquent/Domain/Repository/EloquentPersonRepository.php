<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\BannerIdCollection;
use MiamiOH\Pike\Domain\Collection\PersonCollection;
use MiamiOH\Pike\Domain\Collection\PidmCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\Repository\PersonRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\PersonNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Spriden;

class EloquentPersonRepository extends AbstractBaseRepository implements PersonRepositoryInterface
{
    public function getByUniqueId(UniqueId $uniqueId): Person
    {
        $spriden = $this->getEloquentHandler()->model(Spriden::class)
            ->byUniqueId($uniqueId)
            ->first();

        if ($spriden === null) {
            throw new PersonNotFoundException();
        }

        return $this->constructPerson($spriden);
    }

    private function constructPerson(Spriden $spriden): Person
    {
        return new Person(
            new UniqueId($spriden->getUniqueId()),
            new Pidm($spriden->getPidm()),
            new BannerId($spriden->getBannerId()),
            $spriden->getLastName(),
            $spriden->getFirstName(),
            $spriden->getMiddleName(),
            $spriden->getNamePrefix(),
            $spriden->getNameSuffix(),
            $spriden->getPreferredFirstName()
        );
    }

    public function getByPidm(Pidm $pidm): Person
    {
        $spriden = $this->getEloquentHandler()->model(Spriden::class)
            ->byPidm($pidm)
            ->first();

        if ($spriden === null) {
            throw new PersonNotFoundException();
        }

        return $this->constructPerson($spriden);
    }

    public function getByBannerId(BannerId $bannerId): Person
    {
        $spriden = $this->getEloquentHandler()->model(Spriden::class)
            ->byBannerId($bannerId)
            ->first();

        if ($spriden === null) {
            throw new PersonNotFoundException();
        }

        return $this->constructPerson($spriden);
    }

    public function getCollectionByUniqueIdCollection(
        UniqueIdCollection $uniqueIdCollection
    ): PersonCollection {
        $spridenCollection = $this->getEloquentHandler()->model(Spriden::class)
            ->byUniqueIds($uniqueIdCollection)
            ->get();

        return $this->constructPersonCollection($spridenCollection);
    }

    private function constructPersonCollection(
        EloquentCollection $spridenCollection
    ): PersonCollection {
        $personCollection = new PersonCollection();

        foreach ($spridenCollection as $spriden) {
            $personCollection->push($this->constructPerson($spriden));
        }

        return $personCollection;
    }

    public function getCollectionByPidmCollection(
        PidmCollection $pidmCollection
    ): PersonCollection {
        $spridenCollection = $this->getEloquentHandler()->model(Spriden::class)
            ->byPidms($pidmCollection)
            ->get();

        return $this->constructPersonCollection($spridenCollection);
    }

    public function getCollectionByBannerIdCollection(
        BannerIdCollection $bannerIdCollection
    ): PersonCollection {
        $spridenCollection = $this->getEloquentHandler()->model(Spriden::class)
            ->byBannerIds($bannerIdCollection)
            ->get();

        return $this->constructPersonCollection($spridenCollection);
    }
}//end class
