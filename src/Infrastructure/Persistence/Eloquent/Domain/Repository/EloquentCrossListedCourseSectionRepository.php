<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrossListedCourseSectionCollection;
use MiamiOH\Pike\Domain\Model\CrossListedCourseSection;
use MiamiOH\Pike\Domain\Repository\CrossListedCourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

class EloquentCrossListedCourseSectionRepository extends AbstractBaseRepository implements CrossListedCourseSectionRepositoryInterface
{
    public function getColectionByGuidCollectioin(
        CourseSectionGuidCollection $courseSectionGuids
    ): CrossListedCourseSectionCollection {
        $guidResult = $this->buildMainQuery()->whereIn(
            "g1.ssbsgid_guid",
            $courseSectionGuids->map(function (CourseSectionGuid $guid) {
                return (string)$guid;
            })
        )->get();
        $crossListedSectionCollection = new CrossListedCourseSectionCollection();
        foreach ($guidResult as $item) {
            $section = new CrossListedCourseSection(
                new Crn($item->ssbsect_crn),
                new SubjectCode($item->ssbsect_subj_code),
                $item->ssbsect_seq_numb,
                new CourseNumber($item->ssbsect_crse_numb),
                new CourseSectionGuid($item->guid),
                new CourseSectionGuid($item->host_guid)
            );
            $crossListedSectionCollection->push($section);
        }

        return $crossListedSectionCollection;
    }

    private function buildMainQuery(): Builder
    {
        return $this->getEloquentHandler()->table(EloquentHandler::raw("ssrxlst s1"))
            ->selectRaw("
                ssbsect_crn,
                ssbsect_seq_numb,
                ssbsect_subj_code,
                ssbsect_crse_numb,
                g2.ssbsgid_guid as guid,
                g1.ssbsgid_guid as host_guid
            ")
            ->join(EloquentHandler::raw("ssrxlst s2"), function (JoinClause $join) {
                $join->on('s1.ssrxlst_xlst_group', '=', 's2.ssrxlst_xlst_group')
                    ->on('s1.ssrxlst_term_code', '=', 's2.ssrxlst_term_code');
            })
            ->join(EloquentHandler::raw("ssbsgid g1"), function (JoinClause $join) {
                $join->on('g1.ssbsgid_term_code', '=', 's1.ssrxlst_term_code')
                    ->on('g1.ssbsgid_crn', '=', 's1.ssrxlst_crn');
            })
            ->join(EloquentHandler::raw("ssbsgid g2"), function (JoinClause $join) {
                $join->on('g2.ssbsgid_term_code', '=', 's2.ssrxlst_term_code')
                    ->on('g2.ssbsgid_crn', '=', 's2.ssrxlst_crn')
                    ->on("g2.ssbsgid_guid", "!=", "g1.ssbsgid_guid");
            })
            ->join("ssbsect", function (JoinClause $join) {
                $join->on('ssbsect_term_code', '=', 's2.ssrxlst_term_code')
                    ->on('ssbsect_crn', '=', 's2.ssrxlst_crn');
            });
    }
}//end class
