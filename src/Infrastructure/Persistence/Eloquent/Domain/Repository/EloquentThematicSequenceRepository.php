<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\ThematicSequenceCollection;
use MiamiOH\Pike\Domain\Model\ThematicSequence;
use MiamiOH\Pike\Domain\Repository\ThematicSequenceRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;
use MiamiOH\Pike\Exception\ThematicSequenceNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\ThematicSequence as EloquentThematicSequence;

class EloquentThematicSequenceRepository extends AbstractBaseRepository implements ThematicSequenceRepositoryInterface
{
    public function getAll(): ThematicSequenceCollection
    {
        $thematicSequences = $this->getEloquentHandler()->model(EloquentThematicSequence::class)
            ->orderByCode()
            ->get();

        return $this->constructThematicSequenceCollection($thematicSequences);
    }//end getAll()

    private function constructThematicSequenceCollection(
        EloquentCollection $thematicSequences
    ): ThematicSequenceCollection {
        $thematicSequenceCollection = new ThematicSequenceCollection();

        foreach ($thematicSequences as $thematicSequence) {
            $thematicSequenceCollection->push($this->constructThematicSequence($thematicSequence));
        }

        return $thematicSequenceCollection;
    }//end getByCode()

    public function constructThematicSequence(
        EloquentThematicSequence $eloquentModel
    ): ThematicSequence {
        return new ThematicSequence(
            new ThematicSequenceCode($eloquentModel->getCode()),
            $eloquentModel->getDescription()
        );
    }//end constructThematicSequenceCollection()

    public function getByCode(ThematicSequenceCode $code): ThematicSequence
    {
        $thematicSequence = $this->getEloquentHandler()->model(EloquentThematicSequence::class)
            ->codeEquals($code)
            ->first();

        if ($thematicSequence === null) {
            throw new ThematicSequenceNotFoundException();
        }

        return $this->constructThematicSequence($thematicSequence);
    }//end constructThematicSequence()
}//end class
