<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Query\Builder;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Repository\InstructorAssignmentRepositoryInterface;
use MiamiOH\Pike\Domain\Request\SearchInstructorAssignmentRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\InstructorAssignmentNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Sirasgn;

class EloquentInstructorAssignmentRepository extends AbstractBaseRepository implements InstructorAssignmentRepositoryInterface
{

    /**
     * @var Converter
     */
    private $converter;

    public function __construct(
        EloquentHandler $eloquentHandler,
        Converter $converter
    ) {
        parent::__construct($eloquentHandler);
        $this->converter = $converter;
    }


    /**
     * Get a collection of instructor assignment of a specified course section
     *
     * @param TermCode $termCode
     * @param Crn $crn
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByTermCodeCrn(
        TermCode $termCode,
        Crn $crn
    ): InstructorAssignmentCollection {
        $sirasgnCollection = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq()
            ->byTermCodeCrn($termCode, $crn)
            ->get();

        return $this->constructInstructorAssignmentCollection($sirasgnCollection);
    }

    private function constructInstructorAssignmentCollection(
        EloquentCollection $sirasgnCollection
    ) {
        $instructorAssignmentCollection = new InstructorAssignmentCollection();

        $sirasgnMap = [];
        /**
         * @var Sirasgn $sirasgn
         * */
        foreach ($sirasgnCollection as $sirasgn) {
            $uniqueId = $sirasgn->getUniqueId();
            $courseSectionGuid = $sirasgn->getCourseSectionGuid();

            if (!isset($sirasgnMap[$courseSectionGuid][$uniqueId]) || $sirasgn->getIsPrimaryFlag() === 'Y') {
                $sirasgnMap[$courseSectionGuid][$uniqueId] = $sirasgn;
            }
        }

        foreach ($sirasgnMap as $instructors) {
            foreach ($instructors as $sirasgn) {
                $instructorAssignmentCollection->push($this->constructInstructorAssignment($sirasgn));
            }
        }

        return $instructorAssignmentCollection;
    }

    private function constructInstructorAssignment(Sirasgn $sirasgn)
    {
        return new InstructorAssignment(
            new InstructorAssignmentGuid($sirasgn->getGuid()),
            new UniqueId($sirasgn->getUniqueId()),
            new TermCode($sirasgn->getTermCode()),
            new Crn($sirasgn->getCrn()),
            new CourseSectionGuid($sirasgn->getCourseSectionGuid()),
            $this->converter->yesNoIndicatorToBool($sirasgn->getIsPrimaryFlag())
        );
    }

    /**
     * Get an instructor assignment of a specified course section and instructor's
     * unique id
     *
     * @param UniqueId $uniqueId
     * @param TermCode $termCode
     * @param Crn $crn
     *
     * @return InstructorAssignment
     * @throws InstructorAssignmentNotFoundException
     */
    public function getByUniqueIdTermCodeCrn(
        UniqueId $uniqueId,
        TermCode $termCode,
        Crn $crn
    ): InstructorAssignment {
        $sirasgn = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq()
            ->byUniqueIdTermCodeCrn($uniqueId, $termCode, $crn)
            ->first();

        if ($sirasgn === null) {
            throw new InstructorAssignmentNotFoundException();
        }

        return $this->constructInstructorAssignment($sirasgn);
    }

    /**
     * Get a collection of instructor assignment that a specified instructor has
     * in a specified term
     *
     * @param UniqueId $uniqueId
     * @param TermCode $termCode
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByUniqueIdTermCode(
        UniqueId $uniqueId,
        TermCode $termCode
    ): InstructorAssignmentCollection {
        $sirasgnCollection = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq()
            ->byUniqueIdTermCode($uniqueId, $termCode)
            ->get();

        return $this->constructInstructorAssignmentCollection($sirasgnCollection);
    }

    /**
     * Get an instructor assignment by specified id
     *
     * @param InstructorAssignmentGuid $instructorAssignmentGuid
     *
     * @return InstructorAssignment
     * @throws InstructorAssignmentNotFoundException
     */
    public function getByGuid(
        InstructorAssignmentGuid $instructorAssignmentGuid
    ): InstructorAssignment {
        $sirasgn = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq()
            ->byGuid($instructorAssignmentGuid)
            ->first();

        if ($sirasgn === null) {
            throw new InstructorAssignmentNotFoundException();
        }

        return $this->constructInstructorAssignment($sirasgn);
    }

    /**
     * Get a collection of instructor assignment
     *
     * @param TermCodeCollection $termCodes
     * @param CrnCollection $crns
     * @param UniqueIdCollection $uniques
     *
     * @param Offset|null $offset
     * @param Limit|null $limit
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByTermCodesCrnsUniqueIds(
        TermCodeCollection $termCodes,
        CrnCollection $crns,
        UniqueIdCollection $uniques,
        Offset $offset = null,
        Limit $limit = null
    ): InstructorAssignmentCollection {
        /**
         * @var Sirasgn $sirasgn
         * @var Builder $sirasgn
         * */
        $sirasgn = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq();

        if (count($termCodes) > 0) {
            $sirasgn = $sirasgn->byTermCodes($termCodes);
        }

        if (count($uniques) > 0) {
            $sirasgn = $sirasgn->byUniqueIds($uniques);
        }

        if (count($crns) > 0) {
            $sirasgn = $sirasgn->byCrns($crns);
        }

        if ($offset !== null) {
            $sirasgn->offset($offset->getValue());
        }

        if ($limit !== null) {
            $sirasgn->limit($limit->getValue());
        }

        $sfrstcrCollection = $sirasgn->get();

        return $this->constructInstructorAssignmentCollection($sfrstcrCollection);
    }

    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $guidCollection
    ): InstructorAssignmentCollection {
        $sirasgnCollection = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq()
            ->byCourseSectionGuids($guidCollection)
            ->get();

        return $this->constructInstructorAssignmentCollection($sirasgnCollection);
    }

    public function getCollectionByCourseSectionGuidsUniqueIds(
        CourseSectionGuidCollection $guidCollection,
        UniqueIdCollection $uniqueIds
    ): InstructorAssignmentCollection {
        $sirasgnCollection = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq()
            ->byCourseSectionGuids($guidCollection)
            ->byUniqueIds($uniqueIds)->get();

        return $this->constructInstructorAssignmentCollection($sirasgnCollection);
    }

    public function search(
        SearchInstructorAssignmentRequest $searchRequest
    ): InstructorAssignmentCollection {
        /**
         * @var Builder|Sirasgn $query
         * */
        $query = $this->getEloquentHandler()
            ->model(Sirasgn::class)
            ->joinSsbsect()
            ->joinSzbuniq();

        $termCodeCollection = $searchRequest->getTermCodeCollection();
        if ($termCodeCollection !== null) {
            $query->byTermCodes($termCodeCollection);
        }

        $crnCollection = $searchRequest->getCrnCollection();
        if ($crnCollection !== null) {
            $query->byCrns($crnCollection);
        }

        $uniqueIdCollection = $searchRequest->getUniqueIdCollection();
        if ($uniqueIdCollection !== null) {
            $query->byUniqueIds($uniqueIdCollection);
        }

        $courseSectionGuidCollection = $searchRequest->getCourseSectionGuidCollection();
        if ($courseSectionGuidCollection !== null) {
            $query->byCourseSectionGuids($courseSectionGuidCollection);
        }

        $query->offset($searchRequest->getOffset()->getValue());
        $query->limit($searchRequest->getLimit()->getValue());

        $totalNumOfRecords = $query->count();
        /** @var \Illuminate\Database\Eloquent\Collection $sirasgnCollection */
        $sirasgnCollection = $query->get();

        $assignmentCollection = $this->constructInstructorAssignmentCollection($sirasgnCollection);
        $assignmentCollection->setTotalNumOfItems($totalNumOfRecords);
        return $assignmentCollection;
    }
}//end class
