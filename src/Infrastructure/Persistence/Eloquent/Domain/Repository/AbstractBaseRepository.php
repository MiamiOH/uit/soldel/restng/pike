<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 2:04 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

abstract class AbstractBaseRepository
{
    /**
     * @var EloquentHandler
     */
    protected $eloquentHandler;


    /**
     * BaseEloquentImplementation constructor.
     *
     * @param EloquentHandler $eloquentHandler
     */
    public function __construct(EloquentHandler $eloquentHandler)
    {
        $this->eloquentHandler = $eloquentHandler;
    }//end __construct()


    /**
     * @return EloquentHandler
     */
    public function getEloquentHandler(): EloquentHandler
    {
        return $this->eloquentHandler;
    }//end getEloquentHandler()
}//end class
