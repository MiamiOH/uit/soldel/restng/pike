<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseCollection;
use MiamiOH\Pike\Domain\Collection\CourseGuidTermCodeCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Repository\CourseRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseGuidTermCode;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\SchoolCode;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Scbcrse;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Scbdesc;

class EloquentCourseRepository extends AbstractBaseRepository implements CourseRepositoryInterface
{
    /**
     * @var Converter
     */
    private $converter;

    public function __construct(
        EloquentHandler $eloquentHandler,
        Converter $converter
    ) {
        parent::__construct($eloquentHandler);
        $this->converter = $converter;
    }

    public function getCollectionByCourseGuidTermCodeCollection(
        CourseGuidTermCodeCollection $collection
    ): CourseCollection {
        $courseGuids = $collection->map(function (
            CourseGuidTermCode $courseGuidTermCode
        ) {
            return (string)$courseGuidTermCode->getCourseGuid();
        })->toArray();

        $scbcrseCollection = $this->getEloquentHandler()->model(Scbcrse::class)
            ->with(['school', 'department', 'subject'])
            ->byCourseGuids($courseGuids)
            ->get();

        $scbdescCollection = $this->getEloquentHandler()->model(Scbdesc::class)
            ->byCourseGuids($courseGuids)
            ->get();

        $scbcrseLookup = [];
        /**
         * @var Scbcrse $scbcrse
         * */
        foreach ($scbcrseCollection as $scbcrse) {
            $scbcrseLookup[$scbcrse->getGuid()] = $scbcrse;
        }

        $courseCollection = new CourseCollection();

        /**
         * @var CourseGuidTermCode $item
         * @var Scbdesc $scbdesc
         * */
        foreach ($collection as $item) {
            $courseGuid = (string)$item->getCourseGuid();
            $scbcrse = $scbcrseLookup[$courseGuid];
            /**
             * @var Scbdesc|null $description
             * */
            $description = null;
            foreach ($scbdescCollection as $scbdesc) {
                if ($scbdesc->getCourseGuid() === $courseGuid
                    && $this->isTermInRange(
                        (string)$item->getTermCode(),
                        $scbdesc->getStartTerm(),
                        $scbdesc->getEndTerm()
                    )
                ) {
                    if ($description === null || $this->isTermGreaterThan(
                        $scbdesc->getStartTerm(),
                        $description->getStartTerm()
                    )) {
                        $description = $scbdesc;
                    }
                }
            }
            $courseCollection->push($this->constructCourse(
                $scbcrse,
                $item->getTermCode(),
                $description === null ? null : $description->getDescription()
            ));
        }

        return $courseCollection;
    }

    private function isTermGreaterThan(?string $term1, ?string $term2): bool
    {
        if ($term1 !== null) {
            $term1 = intval($term1);
        }
        if ($term2 !== null) {
            $term2 = intval($term2);
        }

        if ($term1 === null) {
            return false;
        } else {
            if ($term2 === null) {
                return true;
            } else {
                return $term1 > $term2;
            }
        }
    }

    private function isTermInRange(string $term, ?string $start, ?string $end): bool
    {
        $term = intval($term);
        if ($start !== null) {
            $start = intval($start);
        }
        if ($end !== null) {
            $end = intval($end);
        }
        if ($start === null) {
            if ($end === null) {
                return false;
            } else {
                return $term <= $end;
            }
        } else {
            if ($end === null) {
                return $term >= $start;
            } else {
                return $term >= $start && $term <= $end;
            }
        }
    }

    private function constructCourse(
        Scbcrse $scbcrse,
        TermCode $termCode,
        ?string $description
    ) {
        $creditHourDescription = $this->createCreditHourDescription(
            $scbcrse->getCreditHoursHigh(),
            $scbcrse->getCreditHoursLow(),
            $scbcrse->getCreditHourIndicator()
        );

        return new Course(
            new CourseGuid($scbcrse->getGuid()),
            $termCode,
            new SubjectCode($scbcrse->getSubjectCode()),
            new CourseNumber($scbcrse->getNumber()),
            new TermCode($scbcrse->getTermEffectedCode()),
            $scbcrse->getTitle(),
            $description,
            new SchoolCode($scbcrse->getSchoolCode()),
            $scbcrse->getSchoolName(),
            new DepartmentCode($scbcrse->getDepartmentCode()),
            $scbcrse->getDepartmentName(),
            $scbcrse->getSubjectDescription(),
            $this->converter->stringToFloat($scbcrse->getCreditHoursLow()),
            $this->converter->stringToFloat($scbcrse->getCreditHoursHigh() ? $scbcrse->getCreditHoursHigh() : $scbcrse->getCreditHoursLow()),
            $this->converter->stringToFloat($scbcrse->getLectureHoursLow()),
            $this->converter->stringToFloat($scbcrse->getLectureHoursHigh()),
            $this->converter->stringToFloat($scbcrse->getLabHoursLow()),
            $this->converter->stringToFloat($scbcrse->getLabHoursHigh()),
            $creditHourDescription,
            $this->createLabHourDescription(
                $scbcrse->getLabHoursLow(),
                $scbcrse->getLabHoursHigh(),
                $scbcrse->getLabHoursIndicator()
            ),
            $this->createLectureHourDescription(
                $scbcrse->getLectureHoursLow(),
                $scbcrse->getLectureHoursHigh(),
                $scbcrse->getLectureHoursIndicator()
            ),
            $this->getCreditHoursAvailable($creditHourDescription)
        );
    }

    private function createCreditHourDescription(
        ?string $creditHoursHigh,
        ?string $creditHoursLow,
        ?string $creditHourIndicator
    ) {
        if ($creditHoursHigh === null) {
            return $creditHoursLow;
        } else {
            $connectString = $creditHourIndicator ? strtolower($creditHourIndicator) : '-';

            return $creditHoursLow . ' ' . $connectString . ' ' . $creditHoursHigh;
        }
    }

    private function createLabHourDescription(
        ?string $labHoursLow,
        ?string $labHoursHigh,
        ?string $labHoursIndicator
    ) {
        if ($labHoursHigh === null) {
            return $labHoursLow;
        } else {
            $connectString = $labHoursIndicator ? strtolower($labHoursIndicator) : '-';

            return $labHoursLow . ' ' . $connectString . ' ' . $labHoursHigh;
        }
    }

    private function createLectureHourDescription(
        ?string $lectureHoursLow,
        ?string $lectureHoursHigh,
        ?string $lectureHoursIndicator
    ) {
        if ($lectureHoursHigh === null) {
            return $lectureHoursLow;
        } else {
            $connectString = $lectureHoursIndicator ? strtolower($lectureHoursIndicator) : '-';

            return $lectureHoursLow . ' ' . $connectString . ' ' . $lectureHoursHigh;
        }
    }

    private function getCreditHoursAvailable(
        String $creditHourDescription = null
    ): array {
        /**
         * @var float
         */
        $stepForRange = 0.5;
        if (false !== stripos(
            $creditHourDescription,
            "to"
        ) && false !== stripos($creditHourDescription, "or")) {
            return array($this->converter->stringToFloat($creditHourDescription));
        }
        if (false !== stripos($creditHourDescription, "to")) {
            list($minCreditHours, $maxCreditHours) = explode(
                "to",
                $creditHourDescription
            );

            return range($minCreditHours, $maxCreditHours, $stepForRange);
        }
        if (false !== stripos($creditHourDescription, "-")) {
            list($minCreditHours, $maxCreditHours) = explode(
                "-",
                $creditHourDescription
            );

            return range($minCreditHours, $maxCreditHours, $stepForRange);
        }
        if (false !== stripos($creditHourDescription, "or")) {
            list($lowerBound, $upperBound) = explode("or", $creditHourDescription);

            return array(
                $this->converter->stringToFloat($lowerBound),
                $this->converter->stringToFloat($upperBound)
            );
        }

        return array($this->converter->stringToFloat($creditHourDescription));
    }
}//end class
