<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Repository\SearchCourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionRequest;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

class EloquentSearchCourseSectionRepository extends AbstractBaseRepository implements SearchCourseSectionRepositoryInterface
{
    public function search(
        SearchCourseSectionRequest $searchRequest
    ): CourseSectionGuidCollection {
        $searchQuery = $this->getEloquentHandler()
            ->table('ssbsect')
            ->select('ssbsgid_guid')
            ->whereNotNull('ssbsect_ptrm_code')
            ->join(
                'ssbsgid',
                'ssbsgid_domain_surrogate_id',
                '=',
                'ssbsect_surrogate_id'
            );

        $groups = ['ssbsgid_guid'];

        if ($searchRequest->getTermCodeCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsect_term_code',
                $searchRequest->getTermCodeCollection()->toValueArray()
            );
        }

        if ($searchRequest->getCampusCodeCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsect_camp_code',
                $searchRequest->getCampusCodeCollection()->toValueArray()
            );
        }

        if ($searchRequest->getCrnCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsect_crn',
                $searchRequest->getCrnCollection()->toValueArray()
            );
        }

        if ($searchRequest->getSubjectCodeCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsect_subj_code',
                $searchRequest->getSubjectCodeCollection()->toValueArray()
            );
        }

        if ($searchRequest->getCourseNumberCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsect_crse_numb',
                $searchRequest->getCourseNumberCollection()->toValueArray()
            );
        }

        if ($searchRequest->getCourseSectionCodes() !== null) {
            $courseSectionCodes = $searchRequest->getCourseSectionCodes();
            $searchQuery->whereIn(
                EloquentHandler::raw('trim(ssbsect_seq_numb)'),
                $courseSectionCodes
            );
        }

        if ($searchRequest->getCourseSectionStatusCodeCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsect_ssts_code',
                $searchRequest->getCourseSectionStatusCodeCollection()->toValueArray()
            );
        }

        if ($searchRequest->getNumOfMaxEnrollmentsCount() !== null) {
            $searchQuery->whereBetween('ssbsect_max_enrl', [$searchRequest->getNumOfMaxEnrollments(),
                $searchRequest->getNumOfMaxEnrollmentsCount()]);
        } elseif ($searchRequest->getNumOfMaxEnrollments() !== null) {
            $searchQuery->where('ssbsect_max_enrl', '=', $searchRequest->getNumOfMaxEnrollments());
        }

        if ($searchRequest->getNumOfCurrentEnrollmentsCount() !== null) {
            $searchQuery->whereRaw("
            (
              select count(sfrstcr_pidm) 
              from sfrstcr 
              where sfrstcr_term_code = ssbsect_term_code
              and sfrstcr_crn = ssbsect_crn
              and sfrstcr_rsts_code in 
              (
                select stvrsts_code
                from stvrsts
                where stvrsts_incl_sect_enrl = 'Y'
              )
            ) between ? and ?
            ", [$searchRequest->getNumOfCurrentEnrollments(),$searchRequest->getNumOfCurrentEnrollmentsCount()]);
        } elseif ($searchRequest->getNumOfCurrentEnrollments() !== null) {
            $searchQuery->whereRaw("
            (
              select count(sfrstcr_pidm) 
              from sfrstcr 
              where sfrstcr_term_code = ssbsect_term_code
              and sfrstcr_crn = ssbsect_crn
              and sfrstcr_rsts_code in 
              (
                select stvrsts_code
                from stvrsts
                where stvrsts_incl_sect_enrl = 'Y'
              )
            ) = ?
            ", $searchRequest->getNumOfCurrentEnrollments());
        }

        if ($searchRequest->getNumOfActiveEnrollmentsCount() !== null) {
            $searchQuery->whereRaw("
            (
              select count(sfrstcr_pidm) 
              from sfrstcr 
              where sfrstcr_term_code = ssbsect_term_code
              and sfrstcr_crn = ssbsect_crn
              and sfrstcr_rsts_code in 
              (
                select stvrsts_code
                from stvrsts
                where stvrsts_incl_sect_enrl = 'Y'
                and stvrsts_withdraw_ind = 'N'
              )
            ) between ? and ?
            ", [$searchRequest->getNumOfActiveEnrollments(),$searchRequest->getNumOfActiveEnrollmentsCount()]);
        } elseif ($searchRequest->getNumOfActiveEnrollments() !== null) {
            $searchQuery->whereRaw("
            (
              select count(sfrstcr_pidm) 
                from sfrstcr 
               where sfrstcr_term_code = ssbsect_term_code
                 and sfrstcr_crn = ssbsect_crn
                 and sfrstcr_rsts_code in 
              (
                select stvrsts_code
                  from stvrsts
                 where stvrsts_incl_sect_enrl = 'Y'
                   and stvrsts_withdraw_ind = 'N'
              )
            ) = ?
            ", $searchRequest->getNumOfActiveEnrollments());
        }

        if ($searchRequest->getNumOfAvailableEnrollmentsCount() !== null) {
            $searchQuery->whereBetween('ssbsect_seats_avail', [$searchRequest->getNumOfAvailableEnrollments(),
                $searchRequest->getNumOfAvailableEnrollmentsCount()]);
        } elseif ($searchRequest->getNumOfAvailableEnrollments() !== null) {
            $searchQuery->where('ssbsect_seats_avail', '=', $searchRequest->getNumOfAvailableEnrollments());
        }

        if ($searchRequest->getHasSeatAvailable() !== null) {
            if ($searchRequest->getHasSeatAvailable() === true) {
                $searchQuery->where(
                    'ssbsect_seats_avail',
                    '>',
                    0
                );
            }
        }

        if ($searchRequest->getPartOfTermCodeCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsect_ptrm_code',
                $searchRequest->getPartOfTermCodeCollection()->toValueArray()
            );
        }

        if ($searchRequest->getPartOfTermStartDateEnd() !== null) {
            $searchQuery->whereBetween('ssbsect_ptrm_start_date', [$searchRequest->getPartOfTermStartDate(),
                $searchRequest->getPartOfTermStartDateEnd()]);
        } elseif ($searchRequest->getPartOfTermStartDate() !== null) {
            $searchQuery->whereDate(
                'ssbsect_ptrm_start_date',
                '=',
                $searchRequest->getPartOfTermStartDate()
            );
        }

        if ($searchRequest->getPartOfTermEndDateEnd() !== null) {
            $searchQuery->whereBetween('ssbsect_ptrm_end_date', [$searchRequest->getPartOfTermEndDate(),
                $searchRequest->getPartOfTermEndDateEnd()]);
        } elseif ($searchRequest->getPartOfTermEndDate() !== null) {
            $searchQuery->whereDate(
                'ssbsect_ptrm_end_date',
                '=',
                $searchRequest->getPartOfTermEndDate()
            );
        }

        if ($searchRequest->getScheduleStartDate() !== null
            || $searchRequest->getScheduleEndDate() !== null
            || $searchRequest->getScheduleStartTime() !== null
            || $searchRequest->getScheduleEndTime() !== null
            || $searchRequest->getScheduleBuildingCodeCollection() !== null
            || $searchRequest->getScheduleRooms() !== null
            || $searchRequest->getScheduleDays() !== null
        ) {
            $searchQuery->leftJoin('ssrmeet', function (JoinClause $join) {
                $join->on('ssbsect_crn', '=', 'ssrmeet_crn')
                    ->on('ssbsect_term_code', '=', 'ssrmeet_term_code');
            });

            $groups[] = 'ssrmeet_crn';
            $groups[] = 'ssrmeet_term_code';

            if ($searchRequest->getScheduleStartDateEnd() !== null) {
                $searchQuery->whereBetween('ssrmeet_start_date', [$searchRequest->getScheduleStartDate(),
                    $searchRequest->getScheduleStartDateEnd()]);
            } elseif ($searchRequest->getScheduleStartDate() !== null) {
                $searchQuery->whereDate(
                    'ssrmeet_start_date',
                    '=',
                    $searchRequest->getScheduleStartDate()
                );
            }

            if ($searchRequest->getScheduleEndDateEnd() !== null) {
                $searchQuery->whereBetween('ssrmeet_end_date', [$searchRequest->getScheduleEndDate(),
                    $searchRequest->getScheduleEndDateEnd()]);
            } elseif ($searchRequest->getScheduleEndDate() !== null) {
                $searchQuery->whereDate(
                    'ssrmeet_end_date',
                    '=',
                    $searchRequest->getScheduleEndDate()
                );
            }

            if ($searchRequest->getScheduleStartTimeEnd() !== null) {
                $searchQuery->whereBetween('ssrmeet_begin_time', [$searchRequest->getScheduleStartTime()->format('Hi'),
                    $searchRequest->getScheduleStartTimeEnd()->format('Hi')]);
            } elseif ($searchRequest->getScheduleStartTime() !== null) {
                $searchQuery->where(
                    'ssrmeet_begin_time',
                    '=',
                    $searchRequest->getScheduleStartTime()->format('Hi')
                );
            }

            if ($searchRequest->getScheduleEndTimeEnd() !== null) {
                $searchQuery->whereBetween('ssrmeet_end_time', [$searchRequest->getScheduleEndTime()->format('Hi'),
                    $searchRequest->getScheduleEndTimeEnd()->format('Hi')]);
            } elseif ($searchRequest->getScheduleEndTime() !== null) {
                $searchQuery->where(
                    'ssrmeet_end_time',
                    '=',
                    $searchRequest->getScheduleEndTime()->format('Hi')
                );
            }

            if ($searchRequest->getScheduleBuildingCodeCollection() !== null) {
                $searchQuery->whereIn(
                    'ssrmeet_bldg_code',
                    $searchRequest->getScheduleBuildingCodeCollection()->toValueArray()
                );
            }

            if ($searchRequest->getScheduleRooms() !== null) {
                $searchQuery->whereIn(
                    'ssrmeet_room_code',
                    $searchRequest->getScheduleRooms()
                );
            }

            if ($searchRequest->getScheduleDays() !== null) {
                $days = $searchRequest->getScheduleDays();

                if ($days->hasMonday()) {
                    $searchQuery->whereNotNull('ssrmeet_mon_day');
                }
                if ($days->hasTuesday()) {
                    $searchQuery->whereNotNull('ssrmeet_tue_day');
                }
                if ($days->hasWednesday()) {
                    $searchQuery->whereNotNull('ssrmeet_wed_day');
                }
                if ($days->hasThursday()) {
                    $searchQuery->whereNotNull('ssrmeet_thu_day');
                }
                if ($days->hasFriday()) {
                    $searchQuery->whereNotNull('ssrmeet_fri_day');
                }
                if ($days->hasSaturday()) {
                    $searchQuery->whereNotNull('ssrmeet_sat_day');
                }
            }
        }

        if ($searchRequest->getInstructorUniqueIdCollection() !== null) {
            $searchQuery->join('sirasgn', function (JoinClause $join) {
                $join->on('sirasgn_crn', '=', 'ssbsect_crn')
                    ->on('sirasgn_term_code', '=', 'ssbsect_term_code');
            })
                ->join('szbuniq', 'szbuniq_pidm', '=', 'sirasgn_pidm');

            $groups[] = 'sirasgn_crn';
            $groups[] = 'sirasgn_term_code';

            $searchQuery->whereIn(
                'szbuniq_unique_id',
                $searchRequest->getInstructorUniqueIdCollection()->toUpperCaseArray()
            );
        }

        if ($searchRequest->getCourseSectionGuidCollection() !== null) {
            $searchQuery->whereIn(
                'ssbsgid_guid',
                $searchRequest->getCourseSectionGuidCollection()->toValueArray()
            );
        }

        if ($searchRequest->isMidtermGradeSubmissionAvailable() !== null
            || $searchRequest->isFinalGradeSubmissionAvailable() !== null
        ) {
            $searchQuery->leftJoin('sobptrm', function (JoinClause $join) {
                $join->on('sobptrm_term_code', '=', 'ssbsect_term_code')
                    ->on('sobptrm_ptrm_code', '=', 'ssbsect_ptrm_code');
            });

            $groups[] = 'sobptrm_term_code';
            $groups[] = 'sobptrm_ptrm_code';

            if ($searchRequest->isMidtermGradeSubmissionAvailable() === true) {
                $searchQuery->where('sobptrm_mgrd_web_upd_ind', '=', 'Y');
            }

            if ($searchRequest->isFinalGradeSubmissionAvailable() === true) {
                $searchQuery->where('sobptrm_fgrd_web_upd_ind', '=', 'Y');
            }
        }

        $groups[] = 'ssbsect_term_code';
        $groups[] = 'ssbsect_subj_code';
        $groups[] = 'ssbsect_crse_numb';
        $groups[] = 'ssbsect_seq_numb';

        $query = $searchQuery
            ->groupBy($groups);
        $totalNumOfItems = intval($this->getEloquentHandler()
            ->select(
                'select count(1) as num from (' . $query->toSql() . ')',
                $query->getBindings()
            )[0]->num);

        $results = $query
            ->orderBy('ssbsect_term_code')
            ->orderBy('ssbsect_subj_code')
            ->orderBy('ssbsect_crse_numb')
            ->orderBy('ssbsect_seq_numb')
            ->offset($searchRequest->getOffset())
            ->limit($searchRequest->getLimit())
            ->get();

        $courseSectionGuids = $results->map(function ($obj) {
            return $obj->ssbsgid_guid;
        })->toArray();

        $courseSectionGuidCollection = CourseSectionGuidCollection::createFromValueArray($courseSectionGuids);

        $courseSectionGuidCollection->setTotalNumOfItems($totalNumOfItems);

        return $courseSectionGuidCollection;
    }
}//end class
