<?php
/**
 * Author: liaom
 * Date: 5/21/18
 * Time: 1:18 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Carbon\Carbon;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidUniqueIdCollection;
use MiamiOH\Pike\Domain\Collection\GradeModeCodeCollection;
use MiamiOH\Pike\Domain\Collection\StudentLevelCodeCollection;
use MiamiOH\Pike\Domain\Collection\UpdateGradeRequestCollection;
use MiamiOH\Pike\Domain\Collection\UpdateGradeResponseCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\GradeRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\UpdateGradeRepositoryInterface;
use MiamiOH\Pike\Domain\Request\UpdateGradeRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuidUniqueId;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\UpdateGradeResponse;
use MiamiOH\Pike\Exception\Exception;
use MiamiOH\Pike\Exception\InvalidGradeValueException;
use MiamiOH\Pike\Exception\UpdateGradeException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Grade as EloquentGrade;

class EloquentUpdateGradeRepository extends AbstractBaseRepository implements UpdateGradeRepositoryInterface
{

    /**
     * @var CourseSectionRepositoryInterface
     */
    private $courseSectionRepository;
    /**
     * @var CourseSectionEnrollmentRepositoryInterface
     */
    private $courseSectionEnrollmentRepository;
    /**
     * @var Converter
     */
    private $converter;
    /**
     * @var GradeRepositoryInterface
     */
    private $gradeRepository;

    public function __construct(
        EloquentHandler $eloquentHandler,
        CourseSectionRepositoryInterface $courseSectionRepository,
        CourseSectionEnrollmentRepositoryInterface $enrollmentRepository,
        Converter $converter,
        GradeRepositoryInterface $gradeRepository
    ) {
        parent::__construct($eloquentHandler);
        $this->courseSectionRepository = $courseSectionRepository;
        $this->courseSectionEnrollmentRepository = $enrollmentRepository;
        $this->converter = $converter;
        $this->gradeRepository = $gradeRepository;
    }

    public function bulkUpdate(
        UpdateGradeRequestCollection $requestCollection
    ): UpdateGradeResponseCollection {
        if (count($requestCollection) === 0) {
            return new UpdateGradeResponseCollection();
        }

        $courseSectionGuidCollection = new CourseSectionGuidCollection();

        /** @var UpdateGradeRequest $request */
        foreach ($requestCollection as $request) {
            $courseSectionGuidCollection->push($request->getCourseSectionGuid());
        }

        $courseSectionCollection = new CourseSectionCollection();
        if (count($courseSectionGuidCollection) !== 0) {
            $courseSectionCollection = $this->courseSectionRepository
                ->getByGuids($courseSectionGuidCollection);
        }

        $courseSectionLookupByGuid = [];

        /** @var CourseSection $courseSection */
        foreach ($courseSectionCollection as $courseSection) {
            $courseSectionLookupByGuid[(string)$courseSection->getGuid()] = $courseSection;
        }

        $courseSectionGuidUniqueIdCollection = new CourseSectionGuidUniqueIdCollection();

        /** @var UpdateGradeRequest $request */
        foreach ($requestCollection as $request) {
            $courseSectionGuidUniqueIdCollection->push(
                new CourseSectionGuidUniqueId(
                    $request->getCourseSectionGuid(),
                    $request->getStudentUniqueId()
                )
            );
        }

        $enrollmentCollection = $this->courseSectionEnrollmentRepository
            ->getCollectionByCourseSectionGuidUniqueIdCollection($courseSectionGuidUniqueIdCollection);

        $enrollmentLookupByGuidUniqueId = [];
        $gradeModeCodeCollection = new GradeModeCodeCollection();
        $studentLevelCodeCollection = new StudentLevelCodeCollection();
        /** @var CourseSectionEnrollment $enrollment */
        foreach ($enrollmentCollection as $enrollment) {
            $gradeModeCodeCollection->push($enrollment->getGradeModeCode());
            $studentLevelCodeCollection->push($enrollment->getStudentLevelCode());
            if (!isset($enrollmentLookupByGuidUniqueId[(string)$enrollment->getCourseSectionGuid()])) {
                $enrollmentLookupByGuidUniqueId[(string)$enrollment->getCourseSectionGuid()] = [];
            }
            $enrollmentLookupByGuidUniqueId[(string)$enrollment->getCourseSectionGuid()][$enrollment->getUniqueId()->getUppercaseValue()] = $enrollment;
        }

        $gradeValueValidationData = $this->getGradeValueValidationData(
            $gradeModeCodeCollection,
            $studentLevelCodeCollection
        );

        $eloquentGradeCollection = $this->getEloquentHandler()
            ->model(EloquentGrade::class)
            ->byCourseSectionGuidUniqueIdCollection($courseSectionGuidUniqueIdCollection)
            ->get();
        $eloquentGradeLookup = [];
        /** @var EloquentGrade $eloquentGrade */
        foreach ($eloquentGradeCollection as $eloquentGrade) {
            if (!isset($eloquentGradeLookup[$eloquentGrade->getCourseSectionGuid()])) {
                $eloquentGradeLookup[$eloquentGrade->getCourseSectionGuid()] = [];
            }
            $eloquentGradeLookup[$eloquentGrade->getCourseSectionGuid()][$eloquentGrade->getUniqueId()] = $eloquentGrade;
        }

        $responseCollection = new UpdateGradeResponseCollection();

        /** @var UpdateGradeRequest $request */
        foreach ($requestCollection as $index => $request) {
            try {
                $courseSectionGuid = (string)$request->getCourseSectionGuid();
                $uniqueId = $request->getStudentUniqueId()->getUppercaseValue();

                if (!isset($courseSectionLookupByGuid[$courseSectionGuid])) {
                    throw new UpdateGradeException('Course section not found');
                }

                if (!isset($enrollmentLookupByGuidUniqueId[$courseSectionGuid][$uniqueId])) {
                    throw new UpdateGradeException('Student does not enroll in this course section');
                }

                $this->updateHelper(
                    $request,
                    $courseSectionLookupByGuid[$courseSectionGuid],
                    $enrollmentLookupByGuidUniqueId[$courseSectionGuid][$uniqueId],
                    $gradeValueValidationData,
                    $eloquentGradeLookup[$courseSectionGuid][$uniqueId]
                );

                $responseCollection[$index] = new UpdateGradeResponse(
                    $index,
                    true,
                    'Ok'
                );
            } catch (\Exception $e) {
                $responseCollection[$index] = new UpdateGradeResponse(
                    $index,
                    false,
                    $e->getMessage()
                );
            }
        }

        return $responseCollection;
    }

    /**
     * @param UpdateGradeRequest $request
     * @param CourseSection $courseSection
     * @param CourseSectionEnrollment $enrollment
     * @param array $gradeValueValidationData
     * @param EloquentGrade $eloquentGrade
     *
     * @return bool
     * @throws Exception
     * @throws UpdateGradeException
     */
    private function updateHelper(
        UpdateGradeRequest $request,
        CourseSection $courseSection,
        CourseSectionEnrollment $enrollment,
        array $gradeValueValidationData,
        EloquentGrade $eloquentGrade
    ): bool {
        $gradeValue = $request->getGradeValue();
        $gradeType = $request->getGradeType();
        $instructorUniqueId = $request->getInstructorUniqueId();
        $hasAttended = $request->getHasAttended();
        $lastAttendDate = $request->getLastAttendDate();

        if ($hasAttended === false && $lastAttendDate !== null) {
            throw new UpdateGradeException('Last date of attendance and Never Attended can not be set at the same time. You can either choose never attended or enter a last date of attendance');
        }

        if (!$enrollment->isGradeSubmissionEligible()) {
            throw new UpdateGradeException('A grade can only be submitted for the active registration status of "Registered"(RE) and "Web Registered"(RW). OR Grade has already been rolled into academic history and prohibit to change');
        }

        $isPassed = $this->isPassed(
            $gradeValueValidationData,
            $enrollment->getGradeModeCode(),
            $enrollment->getStudentLevelCode(),
            $gradeValue
        );

        if ($isPassed === null) {
            throw new InvalidGradeValueException();
        }

        $isGradeSubmissionAvailable = false;

        if ($gradeType->isMidterm()) {
            $isGradeSubmissionAvailable = $courseSection->isMidtermGradeSubmissionAvailable();
        } elseif ($gradeType->isFinal()) {
            $isGradeSubmissionAvailable = $courseSection->isFinalGradeSubmissionAvailable();
        }

        if ($isGradeSubmissionAvailable === false) {
            throw new UpdateGradeException('Grade submission is not opened now');
        }

        if ($isPassed) {
            if ($hasAttended === false) {
                throw new UpdateGradeException('You can\'t set the hasAttended to \'false\' for a passing grade');
            }

            if ($lastAttendDate !== null) {
                throw new UpdateGradeException('You can\'t set the last attendance date for a passing grade');
            }

            $eloquentGrade->setGrade(
                $gradeType,
                $gradeValue,
                $instructorUniqueId,
                null,
                null
            );
        } else {
            if ($hasAttended !== false && $lastAttendDate === null && $gradeType->isMidterm()) {
                $eloquentGrade->setGrade(
                    $gradeType,
                    $gradeValue,
                    $instructorUniqueId,
                    null,
                    null
                );
            } elseif ($hasAttended === false && $lastAttendDate === null && $gradeType->isFinal()) {
                $eloquentGrade->setGrade(
                    $gradeType,
                    $gradeValue,
                    $instructorUniqueId,
                    0,
                    null
                );
            } elseif ($hasAttended !== false && $lastAttendDate !== null && $gradeType->isFinal()) {
                if (!$this->isLastAttendDateValid($courseSection, $lastAttendDate)) {
                    throw new UpdateGradeException('Last date of attendance is not a valid date. The date must be within the range of section(course) start and end dates');
                }
                $eloquentGrade->setGrade(
                    $gradeType,
                    $gradeValue,
                    $instructorUniqueId,
                    null,
                    $lastAttendDate
                );
            } elseif ($hasAttended !== false && $lastAttendDate === null && $gradeType->isFinal()) {
                throw new UpdateGradeException('You need to enter either the last date of attendance or check the never attended flag for a failing grade');
            }
        }

        return true;
    }

    private function getCourseSectionByGuid(CourseSectionGuid $courseSectionGuid)
    {
        return $this->courseSectionRepository->getByGuid($courseSectionGuid);
    }

    private function getGradeValueValidationData(
        GradeModeCodeCollection $gradeModeCodeCollection,
        StudentLevelCodeCollection $studentLevelCodeCollection
    ): array {
        $results = $this->getEloquentHandler()
            ->table('szvgrde')
            ->whereIn(
                'szvgrde_level_code',
                $studentLevelCodeCollection->toValueArray()
            )
            ->whereIn(
                'szvgrde_gmod_code',
                $gradeModeCodeCollection->toValueArray()
            )
            ->get();

        $data = [];

        foreach ($results as $result) {
            $levelCode = $result->szvgrde_level_code;
            $gradeModeCode = $result->szvgrde_gmod_code;
            $passedIndicator = $result->szvgrde_passed_ind;
            $gradeValue = $result->szvgrde_grde_code;
            if (!isset($data[$levelCode])) {
                $data[$levelCode] = [];
            }
            if (!isset($data[$levelCode][$gradeModeCode])) {
                $data[$levelCode][$gradeModeCode] = [];
            }
            $data[$levelCode][$gradeModeCode][$gradeValue] = $this->converter->yesNoIndicatorToBool($passedIndicator);
        }

        return $data;
    }

    private function isPassed(
        array $validationData,
        GradeModeCode $gradeModeCode,
        StudentLevelCode $studentLevelCode,
        GradeValue $gradeValue
    ): ?bool {
        return $validationData[(string)$studentLevelCode][(string)$gradeModeCode][(string)$gradeValue] ?? null;
    }

    private function isLastAttendDateValid(
        CourseSection $courseSection,
        Carbon $lastAttendDate
    ) {
        if ($lastAttendDate->between(
            $courseSection->getPartOfTerm()->getStartDate(),
            $courseSection->getPartOfTerm()->getEndDate()
        )) {
            return true;
        }

        return false;
    }
}
