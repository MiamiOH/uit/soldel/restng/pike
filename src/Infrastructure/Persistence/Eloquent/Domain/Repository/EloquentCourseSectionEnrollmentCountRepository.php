<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 9:56 AM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentCount;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentCountRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\CourseSectionEnrollmentCountEntity;

class EloquentCourseSectionEnrollmentCountRepository extends AbstractBaseRepository implements CourseSectionEnrollmentCountRepositoryInterface
{
    private $converter;

    public function __construct(
        EloquentHandler $eloquentHandler,
        Converter $converter
    ) {
        parent::__construct($eloquentHandler);
        $this->converter = $converter;
    }


    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $courseSectionGuidCollection
    ): CourseSectionEnrollmentCountCollection {
        $enrollmentCountCollection = $this->getEloquentHandler()
            ->model(CourseSectionEnrollmentCountEntity::class)
            ->byCourseSectionGuids($courseSectionGuidCollection)
            ->get();

        return $this->constructCourseSectionEnrollmentCountCollection(
            $enrollmentCountCollection
        );
    }

    private function constructCourseSectionEnrollmentCountCollection(
        EloquentCollection $collection
    ) {
        $rtn = new CourseSectionEnrollmentCountCollection();

        foreach ($collection as $item) {
            $rtn->push(
                $this->constructCourseSectionEnrollmentCount($item)
            );
        }

        return $rtn;
    }

    private function constructCourseSectionEnrollmentCount(
        CourseSectionEnrollmentCountEntity $enrollmentCount
    ): CourseSectionEnrollmentCount {
        return new CourseSectionEnrollmentCount(
            new CourseSectionGuid(
                $enrollmentCount->getCourseSectionGuid()
            ),
            $this->converter->stringToInt(
                $enrollmentCount->getNumOfMaxEnrollment(),
                0
            ),
            $this->converter->stringToInt(
                $enrollmentCount->getNumOfCurrentEnrollment(),
                0
            ),
            $this->converter->stringToInt(
                $enrollmentCount->getNumOfActiveEnrollment(),
                0
            ),
            $this->converter->stringToInt(
                $enrollmentCount->getNumOfAvailableEnrollment(),
                0
            )
        );
    }
}
