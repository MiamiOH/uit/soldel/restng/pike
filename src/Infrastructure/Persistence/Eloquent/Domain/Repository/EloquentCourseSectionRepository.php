<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseGuidTermCodeCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCrnCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\PartOfTerm;
use MiamiOH\Pike\Domain\Repository\CourseRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseGuidTermCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Exception\CourseSectionNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssbsect;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Stvptrm;

class EloquentCourseSectionRepository extends AbstractBaseRepository implements CourseSectionRepositoryInterface
{
    /**
     * @var CourseRepositoryInterface
     */
    private $courseRepository;
    /**
     * @var Converter
     */
    private $converter;

    public function __construct(
        EloquentHandler $eloquentHandler,
        CourseRepositoryInterface $courseRepository,
        Converter $converter
    ) {
        parent::__construct($eloquentHandler);
        $this->courseRepository = $courseRepository;
        $this->converter = $converter;
    }

    public function getByTermCodeCrn(
        TermCode $termCode,
        Crn $crn
    ): CourseSection {
        $ssbsect = $this->getEloquentHandler()->model(Ssbsect::class)
            ->with('partOfTerm')
            ->byTermCodeCrn($termCode, $crn)
            ->first();

        if ($ssbsect === null) {
            throw new CourseSectionNotFoundException();
        }

        return $this->constructCourseSection($ssbsect, $this->getCourse($ssbsect));
    }

    private function constructCourseSection(Ssbsect $ssbsect, Course $course)
    {
        $termCode = new TermCode($ssbsect->getTermCode());
        $crn = $ssbsect->getCrn();

        $partOfTerm = $this->constructPartOfTerm(
            $ssbsect->getPartOfTerm(),
            $ssbsect->getPartOfTermStartDate(),
            $ssbsect->getPartOfTermEndDate()
        );

        $standardizedDepartmentCode = $ssbsect->getStandardizedDepartmentCode() ?? $ssbsect->getLegacyStandardizedDepartmentCode();

        return new CourseSection(
            new CourseSectionGuid($ssbsect->getGuid()),
            $course,
            $ssbsect->getTitle() ?? $course->getTitle(),
            $termCode,
            new Crn($crn),
            $ssbsect->getTermDescription(),
            $ssbsect->getSectionCode(),
            $ssbsect->getCreditHours() ? $ssbsect->getCreditHours() : $course->getCreditHoursDescription(),
            $ssbsect->getCreditHours() ? $ssbsect->getCreditHours() : $course->getCreditHoursLow(),
            $ssbsect->getCreditHours() ? $ssbsect->getCreditHours() : $course->getCreditHoursHigh(),
            $ssbsect->getCreditHours() ? array($this->converter->stringToFloat($ssbsect->getCreditHours())) : $course->getCreditHoursAvailable(),
            new InstructionalTypeCode($ssbsect->getInstructionalTypeCode()),
            $ssbsect->getInstructionalTypeDescription(),
            new CampusCode($ssbsect->getCampusCode()),
            $ssbsect->getCampusDescription(),
            new CourseSectionStatusCode($ssbsect->getCourseSectionStatusCode()),
            $ssbsect->getCourseSectionStatusDescription(),
            $this->converter->stringToInt($ssbsect->getMaxNumberOfEnrollment()),
            $partOfTerm,
            $this->converter->yesNoIndicatorToBool($ssbsect->getMidtermGradeSubmissionAvailableIndicator()),
            $this->converter->yesNoIndicatorToBool($ssbsect->getFinalGradeSubmissionAvailableIndicator()),
            $this->converter->yesNoIndicatorToBool($ssbsect->getFinalGradeRequiredIndicator()),
            $this->converter->yesNoIndicatorToBool($ssbsect->getPrintIndicator()),
            $ssbsect->getStandardizedDivisionCode() === null ? null : new DivisionCode($ssbsect->getStandardizedDivisionCode()),
            $ssbsect->getStandardizedDivisionName(),
            $standardizedDepartmentCode === null ? null : new DepartmentCode($standardizedDepartmentCode),
            $ssbsect->getStandardizedDepartmentName() ?? $ssbsect->getLegacyStandardizedDepartmentName(),
            $ssbsect->getLegacyStandardizedDepartmentCode() === null ? null : new DepartmentCode($ssbsect->getLegacyStandardizedDepartmentCode()),
            $ssbsect->getLegacyStandardizedDepartmentName()
        );
    }

    private function constructPartOfTerm(
        Stvptrm $stvptrm,
        Carbon $startDate,
        Carbon $endDate
    ): PartOfTerm {
        return new PartOfTerm(
            new PartOfTermCode($stvptrm->getCode()),
            $stvptrm->getDescription(),
            $startDate,
            $endDate
        );
    }

    private function getCourse(Ssbsect $ssbsect): Course
    {
        return $this->courseRepository
            ->getCollectionByCourseGuidTermCodeCollection(
                new CourseGuidTermCodeCollection([
                    new CourseGuidTermCode(
                        new CourseGuid($ssbsect->getCourseGuid()),
                        new TermCode($ssbsect->getTermCode())
                    )
                ])
            )->get(0);
    }

    public function getByGuid(CourseSectionGuid $courseSectionGuid): CourseSection
    {
        $ssbsect = $this->getEloquentHandler()->model(Ssbsect::class)
            ->with('partOfTerm')
            ->byGuid($courseSectionGuid)
            ->first();

        if ($ssbsect === null) {
            throw new CourseSectionNotFoundException();
        }

        return $this->constructCourseSection($ssbsect, $this->getCourse($ssbsect));
    }

    public function getByGuids(
        CourseSectionGuidCollection $courseSectionGuids
    ): CourseSectionCollection {
        $ssbsectCollection = $this->getEloquentHandler()->model(Ssbsect::class)
            ->with('partOfTerm')
            ->byGuids($courseSectionGuids)
            ->get();

        return $this->constructCourseSectionCollection($ssbsectCollection);
    }

    public function getCollectionByTermCodeCrnCollection(
        TermCodeCrnCollection $collection
    ): CourseSectionCollection {
        $ssbsectCollection = $this->getEloquentHandler()->model(Ssbsect::class)
            ->with('partOfTerm')
            ->byTermCodeCrnCollection($collection)
            ->get();

        return $this->constructCourseSectionCollection($ssbsectCollection);
    }

    private function constructCourseSectionCollection(EloquentCollection $ssbsects)
    {
        $courseGuidTermCodeCollection = new CourseGuidTermCodeCollection();

        /**
         * @var Ssbsect $ssbsect
         * */
        foreach ($ssbsects as $ssbsect) {
            $courseGuidTermCodeCollection->push(new CourseGuidTermCode(
                new CourseGuid($ssbsect->getCourseGuid()),
                new TermCode($ssbsect->getTermCode())
            ));
        }

        $courseCollection = $this->courseRepository->getCollectionByCourseGuidTermCodeCollection($courseGuidTermCodeCollection);

        $courseMap = [];

        /**
         * @var Course $course
         * */
        foreach ($courseCollection as $course) {
            $courseMap[$course->getGuid()->getValue()] = $course;
        }

        $courseSectionCollection = new CourseSectionCollection();

        /**
         * @var Ssbsect $ssbsect
         * */
        foreach ($ssbsects as $ssbsect) {
            $courseSectionCollection->push(
                $this->constructCourseSection(
                    $ssbsect,
                    $courseMap[$ssbsect->getCourseGuid()]
                )
            );
        }

        return $courseSectionCollection;
    }
}//end class
