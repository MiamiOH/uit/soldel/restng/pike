<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:33 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Arr;
use MiamiOH\Pike\Common\Collection;
use MiamiOH\Pike\Domain\Collection\CourseSectionCreditHoursDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionLevelDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionSummaryCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionCreditHoursDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionLevelDistribution;
use MiamiOH\Pike\Domain\Model\CourseSectionSummary;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentDistributionRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssrmeet;
use PhpParser\Node\Expr\Array_;

class EloquentCourseSectionEnrollmentDistributionRepository extends AbstractBaseRepository implements CourseSectionEnrollmentDistributionRepositoryInterface
{

    /**
     * Get a collection of course section level distribution by providing
     * a collection of course section id
     *
     * @param CourseSectionGuidCollection $courseSectionGuidCollection
     *
     * @return CourseSectionEnrollmentDistributionCollection
     */
    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $courseSectionGuidCollection
    ): CourseSectionEnrollmentDistributionCollection {
        $courseSectionEnrollmentDistributionCollection = new CourseSectionEnrollmentDistributionCollection();
        foreach ($courseSectionGuidCollection as $courseSectionGuid) {
            $enrollmentDistribution = $this->getEloquentHandler()->select(
                "SELECT sfrstcr_term_code || sfrstcr_crn termcrn,
                 fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code) level_code,
                 stvclas_desc level_desc,
                 sfrstcr_credit_hr credit_hours,
                 COUNT(fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code)) level_count
                 FROM SFRSTCR, STVCLAS, ssbsgid
                 WHERE stvclas_code = fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code)
                 AND sfrstcr_rsts_code IN (
					     	SELECT stvrsts_code
                   FROM stvrsts
                   WHERE stvrsts_incl_sect_enrl = 'Y')
                 AND sfrstcr_term_code = ssbsgid_term_code AND sfrstcr_crn = ssbsgid_crn
                 AND SSBSGID_GUID = ?
                 GROUP BY sfrstcr_term_code, sfrstcr_crn, fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code), stvclas_desc, sfrstcr_credit_hr
                 ORDER BY sfrstcr_term_code, sfrstcr_crn, fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code)
                 ",
                [$courseSectionGuid->getValue()]
            );

            $enrollmentActiveDistribution = $this->getEloquentHandler()->select(
                "SELECT sfrstcr_term_code || sfrstcr_crn termcrn,
                 fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code) level_code,
                 stvclas_desc level_desc,
                 sfrstcr_credit_hr credit_hours,
                 COUNT(fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code)) level_count
                 FROM SFRSTCR, STVCLAS, ssbsgid
                 WHERE stvclas_code = fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code)
                 AND sfrstcr_rsts_code IN (
					     	SELECT stvrsts_code
                   FROM stvrsts
                   WHERE stvrsts_incl_sect_enrl = 'Y'
                          AND stvrsts_withdraw_ind   = 'N')
                 AND sfrstcr_term_code = ssbsgid_term_code AND sfrstcr_crn = ssbsgid_crn
                 AND SSBSGID_GUID = ?
                 GROUP BY sfrstcr_term_code, sfrstcr_crn, fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code), stvclas_desc, sfrstcr_credit_hr
                 ORDER BY sfrstcr_term_code, sfrstcr_crn, fz_calc_start_of_term_clas_fnc(sfrstcr_pidm, sfrstcr_levl_code, sfrstcr_term_code)
                 ",
                [$courseSectionGuid->getValue()]
            );

            $courseSectionEnrollmentDistribution=$this->constructCourseSectionEnrollmentDistribution($courseSectionGuid, $enrollmentDistribution, $enrollmentActiveDistribution);
            $courseSectionEnrollmentDistributionCollection->push($courseSectionEnrollmentDistribution);
        }

        return $courseSectionEnrollmentDistributionCollection;
    }

    private function constructCourseSectionEnrollmentDistribution(
        CourseSectionGuid $courseSectionGuid,
        array $enrollmentDistributionArray,
        array $enrollmentActiveDistributionArray
    ): CourseSectionEnrollmentDistribution {
        $courseSectionLevelDistributionCollection = new CourseSectionLevelDistributionCollection();
        $courseSectionSummaryCollection = new CourseSectionSummaryCollection();
        $courseSectionCreditHoursDistributionCollection = new CourseSectionCreditHoursDistributionCollection();


        $courseSectionLevelDistributionData = array();
        $courseSectionSummaryData = array();
        $courseSectionCreditHoursDistributionData = array();



        foreach ($enrollmentDistributionArray as $enrollmentDistribution) {
            $courseSectionLevelDistributionData[$enrollmentDistribution->level_code]['desc'] = $enrollmentDistribution->level_desc;
            $courseSectionLevelDistributionData[$enrollmentDistribution->level_code]['count'] = $enrollmentDistribution->level_count;
            if (!isset($courseSectionCreditHoursDistributionData[$enrollmentDistribution->credit_hours]['count'])) {
                $courseSectionCreditHoursDistributionData[$enrollmentDistribution->credit_hours]['count'] = 0;
            }
            $courseSectionCreditHoursDistributionData[$enrollmentDistribution->credit_hours]['count'] += $enrollmentDistribution->level_count;

            switch ($enrollmentDistribution->level_code) {
                case '01':
                case '02':
                case '99':
                case 'NC':
                case 'FR':
                    if (!isset($courseSectionSummaryData['undergraduate']['count'])) {
                        $courseSectionSummaryData['undergraduate']['count'] = 0;
                    }
                    $courseSectionSummaryData['undergraduate']['count'] += $enrollmentDistribution->level_count;
                    if (!isset($courseSectionSummaryData['underclassman']['count'])) {
                        $courseSectionSummaryData['underclassman']['count'] = 0;
                    }
                    $courseSectionSummaryData['underclassman']['count'] += $enrollmentDistribution->level_count;
                    break;
                case '03':
                case '04':
                case 'CF':
                    if (!isset($courseSectionSummaryData['undergraduate']['count'])) {
                        $courseSectionSummaryData['undergraduate']['count'] = 0;
                    }
                    $courseSectionSummaryData['undergraduate']['count'] += $enrollmentDistribution->level_count;
                    if (!isset($courseSectionSummaryData['upperclassman']['count'])) {
                        $courseSectionSummaryData['upperclassman']['count'] = 0;
                    }
                    $courseSectionSummaryData['upperclassman']['count'] += $enrollmentDistribution->level_count;
                    break;
                case '10':
                case '11':
                case '12':
                case '13':
                case '14':
                case '15':
                case '16':
                    if (!isset($courseSectionSummaryData['graduate']['count'])) {
                        $courseSectionSummaryData['graduate']['count'] = 0;
                    }
                    $courseSectionSummaryData['graduate']['count'] += $enrollmentDistribution->level_count;
                    break;
                default:
                    if (!isset($courseSectionSummaryData['unknown']['count'])) {
                        $courseSectionSummaryData['unknown']['count'] = 0;
                    }
                    $courseSectionSummaryData['unknown']['count'] += $enrollmentDistribution->level_count;
            }
        }

        foreach ($enrollmentActiveDistributionArray as $enrollmentDistribution) {
            $courseSectionLevelDistributionData[$enrollmentDistribution->level_code]['desc'] = $enrollmentDistribution->level_desc;
            $courseSectionLevelDistributionData[$enrollmentDistribution->level_code]['active'] = $enrollmentDistribution->level_count;
            if (!isset($courseSectionCreditHoursDistributionData[$enrollmentDistribution->credit_hours]['active'])) {
                $courseSectionCreditHoursDistributionData[$enrollmentDistribution->credit_hours]['active'] = 0;
            }
            $courseSectionCreditHoursDistributionData[$enrollmentDistribution->credit_hours]['active'] += $enrollmentDistribution->level_count;

            switch ($enrollmentDistribution->level_code) {
                case '01':
                case '02':
                case '99':
                case 'NC':
                case 'FR':
                    if (!isset($courseSectionSummaryData['undergraduate']['active'])) {
                        $courseSectionSummaryData['undergraduate']['active'] = 0;
                    }
                    $courseSectionSummaryData['undergraduate']['active'] += $enrollmentDistribution->level_count;
                    if (!isset($courseSectionSummaryData['underclassman']['active'])) {
                        $courseSectionSummaryData['underclassman']['active'] = 0;
                    }
                    $courseSectionSummaryData['underclassman']['active'] += $enrollmentDistribution->level_count;
                    break;
                case '03':
                case '04':
                case 'CF':
                    if (!isset($courseSectionSummaryData['undergraduate']['active'])) {
                        $courseSectionSummaryData['undergraduate']['active'] = 0;
                    }
                    $courseSectionSummaryData['undergraduate']['active'] += $enrollmentDistribution->level_count;
                    if (!isset($courseSectionSummaryData['upperclassman']['active'])) {
                        $courseSectionSummaryData['upperclassman']['active'] = 0;
                    }
                    $courseSectionSummaryData['upperclassman']['active'] += $enrollmentDistribution->level_count;
                    break;
                case '10':
                case '11':
                case '12':
                case '13':
                case '14':
                case '15':
                case '16':
                    if (!isset($courseSectionSummaryData['graduate']['active'])) {
                        $courseSectionSummaryData['graduate']['active'] = 0;
                    }
                    $courseSectionSummaryData['graduate']['active'] += $enrollmentDistribution->level_count;
                    break;
                default:
                    if (!isset($courseSectionSummaryData['unknown']['active'])) {
                        $courseSectionSummaryData['unknown']['active'] = 0;
                    }
                    $courseSectionSummaryData['unknown']['active'] += $enrollmentDistribution->level_count;
            }
        }
        foreach ($courseSectionLevelDistributionData as $code => $data) {
            $courseSectionLevelDistribution = $this->constructCourseSectionLevelDistribution(
                $code,
                $data['desc'],
                isset($data['count'])?$data['count']:null,
                isset($data['active'])?$data['active']:null
            );
            $courseSectionLevelDistributionCollection->push($courseSectionLevelDistribution);
        }

        foreach ($courseSectionSummaryData as $code => $data) {
            $courseSectionSummary = $this->constructCourseSectionSummary(
                $code,
                isset($data['count'])?$data['count']:null,
                isset($data['active'])?$data['active']:null
            );
            $courseSectionSummaryCollection->push($courseSectionSummary);
        }

        foreach ($courseSectionCreditHoursDistributionData as $credit => $data) {
            $courseSectionCreditHoursDistribution = $this->constructCourseSectionCreditHoursDistribution(
                $credit,
                isset($data['count'])?$data['count']:null,
                isset($data['active'])?$data['active']:null
            );
            $courseSectionCreditHoursDistributionCollection->push($courseSectionCreditHoursDistribution);
        }

        return new CourseSectionEnrollmentDistribution(
            $courseSectionGuid,
            $courseSectionCreditHoursDistributionCollection,
            $courseSectionLevelDistributionCollection,
            $courseSectionSummaryCollection
        );
    }

    private function constructCourseSectionCreditHoursDistribution(
        float $credit,
        ?int $numberOfCurrentEnrollment,
        ?int $numberOfActiveEnrollment
    ): CourseSectionCreditHoursDistribution {
        return new CourseSectionCreditHoursDistribution(
            $credit,
            $numberOfCurrentEnrollment,
            $numberOfActiveEnrollment
        );
    }

    private function constructCourseSectionSummary(
        string $description,
        ?int $numberOfCurrentEnrollment,
        ?int $numberOfActiveEnrollment
    ): CourseSectionSummary {
        return new CourseSectionSummary(
            $description,
            $numberOfCurrentEnrollment,
            $numberOfActiveEnrollment
        );
    }

    private function constructCourseSectionLevelDistribution(
        string $code,
        string $description,
        ?int $numberOfCurrentEnrollment,
        ?int $numberOfActiveEnrollment
    ): CourseSectionLevelDistribution {
        return new CourseSectionLevelDistribution(
            $code,
            $description,
            $numberOfCurrentEnrollment,
            $numberOfActiveEnrollment
        );
    }
}
