<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\MajorCodeCollection;
use MiamiOH\Pike\Domain\Collection\MajorCollection;
use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\Pike\Domain\Repository\MajorRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;
use MiamiOH\Pike\Exception\MajorNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Major as EloquentMajor;

class EloquentMajorRepository extends AbstractBaseRepository implements MajorRepositoryInterface
{
    public function getAll(): MajorCollection
    {
        $majors = $this->getEloquentHandler()->model(EloquentMajor::class)
            ->orderByMajorCode()
            ->get();

        return $this->constructMajorCollection($majors);
    }//end getAll()

    private function constructMajorCollection(
        EloquentCollection $majors
    ): MajorCollection {
        $majorCollection = new MajorCollection();

        foreach ($majors as $major) {
            $majorCollection->push($this->constructMajor($major));
        }

        return $majorCollection;
    }//end getByCode()

    private function constructMajor(EloquentMajor $eloquentModel): Major
    {
        return new Major(
            new MajorCode($eloquentModel->getCode()),
            $eloquentModel->getDescription()
        );
    }//end getMajorCollectionByCodes()

    public function getByCode(MajorCode $code): Major
    {
        $major = $this->getEloquentHandler()->model(EloquentMajor::class)
            ->codeEquals($code)
            ->first();

        if ($major === null) {
            throw new MajorNotFoundException();
        }

        return $this->constructMajor($major);
    }//end constructMajorCollection()

    /**
     * Get major by multiple major codes.
     *
     * @param MajorCodeCollection $codes
     *
     * @return MajorCollection
     */
    public function getMajorCollectionByCodes(
        MajorCodeCollection $codes
    ): MajorCollection {
        $majors = $this->getEloquentHandler()->model(EloquentMajor::class)
            ->codeIn($codes)
            ->get();

        return $this->constructMajorCollection($majors);
    }//end constructMajor()
}//end class
