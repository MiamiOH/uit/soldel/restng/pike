<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use MiamiOH\Pike\Common\Converter;
use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidUniqueIdCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollmentStatus;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Repository\CourseSectionEnrollmentRepositoryInterface;
use MiamiOH\Pike\Domain\Repository\CourseSectionRepositoryInterface;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionEnrollmentRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\CourseSectionEnrollmentNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Sfrstcr;

class EloquentCourseSectionEnrollmentRepository extends AbstractBaseRepository implements CourseSectionEnrollmentRepositoryInterface
{
    /**
     * @var CourseSectionRepositoryInterface
     */
    private $courseSectionRepository;
    /**
     * @var Converter
     */
    private $converter;

    public function __construct(
        EloquentHandler $eloquentHandler,
        CourseSectionRepositoryInterface $courseSectionRepository,
        Converter $converter
    ) {
        parent::__construct($eloquentHandler);
        $this->courseSectionRepository = $courseSectionRepository;
        $this->converter = $converter;
    }

    public function getByUniqueIdTermCodeCrn(
        UniqueId $uniqueId,
        TermCode $termCode,
        Crn $crn
    ): CourseSectionEnrollment {
        /**
         * @var Sfrstcr|null $sfrstcr
         * */
        $sfrstcr = $this->getEloquentHandler()->model(Sfrstcr::class)
            ->joinSzbuniqAndStvrsts()
            ->byUniqueIdTermCodeCrn($uniqueId, $termCode, $crn)
            ->first();

        if ($sfrstcr === null) {
            throw new CourseSectionEnrollmentNotFoundException();
        }

        $courseSection = $this->courseSectionRepository->getByGuid($sfrstcr->getCourseSectionGuid());

        return $this->constructCourseSectionEnrollment($sfrstcr, $courseSection);
    }

    private function constructCourseSectionEnrollment(
        Sfrstcr $sfrstcr,
        CourseSection $courseSection
    ): CourseSectionEnrollment {
        $termCode = new TermCode($sfrstcr->getTermCode());
        $crn = new Crn($sfrstcr->getCrn());
        $statusCode = new CourseSectionEnrollmentStatusCode($sfrstcr->getStatusCode());
        $statusDescription = $sfrstcr->getStatusDescription();
        $gradeDate = $sfrstcr->getGradeDate();
        $pidm = new Pidm($sfrstcr->getPidm());
        $attendHours = $sfrstcr->getAttendHours();
        $lastAttendDate = $sfrstcr->getLastAttendDate();
        $midtermGradeString = $sfrstcr->getMidtermGradeString();
        $finalGradeString = $sfrstcr->getFinalGradeString();
        $gradeModeCode = $sfrstcr->getGradeModeCode();
        $courseSectionGuid = $sfrstcr->getCourseSectionGuid();
        $uniqueId = new UniqueId($sfrstcr->getUniqueId());


        $courseSectionEnrollmentStatus = new CourseSectionEnrollmentStatus(
            $statusCode,
            $statusDescription
        );

        $isGradeSubmissionEligible = $this->parseGradeSubmissionEligible(
            $statusCode,
            $gradeDate
        );

        $gradeSubmissionEligibleComment = $this->parseGradeSubmissionEligibleComment(
            $isGradeSubmissionEligible,
            $statusCode,
            $statusDescription,
            $gradeDate
        );

        $isMidtermGradeRequired = $this->isMidtermGradeRequired(
            $pidm,
            $termCode,
            $crn
        );

        $hasAttended = $this->hasAttended($attendHours);

        $isEnrollmentActive = $this->isEnrollmentActive($statusCode);

        $gradeCollection = new GradeCollection();

        if ($midtermGradeString !== null) {
            $gradeCollection->push(new Grade(
                new GradeValue($midtermGradeString),
                new GradeModeCode($gradeModeCode),
                GradeType::ofMidterm(),
                new CourseSectionGuid($courseSectionGuid),
                new Crn($crn),
                new TermCode($termCode),
                new UniqueId($uniqueId)
            ));
        }

        if ($finalGradeString !== null) {
            $gradeCollection->push(new Grade(
                new GradeValue($finalGradeString),
                new GradeModeCode($gradeModeCode),
                GradeType::ofFinal(),
                new CourseSectionGuid($courseSectionGuid),
                new Crn($crn),
                new TermCode($termCode),
                new UniqueId($uniqueId)
            ));
        }

        return new CourseSectionEnrollment(
            $hasAttended,
            $isEnrollmentActive,
            $isGradeSubmissionEligible,
            $gradeSubmissionEligibleComment,
            $courseSection->isFinalGradeRequired(),
            $isMidtermGradeRequired,
            $gradeCollection,
            new UniqueId($sfrstcr->getUniqueId()),
            $courseSectionEnrollmentStatus,
            new CourseSectionGuid($sfrstcr->getCourseSectionGuid()),
            floatval($sfrstcr->getCreditHours()),
            new StudentLevelCode($sfrstcr->getLevelCode()),
            new TermCode($sfrstcr->getTermCode()),
            new GradeModeCode($gradeModeCode),
            $lastAttendDate
        );
    }

    private function parseGradeSubmissionEligible(
        CourseSectionEnrollmentStatusCode $statusCode,
        Carbon $gradeDate = null
    ): bool {
        if (($statusCode->getValue() === 'RE' || $statusCode->getValue() === 'RW') && $gradeDate === null) {
            return true;
        } else {
            return false;
        }
    }

    private function parseGradeSubmissionEligibleComment(
        bool $isGradeSubmissionEligible,
        CourseSectionEnrollmentStatusCode $statusCode,
        string $statusDescription,
        Carbon $gradeDate = null
    ): string {
        if ($isGradeSubmissionEligible) {
            return 'Student Registered(RE) or Web-Registered(RW) in this course';
        }

        if (($statusCode->getValue() === 'RE' || $statusCode->getValue() === 'RW') && $gradeDate !== null) {
            return 'Grade has already been rolled into academic history';
        }

        return 'Registration status ' . $statusDescription;
    }

    private function isMidtermGradeRequired(
        Pidm $pidm,
        TermCode $termCode,
        Crn $crn
    ) {
        $indicator = $this->getEloquentHandler()->table('dual')
            ->selectRaw('fz_midterm_grde_reqd_ind(?,?,?) as indicator')
            ->addBinding([
                $pidm->getValue(),
                $crn->getValue(),
                $termCode->getValue()
            ])
            ->first()
            ->indicator;

        return $this->converter->yesNoIndicatorToBool($indicator);
    }

    private function hasAttended(string $attendHours = null)
    {
        if ($attendHours === null || floatval($attendHours) > 0) {
            return true;
        }

        return false;
    }

    private function isEnrollmentActive(
        CourseSectionEnrollmentStatusCode $statusCode
    ) {
        if (in_array($statusCode->getValue(), ['RE', 'RW', 'AU'])) {
            return true;
        }

        return false;
    }

    public function getCollectionByUniqueIdTermCode(
        UniqueId $uniqueId,
        TermCode $termCode
    ): CourseSectionEnrollmentCollection {
        $sfrstcrCollection = $this->getEloquentHandler()
            ->model(Sfrstcr::class)
            ->joinSzbuniqAndStvrsts()
            ->byUniqueIdTermCode($uniqueId, $termCode)
            ->get();

        return $this->constructCourseSectionEnrollmentCollection($sfrstcrCollection);
    }

    private function constructCourseSectionEnrollmentCollection(
        EloquentCollection $sfrstcrCollection
    ) {
        $courseSectionEnrollmentCollection = new CourseSectionEnrollmentCollection();

        $courseSectionGuids = CourseSectionGuidCollection::createFromValueArray($sfrstcrCollection->map(function (Sfrstcr $sfrstcr) {
            return $sfrstcr->getCourseSectionGuid();
        })->toArray());
        $courseSectionDict = $this->courseSectionRepository
            ->getByGuids($courseSectionGuids)
            ->toDictByGuid();

        /** @var Sfrstcr $sfrstcr */
        foreach ($sfrstcrCollection as $sfrstcr) {
            $courseSectionEnrollment = $this->constructCourseSectionEnrollment(
                $sfrstcr,
                $courseSectionDict[$sfrstcr->getCourseSectionGuid()]
            );
            $courseSectionEnrollmentCollection->push($courseSectionEnrollment);
        }

        return $courseSectionEnrollmentCollection;
    }

    public function getCollectionByTermCodeCrn(
        TermCode $termCode,
        Crn $crn
    ): CourseSectionEnrollmentCollection {
        $sfrstcrCollection = $this->getEloquentHandler()
            ->model(Sfrstcr::class)
            ->joinSzbuniqAndStvrsts()
            ->byTermCodeCrn($termCode, $crn)
            ->get();

        return $this->constructCourseSectionEnrollmentCollection($sfrstcrCollection);
    }

    public function getCollectionByTermCodesCrnsUniqueIds(
        TermCodeCollection $termCodes,
        CrnCollection $crns,
        UniqueIdCollection $uniques,
        Offset $offset = null,
        Limit $limit = null
    ): CourseSectionEnrollmentCollection {
        /**
         * @var Sfrstcr $sfrstcrModel
         * @var Builder $sfrstcrModel
         * */
        $sfrstcrModel = $this->getEloquentHandler()
            ->model(Sfrstcr::class)
            ->joinSzbuniqAndStvrsts()
            ->orderByGuidPidm();

        if (count($termCodes) > 0) {
            $sfrstcrModel = $sfrstcrModel->byTermCodes($termCodes);
        }

        if (count($uniques) > 0) {
            $sfrstcrModel = $sfrstcrModel->byUniqueIds($uniques);
        }

        if (count($crns) > 0) {
            $sfrstcrModel = $sfrstcrModel->byCrns($crns);
        }

        if ($offset !== null) {
            $sfrstcrModel->offset($offset->getValue());
        }

        if ($limit !== null) {
            $sfrstcrModel->limit($limit->getValue());
        }


        $sfrstcrCollection = $sfrstcrModel->get();

        return $this->constructCourseSectionEnrollmentCollection($sfrstcrCollection);
    }

    public function getByCourseSectionGuidAndUniqueId(
        CourseSectionGuid $courseSectionGuid,
        UniqueId $uniqueId
    ): CourseSectionEnrollment {
        $sfrstcr = $this->getEloquentHandler()->model(Sfrstcr::class)
            ->joinSzbuniqAndStvrsts()
            ->byCourseSectionGuid($courseSectionGuid)
            ->byUniqueId($uniqueId)
            ->first();

        if ($sfrstcr === null) {
            throw new CourseSectionEnrollmentNotFoundException();
        }

        $courseSection = $this->courseSectionRepository->getByGuid($sfrstcr->getCourseSectionGuid());

        return $this->constructCourseSectionEnrollment($sfrstcr, $courseSection);
    }

    public function getCollectionByCourseSectionGuidUniqueIdCollection(
        CourseSectionGuidUniqueIdCollection $collection
    ): CourseSectionEnrollmentCollection {
        $sfrstcrCollection = $this->getEloquentHandler()
            ->model(Sfrstcr::class)
            ->joinSzbuniqAndStvrsts()
            ->byCourseSectionGuidUniqueIdCollection($collection)
            ->get();

        return $this->constructCourseSectionEnrollmentCollection($sfrstcrCollection);
    }

    public function search(
        SearchCourseSectionEnrollmentRequest $searchRequest
    ): CourseSectionEnrollmentCollection {
        /**
         * @var Builder|Sfrstcr $query
         * */
        $query = $this->getEloquentHandler()
            ->model(Sfrstcr::class)
            ->joinSzbuniqAndStvrsts()
            ->orderByGuidPidm();

        $termCodeCollection = $searchRequest->getTermCodeCollection();
        if ($termCodeCollection !== null) {
            $query->byTermCodes($termCodeCollection);
        }

        $crnCollection = $searchRequest->getCrnCollection();
        if ($crnCollection !== null) {
            $query->byCrns($crnCollection);
        }

        $isActive = $searchRequest->isActive();
        if ($isActive === true) {
            $query->isActive();
        } elseif ($isActive === false) {
            $query->isNotActive();
        }

        $uniqueIdCollection = $searchRequest->getUniqueIdCollection();
        if ($uniqueIdCollection !== null) {
            $query->byUniqueIds($uniqueIdCollection);
        }

        $courseSectionGuidCollection = $searchRequest->getCourseSectionGuidCollection();
        if ($courseSectionGuidCollection !== null) {
            $query->byCourseSectionGuidCollection($courseSectionGuidCollection);
        }

        $totalNumOfRecords = $query->count();

        $query->offset($searchRequest->getOffset()->getValue());
        $query->limit($searchRequest->getLimit()->getValue());

        /** @var Collection $sfrstcrCollection */
        $sfrstcrCollection = $query->get();

        $enrollmentCollection = $this->constructCourseSectionEnrollmentCollection($sfrstcrCollection);
        $enrollmentCollection->setTotalNumOfItems($totalNumOfRecords);
        return $enrollmentCollection;
    }
}//end class
