<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\DepartmentCollection;
use MiamiOH\Pike\Domain\Model\Department;
use MiamiOH\Pike\Domain\Repository\DepartmentRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Exception\DepartmentNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Department as EloquentDepartment;

class EloquentDepartmentRepository extends AbstractBaseRepository implements DepartmentRepositoryInterface
{
    public function getAll(): DepartmentCollection
    {
        $departments = $this->getEloquentHandler()->model(EloquentDepartment::class)
            ->orderByCode()
            ->get();

        return $this->constructDepartmentCollection($departments);
    }//end getAll()

    private function constructDepartmentCollection(
        EloquentCollection $departments
    ): DepartmentCollection {
        $departmentCollection = new DepartmentCollection();

        foreach ($departments as $department) {
            $departmentCollection->push($this->constructDepartment($department));
        }

        return $departmentCollection;
    }//end getByCode()

    private function constructDepartment(
        EloquentDepartment $eloquentDepartment
    ): Department {
        return new Department(
            new DepartmentCode($eloquentDepartment->getCode()),
            $eloquentDepartment->getDescription()
        );
    }//end constructDepartmentCollection()

    public function getByCode(DepartmentCode $code): Department
    {
        $department = $this->getEloquentHandler()->model(EloquentDepartment::class)
            ->codeEquals($code)
            ->first();

        if ($department === null) {
            throw new DepartmentNotFoundException();
        }

        return $this->constructDepartment($department);
    }//end constructDepartment()
}//end class
