<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/7/18
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\GradeTypeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Repository\GradeRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\GradeNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Grade as EloquentGrade;

class EloquentGradeRepository extends AbstractBaseRepository implements GradeRepositoryInterface
{
    public function getCollectionByCourseSectionGuidAndUniqueId(
        CourseSectionGuid $courseSectionGuid,
        UniqueId $uniqueId
    ): GradeCollection {
        /**
         * @var EloquentGrade|null $grade
         */
        $grade = $this->getEloquentHandler()->model(EloquentGrade::class)->byCourseSectionGuidAndUniqueId(
            $courseSectionGuid,
            $uniqueId
        )->first();

        if ($grade === null) {
            throw new GradeNotFoundException();
        }

        $gradeCollection = new GradeCollection();
        $midtermGrade = $this->constructGrade($grade, 'midterm');
        if ($midtermGrade !== null) {
            $gradeCollection->push($midtermGrade);
        }

        $finalGrade = $this->constructGrade($grade, 'final');
        if ($finalGrade !== null) {
            $gradeCollection->push($finalGrade);
        }

        return $gradeCollection;
    }

    private function constructGrade(EloquentGrade $grade, string $type): ?Grade
    {
        if ($type === "final" && $grade->getFinalValue() !== null) {
            return new Grade(
                new GradeValue($grade->getFinalValue()),
                new GradeModeCode($grade->getGradeModeCode()),
                GradeType::ofFinal(),
                new CourseSectionGuid($grade->getCourseSectionGuid()),
                new Crn($grade->getCrn()),
                new TermCode($grade->getTermCode()),
                new UniqueId($grade->getUniqueId())
            );
        } elseif ($type === "midterm" && $grade->getMidtermValue() !== null) {
            return new Grade(
                new GradeValue($grade->getMidtermValue()),
                new GradeModeCode($grade->getGradeModeCode()),
                GradeType::ofMidterm(),
                new CourseSectionGuid($grade->getCourseSectionGuid()),
                new Crn($grade->getCrn()),
                new TermCode($grade->getTermCode()),
                new UniqueId($grade->getUniqueId())
            );
        }

        return null;
    }

    public function getCollectionByTermCodesCrnsUniqueIdsTypes(
        TermCodeCollection $termCodes,
        CrnCollection $crns,
        UniqueIdCollection $uniqueIds,
        GradeTypeCollection $types
    ): GradeCollection {
        $grades = $this->getEloquentHandler()->model(EloquentGrade::class)->JoinSsbsgidAndszbuniq();
        if (count($termCodes) > 0) {
            $grades = $grades->byTermCodes($termCodes);
        }
        if (count($crns) > 0) {
            $grades = $grades->byCrns($crns);
        }
        if (count($uniqueIds) > 0) {
            $grades = $grades->byUniqueIds($uniqueIds);
        }
        $grades = $grades->get();

        $typesArray = $types->toValueArray();
        $gradeCollection = new GradeCollection();
        foreach ($grades as $grade) {
            if (in_array('midterm', $typesArray)) {
                $midtermGrade = $this->constructGrade($grade, 'midterm');
                if ($midtermGrade !== null) {
                    $gradeCollection->push($midtermGrade);
                }
            }
            if (in_array('final', $typesArray)) {
                $finalGrade = $this->constructGrade($grade, 'final');
                if ($finalGrade !== null) {
                    $gradeCollection->push($finalGrade);
                }
            }
        }

        return $gradeCollection;
    }


    public function getCollectionByCourseSectionGuidsUniqueIdsTypes(
        CourseSectionGuidCollection $courseSectionGuids,
        UniqueIdCollection $uniqueIds,
        GradeTypeCollection $types
    ): GradeCollection {
        $grades = $this->getEloquentHandler()->model(EloquentGrade::class)->JoinSsbsgidAndszbuniq();
        if (count($courseSectionGuids) > 0) {
            $grades = $grades->byCourseSectionGuids($courseSectionGuids);
        }

        if (count($uniqueIds) > 0) {
            $grades = $grades->byUniqueIds($uniqueIds);
        }
        $grades = $grades->get();

        $typesArray = $types->toValueArray();
        $gradeCollection = new GradeCollection();
        foreach ($grades as $grade) {
            if (in_array('midterm', $typesArray)) {
                $midtermGrade = $this->constructGrade($grade, 'midterm');
                if ($midtermGrade !== null) {
                    $gradeCollection->push($midtermGrade);
                }
            }
            if (in_array('final', $typesArray)) {
                $finalGrade = $this->constructGrade($grade, 'final');
                if ($finalGrade !== null) {
                    $gradeCollection->push($finalGrade);
                }
            }
        }


        return $gradeCollection;
    }
}
