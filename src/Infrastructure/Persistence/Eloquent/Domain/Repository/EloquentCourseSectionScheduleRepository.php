<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:33 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionSchedule;
use MiamiOH\Pike\Domain\Repository\CourseSectionScheduleRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Ssrmeet;

class EloquentCourseSectionScheduleRepository extends AbstractBaseRepository implements CourseSectionScheduleRepositoryInterface
{

    /**
     * Get a collection of course section schedule by providing
     * a collection of course section id
     *
     * @param CourseSectionGuidCollection $courseSectionGuidCollection
     *
     * @return CourseSectionScheduleCollection
     */
    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $courseSectionGuidCollection
    ): CourseSectionScheduleCollection {
        $ssrmeetCollection = $this->getEloquentHandler()->model(Ssrmeet::class)
            ->byCourseSectionGuids($courseSectionGuidCollection)
            ->get();

        return $this->constructCourseSectionScheduleCollection($ssrmeetCollection);
    }

    private function constructCourseSectionScheduleCollection(
        EloquentCollection $ssrmmetCollection
    ): CourseSectionScheduleCollection {
        $courseSectionScheduleCollection = new CourseSectionScheduleCollection();

        foreach ($ssrmmetCollection as $ssrmeet) {
            $courseSectionScheduleCollection->push(
                $this->constructCourseSectionSchedule($ssrmeet)
            );
        }

        return $courseSectionScheduleCollection;
    }

    private function constructCourseSectionSchedule(
        Ssrmeet $ssrmeet
    ): CourseSectionSchedule {
        $days = $this->constructDays(
            $ssrmeet->getMondayDay(),
            $ssrmeet->getTuesdayDay(),
            $ssrmeet->getWednesdayDay(),
            $ssrmeet->getThursdayDay(),
            $ssrmeet->getFridayDay(),
            $ssrmeet->getSaturdayDay(),
            $ssrmeet->getSundayDay()
        );
        $beginTime = $this->constructTime($ssrmeet->getStartTime());
        $endTime = $this->constructTime($ssrmeet->getEndTime());
        $typeCode = null;
        if ($ssrmeet->getTypeCode()) {
            $typeCode = new CourseSectionScheduleTypeCode($ssrmeet->getTypeCode());
        }
        $buildingCode = null;
        if ($ssrmeet->getBuildingCode()) {
            $buildingCode = new BuildingCode($ssrmeet->getBuildingCode());
        }

        return new CourseSectionSchedule(
            new CourseSectionGuid($ssrmeet->getCourseSectionGuid()),
            $ssrmeet->getStartDate(),
            $ssrmeet->getEndDate(),
            $beginTime,
            $endTime,
            $days,
            $ssrmeet->getRoom(),
            $buildingCode,
            $ssrmeet->getBuildingName(),
            $typeCode,
            $ssrmeet->getTypeDescription()
        );
    }

    private function constructDays(
        ?string $mon,
        ?string $tue,
        ?string $wed,
        ?string $thu,
        ?string $fri,
        ?string $sat,
        ?string $sun
    ): ?CourseSectionScheduleDays {
        $daysString =
            ($mon === null ? '' : 'M') .
            ($tue === null ? '' : 'T') .
            ($wed === null ? '' : 'W') .
            ($thu === null ? '' : 'R') .
            ($fri === null ? '' : 'F') .
            ($sat === null ? '' : 'S') .
            ($sun === null ? '' : 'U');

        if (trim($daysString) === '') {
            return null;
        }

        return new CourseSectionScheduleDays($daysString);
    }

    private function constructTime(?string $timeString): ?Carbon
    {
        if ($timeString === null) {
            return null;
        }

        return Carbon::createFromFormat('Hi', $timeString);
    }
}
