<?php

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 3:02 PM
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Domain\Repository;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\TermCollection;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\Repository\TermRepositoryInterface;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Exception\TermNotFoundException;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models\Term as EloquentTerm;

/**
 * Class EloquentTermRepository
 *
 * @package MiamiOH\Pike\Infrastructure\Domain\Model\Term
 */
class EloquentTermRepository extends AbstractBaseRepository implements TermRepositoryInterface
{


    /**
     * Get all terms.
     *
     * @return TermCollection
     */
    public function getAll(): TermCollection
    {
        $terms = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->get();

        return $this->constructTermCollection($terms);
    }//end getAll()

    /**
     * Map Eloquent Collection to TermCollection
     *
     * @param EloquentCollection $terms
     *
     * @return TermCollection
     */
    private function constructTermCollection(
        EloquentCollection $terms
    ): TermCollection {
        $termCollection = new TermCollection();

        foreach ($terms as $term) {
            $termCollection->push($this->constructTerm($term));
        }

        return $termCollection;
    }//end getByCode()

    /**
     * Map Eloquent Term model to Term domain object
     *
     * @param EloquentTerm $eloquentModel
     *
     * @return Term
     */
    private function constructTerm(EloquentTerm $eloquentModel): Term
    {
        return new Term(
            new TermCode($eloquentModel->getCode()),
            $eloquentModel->getDescription(),
            $eloquentModel->getStartDate(),
            $eloquentModel->getEndDate(),
            $eloquentModel->isDisplayed()
        );
    }//end getCurrentTerm()

    /**
     * Get next academic term of specified term. Default is current term.
     *
     * @param TermCode|null $termCode
     *
     * @return Term
     * @throws \Exception
     */
    public function getNextTerm(TermCode $termCode = null): Term
    {
        $term = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->nextTerm($termCode)
            ->first();

        if ($term === null) {
            throw new TermNotFoundException('Next term does not exist');
        }

        return $this->constructTerm($term);
    }//end getNextTerm()


    /**
     * Get previous academic term of specified term. Default is current term.
     *
     * @param TermCode|null $termCode
     *
     * @return Term
     * @throws \Exception
     */
    public function getPrevTerm(TermCode $termCode = null): Term
    {
        $term = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->prevTerm($termCode)
            ->first();

        if ($term === null) {
            throw new TermNotFoundException('Previous term does not exist');
        }

        return $this->constructTerm($term);
    }//end getPrevTerm()


    /**
     * Get term by multiple term codes.
     *
     * @param TermCodeCollection $codes
     *
     * @return TermCollection
     */
    public function getTermCollectionByCodes(
        TermCodeCollection $codes
    ): TermCollection {
        $terms = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->codeIn($codes)
            ->get();

        return $this->constructTermCollection($terms);
    }//end getTermCollectionByCodes()


    /**
     * Get next number of terms of specified term. Default is current term.
     *
     * @param int $numOfTerms
     *
     * @param TermCode|null $termCode
     *
     * @return TermCollection
     * @throws \Exception
     */
    public function getNextTerms(
        int $numOfTerms,
        TermCode $termCode = null
    ): TermCollection {
        $term = null;

        if ($numOfTerms < 0) {
            throw new InvalidArgumentException('numOfTerms must be a positive integer');
        }

        if ($termCode === null) {
            $term = $this->getCurrentTerm();
        } else {
            $term = $this->getByCode($termCode);
        }

        $startDate = $term->getStartDate();

        $termsStartAfterCurrentTerm = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->startDateAfter($startDate)
            ->take($numOfTerms)
            ->get();

        return $this->constructTermCollection($termsStartAfterCurrentTerm);
    }//end getNextTerms()

    /**
     * Get current academic term.
     *
     * @return Term
     * @throws \Exception
     */
    public function getCurrentTerm(): Term
    {
        $term = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->currentTerm()
            ->first();

        if ($term === null) {
            throw new TermNotFoundException('Current term does not exist');
        }

        return $this->constructTerm($term);
    }//end getPrevTerms()

    /**
     * Get term by term code.
     *
     * @param TermCode $code
     *
     * @return Term
     * @throws TermNotFoundException
     */
    public function getByCode(TermCode $code): Term
    {
        $term = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->CodeEquals($code)
            ->first();

        if ($term === null) {
            throw new TermNotFoundException();
        }

        return $this->constructTerm($term);
    }//end constructTermCollection()

    /**
     * Get previous number of terms of specified term. Default is current term.
     *
     * @param int $numOfTerms
     * @param TermCode|null $termCode
     *
     * @return TermCollection
     * @throws \Exception
     */
    public function getPrevTerms(
        int $numOfTerms,
        TermCode $termCode = null
    ): TermCollection {
        $term = null;

        if ($numOfTerms < 0) {
            throw new InvalidArgumentException('numOfTerms must be a positive integer');
        }

        if ($termCode === null) {
            $term = $this->getCurrentTerm();
        } else {
            $term = $this->getByCode($termCode);
        }

        $startDate = $term->getStartDate();

        $termsStartAfterCurrentTerm = $this->getEloquentHandler()->model(EloquentTerm::class)
            ->startDateBefore($startDate)
            ->orderByDesc('stvterm_start_date')
            ->take($numOfTerms)
            ->get();

        return $this->constructTermCollection($termsStartAfterCurrentTerm)->reverse();
    }//end constructTerm()
}//end class
