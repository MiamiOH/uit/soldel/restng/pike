<?php

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Connection;
use Yajra\Oci8\Connectors\OracleConnector;
use Yajra\Oci8\Oci8Connection;

/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 9:08 AM
 *
 * @codeCoverageIgnore
 */
class EloquentFactory
{
    private $capsule;


    /**
     * EloquentFactory constructor.
     */
    public function __construct()
    {
        $this->capsule = new Capsule;
        Connection::resolverFor(
            'oracle',
            function ($connection, $database, $prefix, $config) {
                $connector = new OracleConnector();
                $connection = $connector->connect($config);
                $db = new Oci8Connection($connection, $database, $prefix, $config);
                if (!empty($config['skip_session_vars'])) {
                    return $db;
                }

                // set oracle session variables
                $sessionVars = [
                    'NLS_TIME_FORMAT' => 'HH24:MI:SS',
                    'NLS_DATE_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
                    'NLS_TIMESTAMP_FORMAT' => 'YYYY-MM-DD HH24:MI:SS',
                    'NLS_TIMESTAMP_TZ_FORMAT' => 'YYYY-MM-DD HH24:MI:SS TZH:TZM',
                    'NLS_NUMERIC_CHARACTERS' => '.,',
                ];
                // Like Postgres, Oracle allows the concept of "schema"
                if (isset($config['schema'])) {
                    $sessionVars['CURRENT_SCHEMA'] = $config['schema'];
                }

                if (isset($config['session'])) {
                    $sessionVars = array_merge($sessionVars, $config['session']);
                }

                $db->setSessionVars($sessionVars);

                return $db;
            }
        );
        $this->capsule->setAsGlobal();
        $this->capsule->bootEloquent();
    }//end __construct()


    public function addConnection(array $config, string $connectionName = 'default')
    {
        $this->capsule->addConnection($config, $connectionName);
    }//end addConnection()


    public function getEloquentHandler(string $connectionName = 'default')
    {
        return new EloquentHandler($this->capsule, $connectionName);
    }//end getEloquentHandler()
}//end class
