<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: ssrmeet, ssbsgid, stvbldg, gtvmtyp
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;

class Ssrmeet extends Model
{
    protected $table = 'ssrmeet';
    protected $dates = [
        'ssrmeet_start_date',
        'ssrmeet_end_date'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->join('ssbsgid', function (JoinClause $join) {
                    $join->on('ssbsgid_crn', '=', 'ssrmeet_crn')
                        ->on('ssbsgid_term_code', '=', 'ssrmeet_term_code');
                })
                    ->leftJoin('stvbldg', 'stvbldg_code', '=', 'ssrmeet_bldg_code')
                    ->leftJoin('gtvmtyp', 'gtvmtyp_code', '=', 'ssrmeet_mtyp_code');
            }
        );
    }//end boot()


    public function scopeByCourseSectionGuids(
        Builder $query,
        CourseSectionGuidCollection $courseSectionGuidCollection
    ) {
        $query->whereIn(
            'ssbsgid_guid',
            $courseSectionGuidCollection->toValueArray()
        );
    }

    public function getStartDate()
    {
        return $this->ssrmeet_start_date;
    }

    public function getEndDate()
    {
        return $this->ssrmeet_end_date;
    }

    public function getStartTime()
    {
        return $this->ssrmeet_begin_time;
    }

    public function getEndTime()
    {
        return $this->ssrmeet_end_time;
    }

    public function getRoom()
    {
        return $this->ssrmeet_room_code;
    }

    public function getBuildingCode()
    {
        return $this->ssrmeet_bldg_code;
    }

    public function getBuildingName()
    {
        return $this->stvbldg_desc;
    }

    public function getMondayDay()
    {
        return $this->ssrmeet_mon_day;
    }

    public function getTuesdayDay()
    {
        return $this->ssrmeet_tue_day;
    }

    public function getWednesdayDay()
    {
        return $this->ssrmeet_wed_day;
    }

    public function getThursdayDay()
    {
        return $this->ssrmeet_thu_day;
    }

    public function getFridayDay()
    {
        return $this->ssrmeet_fri_day;
    }

    public function getSaturdayDay()
    {
        return $this->ssrmeet_sat_day;
    }

    public function getSundayDay()
    {
        return $this->ssrmeet_sun_day;
    }

    public function getTypeCode()
    {
        return $this->ssrmeet_mtyp_code;
    }

    public function getTypeDescription()
    {
        return $this->gtvmtyp_desc;
    }

    public function getCourseSectionGuid()
    {
        return $this->ssbsgid_guid;
    }
}//end class
