<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: ssbsect, ssbsgid, stvschd, stvcamp, stvssts, stvterm, scbcrse, scbcgid,
 * sobptrm, baninst1.IA_ref_standardize_department,
 * baninst1.IA_ref_standardize_division
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCrnCollection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\TermCodeCrn;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

/**
 * Class Ssbsect
 *
 * @package MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models
 */
class Ssbsect extends Model
{
    /**
     * @var string
     */
    protected $table = 'ssbsect';
    /**
     * @var array
     */
    protected $dates = [
        'ssbsect_ptrm_start_date',
        'ssbsect_ptrm_end_date',
    ];

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->join(
                    'ssbsgid',
                    'ssbsgid_domain_surrogate_id',
                    '=',
                    'ssbsect_surrogate_id'
                )->join(
                    'stvschd',
                    'stvschd_code',
                    '=',
                    'ssbsect_schd_code'
                )->join(
                    'stvcamp',
                    'stvcamp_code',
                    '=',
                    'ssbsect_camp_code'
                )->join(
                    'stvssts',
                    'stvssts_code',
                    '=',
                    'ssbsect_ssts_code'
                )->join(
                    'stvterm',
                    'stvterm_code',
                    '=',
                    'ssbsect_term_code'
                )->join('scbcrse', function (JoinClause $join) {
                    $join->on('scbcrse_subj_code', '=', 'ssbsect_subj_code')
                        ->on('scbcrse_crse_numb', '=', 'ssbsect_crse_numb')
                        ->on('scbcrse_eff_term', '=', new Expression("
                    (select max(scbcrse_eff_term)
                    from scbcrse
                    where scbcrse_subj_code = ssbsect_subj_code
                    and scbcrse_crse_numb = ssbsect_crse_numb
                    and scbcrse_eff_term <= ssbsect_term_code)
                    "));
                })->join('scbcgid', function (JoinClause $join) {
                    $join->on('scbcgid_subj_code', '=', 'scbcrse_subj_code')
                        ->on('scbcgid_crse_numb', '=', 'scbcrse_crse_numb')
                        ->on('scbcgid_term_code_eff', '=', 'scbcrse_eff_term');
                })->leftJoin('sobptrm', function (JoinClause $join) {
                    $join->on('sobptrm_term_code', '=', 'ssbsect_term_code')
                        ->on('sobptrm_ptrm_code', '=', 'ssbsect_ptrm_code');
                })
                    ->leftJoin(
                        EloquentHandler::raw('baninst1.IA_ref_standardize_department SD1'),
                        function (JoinClause $join) {
                            $join->on('ssbsect_subj_code', '=', 'SD1.department_cd')
                                ->on('ssbsect_camp_code', '=', 'SD1.campus_group_cd')
                                ->where(
                                    'ssbsect_ptrm_end_date',
                                    '>',
                                    EloquentHandler::raw("nvl(SD1.effective_from_dt, to_date('1900-01-01', 'YYYY-MM-DD'))")
                                )
                                ->where(
                                    'ssbsect_ptrm_end_date',
                                    '<=',
                                    EloquentHandler::raw("nvl(SD1.effective_to_dt, to_date('9999-12-31', 'YYYY-MM-DD'))")
                                );
                        }
                    )
                    ->leftJoin(
                        EloquentHandler::raw('baninst1.IA_ref_standardize_department SD2'),
                        function (JoinClause $join) {
                            $join->on('ssbsect_subj_code', '=', 'SD2.department_cd')
                                ->where('SD2.campus_group_cd', '=', 'O')
                                ->where(
                                    'ssbsect_ptrm_end_date',
                                    '>',
                                    EloquentHandler::raw("nvl(SD2.effective_from_dt, to_date('1900-01-01', 'YYYY-MM-DD'))")
                                )
                                ->where(
                                    'ssbsect_ptrm_end_date',
                                    '<=',
                                    EloquentHandler::raw("nvl(SD2.effective_to_dt,to_date('9999-12-31', 'YYYY-MM-DD'))")
                                );
                        }
                    )
                    ->leftJoin(
                        EloquentHandler::raw('baninst1.IA_ref_standardize_division'),
                        function (JoinClause $join) {
                            $join->on('scbcrse_coll_code', '=', 'division_cd')
                                ->where(
                                    'ssbsect_ptrm_end_date',
                                    '>',
                                    EloquentHandler::raw("nvl(IA_ref_standardize_division.effective_from_dt, to_date('1900-01-01', 'YYYY-MM-DD'))")
                                )
                                ->where(
                                    'ssbsect_ptrm_end_date',
                                    '<=',
                                    EloquentHandler::raw("nvl(IA_ref_standardize_division.effective_to_dt, to_date('9999-12-31', 'YYYY-MM-DD'))")
                                );
                        }
                    )
                    ->whereNotNull('ssbsect_ptrm_code')
                    ->selectRaw("
                        ssbsect_crn,
                        ssbsect_term_code,
                        ssbsect_subj_code,
                        ssbsect_crse_numb,
                        ssbsect_crse_title,
                        ssbsgid_guid,
                        ssbsect_schd_code,
                        stvschd_desc,
                        ssbsect_camp_code,
                        stvcamp_desc,
                        ssbsect_ssts_code,
                        stvssts_desc,
                        ssbsect_credit_hrs,
                        stvterm_desc,
                        ssbsect_seq_numb,
                        ssbsect_max_enrl,
                        scbcgid_guid,
                        ssbsect_gradable_ind,
                        ssbsect_prnt_ind,
                        sobptrm_mgrd_web_upd_ind,
                        sobptrm_fgrd_web_upd_ind,
                        ssbsect_ptrm_code,
                        ssbsect_ptrm_start_date,
                        ssbsect_ptrm_end_date,
                        standardized_division_cd as standardized_div_code,
                        standardized_division_nm as standardized_div_name,
                        SD1.standardized_department_cd as standardized_dept_code,
                        SD1.standardized_department_nm as standardized_dept_name,
                        SD2.standardized_department_cd as standardized_dept_code_2,
                        SD2.standardized_department_nm as standardized_dept_name_2
                    ")
                    ->orderBy("ssbsect_term_code")
                    ->orderBy("ssbsect_subj_code")
                    ->orderBy("ssbsect_crse_numb")
                    ->orderBy('ssbsect_seq_numb');
            }
        );
    }//end boot()

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function partOfTerm()
    {
        return $this->hasOne(Stvptrm::class, 'stvptrm_code', 'ssbsect_ptrm_code');
    }

    /**
     * @param Builder $query
     * @param TermCode $termCode
     * @param Crn $crn
     */
    public function scopeByTermCodeCrn(Builder $query, TermCode $termCode, Crn $crn)
    {
        $query->where('ssbsect_crn', '=', $crn->getValue())
            ->where('ssbsect_term_code', '=', $termCode->getValue());
    }

    /**
     * @param Builder $query
     * @param CourseSectionGuid $identity
     */
    public function scopebyGuid(Builder $query, CourseSectionGuid $identity)
    {
        $query->where('ssbsgid_guid', '=', $identity->getValue());
    }

    public function scopeByTermCodeCrnCollection(
        Builder $query,
        TermCodeCrnCollection $collection
    ) {
        $query->where(function (Builder $subQuery) use ($collection) {
            /**
             * @var TermCodeCrn $item
             * */
            foreach ($collection as $item) {
                $subQuery->orWhere(function (Builder $q) use ($item) {
                    $q->where('ssbsect_term_code', '=', (string)$item->getTermCode())
                        ->where('ssbsect_crn', '=', (string)$item->getCrn());
                });
            }
        });
    }

    /**
     * @param Builder $query
     * @param CourseSectionGuidCollection $guids
     */
    public function scopebyGuids(Builder $query, CourseSectionGuidCollection $guids)
    {
        $query->whereIn(
            'ssbsgid_guid',
            $guids->toValueArray()
        );
    }

    /**
     * @return string
     */
    public function getCrn(): string
    {
        return $this->ssbsect_crn;
    }

    public function getTitle(): ?string
    {
        return $this->ssbsect_crse_title;
    }

    /**
     * @return string
     */
    public function getTermCode(): string
    {
        return $this->ssbsect_term_code;
    }

    /**
     * @return string
     */
    public function getSubjectCode(): string
    {
        return $this->ssbsect_subj_code;
    }

    /**
     * @return string
     */
    public function getCourseNumber(): string
    {
        return $this->ssbsect_crse_numb;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->ssbsgid_guid;
    }

    /**
     * @return mixed
     */
    public function getInstructionalTypeCode()
    {
        return $this->ssbsect_schd_code;
    }

    /**
     * @return mixed
     */
    public function getInstructionalTypeDescription()
    {
        return $this->stvschd_desc;
    }

    /**
     * @return mixed
     */
    public function getCampusCode()
    {
        return $this->ssbsect_camp_code;
    }

    /**
     * @return mixed
     */
    public function getCampusDescription()
    {
        return $this->stvcamp_desc;
    }

    /**
     * @return mixed
     */
    public function getCourseSectionStatusCode()
    {
        return $this->ssbsect_ssts_code;
    }

    /**
     * @return mixed
     */
    public function getCourseSectionStatusDescription()
    {
        return $this->stvssts_desc;
    }

    /**
     * @return mixed
     */
    public function getCreditHours()
    {
        return $this->ssbsect_credit_hrs;
    }

    /**
     * @return mixed
     */
    public function getTermDescription()
    {
        return $this->stvterm_desc;
    }

    /**
     * @return mixed
     */
    public function getSectionCode()
    {
        return $this->ssbsect_seq_numb;
    }

    /**
     * @return mixed
     */
    public function getMaxNumberOfEnrollment()
    {
        return $this->ssbsect_max_enrl;
    }

    /**
     * @return mixed
     */
    public function getCourseGuid()
    {
        return $this->scbcgid_guid;
    }

    /**
     * @return Stvptrm
     */
    public function getPartOfTerm(): Stvptrm
    {
        return $this->partOfTerm;
    }

    public function getPartOfTermStartDate(): Carbon
    {
        return $this->ssbsect_ptrm_start_date;
    }

    public function getPartOfTermEndDate(): Carbon
    {
        return $this->ssbsect_ptrm_end_date;
    }

    /**
     * @return string either 'Y' or 'N'
     */
    public function getMidtermGradeSubmissionAvailableIndicator(): string
    {
        return $this->sobptrm_mgrd_web_upd_ind;
    }

    /**
     * @return string either 'Y' or 'N'
     */
    public function getFinalGradeSubmissionAvailableIndicator(): string
    {
        return $this->sobptrm_fgrd_web_upd_ind;
    }

    /**
     *
     * @return string either 'Y' or 'N'
     */
    public function getFinalGradeRequiredIndicator(): string
    {
        return $this->ssbsect_gradable_ind;
    }


    /**
     *
     * @return string either 'Y' or 'N'
     */
    public function getPrintIndicator(): string
    {
        return $this->ssbsect_prnt_ind;
    }

    public function getStandardizedDivisionCode()
    {
        return $this->standardized_div_code;
    }

    public function getStandardizedDivisionName()
    {
        return $this->standardized_div_name;
    }

    public function getStandardizedDepartmentCode()
    {
        return $this->standardized_dept_code;
    }

    public function getStandardizedDepartmentName()
    {
        return $this->standardized_dept_name;
    }

    public function getLegacyStandardizedDepartmentCode()
    {
        return $this->standardized_dept_code_2;
    }

    public function getLegacyStandardizedDepartmentName()
    {
        return $this->standardized_dept_name_2;
    }
}//end class
