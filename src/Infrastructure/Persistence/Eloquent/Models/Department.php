<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: stvdept
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;

class Department extends Model
{
    protected $table = 'stvdept';


    public function scopeOrderByCode(
        Builder $query,
        string $direction = 'asc'
    ): Builder {
        return $query->orderBy('stvdept_code', $direction);
    }//end scopeOrderByCode()


    public function scopeCodeEquals(Builder $query, DepartmentCode $code): Builder
    {
        return $query->where('stvdept_code', '=', $code->getValue());
    }//end scopeCodeEquals()

    public function getCode(): string
    {
        return $this->stvdept_code;
    }

    public function getDescription(): string
    {
        return $this->stvdept_desc;
    }
}//end class
