<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: szbuniq, sfrstcr, stvrsts, ssbsgid
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidUniqueIdCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuidUniqueId;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class Sfrstcr extends Model
{
    protected $table = 'sfrstcr';

    protected $dates = [
        'sfrstcr_grde_date',
        'sfrstcr_last_attend',
    ];

    public function scopeJoinSzbuniqAndStvrsts(Builder $query)
    {
        $query->join('szbuniq', 'sfrstcr_pidm', '=', 'szbuniq_pidm')
            ->join('stvrsts', 'sfrstcr_rsts_code', '=', 'stvrsts_code')
            ->join('ssbsgid', function (JoinClause $join) {
                $join->on('sfrstcr_crn', '=', 'ssbsgid_crn')
                    ->on('sfrstcr_term_code', '=', 'ssbsgid_term_code');
            });
    }

    public function scopeOrderByGuidPidm(Builder $query)
    {
        return $query->orderBy( 'ssbsgid_guid','desc')
            ->orderBy('sfrstcr_pidm','desc');
    }

    public function scopeByUniqueIdTermCodeCrn(
        Builder $query,
        UniqueId $uniqueId,
        TermCode $termCode,
        Crn $crn
    ) {
        $query->where('szbuniq_unique_id', '=', $uniqueId->getUppercaseValue())
            ->where('sfrstcr_term_code', '=', $termCode->getValue())
            ->where('sfrstcr_crn', '=', $crn->getValue());
    }

    public function scopeByCourseSectionGuidUniqueIdCollection(
        Builder $query,
        CourseSectionGuidUniqueIdCollection $collection
    ) {
        $query->where(function (Builder $subQuery) use ($collection) {
            /**
             * @var CourseSectionGuidUniqueId $item
             * */
            foreach ($collection as $item) {
                $subQuery->orWhere(function (Builder $q) use ($item) {
                    $q->where(
                        'ssbsgid_guid',
                        '=',
                        (string)$item->getCourseSectionGuid()
                        )
                            ->where(
                                'szbuniq_unique_id',
                                '=',
                                $item->getUniqueId()->getUppercaseValue()
                            );
                });
            }
        });
    }

    public function scopeByUniqueIdTermCode(
        Builder $query,
        UniqueId $uniqueId,
        TermCode $termCode
    ) {
        $query->where('szbuniq_unique_id', '=', $uniqueId->getUppercaseValue())
            ->where('sfrstcr_term_code', '=', $termCode->getValue());
    }

    public function scopeByCourseSectionGuid(
        Builder $query,
        CourseSectionGuid $courseSectionGuid
    ) {
        $query->where('ssbsgid_guid', '=', $courseSectionGuid->getValue());
    }

    public function scopeByUniqueId(
        Builder $query,
        UniqueId $uniqueId
    ) {
        $query->where('szbuniq_unique_id', '=', $uniqueId->getUppercaseValue());
    }

    public function scopeByTermCodeCrn(
        Builder $query,
        TermCode $termCode,
        Crn $crn
    ) {
        $query->where('sfrstcr_term_code', '=', $termCode->getValue())
            ->where('sfrstcr_crn', '=', $crn->getValue());
    }


    public function scopeByTermCodes(
        Builder $query,
        TermCodeCollection $termCodes
    ) {
        $query->whereIn(
            'sfrstcr_term_code',
            $termCodes->toValueArray()
        );
    }

    public function scopeIsActive(Builder $query)
    {
        $query->whereIn('sfrstcr_rsts_code', ['RE', 'RW', 'AU']);
    }

    public function scopeIsNotActive(Builder $query)
    {
        $query->whereNotIn('sfrstcr_rsts_code', ['RE', 'RW', 'AU']);
    }

    public function scopebyUniqueIds(
        Builder $query,
        UniqueIdCollection $uniqueIds
    ) {
        $query->whereIn('szbuniq_unique_id', $uniqueIds->toUpperCaseArray());
    }

    public function scopeByCrns(
        Builder $query,
        CrnCollection $crns
    ) {
        $query->whereIn(
            'sfrstcr_crn',
            $crns->toValueArray()
        );
    }

    public function scopeByCourseSectionGuidCollection(
        Builder $query,
        CourseSectionGuidCollection $courseSectionGuidCollection
    ) {
        $query->whereIn(
            'ssbsgid_guid',
            $courseSectionGuidCollection->toValueArray()
        );
    }

    public function getUniqueId()
    {
        return $this->szbuniq_unique_id;
    }

    public function getTermCode()
    {
        return $this->sfrstcr_term_code;
    }

    public function getCrn()
    {
        return $this->sfrstcr_crn;
    }

    public function getAttendHours()
    {
        return $this->sfrstcr_attend_hr;
    }

    public function getLastAttendDate()
    {
        return $this->sfrstcr_last_attend;
    }

    public function getStatusCode()
    {
        return $this->sfrstcr_rsts_code;
    }

    public function getStatusDescription()
    {
        return $this->stvrsts_desc;
    }

    public function getGradeDate()
    {
        return $this->sfrstcr_grde_date;
    }

    public function getMidtermGradeString()
    {
        return $this->sfrstcr_grde_code_mid;
    }

    public function getFinalGradeString()
    {
        return $this->sfrstcr_grde_code;
    }

    public function getCreditHours()
    {
        return $this->sfrstcr_credit_hr;
    }

    public function getLevelCode()
    {
        return $this->sfrstcr_levl_code;
    }

    public function getPidm()
    {
        return $this->sfrstcr_pidm;
    }

    public function getCourseSectionGuid()
    {
        return $this->ssbsgid_guid;
    }

    public function getGuid()
    {
        return $this->sfrstcr_guid;
    }

    public function getGradeModeCode()
    {
        return $this->sfrstcr_gmod_code;
    }
}//end class
