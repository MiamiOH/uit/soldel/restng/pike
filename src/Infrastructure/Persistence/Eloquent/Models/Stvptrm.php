<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 3:14 PM
 *
 * Tables: stvptrm
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class Stvptrm extends Model
{
    protected $table = 'stvptrm';

    public function getCode(): string
    {
        return $this->stvptrm_code;
    }

    public function getDescription(): string
    {
        return $this->stvptrm_desc;
    }
}
