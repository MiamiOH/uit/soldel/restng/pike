<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: scbcrse, scbcgid, scbdesc
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;

class Scbdesc extends Model
{
    protected $table = 'scbdesc';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->join('scbcgid', function (JoinClause $join) {
                    $join->on('scbdesc_subj_code', '=', 'scbcgid_subj_code')
                        ->on('scbdesc_crse_numb', '=', 'scbcgid_crse_numb')
                        ->on('scbdesc_term_code_eff', '>=', 'scbcgid_term_code_eff');
                })
                    ->select([
                        'scbdesc_term_code_end',
                        'scbdesc_term_code_eff',
                        'scbdesc_text_narrative',
                        'scbcgid_guid'
                    ]);
            }
        );
    }//end boot()

    public function scopeByCourseGuids(
        Builder $query,
        array $courseGuids
    ) {
        $query->whereIn('scbcgid_guid', $courseGuids);
    }

    public function getDescription(): ?string
    {
        return $this->scbdesc_text_narrative;
    }

    public function getStartTerm(): ?string
    {
        return $this->scbdesc_term_code_eff;
    }

    public function getEndTerm(): ?string
    {
        return $this->scbdesc_term_code_end;
    }

    public function getCourseGuid(): string
    {
        return $this->scbcgid_guid;
    }
}//end class
