<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 5:12 PM
 *
 * Tables: stvdept
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class Stvdept extends Model
{
    protected $table = 'stvdept';
}
