<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: sirasgn, ssbsect, szbuniq, ssbsgid
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class Sirasgn extends Model
{
    protected $table = 'sirasgn';

    public function scopeJoinSsbsect(Builder $query)
    {
        $query->join('ssbsect', function (JoinClause $join) {
            $join->on('sirasgn_crn', '=', 'ssbsect_crn')
                ->on('sirasgn_term_code', '=', 'ssbsect_term_code');
        })->join(
            'ssbsgid',
            'ssbsgid_domain_surrogate_id',
            '=',
            'ssbsect_surrogate_id'
        );
    }

    public function scopeJoinSzbuniq(Builder $query)
    {
        $query->join('szbuniq', 'sirasgn_pidm', '=', 'szbuniq_pidm');
    }

    public function scopeByTermCodeCrn(Builder $query, TermCode $termCode, Crn $crn)
    {
        $query->where('sirasgn_crn', '=', $crn->getValue())
            ->where('sirasgn_term_code', '=', $termCode->getValue());
    }

    public function scopeByUniqueIdTermCodeCrn(
        Builder $query,
        UniqueId $uniqueId,
        TermCode $termCode,
        Crn $crn
    ) {
        $query->where('sirasgn_crn', '=', $crn->getValue())
            ->where('sirasgn_term_code', '=', $termCode->getValue())
            ->where('szbuniq_unique_id', '=', $uniqueId->getUppercaseValue());
    }

    public function scopeByUniqueIdTermCode(
        Builder $query,
        UniqueId $uniqueId,
        TermCode $termCode
    ) {
        $query->where('sirasgn_term_code', '=', $termCode->getValue())
            ->where('szbuniq_unique_id', '=', $uniqueId->getUppercaseValue());
    }

    public function scopeByCourseSectionGuids(
        Builder $query,
        CourseSectionGuidCollection $guidCollection
    ) {
        $query->whereIn(
            'ssbsgid_guid',
            $guidCollection->toValueArray()
            );
    }

    public function scopebyGuid(Builder $query, InstructorAssignmentGuid $identity)
    {
        $query->where('sirasgn_guid', '=', $identity->getValue());
    }

    public function scopeByTermCodes(
        Builder $query,
        TermCodeCollection $termCodes
    ) {
        $query->whereIn(
            'sirasgn_term_code',
            $termCodes->toValueArray()
        );
    }

    public function scopebyUniqueIds(
        Builder $query,
        UniqueIdCollection $uniqueIds
    ) {
        $query->whereIn('szbuniq_unique_id', $uniqueIds->toUpperCaseArray());
    }

    public function scopebyCrns(
        Builder $query,
        CrnCollection $crns
    ) {
        $query->whereIn(
            'sirasgn_crn',
            $crns->toValueArray()
        );
    }


    public function getTermCode(): string
    {
        return $this->sirasgn_term_code;
    }

    public function getUniqueId(): string
    {
        return strtolower($this->szbuniq_unique_id);
    }

    public function getCrn(): string
    {
        return $this->sirasgn_crn;
    }

    public function getGuid(): string
    {
        return $this->sirasgn_guid;
    }

    public function getCourseSectionGuid(): string
    {
        return $this->ssbsgid_guid;
    }

    public function getIsPrimaryFlag()
    {
        return $this->sirasgn_primary_ind;
    }
}//end class
