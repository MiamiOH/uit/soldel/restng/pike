<?php
/**
 * Author: liaom
 * Date: 5/10/18
 * Time: 11:32 AM
 *
 * Tables: ssrattr, ssbsgid, stvattr
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;

class Ssrattr extends Model
{
    protected $table = 'ssrattr';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->join('ssbsgid', function (JoinClause $join) {
                    $join->on('ssbsgid_crn', '=', 'ssrattr_crn')
                        ->on('ssbsgid_term_code', '=', 'ssrattr_term_code');
                })
                    ->join(
                        'stvattr',
                        'stvattr_code',
                        '=',
                        'ssrattr_attr_code'
                    );
            }
        );
    }//end boot()

    public function scopeByCourseSectionGuids(
        Builder $query,
        CourseSectionGuidCollection $courseSectionGuidCollection
    ) {
        $query->whereIn(
            'ssbsgid_guid',
            $courseSectionGuidCollection->toValueArray()
        );
    }

    public function getCode(): string
    {
        return $this->ssrattr_attr_code;
    }

    public function getDescription(): string
    {
        return $this->stvattr_desc;
    }

    public function getCourseSectionGuid(): string
    {
        return $this->ssbsgid_guid;
    }
}
