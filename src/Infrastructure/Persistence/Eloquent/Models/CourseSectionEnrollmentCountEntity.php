<?php
/**
 * Author: liaom
 * Date: 5/14/18
 * Time: 10:24 AM
 *
 * Tables: ssbsect, ssbsgid, sfrstcr, stvrsts
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

class CourseSectionEnrollmentCountEntity extends Model
{
    protected $table = 'ssbsect';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->join(
                    'ssbsgid',
                    'ssbsgid_domain_surrogate_id',
                    '=',
                    'ssbsect_surrogate_id'
                )->select([
                    'ssbsect_max_enrl',
                    'ssbsect_seats_avail',
                    'ssbsgid_guid',
                    EloquentHandler::raw("
                    (
                      select count(sfrstcr_pidm) 
                      from sfrstcr 
                      where sfrstcr_term_code = ssbsect_term_code
                      and sfrstcr_crn = ssbsect_crn
					  and sfrstcr_rsts_code in 
					  (
					    select stvrsts_code
                        from stvrsts
                        where stvrsts_incl_sect_enrl = 'Y'
                      )
                    ) as enrl
                    "),
                    EloquentHandler::raw("
                    (
                      select count(sfrstcr_pidm)
                      from sfrstcr
                      where sfrstcr_term_code = ssbsect_term_code
                      and sfrstcr_crn = ssbsect_crn
                      and sfrstcr_rsts_code in 
                      (
                        select stvrsts_code
                        from stvrsts
                        where stvrsts_incl_sect_enrl = 'Y'
                        and stvrsts_withdraw_ind = 'N'
                      )
                    ) as enrl_active
                    ")
                ]);
            }
        );
    }//end boot()

    public function scopeByCourseSectionGuids(
        Builder $query,
        CourseSectionGuidCollection $courseSectionGuids
    ) {
        $query->whereIn(
            'ssbsgid_guid',
            $courseSectionGuids->toValueArray()
        );
    }

    public function getNumOfMaxEnrollment()
    {
        return $this->ssbsect_max_enrl;
    }

    public function getNumOfCurrentEnrollment()
    {
        return $this->enrl;
    }

    public function getNumOfActiveEnrollment()
    {
        return $this->enrl_active;
    }

    public function getNumOfAvailableEnrollment()
    {
        return $this->ssbsect_seats_avail;
    }

    public function getCourseSectionGuid()
    {
        return $this->ssbsgid_guid;
    }
}
