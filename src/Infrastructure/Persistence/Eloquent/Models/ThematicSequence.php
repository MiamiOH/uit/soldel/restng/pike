<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: stvmajr
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

class ThematicSequence extends Model
{
    protected $table = 'stvmajr';


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->where('stvmajr_valid_concentratn_ind', '=', 'Y')
                    ->where(EloquentHandler::raw('length(stvmajr_code)'), '=', '4');
            }
        );
    }//end boot()


    public function scopeCodeEquals(
        Builder $query,
        ThematicSequenceCode $code
    ): Builder {
        return $query->where('stvmajr_code', $code->getValue());
    }//end scopeCodeEquals()


    public function scopeOrderByCode(
        Builder $query,
        string $direction = 'asc'
    ): Builder {
        return $query->orderBy('stvmajr_code', $direction);
    }//end scopeOrderByCode()

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->stvmajr_code;
    }//end getCode()


    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->stvmajr_desc;
    }//end getDescription()
}//end class
