<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 5:13 PM
 *
 * Tables: stvsubj
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class Stvsubj extends Model
{
    protected $table = 'stvsubj';
}
