<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: stvmajr
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use MiamiOH\Pike\Domain\Collection\MajorCodeCollection;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;

class Major extends Model
{
    protected $table = 'stvmajr';


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->where('stvmajr_valid_major_ind', '=', 'Y');
            }
        );
    }//end boot()


    public function scopeCodeEquals(Builder $query, MajorCode $code): Builder
    {
        return $query->where('stvmajr_code', $code->getValue());
    }//end scopeCodeEquals()


    public function scopeOrderByMajorCode(
        Builder $query,
        string $direction = 'asc'
    ): Builder {
        return $query->orderBy('stvmajr_code', $direction);
    }//end scopeOrderByMajorCode()


    public function scopeCodeIn(Builder $query, MajorCodeCollection $codes): Builder
    {
        return $query->whereIn(
            'stvmajr_code',
            $codes->toValueArray()
        );
    }//end scopeCodeIn()


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->stvmajr_code;
    }//end getCode()


    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->stvmajr_desc;
    }//end getDescription()
}//end class
