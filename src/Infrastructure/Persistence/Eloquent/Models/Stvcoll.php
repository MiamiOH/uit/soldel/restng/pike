<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 5:12 PM
 *
 * Tables: stvcoll
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Model;

class Stvcoll extends Model
{
    protected $table = 'stvcoll';
}
