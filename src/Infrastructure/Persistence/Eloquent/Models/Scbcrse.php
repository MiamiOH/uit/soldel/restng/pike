<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: scbcrse, scbcgid, scbdesc
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;

class Scbcrse extends Model
{
    protected $table = 'scbcrse';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->join('scbcgid', function (JoinClause $join) {
                    $join->on('scbcgid_subj_code', '=', 'scbcrse_subj_code')
                        ->on('scbcgid_crse_numb', '=', 'scbcrse_crse_numb')
                        ->on('scbcgid_term_code_eff', '=', 'scbcrse_eff_term');
                })->select([
                    'scbcrse_eff_term',
                    'scbcrse_subj_code',
                    'scbcrse_crse_numb',
                    'scbcrse_title',
                    'scbcgid_guid',
                    'scbcrse_coll_code',
                    'scbcrse_dept_code',
                    'scbcrse_credit_hr_high',
                    'scbcrse_credit_hr_low',
                    'scbcrse_lec_hr_high',
                    'scbcrse_lec_hr_low',
                    'scbcrse_lec_hr_ind',
                    'scbcrse_lab_hr_high',
                    'scbcrse_lab_hr_low',
                    'scbcrse_lab_hr_ind',
                    'scbcrse_credit_hr_ind'
                ]);
            }
        );
    }//end boot()

    public function school()
    {
        return $this->hasOne(Stvcoll::class, 'stvcoll_code', 'scbcrse_coll_code');
    }

    public function department()
    {
        return $this->hasOne(Stvdept::class, 'stvdept_code', 'scbcrse_dept_code');
    }

    public function subject()
    {
        return $this->hasOne(Stvsubj::class, 'stvsubj_code', 'scbcrse_subj_code');
    }

    public function scopeByCourseGuids(
        Builder $query,
        array $courseGuids
    ) {
        $query->whereIn('scbcgid_guid', $courseGuids);
    }

    public function getTermEffectedCode(): string
    {
        return $this->scbcrse_eff_term;
    }

    public function getSubjectCode(): string
    {
        return $this->scbcrse_subj_code;
    }

    public function getSubjectDescription(): string
    {
        return $this->subject->stvsubj_desc;
    }

    public function getNumber(): string
    {
        return $this->scbcrse_crse_numb;
    }

    public function getTitle(): string
    {
        return $this->scbcrse_title;
    }

    public function getGuid(): string
    {
        return $this->scbcgid_guid;
    }

    public function getSchoolCode(): string
    {
        return $this->scbcrse_coll_code;
    }

    public function getSchoolName(): string
    {
        return $this->school->stvcoll_desc;
    }

    public function getDepartmentCode(): string
    {
        return $this->scbcrse_dept_code;
    }

    public function getDepartmentName(): string
    {
        return $this->department->stvdept_desc;
    }

    public function getCreditHoursHigh()
    {
        return $this->scbcrse_credit_hr_high;
    }

    public function getCreditHoursLow()
    {
        return $this->scbcrse_credit_hr_low;
    }

    public function getLectureHoursHigh()
    {
        return $this->scbcrse_lec_hr_high;
    }

    public function getLectureHoursLow()
    {
        return $this->scbcrse_lec_hr_low;
    }

    public function getLectureHoursIndicator()
    {
        return $this->scbcrse_lec_hr_ind;
    }

    public function getLabHoursHigh()
    {
        return $this->scbcrse_lab_hr_high;
    }

    public function getLabHoursLow()
    {
        return $this->scbcrse_lab_hr_low;
    }

    public function getLabHoursIndicator()
    {
        return $this->scbcrse_lab_hr_ind;
    }

    public function getCreditHourIndicator()
    {
        return $this->scbcrse_credit_hr_ind;
    }
}//end class
