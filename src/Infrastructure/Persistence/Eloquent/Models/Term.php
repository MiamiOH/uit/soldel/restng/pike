<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: stvterm
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Infrastructure\Persistence\Eloquent\EloquentHandler;

class Term extends Model
{
    protected $table = 'stvterm';
    protected $dates = [
        'stvterm_start_date',
        'stvterm_end_date',
    ];


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->leftJoin(
                    'sobterm',
                    'stvterm_code',
                    '=',
                    'sobterm_term_code'
                )
                    ->orderBy('stvterm_start_date');
            }
        );
    }//end boot()


    public function scopeCurrentTerm(Builder $query): Builder
    {
        return $query->where(
            'stvterm_code',
            '=',
            EloquentHandler::raw('fz_get_term(varInDate=>TRUNC(SYSDATE))')
        );
    }//end scopeCurrentTerm()


    public function scopeNextTerm(Builder $query, TermCode $termCode = null): Builder
    {
        $nextTermCode = static::where(
            'stvterm_code',
            '>',
            $termCode === null ? EloquentHandler::raw('fz_get_term(varInDate=>TRUNC(SYSDATE))') : $termCode->getValue()
        )->min('stvterm_code');

        return $query->where('stvterm_code', '=', $nextTermCode);
    }//end scopeNextTerm()


    public function scopePrevTerm(Builder $query, TermCode $termCode = null): Builder
    {
        $prevTermCode = static::where(
            'stvterm_code',
            '<',
            $termCode === null ? EloquentHandler::raw('fz_get_term(varInDate=>TRUNC(SYSDATE))') : $termCode->getValue()
        )->max('stvterm_code');

        return $query->where('stvterm_code', '=', $prevTermCode);
    }//end scopePrevTerm()


    public function scopeCodeIn(
        Builder $query,
        TermCodeCollection $codeCollection
    ): Builder {
        return $query->whereIn(
            'stvterm_code',
            $codeCollection->toValueArray()
        );
    }//end scopeCodeIn()


    public function scopeCodeEquals(Builder $query, TermCode $code): Builder
    {
        return $query->where('stvterm_code', '=', $code->getValue());
    }//end scopeCodeEquals()


    public function scopeStartDateAfter(Builder $query, Carbon $date): Builder
    {
        return $query->whereDate('stvterm_start_date', '>', $date);
    }//end scopeStartDateAfter()


    public function scopeStartDateBefore(Builder $query, Carbon $date): Builder
    {
        return $query->whereDate('stvterm_start_date', '<', $date);
    }//end scopeStartDateBefore()


    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->stvterm_code;
    }//end getCode()


    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->stvterm_desc;
    }//end getDescription()


    /**
     * @return Carbon
     */
    public function getStartDate(): Carbon
    {
        return $this->stvterm_start_date;
    }//end getStartDate()


    /**
     * @return Carbon
     */
    public function getEndDate(): Carbon
    {
        return $this->stvterm_end_date;
    }//end getEndDate()


    /**
     * @return bool
     */
    public function isDisplayed(): bool
    {
        return $this->sobterm_dynamic_sched_term_ind === 'Y' ? true : false;
    }//end isDisplayed()


    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->stvterm_code = $code;
    }//end setCode()


    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->stvterm_desc = $description;
    }//end setDescription()


    /**
     * @param Carbon $startDate
     */
    public function setStartDate(Carbon $startDate)
    {
        $this->stvterm_start_date = $startDate;
    }//end setStartDate()


    /**
     * @param Carbon $endDate
     */
    public function setEndDate(Carbon $endDate)
    {
        $this->stvterm_end_date = $endDate;
    }//end setEndDate()


    /**
     * @param bool $isDisplayed
     */
    public function setIsDisplayed(bool $isDisplayed)
    {
        $this->sobterm_dynamic_sched_term_ind = $isDisplayed;
    }//end setIsDisplayed()
}//end class
