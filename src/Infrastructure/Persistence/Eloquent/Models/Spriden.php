<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 4/4/18
 * Time: 11:25 AM
 *
 * Tables: spriden, szbuniq, spbpers
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use MiamiOH\Pike\Domain\Collection\BannerIdCollection;
use MiamiOH\Pike\Domain\Collection\PidmCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class Spriden extends Model
{
    protected $table = 'spriden';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'default',
            function (Builder $query) {
                $query->join('szbuniq', 'szbuniq_pidm', '=', 'spriden_pidm')
                    ->join('spbpers', 'spbpers_pidm', '=', 'spriden_pidm')
                    ->whereNull('spriden_change_ind');
            }
        );
    }//end boot()

    public function scopeByUniqueId(Builder $query, UniqueId $uniqueId)
    {
        $query->where('szbuniq_unique_id', '=', $uniqueId->getUppercaseValue());
    }

    public function scopeByUniqueIds(
        Builder $query,
        UniqueIdCollection $uniqueIdCollection
    ) {
        $query->whereIn(
            'szbuniq_unique_id',
            $uniqueIdCollection->toUpperCaseArray()
        );
    }

    public function scopeByPidm(Builder $query, Pidm $pidm)
    {
        $query->where('spriden_pidm', '=', $pidm->getValue());
    }

    public function scopeByPidms(Builder $query, PidmCollection $pidmCollection)
    {
        $query->whereIn(
            'spriden_pidm',
            $pidmCollection->toValueArray()
        );
    }

    public function scopeByBannerId(Builder $query, BannerId $bannerId)
    {
        $query->where('spriden_id', '=', $bannerId->getValue());
    }

    public function scopeByBannerIds(
        Builder $query,
        BannerIdCollection $bannerIdCollection
    ) {
        $query->whereIn(
            'spriden_id',
            $bannerIdCollection->toValueArray()
        );
    }

    public function getPidm(): string
    {
        return $this->spriden_pidm;
    }

    public function getBannerId(): string
    {
        return $this->spriden_id;
    }

    public function getUniqueId(): string
    {
        return $this->szbuniq_unique_id;
    }

    public function getLastName(): ?string
    {
        return $this->spriden_last_name;
    }

    public function getFirstName(): ?string
    {
        return $this->spriden_first_name;
    }

    public function getMiddleName(): ?string
    {
        return $this->spriden_mi;
    }

    public function getNamePrefix(): ?string
    {
        return $this->spbpers_name_prefix;
    }

    public function getNameSuffix(): ?string
    {
        return $this->spbpers_name_suffix;
    }

    public function getPreferredFirstName(): ?string
    {
        return $this->spbpers_pref_first_name;
    }
}//end class
