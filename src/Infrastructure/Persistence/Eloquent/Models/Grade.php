<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/7/18
 * Time: 3:19 PM
 *
 * Tables: sfrstcr, ssbsgid, szbuniq
 */

namespace MiamiOH\Pike\Infrastructure\Persistence\Eloquent\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidUniqueIdCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuidUniqueId;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Exception\Exception;

class Grade extends Model
{
    protected $table = 'sfrstcr';
    protected $guarded = [];
    public $timestamps = [];
    protected $primaryKey = 'sfrstcr_surrogate_id';
    public $incrementing = false;

    protected $dates = [
        'sfrstcr_last_attend',
        'sfrstcr_activity_date'
    ];

    public function scopeByCourseSectionGuidAndUniqueId(
        Builder $query,
        CourseSectionGuid $courseSectionGuid,
        UniqueId $uniqueId
    ) {
        $query->joinSsbsgidAndszbuniq()
            ->where('szbuniq_unique_id', '=', $uniqueId->getUppercaseValue())
            ->where('ssbsgid_guid', '=', $courseSectionGuid->getValue());
    }

    public function scopeByCourseSectionGuidUniqueIdCollection(
        Builder $query,
        CourseSectionGuidUniqueIdCollection $collection
    ) {
        $query->joinSsbsgidAndszbuniq()
            ->where(function (Builder $subQuery) use ($collection) {
                /**
                 * @var CourseSectionGuidUniqueId $item
                 * */
                foreach ($collection as $item) {
                    $subQuery->orWhere(function (Builder $q) use ($item) {
                        $q->where(
                            'ssbsgid_guid',
                            '=',
                            (string)$item->getCourseSectionGuid()
                        )
                            ->where(
                                'szbuniq_unique_id',
                                '=',
                                $item->getUniqueId()->getUppercaseValue()
                            );
                    });
                }
            });
    }

    public function scopeJoinSsbsgidAndszbuniq(Builder $query)
    {
        $query->join('ssbsgid', function (JoinClause $join) {
            $join->on('sfrstcr_crn', '=', 'ssbsgid_crn')
                ->on("sfrstcr_term_code", '=', 'ssbsgid_term_code');
        })->join('szbuniq', 'sfrstcr_pidm', '=', 'szbuniq_pidm');
    }

    public function scopeByUniqueIds(Builder $query, UniqueIdCollection $uniqueIds)
    {
        $query->whereIn(
            'szbuniq_unique_id',
            $uniqueIds->toUpperCaseArray()
        );
    }

    public function scopeByCourseSectionGuids(
        Builder $query,
        CourseSectionGuidCollection $courseSectionGuids
    ) {
        $query->whereIn(
            'ssbsgid_guid',
            $courseSectionGuids->toValueArray()
        );
    }

    public function scopeByTermCodes(Builder $query, TermCodeCollection $termCodes)
    {
        $query->whereIn(
            'sfrstcr_term_code',
            $termCodes->toValueArray()
        );
    }

    public function scopeByCrns(Builder $query, CrnCollection $crns)
    {
        $query->whereIn(
            'sfrstcr_crn',
            $crns->toValueArray()
        );
    }

    public function scopeByTermCodesCrnsUniqueIds(
        Builder $query,
        TermCodeCollection $termCodes,
        CrnCollection $crns,
        UniqueIdCollection $uniqueIds
    ) {
        $query->join('ssbsgid', function (JoinClause $join) {
            $join->on('sfrstcr_crn', '=', 'ssbsgid_crn')
                ->on("sfrstcr_term_code", '=', 'ssbsgid_term_code');
        })->join('szbuniq', 'sfrstcr_pidm', '=', 'szbuniq_pidm')
            ->whereIn(
                'szbuniq_unique_id',
                $uniqueIds->toUpperCaseArray()
            )
            ->whereIn(
                'sfrstcr_crn',
                $crns->toValueArray()
            )
            ->whereIn(
                'sfrstcr_term_code',
                $termCodes->toValueArray()
            );
    }

    public function getMidtermValue(): ?string
    {
        return $this->sfrstcr_grde_code_mid;
    }

    public function getFinalValue(): ?string
    {
        return $this->sfrstcr_grde_code;
    }

    public function getGradeModeCode(): ?string
    {
        return $this->sfrstcr_gmod_code;
    }

    public function getCrn(): ?string
    {
        return $this->sfrstcr_crn;
    }

    public function getTermCode(): ?string
    {
        return $this->sfrstcr_term_code;
    }

    public function getUniqueId(): ?string
    {
        return $this->szbuniq_unique_id;
    }

    public function getCourseSectionGuid(): ?string
    {
        return $this->ssbsgid_guid;
    }

    public function setGrade(
        GradeType $gradeType,
        GradeValue $gradeValue,
        UniqueId $requesterUniqueId,
        ?float $attendHours,
        ?Carbon $lastAttendDate
    ) {
        if ($gradeType->isFinal()) {
            $this->sfrstcr_grde_code = (string)$gradeValue;
        } elseif ($gradeType->isMidterm()) {
            $this->sfrstcr_grde_code_mid = (string)$gradeValue;
        } else {
            throw new Exception('Unsupported grade type');
        }

        $this->sfrstcr_attend_hr = $attendHours;
        $this->sfrstcr_last_attend = $lastAttendDate;
        $this->sfrstcr_user = (string)$requesterUniqueId;
        $this->sfrstcr_activity_date = Carbon::now();

        $this->save();
    }
}
