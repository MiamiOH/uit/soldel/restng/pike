<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 5:19 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidUniqueIdException;

final class UniqueId extends AbstractSingleAttributeValueObject
{
    /**
     * @return string
     */
    public function getValue(): string
    {
        return strtolower($this->value);
    }

    public function getUppercaseValue(): string
    {
        return strtoupper($this->value);
    }

    protected function validate(string $value): void
    {
        //$value = strtoupper($value);

        //if (strlen($value) > 8 || !preg_match('/^[A-Z]+[0-9]*$/', $value)) {
        //    throw new InvalidUniqueIdException();
        //}
    }
}
