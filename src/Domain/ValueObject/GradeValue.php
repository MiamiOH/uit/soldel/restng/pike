<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 5:19 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidGradeValueException;

final class GradeValue extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (strlen($value) > 6) {
            throw new InvalidGradeValueException();
        }
    }
}
