<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 5:10 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidCrnException;

final class Crn extends AbstractSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if ($value !== strval(intval($value))) {
            throw new InvalidCrnException();
        }
    }
}
