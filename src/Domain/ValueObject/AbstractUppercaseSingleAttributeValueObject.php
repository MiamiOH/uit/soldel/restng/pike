<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 8:54 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

abstract class AbstractUppercaseSingleAttributeValueObject extends AbstractSingleAttributeValueObject
{
    public function __construct(string $value)
    {
        $value = strtoupper($value);
        parent::__construct($value);
    }
}
