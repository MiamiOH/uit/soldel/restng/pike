<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 5:10 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidBuildingCodeException;

final class BuildingCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (strlen($value) > 6) {
            throw new InvalidBuildingCodeException();
        }
    }
}
