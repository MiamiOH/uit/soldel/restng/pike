<?php
/**
 * Author: liaom
 * Date: 5/3/18
 * Time: 1:11 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidCourseGuidException;

class CourseGuid extends Guid
{
    protected static $exceptionName = InvalidCourseGuidException::class;
}
