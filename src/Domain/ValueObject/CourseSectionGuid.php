<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 3:40 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidCourseSectionGuidException;

final class CourseSectionGuid extends Guid
{
    protected static $exceptionName = InvalidCourseSectionGuidException::class;
}
