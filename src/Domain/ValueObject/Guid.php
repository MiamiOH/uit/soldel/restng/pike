<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 9:06 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidGuidException;

class Guid extends AbstractSingleAttributeValueObject
{
    protected static $exceptionName = InvalidGuidException::class;

    public static function create(): self
    {
        mt_srand((int)((double)microtime() * 10000));
        $charid = strtoupper(md5(uniqid((string)rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);

        return new static(strtolower($uuid));
    }

    protected function validate(string $value): void
    {
        if (!preg_match(
            '/^\{?[a-f\d]{8}-(?:[a-f\d]{4}-){3}[a-f\d]{12}\}?$/i',
            $value
        )) {
            throw new static::$exceptionName;
        }
    }
}
