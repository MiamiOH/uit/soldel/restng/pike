<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:15 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidCourseSectionScheduleDaysException;

class CourseSectionScheduleDays extends AbstractUppercaseSingleAttributeValueObject
{
    public function hasMonday(): bool
    {
        return str_contains($this->value, 'M');
    }

    public function hasTuesday(): bool
    {
        return str_contains($this->value, 'T');
    }

    public function hasWednesday(): bool
    {
        return str_contains($this->value, 'W');
    }

    public function hasThursday(): bool
    {
        return str_contains($this->value, 'R');
    }

    public function hasFriday(): bool
    {
        return str_contains($this->value, 'F');
    }

    public function hasSaturday(): bool
    {
        return str_contains($this->value, 'S');
    }

    public function hasSunday(): bool
    {
        return str_contains($this->value, 'U');
    }

    protected function validate(string $value): void
    {
        if (preg_match('/[^MRWTFSU]/', $value)) {
            throw new InvalidCourseSectionScheduleDaysException();
        }
    }
}
