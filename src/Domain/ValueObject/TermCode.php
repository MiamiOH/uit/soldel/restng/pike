<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 4:34 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidTermCodeException;

final class TermCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (!in_array($value, ['000000', '999999', 'AR2000', 'PB2000'])) {
            if (!preg_match('/^[0-9]{4}(10|15|20|30)$/', $value)) {
                throw new InvalidTermCodeException();
            }
        }
    }
}
