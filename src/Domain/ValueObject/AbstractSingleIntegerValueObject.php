<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 8:54 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

abstract class AbstractSingleIntegerValueObject
{
    /**
     * @var int
     */
    protected $value;

    /**
     * AbstractSingleIntegerValueObject constructor.
     *
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    abstract protected function validate(int $value): void;

    public function __toString()
    {
        return (string)$this->getValue();
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
