<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 5:10 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidOffsetException;

final class Offset extends AbstractSingleIntegerValueObject
{
    public const DEFAULT = 1;

    protected function validate(int $value): void
    {
        if ($value < 1) {
            throw new InvalidOffsetException();
        }
    }

    public function getValue(): int
    {
        return parent::getValue() - 1;
    }
}
