<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/8/18
 * Time: 10:44 AM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidGradeModeCodeException;

class GradeModeCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (!preg_match('/^[A-Z]{1}$/', $value)) {
            throw new InvalidGradeModeCodeException();
        }
    }
}
