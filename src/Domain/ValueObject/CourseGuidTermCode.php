<?php
/**
 * Author: liaom
 * Date: 5/22/18
 * Time: 11:20 AM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

class CourseGuidTermCode
{
    /**
     * @var CourseGuid
     */
    private $courseGuid;
    /**
     * @var TermCode
     */
    private $termCode;

    /**
     * CourseGuidTermCode constructor.
     *
     * @param CourseGuid $courseGuid
     * @param TermCode $termCode
     */
    public function __construct(CourseGuid $courseGuid, TermCode $termCode)
    {
        $this->courseGuid = $courseGuid;
        $this->termCode = $termCode;
    }

    /**
     * @return CourseGuid
     */
    public function getCourseGuid(): CourseGuid
    {
        return $this->courseGuid;
    }

    /**
     * @return TermCode
     */
    public function getTermCode(): TermCode
    {
        return $this->termCode;
    }
}
