<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 8:54 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

abstract class AbstractSingleAttributeValueObject
{
    /**
     * @var string
     */
    protected $value;

    /**
     * AbstractSingleAttributeValueObject constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    abstract protected function validate(string $value): void;

    public function __toString()
    {
        return $this->getValue();
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
