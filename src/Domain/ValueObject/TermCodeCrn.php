<?php
/**
 * Author: liaom
 * Date: 5/22/18
 * Time: 11:20 AM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

class TermCodeCrn
{
    /**
     * @var TermCode
     */
    private $termCode;
    /**
     * @var Crn
     */
    private $crn;

    /**
     * TermCodeCrn constructor.
     *
     * @param string $termCode
     * @param string $crn
     */
    public function __construct(string $termCode, string $crn)
    {
        $this->termCode = new TermCode($termCode);
        $this->crn = new Crn($crn);
    }

    /**
     * @return TermCode
     */
    public function getTermCode(): TermCode
    {
        return $this->termCode;
    }

    /**
     * @return Crn
     */
    public function getCrn(): Crn
    {
        return $this->crn;
    }
}
