<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 4:34 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidInstructionalTypeCode;

final class InstructionalTypeCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (!preg_match('/^[A-Z0-9]{1}$/', $value)) {
            throw new InvalidInstructionalTypeCode();
        }
    }
}
