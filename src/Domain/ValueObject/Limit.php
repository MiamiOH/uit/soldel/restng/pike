<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 5:10 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidLimitException;

final class Limit extends AbstractSingleIntegerValueObject
{
    public const MAX = 200;

    protected function validate(int $value): void
    {
        if ($value < 1 || $value > 200) {
            throw new InvalidLimitException();
        }
    }
}
