<?php
/**
 * Author: liaom
 * Date: 5/10/18
 * Time: 2:07 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidCourseSectionAttributeCodeException;

class CourseSectionAttributeCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (strlen($value) > 4) {
            throw new InvalidCourseSectionAttributeCodeException();
        }
    }
}
