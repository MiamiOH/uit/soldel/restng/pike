<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/8/18
 * Time: 10:52 AM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidGradeTypeException;

class GradeType extends AbstractSingleAttributeValueObject
{
    private const MIDTERM = 'midterm';
    private const FINAL = 'final';

    public static function ofMidterm()
    {
        return new self(static::MIDTERM);
    }

    public static function ofFinal()
    {
        return new self(static::FINAL);
    }

    public function isMidterm(): bool
    {
        return $this->getValue() === static::MIDTERM;
    }

    public function isFinal(): bool
    {
        return $this->getValue() === static::FINAL;
    }

    protected function validate(string $value): void
    {
        if ($value !== static::MIDTERM && $value !== static::FINAL) {
            throw new InvalidGradeTypeException();
        }
    }
}
