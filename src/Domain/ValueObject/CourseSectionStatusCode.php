<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 4:34 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidCourseSectionStatusCodeException;

final class CourseSectionStatusCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (!preg_match('/^[A-Z]{1}$/', $value)) {
            throw new InvalidCourseSectionStatusCodeException();
        }
    }
}
