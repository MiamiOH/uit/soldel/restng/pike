<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:27 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidCourseSectionScheduleTypeCodeException;

class CourseSectionScheduleTypeCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (!preg_match('/^[A-Z]{4}$/', $value)) {
            throw new InvalidCourseSectionScheduleTypeCodeException();
        }
    }
}
