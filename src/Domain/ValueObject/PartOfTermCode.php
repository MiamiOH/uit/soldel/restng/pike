<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 4:34 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidPartOfTermCodeException;

final class PartOfTermCode extends AbstractUppercaseSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (strlen($value) > 3) {
            throw new InvalidPartOfTermCodeException();
        }
    }
}
