<?php
/**
 * Author: liaom
 * Date: 4/30/18
 * Time: 5:19 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

use MiamiOH\Pike\Exception\InvalidBannerIdException;

final class BannerId extends AbstractSingleAttributeValueObject
{
    protected function validate(string $value): void
    {
        if (!preg_match('/^\+[0-9]{8,9}$/', $value)) {
            throw new InvalidBannerIdException();
        }
    }
}
