<?php
/**
 * Author: liaom
 * Date: 5/22/18
 * Time: 12:53 PM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

class UpdateGradeResponse
{
    /**
     * @var int
     */
    private $requestIndex;
    /**
     * @var bool
     */
    private $isSuccessful;
    /**
     * @var string
     */
    private $message;

    /**
     * UpdateGradeResponse constructor.
     *
     * @param int $requestIndex
     * @param bool $isSuccessful
     * @param string $message
     */
    public function __construct(
        int $requestIndex,
        bool $isSuccessful,
        string $message
    ) {
        $this->requestIndex = $requestIndex;
        $this->isSuccessful = $isSuccessful;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getRequestIndex(): int
    {
        return $this->requestIndex;
    }

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->isSuccessful;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
