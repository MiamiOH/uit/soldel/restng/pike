<?php
/**
 * Author: liaom
 * Date: 5/22/18
 * Time: 11:18 AM
 */

namespace MiamiOH\Pike\Domain\ValueObject;

final class CourseSectionGuidUniqueId
{
    /**
     * @var CourseSectionGuid
     */
    private $courseSectionGuid;
    /**
     * @var UniqueId
     */
    private $uniqueId;

    /**
     * CourseSectionGuidUniqueId constructor.
     *
     * @param string $courseSectionGuid
     * @param string $uniqueId
     */
    public function __construct(
        string $courseSectionGuid,
        string $uniqueId
    ) {
        $this->courseSectionGuid = new CourseSectionGuid($courseSectionGuid);
        $this->uniqueId = new UniqueId($uniqueId);
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    /**
     * @return UniqueId
     */
    public function getUniqueId(): UniqueId
    {
        return $this->uniqueId;
    }
}
