<?php
/**
 * Author: liaom
 * Date: 5/16/18
 * Time: 11:26 AM
 */

namespace MiamiOH\Pike\Domain\Request;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\Collection\BuildingCodeCollection;
use MiamiOH\Pike\Domain\Collection\CampusCodeCollection;
use MiamiOH\Pike\Domain\Collection\CourseNumberCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionStatusCodeCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\PartOfTermCodeCollection;
use MiamiOH\Pike\Domain\Collection\SubjectCodeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Exception\InvalidArgumentException;

class SearchCourseSectionRequest
{
    /**
     * @var CrnCollection|null
     */
    private $crnCollection;
    /**
     * @var SubjectCodeCollection|null
     */
    private $subjectCodeCollection;
    /**
     * @var CourseNumberCollection|null
     */
    private $courseNumberCollection;
    /**
     * @var array|null
     */
    private $courseSectionCodes;
    /**
     * @var CourseSectionStatusCodeCollection|null
     */
    private $courseSectionStatusCodeCollection;
    /**
     * @var int|null
     */
    private $numOfMaxEnrollments;
    /**
     * @var int|null
     */
    private $numOfMaxEnrollmentsCount;
    /**
     * @var int|null
     */
    private $numOfCurrentEnrollments;
    /**
     * @var int|null
     */
    private $numOfCurrentEnrollmentsCount;
    /**
     * @var int|null
     */
    private $numOfActiveEnrollments;
    /**
     * @var int|null
     */
    private $numOfActiveEnrollmentsCount;
    /**
     * @var int|null
     */
    private $numOfAvailableEnrollments;
    /**
     * @var int|null
     */
    private $numOfAvailableEnrollmentsCount;
    /**
     * @var bool|null
     */
    private $isMidtermGradeSubmissionAvailable;
    /**
     * @var bool|null
     */
    private $isFinalGradeSubmissionAvailable;
    /**
     * @var bool|null
     */
    private $hasSeatAvailable;
    /**
     * @var PartOfTermCodeCollection|null
     */
    private $partOfTermCodeCollection;
    /**
     * @var Carbon|null
     */
    private $partOfTermStartDate;
    /**
     * @var Carbon|null
     */
    private $partOfTermStartDateEnd;
    /**
     * @var Carbon|null
     */
    private $partOfTermEndDate;
    /**
     * @var Carbon|null
     */
    private $partOfTermEndDateEnd;
    /**
     * @var Carbon|null
     */
    private $scheduleStartDate;
    /**
     * @var Carbon|null
     */
    private $scheduleStartDateEnd;
    /**
     * @var Carbon|null
     */
    private $scheduleEndDate;
    /**
     * @var Carbon|null
     */
    private $scheduleEndDateEnd;
    /**
     * @var Carbon|null
     */
    private $scheduleStartTime;
    /**
     * @var Carbon|null
     */
    private $scheduleStartTimeEnd;
    /**
     * @var Carbon|null
     */
    private $scheduleEndTime;
    /**
     * @var Carbon|null
     */
    private $scheduleEndTimeEnd;
    /**
     * @var BuildingCodeCollection|null
     */
    private $scheduleBuildingCodeCollection;
    /**
     * @var array|null
     */
    private $scheduleRooms;
    /**
     * @var CourseSectionScheduleDays|null
     */
    private $scheduleDays;
    /**
     * @var UniqueIdCollection|null
     */
    private $instructorUniqueIdCollection;
    /**
     * @var CourseSectionGuidCollection|null
     */
    private $courseSectionGuidCollection;
    /**
     * @var Offset
     */
    private $offset;
    /**
     * @var Limit
     */
    private $limit;
    /**
     * @var TermCodeCollection|null
     */
    private $termCodeCollection;
    /**
     * @var CampusCodeCollection|null
     */
    private $campusCodeCollection;

    /**
     * @return CrnCollection|null
     */
    public function getCrnCollection(): ?CrnCollection
    {
        return $this->crnCollection;
    }

    /**
     * @return SubjectCodeCollection|null
     */
    public function getSubjectCodeCollection(): ?SubjectCodeCollection
    {
        return $this->subjectCodeCollection;
    }

    /**
     * @return CourseNumberCollection|null
     */
    public function getCourseNumberCollection(): ?CourseNumberCollection
    {
        return $this->courseNumberCollection;
    }

    /**
     * @param array|null $courseNumbers
     */
    public function setCourseNumbers(
        ?array $courseNumbers
    ): void {
        if ($courseNumbers === null || count($courseNumbers) === 0) {
            $this->courseNumberCollection = null;
        } else {
            $this->courseNumberCollection = CourseNumberCollection::createFromValueArray($courseNumbers);
        }
    }

    /**
     * @return array|null
     */
    public function getCourseSectionCodes(): ?array
    {
        return $this->courseSectionCodes;
    }

    /**
     * @param array|null $courseSectionCodes
     */
    public function setCourseSectionCodes(?array $courseSectionCodes): void
    {
        if ($courseSectionCodes === null || count($courseSectionCodes) === 0) {
            $this->courseSectionCodes = null;
        } else {
            $this->courseSectionCodes = array_map(function (string $code) {
                return strtoupper(trim($code));
            }, $courseSectionCodes);
        }
    }

    /**
     * @return CourseSectionStatusCodeCollection|null
     */
    public function getCourseSectionStatusCodeCollection(
    ): ?CourseSectionStatusCodeCollection {
        return $this->courseSectionStatusCodeCollection;
    }

    /**
     * @param array|null $courseSectionStatusCodes
     */
    public function setCourseSectionStatusCodes(
        ?array $courseSectionStatusCodes
    ): void {
        if ($courseSectionStatusCodes === null || count($courseSectionStatusCodes) === 0) {
            $this->courseSectionStatusCodeCollection = null;
        } else {
            $this->courseSectionStatusCodeCollection = CourseSectionStatusCodeCollection::createFromValueArray($courseSectionStatusCodes);
        }
    }

    /**
     * @return int|null
     */
    public function getNumOfMaxEnrollments(): ?int
    {
        return $this->numOfMaxEnrollments;
    }
    /**
     * @return int|null
     */
    public function getNumOfMaxEnrollmentsCount(): ?int
    {
        return $this->numOfMaxEnrollmentsCount;
    }

    /**
     * @param int|null $numOfMaxEnrollments
     */
    public function setNumOfMaxEnrollments(?int $numOfMaxEnrollments, int $numOfMaxEnrollmentsCount = null): void
    {
        if (($numOfMaxEnrollments !== null && $numOfMaxEnrollments < 1) || ($numOfMaxEnrollmentsCount !== null && $numOfMaxEnrollmentsCount < 1)) {
            throw new InvalidArgumentException('Invalid numOfMaxEnrollments');
        }
        $this->numOfMaxEnrollments = $numOfMaxEnrollments;
        $this->numOfMaxEnrollmentsCount = $numOfMaxEnrollmentsCount;
    }

    /**
     * @return int|null
     */
    public function getNumOfCurrentEnrollments(): ?int
    {
        return $this->numOfCurrentEnrollments;
    }
    /**
     * @return int|null
     */
    public function getNumOfCurrentEnrollmentsCount(): ?int
    {
        return $this->numOfCurrentEnrollmentsCount;
    }

    /**
     * @param int|null $numOfCurrentEnrollments
     */
    public function setNumOfCurrentEnrollments(?int $numOfCurrentEnrollments, int $numOfCurrentEnrollmentsCount = null): void
    {
        if (($numOfCurrentEnrollments !== null && $numOfCurrentEnrollments < 1) || ($numOfCurrentEnrollmentsCount !== null && $numOfCurrentEnrollmentsCount < 1)) {
            throw new InvalidArgumentException('Invalid numOfCurrentEnrollments');
        }
        $this->numOfCurrentEnrollments = $numOfCurrentEnrollments;
        $this->numOfCurrentEnrollmentsCount = $numOfCurrentEnrollmentsCount;
    }

    /**
     * @return int|null
     */
    public function getNumOfActiveEnrollments(): ?int
    {
        return $this->numOfActiveEnrollments;
    }
    /**
     * @return int|null
     */
    public function getNumOfActiveEnrollmentsCount(): ?int
    {
        return $this->numOfActiveEnrollmentsCount;
    }

    /**
     * @param int|null $numOfActiveEnrollments
     */
    public function setNumOfActiveEnrollments(?int $numOfActiveEnrollments, int $numOfActiveEnrollmentsCount = null): void
    {
        if (($numOfActiveEnrollments !== null && $numOfActiveEnrollments < 1) || ($numOfActiveEnrollmentsCount !== null && $numOfActiveEnrollmentsCount < 1)) {
            throw new InvalidArgumentException('Invalid numOfActiveEnrollments');
        }
        $this->numOfActiveEnrollments = $numOfActiveEnrollments;
        $this->numOfActiveEnrollmentsCount = $numOfActiveEnrollmentsCount;
    }

    /**
     * @return int|null
     */
    public function getNumOfAvailableEnrollments(): ?int
    {
        return $this->numOfAvailableEnrollments;
    }
    /**
     * @return int|null
     */
    public function getNumOfAvailableEnrollmentsCount(): ?int
    {
        return $this->numOfAvailableEnrollmentsCount;
    }

    /**
     * @param int|null $numOfAvailableEnrollments
     */
    public function setNumOfAvailableEnrollments(?int $numOfAvailableEnrollments, int $numOfAvailableEnrollmentsCount = null): void
    {
        if (($numOfAvailableEnrollments !== null && $numOfAvailableEnrollments < 1) || ($numOfAvailableEnrollmentsCount !== null && $numOfAvailableEnrollmentsCount < 1)) {
            throw new InvalidArgumentException('Invalid numOfAvailableEnrollments');
        }
        $this->numOfAvailableEnrollments = $numOfAvailableEnrollments;
        $this->numOfAvailableEnrollmentsCount = $numOfAvailableEnrollmentsCount;
    }

    /**
     * @return bool|null
     */
    public function isMidtermGradeSubmissionAvailable(): ?bool
    {
        return $this->isMidtermGradeSubmissionAvailable;
    }

    /**
     * @return bool|null
     */
    public function isFinalGradeSubmissionAvailable(): ?bool
    {
        return $this->isFinalGradeSubmissionAvailable;
    }

    /**
     * @return bool|null
     */
    public function getHasSeatAvailable(): ?bool
    {
        return $this->hasSeatAvailable;
    }

    /**
     * @param bool|null $hasSeatAvailable
     */
    public function setHasSeatAvailable(?bool $hasSeatAvailable): void
    {
        $this->hasSeatAvailable = $hasSeatAvailable;
    }

    /**
     * @return PartOfTermCodeCollection|null
     */
    public function getPartOfTermCodeCollection(): ?PartOfTermCodeCollection
    {
        return $this->partOfTermCodeCollection;
    }

    /**
     * @param array|null $partOfTermCodes
     */
    public function setPartOfTermCodes(
        ?array $partOfTermCodes
    ): void {
        if ($partOfTermCodes === null || count($partOfTermCodes) === 0) {
            $this->partOfTermCodeCollection = null;
        } else {
            $this->partOfTermCodeCollection = PartOfTermCodeCollection::createFromValueArray($partOfTermCodes);
        }
    }

    /**
     * @return Carbon|null
     */
    public function getPartOfTermStartDate(): ?Carbon
    {
        return $this->partOfTermStartDate;
    }

    /**
     * @return Carbon|null
     */
    public function getPartOfTermStartDateEnd(): ?Carbon
    {
        return $this->partOfTermStartDateEnd;
    }

    /**
     * @param Carbon|null $partOfTermStartDate
     */
    public function setPartOfTermStartDate(?Carbon $partOfTermStartDate, Carbon $partOfTermStartDateEnd = null): void
    {
        $this->partOfTermStartDate = $partOfTermStartDate;
        $this->partOfTermStartDateEnd  = $partOfTermStartDateEnd;
    }

    /**
     * @return Carbon|null
     */
    public function getPartOfTermEndDate(): ?Carbon
    {
        return $this->partOfTermEndDate;
    }

    /**
     * @return Carbon|null
     */
    public function getPartOfTermEndDateEnd(): ?Carbon
    {
        return $this->partOfTermEndDateEnd;
    }

    /**
     * @param Carbon|null $partOfTermEndDate
     */
    public function setPartOfTermEndDate(?Carbon $partOfTermEndDate, Carbon $partOfTermEndDateEnd = null): void
    {
        $this->partOfTermEndDate = $partOfTermEndDate;
        $this->partOfTermEndDateEnd  = $partOfTermEndDateEnd;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleStartDate(): ?Carbon
    {
        return $this->scheduleStartDate;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleStartDateEnd(): ?Carbon
    {
        return $this->scheduleStartDateEnd;
    }

    /**
     * @param Carbon|null $scheduleStartDate
     */
    public function setScheduleStartDate(?Carbon $scheduleStartDate, Carbon $scheduleStartDateEnd = null): void
    {
        $this->scheduleStartDate = $scheduleStartDate;
        $this->scheduleStartDateEnd = $scheduleStartDateEnd;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleEndDate(): ?Carbon
    {
        return $this->scheduleEndDate;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleEndDateEnd(): ?Carbon
    {
        return $this->scheduleEndDateEnd;
    }

    /**
     * @param Carbon|null $scheduleEndDate
     */
    public function setScheduleEndDate(?Carbon $scheduleEndDate, Carbon $scheduleEndDateEnd = null): void
    {
        $this->scheduleEndDate = $scheduleEndDate;
        $this->scheduleEndDateEnd = $scheduleEndDateEnd;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleStartTime(): ?Carbon
    {
        return $this->scheduleStartTime;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleStartTimeEnd(): ?Carbon
    {
        return $this->scheduleStartTimeEnd;
    }

    /**
     * @param Carbon|null $scheduleStartTime
     */
    public function setScheduleStartTime(?Carbon $scheduleStartTime, Carbon $scheduleStartTimeEnd = null): void
    {
        $this->scheduleStartTime = $scheduleStartTime;
        $this->scheduleStartTimeEnd = $scheduleStartTimeEnd;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleEndTime(): ?Carbon
    {
        return $this->scheduleEndTime;
    }

    /**
     * @return Carbon|null
     */
    public function getScheduleEndTimeEnd(): ?Carbon
    {
        return $this->scheduleEndTimeEnd;
    }

    /**
     * @param Carbon|null $scheduleEndTime
     */
    public function setScheduleEndTime(?Carbon $scheduleEndTime, Carbon $scheduleEndTimeEnd = null): void
    {
        $this->scheduleEndTime = $scheduleEndTime;
        $this->scheduleEndTimeEnd = $scheduleEndTimeEnd;
    }

    /**
     * @return BuildingCodeCollection|null
     */
    public function getScheduleBuildingCodeCollection(): ?BuildingCodeCollection
    {
        return $this->scheduleBuildingCodeCollection;
    }

    /**
     * @param array|null $scheduleBuildingCodes
     */
    public function setScheduleBuildingCodes(
        ?array $scheduleBuildingCodes
    ): void {
        if ($scheduleBuildingCodes === null || count($scheduleBuildingCodes) === 0) {
            $this->scheduleBuildingCodeCollection = null;
        } else {
            $this->scheduleBuildingCodeCollection = BuildingCodeCollection::createFromValueArray($scheduleBuildingCodes);
        }
    }

    /**
     * @return array|null
     */
    public function getScheduleRooms(): ?array
    {
        return $this->scheduleRooms;
    }

    /**
     * @param array|null $scheduleRooms
     */
    public function setScheduleRooms(?array $scheduleRooms): void
    {
        if ($scheduleRooms === null || count($scheduleRooms) === 0) {
            $this->scheduleRooms = null;
        } else {
            $this->scheduleRooms = array_map(function (string $room) {
                return strtoupper(trim($room));
            }, $scheduleRooms);
        }
    }

    /**
     * @return CourseSectionScheduleDays|null
     */
    public function getScheduleDays(): ?CourseSectionScheduleDays
    {
        return $this->scheduleDays;
    }

    /**
     * @param null|string $days
     */
    public function setScheduleDays(?string $days): void
    {
        if ($days === null) {
            $this->scheduleDays = null;
        } else {
            $this->scheduleDays = new CourseSectionScheduleDays($days);
        }
    }

    /**
     * @return UniqueIdCollection|null
     */
    public function getInstructorUniqueIdCollection(): ?UniqueIdCollection
    {
        return $this->instructorUniqueIdCollection;
    }

    /**
     * @param array|null $instructorUniqueIds
     */
    public function setInstructorUniqueIds(
        ?array $instructorUniqueIds
    ): void {
        if ($instructorUniqueIds === null || count($instructorUniqueIds) === 0) {
            $this->instructorUniqueIdCollection = null;
        } else {
            $this->instructorUniqueIdCollection = UniqueIdCollection::createFromValueArray($instructorUniqueIds);
        }
    }

    /**
     * @return CourseSectionGuidCollection|null
     */
    public function getCourseSectionGuidCollection(): ?CourseSectionGuidCollection
    {
        return $this->courseSectionGuidCollection;
    }

    /**
     * @param array|null $courseSectionGuids
     */
    public function setCourseSectionGuids(
        ?array $courseSectionGuids
    ): void {
        if ($courseSectionGuids === null || count($courseSectionGuids) === 0) {
            $this->courseSectionGuidCollection = null;
        } else {
            $this->courseSectionGuidCollection = CourseSectionGuidCollection::createFromValueArray($courseSectionGuids);
        }
    }

    /**
     * @param array|null $crns
     */
    public function setCrns(?array $crns): void
    {
        if ($crns === null || count($crns) === 0) {
            $this->crnCollection = null;
        } else {
            $this->crnCollection = CrnCollection::createFromValueArray($crns);
        }
    }

    /**
     * @param array|null $subjectCodes
     */
    public function setSubjectCodes(
        ?array $subjectCodes
    ): void {
        if ($subjectCodes === null || count($subjectCodes) === 0) {
            $this->subjectCodeCollection = null;
        } else {
            $this->subjectCodeCollection = SubjectCodeCollection::createFromValueArray($subjectCodes);
        }
    }

    /**
     * @param bool|null $isMidtermGradeSubmissionAvailable
     */
    public function setIsMidtermGradeSubmissionAvailable(
        ?bool $isMidtermGradeSubmissionAvailable
    ): void {
        $this->isMidtermGradeSubmissionAvailable = $isMidtermGradeSubmissionAvailable;
    }

    /**
     * @param bool|null $isFinalGradeSubmissionAvailable
     */
    public function setIsFinalGradeSubmissionAvailable(
        ?bool $isFinalGradeSubmissionAvailable
    ): void {
        $this->isFinalGradeSubmissionAvailable = $isFinalGradeSubmissionAvailable;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        if ($this->offset === null) {
            return 1;
        }

        return $this->offset->getValue();
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset): void
    {
        $this->offset = new Offset($offset);
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        if ($this->limit === null) {
            return 50;
        }

        return $this->limit->getValue();
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = new Limit($limit);
    }

    /**
     * @return TermCodeCollection|null
     */
    public function getTermCodeCollection(): ?TermCodeCollection
    {
        return $this->termCodeCollection;
    }

    /**
     * @param array|null $termCodes
     */
    public function setTermCodes(
        ?array $termCodes
    ): void {
        if ($termCodes === null || count($termCodes) === 0) {
            $this->termCodeCollection = null;
        } else {
            $this->termCodeCollection = TermCodeCollection::createFromValueArray($termCodes);
        }
    }

    /**
     * @return CampusCodeCollection|null
     */
    public function getCampusCodeCollection(): ?CampusCodeCollection
    {
        return $this->campusCodeCollection;
    }

    /**
     * @param array|null $campusCodes
     */
    public function setCampusCodes(
        ?array $campusCodes
    ): void {
        if ($campusCodes === null || count($campusCodes) === 0) {
            $this->campusCodeCollection = null;
        } else {
            $this->campusCodeCollection = CampusCodeCollection::createFromValueArray($campusCodes);
        }
    }
}
