<?php
/**
 * Author: liaom
 * Date: 5/21/18
 * Time: 11:09 AM
 */

namespace MiamiOH\Pike\Domain\Request;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class UpdateGradeRequest
{
    /**
     * @var CourseSectionGuid
     */
    private $courseSectionGuid;
    /**
     * @var UniqueId
     */
    private $studentUniqueId;
    /**
     * @var GradeType
     */
    private $gradeType;
    /**
     * @var GradeValue
     */
    private $gradeValue;
    /**
     * @var UniqueId
     */
    private $instructorUniqueId;
    /**
     * @var bool|null
     */
    private $hasAttended;
    /**
     * @var Carbon|null
     */
    private $lastAttendDate;

    /**
     * UpdateGradeRequest constructor.
     *
     * @param string $courseSectionGuid
     * @param string $studentUniqueId
     * @param string $gradeType
     * @param string $gradeValue
     * @param string $instructorUniqueId
     * @param bool|null $hasAttended
     * @param Carbon|null $lastAttendDate
     */
    public function __construct(
        string $courseSectionGuid,
        string $studentUniqueId,
        string $gradeType,
        string $gradeValue,
        string $instructorUniqueId,
        ?bool $hasAttended,
        ?Carbon $lastAttendDate
    ) {
        $this->courseSectionGuid = new CourseSectionGuid($courseSectionGuid);
        $this->studentUniqueId = new UniqueId($studentUniqueId);
        $this->gradeType = new GradeType($gradeType);
        $this->gradeValue = new GradeValue($gradeValue);
        $this->instructorUniqueId = new UniqueId($instructorUniqueId);
        $this->hasAttended = $hasAttended;
        $this->lastAttendDate = $lastAttendDate;
    }


    /**
     * @return GradeValue
     */
    public function getGradeValue(): GradeValue
    {
        return $this->gradeValue;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    /**
     * @return UniqueId
     */
    public function getStudentUniqueId(): UniqueId
    {
        return $this->studentUniqueId;
    }

    /**
     * @return UniqueId
     */
    public function getInstructorUniqueId(): UniqueId
    {
        return $this->instructorUniqueId;
    }

    /**
     * @return GradeType
     */
    public function getGradeType(): GradeType
    {
        return $this->gradeType;
    }

    /**
     * @return bool|null
     */
    public function getHasAttended(): ?bool
    {
        return $this->hasAttended;
    }

    /**
     * @return Carbon|null
     */
    public function getLastAttendDate(): ?Carbon
    {
        return $this->lastAttendDate;
    }
}
