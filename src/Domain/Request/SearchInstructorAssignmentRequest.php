<?php
/**
 * Author: liaom
 * Date: 5/16/18
 * Time: 11:26 AM
 */

namespace MiamiOH\Pike\Domain\Request;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;

class SearchInstructorAssignmentRequest
{
    /**
     * @var TermCodeCollection|null
     */
    private $termCodeCollection;
    /**
     * @var CrnCollection|null
     */
    private $crnCollection;
    /**
     * @var UniqueIdCollection|null
     */
    private $uniqueIdCollection;
    /**
     * @var CourseSectionGuidCollection|null
     */
    private $courseSectionGuidCollection;
    /**
     * @var Offset
     */
    private $offset;
    /**
     * @var Limit
     */
    private $limit;

    public function setTermCodes(array $termCodes)
    {
        if (count($termCodes) === 0) {
            $this->termCodeCollection = null;
        } else {
            $this->termCodeCollection = TermCodeCollection::createFromValueArray($termCodes);
        }
    }

    public function setCrns(array $crns)
    {
        if (count($crns) === 0) {
            $this->crnCollection = null;
        } else {
            $this->crnCollection = CrnCollection::createFromValueArray($crns);
        }
    }

    public function setUniqueIds(array $uniqueIds)
    {
        if (count($uniqueIds) === 0) {
            $this->uniqueIdCollection = null;
        } else {
            $this->uniqueIdCollection = UniqueIdCollection::createFromValueArray($uniqueIds);
        }
    }

    public function setCourseSectionGuids(array $courseSectionGuids)
    {
        if (count($courseSectionGuids) === 0) {
            $this->courseSectionGuidCollection = null;
        } else {
            $this->courseSectionGuidCollection = CourseSectionGuidCollection::createFromValueArray($courseSectionGuids);
        }
    }

    public function setOffset(int $offset)
    {
        $this->offset = new Offset($offset);
    }

    public function setLimit(int $limit)
    {
        $this->limit = new Limit($limit);
    }

    /**
     * @return TermCodeCollection|null
     */
    public function getTermCodeCollection(): ?TermCodeCollection
    {
        return $this->termCodeCollection;
    }

    /**
     * @return CrnCollection|null
     */
    public function getCrnCollection(): ?CrnCollection
    {
        return $this->crnCollection;
    }

    /**
     * @return UniqueIdCollection|null
     */
    public function getUniqueIdCollection(): ?UniqueIdCollection
    {
        return $this->uniqueIdCollection;
    }

    /**
     * @return CourseSectionGuidCollection|null
     */
    public function getCourseSectionGuidCollection(): ?CourseSectionGuidCollection
    {
        return $this->courseSectionGuidCollection;
    }

    /**
     * @return Offset
     */
    public function getOffset(): Offset
    {
        if ($this->offset === null) {
            return new Offset(Offset::DEFAULT);
        }

        return $this->offset;
    }

    /**
     * @return Limit
     */
    public function getLimit(): Limit
    {
        if ($this->limit === null) {
            return new Limit(Limit::MAX);
        }

        return $this->limit;
    }
}
