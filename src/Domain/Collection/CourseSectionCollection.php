<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/2/18
 * Time: 12:00 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Common\Collection;
use MiamiOH\Pike\Domain\Model\CourseSection;

class CourseSectionCollection extends Collection
{
    public function toDictByGuid(): array
    {
        $dict = [];

        /** @var CourseSection $courseSection */
        foreach ($this as $courseSection) {
            $dict[(string)$courseSection->getGuid()] = $courseSection;
        }

        return $dict;
    }
}
