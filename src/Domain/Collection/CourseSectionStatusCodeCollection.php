<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 9:55 AM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;

class CourseSectionStatusCodeCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = CourseSectionStatusCode::class;
}
