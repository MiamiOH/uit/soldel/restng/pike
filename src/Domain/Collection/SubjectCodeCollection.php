<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:48 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\SubjectCode;

class SubjectCodeCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = SubjectCode::class;
}//end class
