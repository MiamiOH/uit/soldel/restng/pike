<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:48 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\BannerId;

class BannerIdCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = BannerId::class;
}//end class
