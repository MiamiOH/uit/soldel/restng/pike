<?php
/**
 * Author: liaom
 * Date: 5/3/18
 * Time: 1:35 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\CourseNumber;

class CourseNumberCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = CourseNumber::class;
}
