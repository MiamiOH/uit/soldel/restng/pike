<?php
/**
 * Author: liaom
 * Date: 5/21/18
 * Time: 3:47 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;

class GradeModeCodeCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = GradeModeCode::class;
}
