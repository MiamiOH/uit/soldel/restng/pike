<?php
/**
 * Author: liaom
 * Date: 5/3/18
 * Time: 1:35 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\MajorCode;

class MajorCodeCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = MajorCode::class;
}
