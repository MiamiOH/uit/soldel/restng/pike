<?php
/**
 * Author: liaom
 * Date: 5/3/18
 * Time: 1:35 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;

class CourseSectionGuidCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = CourseSectionGuid::class;
}
