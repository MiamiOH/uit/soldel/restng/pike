<?php
/**
 * Author: liaom
 * Date: 5/17/18
 * Time: 11:58 AM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\CampusCode;

class CampusCodeCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = CampusCode::class;
}
