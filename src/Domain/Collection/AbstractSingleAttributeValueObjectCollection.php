<?php
/**
 * Author: liaom
 * Date: 5/16/18
 * Time: 11:49 AM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Common\Collection;
use MiamiOH\Pike\Domain\ValueObject\AbstractSingleAttributeValueObject;
use MiamiOH\Pike\Exception\InvalidArgumentException;

class AbstractSingleAttributeValueObjectCollection extends Collection
{
    protected static $valueObjectClassName = null;

    public static function createFromValueArray(array $values)
    {
        if (static::$valueObjectClassName === null) {
            throw new InvalidArgumentException('Global variable $valueObjectClassName must be set');
        }

        return new static(
            array_map(
                function (string $value) {
                    return new static::$valueObjectClassName($value);
                },
                $values
            )
        );
    }

    public function toValueArray()
    {
        return $this->map(function (AbstractSingleAttributeValueObject $obj) {
            return $obj->getValue();
        })->toArray();
    }
}
