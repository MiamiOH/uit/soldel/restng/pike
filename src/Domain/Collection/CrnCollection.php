<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:48 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\Crn;

class CrnCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = Crn::class;
}//end class
