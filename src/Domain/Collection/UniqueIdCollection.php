<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:48 PM
 */

namespace MiamiOH\Pike\Domain\Collection;

use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class UniqueIdCollection extends AbstractSingleAttributeValueObjectCollection
{
    protected static $valueObjectClassName = UniqueId::class;

    public function toUpperCaseArray()
    {
        return $this->map(function (UniqueId $uniqueId) {
            return $uniqueId->getUppercaseValue();
        })->toArray();
    }
}//end class
