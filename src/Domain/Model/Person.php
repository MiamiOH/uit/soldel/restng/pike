<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:46 AM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class Person
{
    /**
     * @var UniqueId
     */
    protected $uniqueId;

    /**
     * @var Pidm
     */
    protected $pidm;

    /**
     * @var BannerId
     */
    protected $bannerId;

    /**
     * @var string|null
     */
    protected $lastName;
    /**
     * @var string|null
     */
    protected $firstName;
    /**
     * @var string|null
     */
    protected $middleName;
    /**
     * @var string|null
     */
    protected $prefix;
    /**
     * @var string|null
     */
    protected $suffix;
    /**
     * @var string|null
     */
    protected $preferredFirstName;

    /**
     * Person constructor.
     *
     * @param UniqueId $uniqueId
     * @param Pidm $pidm
     * @param BannerId $bannerId
     * @param null|string $lastName
     * @param null|string $firstName
     * @param null|string $middleName
     * @param null|string $prefix
     * @param null|string $suffix
     * @param null|string $preferredFirstName
     */
    public function __construct(
        UniqueId $uniqueId,
        Pidm $pidm,
        BannerId $bannerId,
        ?string $lastName,
        ?string $firstName,
        ?string $middleName,
        ?string $prefix,
        ?string $suffix,
        ?string $preferredFirstName
    ) {
        $this->uniqueId = $uniqueId;
        $this->pidm = $pidm;
        $this->bannerId = $bannerId;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->prefix = $prefix;
        $this->suffix = $suffix;
        $this->preferredFirstName = $preferredFirstName;
    }

    /**
     * @return UniqueId
     */
    public function getUniqueId(): UniqueId
    {
        return $this->uniqueId;
    }

    /**
     * @return Pidm
     */
    public function getPidm(): Pidm
    {
        return $this->pidm;
    }

    /**
     * @return BannerId
     */
    public function getBannerId(): BannerId
    {
        return $this->bannerId;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return null|string
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @return null|string
     */
    public function getPrefix(): ?string
    {
        return $this->prefix;
    }

    /**
     * @return null|string
     */
    public function getSuffix(): ?string
    {
        return $this->suffix;
    }

    /**
     * @return null|string
     */
    public function getPreferredFirstName(): ?string
    {
        return $this->preferredFirstName;
    }

    public function getInformalDisplayedName(): string
    {
        return sprintf(
            "%s%s %s%s",
            $this->getFirstName(),
            ($this->getMiddleName() !== null ? ' ' . $this->getMiddleName() : ''),
            $this->getLastName(),
            $this->getSuffix() !== null ? ', ' . $this->getSuffix() : ''
        );
    }

    public function getFormalDisplayedName(): string
    {
        return sprintf(
            "%s %s%s %s%s",
            $this->getPrefix(),
            $this->getFirstName(),
            $this->getMiddleName() !== null ? ' ' . $this->getMiddleName() : '',
            $this->getLastName(),
            $this->getSuffix() !== null ? ', ' . $this->getSuffix() : ''
        );
    }

    public function getInformalSortedName(): string
    {
        return sprintf(
            "%s, %s %s%s",
            $this->getLastName(),
            $this->getFirstName(),
            $this->getMiddleName() ?? '',
            $this->getSuffix() !== null ? ', ' . $this->getSuffix() : ''
        );
    }

    public function getFormalSortedName(): string
    {
        return sprintf(
            "%s, %s%s %s%s",
            $this->getLastName(),
            $this->getFirstName(),
            $this->getMiddleName() !== null ? ' ' . $this->getMiddleName() : '',
            $this->getPrefix(),
            $this->getSuffix() !== null ? ', ' . $this->getSuffix() : ''
        );
    }
}
