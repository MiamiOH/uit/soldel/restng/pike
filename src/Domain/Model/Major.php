<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:45 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\MajorCode;

class Major
{
    /**
     * @var MajorCode
     */
    protected $code;
    /**
     * @var string
     */
    protected $description;


    /**
     * Department constructor.
     *
     * @param MajorCode $code
     * @param string $description
     */
    public function __construct(MajorCode $code, string $description)
    {
        $this->code = $code;
        $this->description = $description;
    }//end __construct()


    /**
     * @return MajorCode
     */
    public function getCode(): MajorCode
    {
        return $this->code;
    }//end getCode()


    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }//end getDescription()
}//end class
