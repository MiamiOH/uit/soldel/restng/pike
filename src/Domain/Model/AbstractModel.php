<?php
/**
 * Author: liaom
 * Date: 4/20/18
 * Time: 5:29 PM
 */

namespace MiamiOH\Pike\Domain\Model;

abstract class AbstractModel
{
    public function toArray(): array
    {
        return get_object_vars($this);
    }//end toArray()
}//end class
