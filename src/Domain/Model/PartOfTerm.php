<?php
/**
 * Author: liaom
 * Date: 5/8/18
 * Time: 4:34 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\ValueObject\PartOfTermCode;

class PartOfTerm
{
    /**
     * @var PartOfTermCode
     */
    protected $code;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var Carbon
     */
    protected $startDate;
    /**
     * @var Carbon
     */
    protected $endDate;

    /**
     * PartOfTerm constructor.
     *
     * @param PartOfTermCode $code
     * @param string $description
     * @param Carbon $startDate
     * @param Carbon $endDate
     */
    public function __construct(
        PartOfTermCode $code,
        string $description,
        Carbon $startDate,
        Carbon $endDate
    ) {
        $this->code = $code;
        $this->description = $description;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return PartOfTermCode
     */
    public function getCode(): PartOfTermCode
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return Carbon
     */
    public function getStartDate(): Carbon
    {
        return $this->startDate;
    }

    /**
     * @return Carbon
     */
    public function getEndDate(): Carbon
    {
        return $this->endDate;
    }
}
