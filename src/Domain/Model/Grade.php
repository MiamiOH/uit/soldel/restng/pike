<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/7/18
 * Time: 2:46 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class Grade
{
    /**
     * @var GradeValue
     */
    protected $value;

    /**
     * @var GradeModeCode
     */
    protected $gradeModeCode;

    /**
     * @var GradeType
     */
    protected $type;

    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;
    /**
     * @var Crn
     */
    protected $crn;
    /**
     * @var TermCode
     */
    protected $termCode;

    /**
     * @var UniqueId
     */
    protected $uniqueId;

    /**
     * Grade constructor.
     *
     * @param GradeValue $value
     * @param GradeModeCode $gradeModeCode
     * @param GradeType $type
     * @param CourseSectionGuid $courseSectionGuid
     * @param Crn $crn
     * @param TermCode $termCode
     * @param UniqueId $uniqueId
     */
    public function __construct(
        GradeValue $value,
        GradeModeCode $gradeModeCode,
        GradeType $type,
        CourseSectionGuid $courseSectionGuid,
        Crn $crn,
        TermCode $termCode,
        UniqueId $uniqueId
    ) {
        $this->value = $value;
        $this->gradeModeCode = $gradeModeCode;
        $this->type = $type;
        $this->courseSectionGuid = $courseSectionGuid;
        $this->crn = $crn;
        $this->termCode = $termCode;
        $this->uniqueId = $uniqueId;
    }

    /**
     * @return GradeValue
     */
    public function getValue(): GradeValue
    {
        return $this->value;
    }

    /**
     * @return GradeModeCode
     */
    public function getGradeModeCode(): GradeModeCode
    {
        return $this->gradeModeCode;
    }

    /**
     * @return GradeType
     */
    public function getType(): GradeType
    {
        return $this->type;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    /**
     * @return Crn
     */
    public function getCrn(): Crn
    {
        return $this->crn;
    }

    /**
     * @return TermCode
     */
    public function getTermCode(): TermCode
    {
        return $this->termCode;
    }

    /**
     * @return UniqueId
     */
    public function getUniqueId(): UniqueId
    {
        return $this->uniqueId;
    }
}
