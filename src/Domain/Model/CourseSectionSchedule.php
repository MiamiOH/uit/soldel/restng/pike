<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:04 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;

class CourseSectionSchedule
{
    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;
    /**
     * @var Carbon
     */
    protected $startDate;
    /**
     * @var Carbon
     */
    protected $endDate;
    /**
     * @var Carbon|null
     */
    protected $startTime;
    /**
     * @var Carbon|null
     */
    protected $endTime;
    /**
     * @var CourseSectionScheduleDays|null
     */
    protected $days;
    /**
     * @var string|null
     */
    protected $room;
    /**
     * @var BuildingCode|null
     */
    protected $buildingCode;
    /**
     * @var string|null
     */
    protected $buildingName;
    /**
     * @var CourseSectionScheduleTypeCode|null
     */
    protected $typeCode;
    /**
     * @var string|null
     */
    protected $typeDescription;

    /**
     * CourseSectionSchedule constructor.
     *
     * @param CourseSectionGuid $courseSectionGuid
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param Carbon|null $startTime
     * @param Carbon|null $endTime
     * @param CourseSectionScheduleDays|null $days
     * @param null|string $room
     * @param BuildingCode|null $buildingCode
     * @param null|string $buildingName
     * @param CourseSectionScheduleTypeCode|null $typeCode
     * @param null|string $typeDescription
     */
    public function __construct(
        CourseSectionGuid $courseSectionGuid,
        Carbon $startDate,
        Carbon $endDate,
        ?Carbon $startTime,
        ?Carbon $endTime,
        ?CourseSectionScheduleDays $days,
        ?string $room,
        ?BuildingCode $buildingCode,
        ?string $buildingName,
        ?CourseSectionScheduleTypeCode $typeCode,
        ?string $typeDescription
    ) {
        $this->courseSectionGuid = $courseSectionGuid;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->days = $days;
        $this->room = $room;
        $this->buildingCode = $buildingCode;
        $this->buildingName = $buildingName;
        $this->typeCode = $typeCode;
        $this->typeDescription = $typeDescription;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    /**
     * @return Carbon
     */
    public function getStartDate(): Carbon
    {
        return $this->startDate;
    }

    /**
     * @return Carbon
     */
    public function getEndDate(): Carbon
    {
        return $this->endDate;
    }

    /**
     * @return Carbon|null
     */
    public function getStartTime(): ?Carbon
    {
        return $this->startTime;
    }

    /**
     * @return Carbon|null
     */
    public function getEndTime(): ?Carbon
    {
        return $this->endTime;
    }

    /**
     * @return CourseSectionScheduleDays|null
     */
    public function getDays(): ?CourseSectionScheduleDays
    {
        return $this->days;
    }

    /**
     * @return null|string
     */
    public function getRoom(): ?string
    {
        return $this->room;
    }

    /**
     * @return BuildingCode|null
     */
    public function getBuildingCode(): ?BuildingCode
    {
        return $this->buildingCode;
    }

    /**
     * @return null|string
     */
    public function getBuildingName(): ?string
    {
        return $this->buildingName;
    }

    /**
     * @return CourseSectionScheduleTypeCode|null
     */
    public function getTypeCode(): ?CourseSectionScheduleTypeCode
    {
        return $this->typeCode;
    }

    /**
     * @return null|string
     */
    public function getTypeDescription(): ?string
    {
        return $this->typeDescription;
    }
}
