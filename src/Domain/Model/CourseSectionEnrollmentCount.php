<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 9:46 AM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;

class CourseSectionEnrollmentCount
{
    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;
    /**
     * @var int
     */
    protected $numOfMaxEnrollments;
    /**
     * @var int
     */
    protected $numOfCurrentEnrollments;
    /**
     * @var int
     */
    protected $numOfActiveEnrollments;
    /**
     * @var int
     */
    protected $numOfAvailableEnrollments;

    /**
     * CourseSectionEnrollmentCount constructor.
     *
     * @param CourseSectionGuid $courseSectionGuid
     * @param int $numOfMaxEnrollments
     * @param int $numOfCurrentEnrollments
     * @param int $numOfActiveEnrollments
     * @param int $numOfAvailableEnrollments
     */
    public function __construct(
        CourseSectionGuid $courseSectionGuid,
        int $numOfMaxEnrollments,
        int $numOfCurrentEnrollments,
        int $numOfActiveEnrollments,
        int $numOfAvailableEnrollments
    ) {
        $this->courseSectionGuid = $courseSectionGuid;
        $this->numOfMaxEnrollments = $numOfMaxEnrollments;
        $this->numOfCurrentEnrollments = $numOfCurrentEnrollments;
        $this->numOfActiveEnrollments = $numOfActiveEnrollments;
        $this->numOfAvailableEnrollments = $numOfAvailableEnrollments;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    /**
     * @return int
     */
    public function getNumOfMaxEnrollments(): int
    {
        return $this->numOfMaxEnrollments;
    }

    /**
     * @return int
     */
    public function getNumOfCurrentEnrollments(): int
    {
        return $this->numOfCurrentEnrollments;
    }

    /**
     * @return int
     */
    public function getNumOfActiveEnrollments(): int
    {
        return $this->numOfActiveEnrollments;
    }

    /**
     * @return int
     */
    public function getNumOfAvailableEnrollments(): int
    {
        return $this->numOfAvailableEnrollments;
    }
}
