<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:04 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\Collection\CourseSectionCreditHoursDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionLevelDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionSummaryCollection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;

class CourseSectionEnrollmentDistribution
{
    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;
    /**
     * @var CourseSectionCreditHoursDistributionCollection
     */
    protected $courseSectionCreditHoursDistributionCollection;
    /**
     * @var CourseSectionLevelDistributionCollection
     */
    protected $courseSectionLevelDistributionCollection;
    /**
     * @var CourseSectionSummaryCollection
     */
    protected $courseSectionSummaryCollection;


    /**
     * CourseSectionEnrollmentDistribution constructor.
     *
     * @param CourseSectionGuid $courseSectionGuid
     * @param CourseSectionCreditHoursDistributionCollection $courseSectionCreditHoursDistributionCollection
     * @param CourseSectionLevelDistributionCollection $courseSectionLevelDistributionCollection
     * @param CourseSectionSummaryCollection $courseSectionSummaryCollection
     */
    public function __construct(
        CourseSectionGuid $courseSectionGuid,
        CourseSectionCreditHoursDistributionCollection $courseSectionCreditHoursDistributionCollection,
        CourseSectionLevelDistributionCollection $courseSectionLevelDistributionCollection,
        CourseSectionSummaryCollection $courseSectionSummaryCollection
    ) {
        $this->courseSectionGuid = $courseSectionGuid;
        $this->courseSectionCreditHoursDistributionCollection = $courseSectionCreditHoursDistributionCollection;
        $this->courseSectionLevelDistributionCollection = $courseSectionLevelDistributionCollection;
        $this->courseSectionSummaryCollection = $courseSectionSummaryCollection;
    }

    /**
     * @return CourseSectionCreditHoursDistributionCollection
     */
    public function getCourseSectionCreditHoursDistributionCollection(): CourseSectionCreditHoursDistributionCollection
    {
        return $this->courseSectionCreditHoursDistributionCollection;
    }

    /**
     * @return CourseSectionLevelDistributionCollection
     */
    public function getCourseSectionLevelDistributionCollection(): CourseSectionLevelDistributionCollection
    {
        return $this->courseSectionLevelDistributionCollection;
    }

    /**
     * @return CourseSectionSummaryCollection
     */
    public function getCourseSectionSummaryCollection(): CourseSectionSummaryCollection
    {
        return $this->courseSectionSummaryCollection;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }
}
