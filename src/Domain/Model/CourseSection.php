<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 1:31 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CampusCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionStatusCode;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\DivisionCode;
use MiamiOH\Pike\Domain\ValueObject\InstructionalTypeCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

class CourseSection
{
    /**
     * @var CourseSectionGuid
     */
    protected $guid;
    /**
     * @var Course
     */
    protected $course;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var TermCode
     */
    protected $termCode;
    /**
     * @var Crn
     */
    protected $crn;

    /**
     * @var string
     */
    protected $termDescription;
    /**
     * @var string
     */
    protected $sectionCode;
    /**
     * @var string
     */
    protected $creditHoursDescription;
    /**
     * @var float
     */
    protected $creditHoursLow;
    /**
     * @var float
     */
    protected $creditHoursHigh;
    /**
     * @var array
     */
    protected $creditHoursAvailable;
    /**
     * @var InstructionalTypeCode
     */
    protected $instructionalTypeCode;
    /**
     * @var string
     */
    protected $instructionalTypeDescription;
    /**
     * @var CampusCode
     */
    protected $campusCode;
    /**
     * @var string
     */
    protected $campusDescription;
    /**
     * @var CourseSectionStatusCode
     */
    protected $courseSectionStatusCode;
    /**
     * @var string
     */
    protected $courseSectionStatusDescription;
    /**
     * @var float
     */
    protected $maxNumberOfEnrollment;

    /**
     * @var PartOfTerm
     */
    protected $partOfTerm;

    /**
     * @var bool
     */
    protected $isMidtermGradeSubmissionAvailable;
    /**
     * @var bool
     */
    protected $isFinalGradeSubmissionAvailable;
    /**
     * @var bool
     */
    protected $isFinalGradeRequired;
    /**
     * @var bool
     */
    protected $isDisplayed;
    /**
     * @var DivisionCode|null
     */
    protected $standardizedDivisionCode;
    /**
     * @var string|null
     */
    protected $standardizedDivisionName;
    /**
     * @var DepartmentCode|null
     */
    protected $standardizedDepartmentCode;
    /**
     * @var string|null
     */
    protected $standardizedDepartmentName;
    /**
     * @var DepartmentCode|null
     */
    protected $legacyStandardizedDepartmentCode;
    /**
     * @var string|null
     */
    protected $legacyStandardizedDepartmentName;

    /**
     * CourseSection constructor.
     *
     * @param CourseSectionGuid $guid
     * @param Course $course
     * @param string $title
     * @param TermCode $termCode
     * @param Crn $crn
     * @param string $termDescription
     * @param string $sectionCode
     * @param string $creditHoursDescription
     * @param float $creditHoursLow
     * @param float $creditHoursHigh
     * @param array $creditHoursAvailable
     * @param InstructionalTypeCode $instructionalTypeCode
     * @param string $instructionalTypeDescription
     * @param CampusCode $campusCode
     * @param string $campusDescription
     * @param CourseSectionStatusCode $courseSectionStatusCode
     * @param string $courseSectionStatusDescription
     * @param float $maxNumberOfEnrollment
     * @param PartOfTerm $partOfTerm
     * @param bool $isMidtermGradeSubmissionAvailable
     * @param bool $isFinalGradeSubmissionAvailable
     * @param bool $isFinalGradeRequired
     * @param bool $isDisplayed
     * @param DivisionCode|null $standardizedDivisionCode
     * @param null|string $standardizedDivisionName
     * @param DepartmentCode|null $standardizedDepartmentCode
     * @param null|string $standardizedDepartmentName
     * @param DepartmentCode|null $legacyStandardizedDepartmentCode
     * @param null|string $legacyStandardizedDepartmentName
     */
    public function __construct(
        CourseSectionGuid $guid,
        Course $course,
        string $title,
        TermCode $termCode,
        Crn $crn,
        string $termDescription,
        string $sectionCode,
        string $creditHoursDescription,
        float $creditHoursLow,
        float $creditHoursHigh,
        array $creditHoursAvailable,
        InstructionalTypeCode $instructionalTypeCode,
        string $instructionalTypeDescription,
        CampusCode $campusCode,
        string $campusDescription,
        CourseSectionStatusCode $courseSectionStatusCode,
        string $courseSectionStatusDescription,
        float $maxNumberOfEnrollment,
        PartOfTerm $partOfTerm,
        bool $isMidtermGradeSubmissionAvailable,
        bool $isFinalGradeSubmissionAvailable,
        bool $isFinalGradeRequired,
        bool $isDisplayed,
        ?DivisionCode $standardizedDivisionCode,
        ?string $standardizedDivisionName,
        ?DepartmentCode $standardizedDepartmentCode,
        ?string $standardizedDepartmentName,
        ?DepartmentCode $legacyStandardizedDepartmentCode,
        ?string $legacyStandardizedDepartmentName
    ) {
        $this->guid = $guid;
        $this->course = $course;
        $this->title = $title;
        $this->termCode = $termCode;
        $this->crn = $crn;
        $this->termDescription = $termDescription;
        $this->sectionCode = $sectionCode;
        $this->creditHoursDescription = $creditHoursDescription;
        $this->creditHoursLow = $creditHoursLow;
        $this->creditHoursHigh = $creditHoursHigh;
        $this->creditHoursAvailable = $creditHoursAvailable;
        $this->instructionalTypeCode = $instructionalTypeCode;
        $this->instructionalTypeDescription = $instructionalTypeDescription;
        $this->campusCode = $campusCode;
        $this->campusDescription = $campusDescription;
        $this->courseSectionStatusCode = $courseSectionStatusCode;
        $this->courseSectionStatusDescription = $courseSectionStatusDescription;
        $this->maxNumberOfEnrollment = $maxNumberOfEnrollment;
        $this->partOfTerm = $partOfTerm;
        $this->isMidtermGradeSubmissionAvailable = $isMidtermGradeSubmissionAvailable;
        $this->isFinalGradeSubmissionAvailable = $isFinalGradeSubmissionAvailable;
        $this->isFinalGradeRequired = $isFinalGradeRequired;
        $this->isDisplayed = $isDisplayed;
        $this->standardizedDivisionCode = $standardizedDivisionCode;
        $this->standardizedDivisionName = $standardizedDivisionName;
        $this->standardizedDepartmentCode = $standardizedDepartmentCode;
        $this->standardizedDepartmentName = $standardizedDepartmentName;
        $this->legacyStandardizedDepartmentCode = $legacyStandardizedDepartmentCode;
        $this->legacyStandardizedDepartmentName = $legacyStandardizedDepartmentName;
    }


    /**
     * @return CourseSectionGuid
     */
    public function getGuid(): CourseSectionGuid
    {
        return $this->guid;
    }

    /**
     * @return Course
     */
    public function getCourse(): Course
    {
        return $this->course;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return TermCode
     */
    public function getTermCode(): TermCode
    {
        return $this->termCode;
    }

    /**
     * @return Crn
     */
    public function getCrn(): Crn
    {
        return $this->crn;
    }

    public function getCourseTitle(): string
    {
        return $this->course->getTitle();
    }

    /**
     * @return string
     */
    public function getTermDescription(): string
    {
        return $this->termDescription;
    }

    /**
     * @return string
     */
    public function getSectionCode(): string
    {
        return trim($this->sectionCode);
    }

    /**
     * @return string
     */
    public function getCreditHoursDescription(): string
    {
        return $this->creditHoursDescription;
    }

    /**
     * @return float
     */
    public function getCreditHoursLow(): float
    {
        return $this->creditHoursLow;
    }

    /**
     * @return float
     */
    public function getCreditHoursHigh(): float
    {
        return $this->creditHoursHigh;
    }

    /**
     * @return array
     */
    public function getCreditHoursAvailable(): array
    {
        return $this->creditHoursAvailable;
    }

    /**
     * @return InstructionalTypeCode
     */
    public function getInstructionalTypeCode(): InstructionalTypeCode
    {
        return $this->instructionalTypeCode;
    }

    /**
     * @return string
     */
    public function getInstructionalTypeDescription(): string
    {
        return $this->instructionalTypeDescription;
    }

    /**
     * @return CampusCode
     */
    public function getCampusCode(): CampusCode
    {
        return $this->campusCode;
    }

    /**
     * @return string
     */
    public function getCampusDescription(): string
    {
        return $this->campusDescription;
    }

    /**
     * @return CourseSectionStatusCode
     */
    public function getCourseSectionStatusCode(): CourseSectionStatusCode
    {
        return $this->courseSectionStatusCode;
    }

    /**
     * @return string
     */
    public function getCourseSectionStatusDescription(): string
    {
        return $this->courseSectionStatusDescription;
    }

    /**
     * @return float
     */
    public function getMaxNumberOfEnrollment(): float
    {
        return $this->maxNumberOfEnrollment;
    }

    /**
     * @return PartOfTerm
     */
    public function getPartOfTerm(): PartOfTerm
    {
        return $this->partOfTerm;
    }

    /**
     * @return string
     */
    public function getSectionName(): string
    {
        return $this->course->getSubjectCode() . ' ' . $this->course->getNumber() . ' ' . $this->getSectionCode();
    }

    /**
     * @return bool
     */
    public function isMidtermGradeSubmissionAvailable(): bool
    {
        return $this->isMidtermGradeSubmissionAvailable;
    }

    /**
     * @return bool
     */
    public function isFinalGradeSubmissionAvailable(): bool
    {
        return $this->isFinalGradeSubmissionAvailable;
    }

    /**
     * @return bool
     */
    public function isFinalGradeRequired(): bool
    {
        return $this->isFinalGradeRequired;
    }

    /**
     * @return bool
     */
    public function isDisplayed(): bool
    {
        return $this->isDisplayed;
    }

    /**
     * @return DivisionCode|null
     */
    public function getStandardizedDivisionCode(): ?DivisionCode
    {
        return $this->standardizedDivisionCode;
    }

    /**
     * @return null|string
     */
    public function getStandardizedDivisionName(): ?string
    {
        return $this->standardizedDivisionName;
    }

    /**
     * @return DepartmentCode|null
     */
    public function getStandardizedDepartmentCode(): ?DepartmentCode
    {
        return $this->standardizedDepartmentCode;
    }

    /**
     * @return null|string
     */
    public function getStandardizedDepartmentName(): ?string
    {
        return $this->standardizedDepartmentName;
    }

    /**
     * @return DepartmentCode|null
     */
    public function getLegacyStandardizedDepartmentCode(): ?DepartmentCode
    {
        return $this->legacyStandardizedDepartmentCode;
    }

    /**
     * @return null|string
     */
    public function getLegacyStandardizedDepartmentName(): ?string
    {
        return $this->legacyStandardizedDepartmentName;
    }
}
