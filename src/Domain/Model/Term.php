<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:46 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

class Term extends AbstractModel
{
    /**
     * @var TermCode
     */
    protected $code;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var Carbon
     */
    protected $startDate;
    /**
     * @var Carbon
     */
    protected $endDate;
    /**
     * @var boolean
     */
    protected $isDisplayed;

    /**
     * Term constructor.
     *
     * @param TermCode $code
     * @param string $description
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @param bool $isDisplayed
     */
    public function __construct(
        TermCode $code,
        string $description,
        Carbon $startDate,
        Carbon $endDate,
        bool $isDisplayed
    ) {
        $this->code = $code;
        $this->description = $description;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->isDisplayed = $isDisplayed;
    }//end __construct()


    /**
     * @return TermCode
     */
    public function getCode(): TermCode
    {
        return $this->code;
    }//end getCode()


    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }//end getDescription()


    /**
     * @return Carbon
     */
    public function getStartDate(): Carbon
    {
        return $this->startDate;
    }//end getStartDate()


    /**
     * @return Carbon
     */
    public function getEndDate(): Carbon
    {
        return $this->endDate;
    }//end getEndDate()


    /**
     * @return bool
     */
    public function isDisplayed(): bool
    {
        return $this->isDisplayed;
    }//end isDisplayed()
}//end class
