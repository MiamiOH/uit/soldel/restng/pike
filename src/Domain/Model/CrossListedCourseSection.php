<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 6/18/18
 * Time: 11:24 AM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;

class CrossListedCourseSection
{
    /**
     * @var Crn
     */
    protected $crn;

    /**
     * @var SubjectCode
     */
    protected $subjectCode;

    /**
     * @var string
     */
    protected $sectionCode;

    /**
     * @var CourseNumber
     */
    protected $courseNumber;

    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;

    /**
     * @var CourseSectionGuid
     */
    protected $hostCourseSectionGuid;

    /**
     * CrossListedCourseSection constructor.
     * @param Crn $crn
     * @param SubjectCode $subjectCode
     * @param string $sectionCode
     * @param CourseNumber $courseNumber
     * @param CourseSectionGuid $courseSectionGuid
     * @param CourseSectionGuid $hostCourseSectionGuid
     */

    public function __construct(
        Crn $crn,
        SubjectCode $subjectCode,
        string $sectionCode,
        CourseNumber $courseNumber,
        CourseSectionGuid $courseSectionGuid,
        CourseSectionGuid $hostCourseSectionGuid
    ) {
        $this->crn = $crn;
        $this->subjectCode = $subjectCode;
        $this->sectionCode = $sectionCode;
        $this->courseNumber = $courseNumber;
        $this->courseSectionGuid = $courseSectionGuid;
        $this->hostCourseSectionGuid = $hostCourseSectionGuid;
    }

    /**
     * @return Crn
     */
    public function getCrn(): Crn
    {
        return $this->crn;
    }

    /**
     * @return SubjectCode
     */
    public function getSubjectCode(): SubjectCode
    {
        return $this->subjectCode;
    }

    /**
     * @return string
     */
    public function getSectionCode(): string
    {
        return trim($this->sectionCode);
    }

    /**
     * @return string
     */
    public function getSectionName(): string
    {
        return $this->getSubjectCode() . ' ' . $this->getCourseNumber() . ' ' . $this->getSectionCode();
    }

    /**
     * @return CourseNumber
     */
    public function getCourseNumber(): CourseNumber
    {
        return $this->courseNumber;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getHostCourseSectionGuid(): CourseSectionGuid
    {
        return $this->hostCourseSectionGuid;
    }
}
