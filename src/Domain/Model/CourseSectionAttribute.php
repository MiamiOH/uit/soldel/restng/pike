<?php
/**
 * Author: liaom
 * Date: 5/10/18
 * Time: 1:40 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionAttributeCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;

class CourseSectionAttribute
{
    /**
     * @var CourseSectionAttributeCode
     */
    protected $code;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;

    /**
     * CourseSectionAttribute constructor.
     *
     * @param CourseSectionAttributeCode $code
     * @param string $description
     * @param CourseSectionGuid $courseSectionGuid
     */
    public function __construct(
        CourseSectionAttributeCode $code,
        string $description,
        CourseSectionGuid $courseSectionGuid
    ) {
        $this->code = $code;
        $this->description = $description;
        $this->courseSectionGuid = $courseSectionGuid;
    }

    /**
     * @return CourseSectionAttributeCode
     */
    public function getCode(): CourseSectionAttributeCode
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }
}
