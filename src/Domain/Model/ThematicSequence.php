<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:46 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;

class ThematicSequence
{
    /**
     * @var ThematicSequenceCode
     */
    protected $code;
    /**
     * @var string
     */
    protected $description;


    /**
     * Department constructor.
     *
     * @param ThematicSequenceCode $code
     * @param string $description
     */
    public function __construct(ThematicSequenceCode $code, string $description)
    {
        $this->code = $code;
        $this->description = $description;
    }//end __construct()


    /**
     * @return ThematicSequenceCode
     */
    public function getCode(): ThematicSequenceCode
    {
        return $this->code;
    }//end getCode()


    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }//end getDescription()
}//end class
