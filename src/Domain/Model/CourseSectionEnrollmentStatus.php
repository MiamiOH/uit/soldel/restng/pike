<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 3:43 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionEnrollmentStatusCode;

class CourseSectionEnrollmentStatus
{
    /**
     * @var CourseSectionEnrollmentStatusCode
     */
    protected $code;
    /**
     * @var string
     */
    protected $description;

    /**
     * CourseSectionEnrollmentStatus constructor.
     *
     * @param CourseSectionEnrollmentStatusCode $code
     * @param string $description
     */
    public function __construct(
        CourseSectionEnrollmentStatusCode $code,
        string $description
    ) {
        $this->code = $code;
        $this->description = $description;
    }

    /**
     * @return CourseSectionEnrollmentStatusCode
     */
    public function getCode(): CourseSectionEnrollmentStatusCode
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
