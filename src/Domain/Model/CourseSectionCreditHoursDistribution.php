<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:04 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;

class CourseSectionCreditHoursDistribution
{
    /**
     * @var float
     */
    protected $credit;
    /**
     * @var number|null
     */
    protected $numberOfCurrentEnrollment;
    /**
     * @var number|null
     */
    protected $numberOfActiveEnrollment;


    /**
     * CourseSectionCreditHoursDistribution constructor.
     *
     * @param float $credit
     * @param int|null $numberOfCurrentEnrollment
     * @param int|null $numberOfActiveEnrollment
     */
    public function __construct(
        float $credit,
        ?int $numberOfCurrentEnrollment,
        ?int $numberOfActiveEnrollment
    ) {
        $this->credit = $credit;
        $this->numberOfCurrentEnrollment = $numberOfCurrentEnrollment;
        $this->numberOfActiveEnrollment = $numberOfActiveEnrollment;
    }



    /**
     * @return float
     */
    public function getCredit(): float
    {
        return $this->credit;
    }

    /**
     * @return int
     */
    public function getNumberOfCurrentEnrollment(): ?int
    {
        return $this->numberOfCurrentEnrollment;
    }

    /**
     * @return int
     */
    public function getNumberOfActiveEnrollment(): ?int
    {
        return $this->numberOfActiveEnrollment;
    }
}
