<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 3:44 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\StudentLevelCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class CourseSectionEnrollment
{
    /**
     * @var bool
     */
    protected $hasAttended;
    /**
     * @var Carbon
     */
    protected $lastAttendDate;
    /**
     * @var bool
     */
    protected $isEnrollmentActive;
    /**
     * @var bool
     */
    protected $isGradeSubmissionEligible;
    /**
     * @var string
     */
    protected $gradeSubmissionEligibleComment;
    /**
     * @var bool
     */
    protected $isFinalGradeRequired;
    /**
     * @var bool
     */
    protected $isMidtermGradeRequired;
    /**
     * @var GradeCollection
     */
    protected $grades;
    /**
     * @var UniqueId
     */
    protected $uniqueId;
    /**
     * @var CourseSectionEnrollmentStatus
     */
    protected $status;
    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;
    /**
     * @var float
     */
    protected $creditHours;
    /**
     * @var StudentLevelCode
     */
    protected $studentLevelCode;

    /**
     * @var TermCode
     */
    protected $termCode;

    /**
     * @var GradeModeCode
     */
    protected $gradeModeCode;

    /**
     * CourseSectionEnrollment constructor.
     *
     * @param bool $hasAttended
     * @param bool $isEnrollmentActive
     * @param bool $isGradeSubmissionEligible
     * @param string $gradeSubmissionEligibleComment
     * @param bool $isFinalGradeRequired
     * @param bool $isMidtermGradeRequired
     * @param GradeCollection $grades
     * @param UniqueId $uniqueId
     * @param CourseSectionEnrollmentStatus $status
     * @param CourseSectionGuid $courseSectionGuid
     * @param float $creditHours
     * @param StudentLevelCode $studentLevelCode
     * @param TermCode $termCode
     * @param GradeModeCode $gradeModeCode
     */
    public function __construct(
        bool $hasAttended,
        bool $isEnrollmentActive,
        bool $isGradeSubmissionEligible,
        string $gradeSubmissionEligibleComment,
        bool $isFinalGradeRequired,
        bool $isMidtermGradeRequired,
        GradeCollection $grades,
        UniqueId $uniqueId,
        CourseSectionEnrollmentStatus $status,
        CourseSectionGuid $courseSectionGuid,
        float $creditHours,
        StudentLevelCode $studentLevelCode,
        TermCode $termCode,
        GradeModeCode $gradeModeCode,
        Carbon $lastAttendDate = null
    ) {
        $this->hasAttended = $hasAttended;
        $this->isEnrollmentActive = $isEnrollmentActive;
        $this->isGradeSubmissionEligible = $isGradeSubmissionEligible;
        $this->gradeSubmissionEligibleComment = $gradeSubmissionEligibleComment;
        $this->isFinalGradeRequired = $isFinalGradeRequired;
        $this->isMidtermGradeRequired = $isMidtermGradeRequired;
        $this->grades = $grades;
        $this->uniqueId = $uniqueId;
        $this->status = $status;
        $this->courseSectionGuid = $courseSectionGuid;
        $this->creditHours = $creditHours;
        $this->studentLevelCode = $studentLevelCode;
        $this->termCode = $termCode;
        $this->gradeModeCode = $gradeModeCode;
        $this->lastAttendDate = $lastAttendDate;
    }


    public function hasAttended(): bool
    {
        return $this->hasAttended;
    }

    public function lastAttendDate(): ?Carbon
    {
        return $this->lastAttendDate;
    }

    public function isEnrollmentActive(): bool
    {
        return $this->isEnrollmentActive;
    }

    public function isGradeSubmissionEligible(): bool
    {
        return $this->isGradeSubmissionEligible;
    }

    public function getGradeSubmissionEligibleComment(): string
    {
        return $this->gradeSubmissionEligibleComment;
    }

    public function isFinalGradeRequired(): bool
    {
        return $this->isFinalGradeRequired;
    }

    public function isMidtermGradeRequired(): bool
    {
        return $this->isMidtermGradeRequired;
    }

    /**
     * @return GradeCollection
     */
    public function getGrades(): GradeCollection
    {
        return $this->grades;
    }

    public function getStatusCode(): string
    {
        return $this->status->getCode();
    }

    public function getStatusDescription(): string
    {
        return $this->status->getDescription();
    }

    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    public function getStudentLevelCode(): StudentLevelCode
    {
        return $this->studentLevelCode;
    }

    public function getUniqueId(): UniqueId
    {
        return $this->uniqueId;
    }

    /**
     * @return float
     */
    public function getCreditHours(): float
    {
        return $this->creditHours;
    }

    /**
     * @return TermCode
     */
    public function getTermCode(): TermCode
    {
        return $this->termCode;
    }

    /**
     * @return GradeModeCode
     */
    public function getGradeModeCode(): GradeModeCode
    {
        return $this->gradeModeCode;
    }
}
