<?php
/**
 * Author: liaom
 * Date: 5/9/18
 * Time: 3:04 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use Carbon\Carbon;
use MiamiOH\Pike\Domain\ValueObject\BuildingCode;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleDays;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionScheduleTypeCode;

class CourseSectionSummary
{
    /**
     * @var string
     */
    protected $description;
    /**
     * @var number|null
     */
    protected $numberOfCurrentEnrollment;
    /**
     * @var number|null
     */
    protected $numberOfActiveEnrollment;


    /**
     * CourseSectionSummary constructor.
     *
     * @param string $description
     * @param int|null $numberOfCurrentEnrollment
     * @param int|null $numberOfActiveEnrollment
     */
    public function __construct(
        string $description,
        ?int $numberOfCurrentEnrollment,
        ?int $numberOfActiveEnrollment
    ) {
        $this->description = $description;
        $this->numberOfCurrentEnrollment = $numberOfCurrentEnrollment;
        $this->numberOfActiveEnrollment = $numberOfActiveEnrollment;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getNumberOfCurrentEnrollment(): ?int
    {
        return $this->numberOfCurrentEnrollment;
    }

    /**
     * @return int
     */
    public function getNumberOfActiveEnrollment(): ?int
    {
        return $this->numberOfActiveEnrollment;
    }
}
