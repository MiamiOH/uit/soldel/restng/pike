<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:43 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;

class Department
{
    /**
     * @var DepartmentCode
     */
    protected $code;
    /**
     * @var string
     */
    protected $description;


    /**
     * Department constructor.
     *
     * @param DepartmentCode $code
     * @param string $description
     */
    public function __construct(DepartmentCode $code, string $description)
    {
        $this->code = $code;
        $this->description = $description;
    }//end __construct()


    /**
     * @return DepartmentCode
     */
    public function getCode(): DepartmentCode
    {
        return $this->code;
    }//end getCode()


    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }//end getDescription()
}//end class
