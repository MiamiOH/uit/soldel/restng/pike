<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 1:31 PM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;
use MiamiOH\Pike\Domain\ValueObject\SchoolCode;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

/**
 * Class Course
 *
 * @package MiamiOH\Pike\Domain\Model
 */
class Course
{
    /**
     * @var CourseGuid
     */
    protected $guid;
    /**
     * @var TermCode
     */
    protected $termCode;
    /**
     * @var SubjectCode
     */
    protected $subjectCode;
    /**
     * @var CourseNumber
     */
    protected $number;
    /**
     * @var TermCode
     */
    protected $termEffectedCode;
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var SchoolCode
     */
    protected $schoolCode;
    /**
     * @var string
     */
    protected $schoolName;
    /**
     * @var DepartmentCode
     */
    protected $departmentCode;
    /**
     * @var string
     */
    protected $departmentName;

    /**
     * @var string
     */
    protected $subjectDescription;

    /**
     * @var float|null
     */
    protected $creditHoursLow;
    /**
     * @var float|null
     */
    protected $creditHoursHigh;
    /**
     * @var float|null
     */
    protected $lectureHoursLow;
    /**
     * @var float|null
     */
    protected $lectureHoursHigh;
    /**
     * @var float|null
     */
    protected $labHoursLow;
    /**
     * @var float|null
     */
    protected $labHoursHigh;

    /**
     * @var string|null
     */
    protected $creditHoursDescription;
    /**
     * @var string|null
     */
    protected $labHoursDescription;
    /**
     * @var string|null
     */
    protected $lectureHoursDescription;
    /**
     * @var array
     */
    protected $creditHoursAvailable;

    /**
     * Course constructor.
     *
     * @param CourseGuid $guid
     * @param TermCode $termCode
     * @param SubjectCode $subjectCode
     * @param CourseNumber $number
     * @param TermCode $termEffectedCode
     * @param string $title
     * @param null|string $description
     * @param SchoolCode $schoolCode
     * @param string $schoolName
     * @param DepartmentCode $departmentCode
     * @param string $departmentName
     * @param string $subjectDescription
     * @param float|null $creditHoursLow
     * @param float|null $creditHoursHigh
     * @param float|null $lectureHoursLow
     * @param float|null $lectureHoursHigh
     * @param float|null $labHoursLow
     * @param float|null $labHoursHigh
     * @param null|string $creditHoursDescription
     * @param null|string $labHoursDescription
     * @param null|string $lectureHoursDescription
     * @param array $creditHoursAvailable
     */
    public function __construct(
        CourseGuid $guid,
        TermCode $termCode,
        SubjectCode $subjectCode,
        CourseNumber $number,
        TermCode $termEffectedCode,
        string $title,
        ?string $description,
        SchoolCode $schoolCode,
        string $schoolName,
        DepartmentCode $departmentCode,
        string $departmentName,
        string $subjectDescription,
        ?float $creditHoursLow,
        ?float $creditHoursHigh,
        ?float $lectureHoursLow,
        ?float $lectureHoursHigh,
        ?float $labHoursLow,
        ?float $labHoursHigh,
        ?string $creditHoursDescription,
        ?string $labHoursDescription,
        ?string $lectureHoursDescription,
        array $creditHoursAvailable
    ) {
        $this->guid = $guid;
        $this->termCode = $termCode;
        $this->subjectCode = $subjectCode;
        $this->number = $number;
        $this->termEffectedCode = $termEffectedCode;
        $this->title = $title;
        $this->description = $description;
        $this->schoolCode = $schoolCode;
        $this->schoolName = $schoolName;
        $this->departmentCode = $departmentCode;
        $this->departmentName = $departmentName;
        $this->subjectDescription = $subjectDescription;
        $this->creditHoursLow = $creditHoursLow;
        $this->creditHoursHigh = $creditHoursHigh;
        $this->lectureHoursLow = $lectureHoursLow;
        $this->lectureHoursHigh = $lectureHoursHigh;
        $this->labHoursLow = $labHoursLow;
        $this->labHoursHigh = $labHoursHigh;
        $this->creditHoursDescription = $creditHoursDescription;
        $this->labHoursDescription = $labHoursDescription;
        $this->lectureHoursDescription = $lectureHoursDescription;
        $this->creditHoursAvailable = $creditHoursAvailable;
    }

    /**
     * @return null|string
     */
    public function getLabHoursDescription(): ?string
    {
        return $this->labHoursDescription;
    }

    /**
     * @return TermCode
     */
    public function getTermCode(): TermCode
    {
        return $this->termCode;
    }

    /**
     * @return null|string
     */
    public function getLectureHoursDescription(): ?string
    {
        return $this->lectureHoursDescription;
    }

    /**
     * @return null|string
     */
    public function getCreditHoursDescription(): ?string
    {
        return $this->creditHoursDescription;
    }

    /**
     * @return CourseGuid
     */
    public function getGuid(): CourseGuid
    {
        return $this->guid;
    }

    /**
     * @return SubjectCode
     */
    public function getSubjectCode(): SubjectCode
    {
        return $this->subjectCode;
    }

    /**
     * @return CourseNumber
     */
    public function getNumber(): CourseNumber
    {
        return $this->number;
    }

    /**
     * @return TermCode
     */
    public function getTermEffectedCode(): TermCode
    {
        return $this->termEffectedCode;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return SchoolCode
     */
    public function getSchoolCode(): SchoolCode
    {
        return $this->schoolCode;
    }

    /**
     * @return string
     */
    public function getSchoolName(): string
    {
        return $this->schoolName;
    }

    /**
     * @return DepartmentCode
     */
    public function getDepartmentCode(): DepartmentCode
    {
        return $this->departmentCode;
    }

    /**
     * @return string
     */
    public function getDepartmentName(): string
    {
        return $this->departmentName;
    }

    /**
     * @return string
     */
    public function getSubjectDescription(): string
    {
        return $this->subjectDescription;
    }

    /**
     * @return float|null
     */
    public function getCreditHoursLow(): ?float
    {
        return $this->creditHoursLow;
    }

    /**
     * @return float|null
     */
    public function getCreditHoursHigh(): ?float
    {
        return $this->creditHoursHigh;
    }

    /**
     * @return float|null
     */
    public function getLectureHoursLow(): ?float
    {
        return $this->lectureHoursLow;
    }

    /**
     * @return float|null
     */
    public function getLectureHoursHigh(): ?float
    {
        return $this->lectureHoursHigh;
    }

    /**
     * @return float|null
     */
    public function getLabHoursLow(): ?float
    {
        return $this->labHoursLow;
    }

    /**
     * @return float|null
     */
    public function getLabHoursHigh(): ?float
    {
        return $this->labHoursHigh;
    }
    /**
     * @return array
     */
    public function getCreditHoursAvailable(): array
    {
        return $this->creditHoursAvailable;
    }
}
