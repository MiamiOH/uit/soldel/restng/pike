<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:46 AM
 */

namespace MiamiOH\Pike\Domain\Model;

use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

class InstructorAssignment
{
    /**
     * @var InstructorAssignmentGuid
     */
    protected $guid;
    /**
     * @var UniqueId
     */
    protected $uniqueId;
    /**
     * @var TermCode
     */
    protected $termCode;
    /**
     * @var Crn
     */
    protected $crn;
    /**
     * @var CourseSectionGuid
     */
    protected $courseSectionGuid;
    /**
     * @var bool
     */
    protected $isPrimary;

    /**
     * InstructorAssignment constructor.
     *
     * @param InstructorAssignmentGuid $guid
     * @param UniqueId $uniqueId
     * @param TermCode $termCode
     * @param Crn $crn
     * @param CourseSectionGuid $courseSectionGuid
     * @param bool $isPrimary
     */
    public function __construct(
        InstructorAssignmentGuid $guid,
        UniqueId $uniqueId,
        TermCode $termCode,
        Crn $crn,
        CourseSectionGuid $courseSectionGuid,
        bool $isPrimary
    ) {
        $this->guid = $guid;
        $this->uniqueId = $uniqueId;
        $this->termCode = $termCode;
        $this->crn = $crn;
        $this->courseSectionGuid = $courseSectionGuid;
        $this->isPrimary = $isPrimary;
    }


    /**
     * @return UniqueId
     */
    public function getUniqueId(): UniqueId
    {
        return $this->uniqueId;
    }

    /**
     * @return TermCode
     */
    public function getTermCode(): TermCode
    {
        return $this->termCode;
    }

    /**
     * @return Crn
     */
    public function getCrn(): Crn
    {
        return $this->crn;
    }

    /**
     * @return InstructorAssignmentGuid
     */
    public function getGuid(): InstructorAssignmentGuid
    {
        return $this->guid;
    }

    /**
     * @return CourseSectionGuid
     */
    public function getCourseSectionGuid(): CourseSectionGuid
    {
        return $this->courseSectionGuid;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->isPrimary;
    }
}
