<?php
/**
 * Author: liaom
 * Date: 5/11/18
 * Time: 9:54 AM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCountCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;

interface CourseSectionEnrollmentCountRepositoryInterface
{
    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $courseSectionGuidCollection
    ): CourseSectionEnrollmentCountCollection;
}
