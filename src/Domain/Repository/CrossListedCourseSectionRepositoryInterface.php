<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrossListedCourseSectionCollection;

interface CrossListedCourseSectionRepositoryInterface
{
    public function getColectionByGuidCollectioin(
        CourseSectionGuidCollection $courseSectionGuids
    ): CrossListedCourseSectionCollection;
}//end interface
