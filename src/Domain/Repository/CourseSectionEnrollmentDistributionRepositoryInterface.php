<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentDistributionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;

interface CourseSectionEnrollmentDistributionRepositoryInterface
{
    /**
     * Get a collection of course section level distribution by providing
     * a collection of course section id
     *
     * @param CourseSectionGuidCollection $courseSectionGuidCollection
     *
     * @return CourseSectionEnrollmentDistributionCollection
     */
    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $courseSectionGuidCollection
    ): CourseSectionEnrollmentDistributionCollection;
}//end interface
