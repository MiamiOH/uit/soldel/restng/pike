<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCrnCollection;
use MiamiOH\Pike\Domain\Model\CourseSection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

interface CourseSectionRepositoryInterface
{
    public function getByTermCodeCrn(
        TermCode $termCode,
        Crn $crn
    ): CourseSection;

    public function getByGuid(CourseSectionGuid $courseSectionGuid): CourseSection;

    public function getByGuids(
        CourseSectionGuidCollection $courseSectionGuids
    ): CourseSectionCollection;

    public function getCollectionByTermCodeCrnCollection(
        TermCodeCrnCollection $collection
    ): CourseSectionCollection;
}//end interface
