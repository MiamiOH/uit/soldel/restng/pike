<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:48 AM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionEnrollmentCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionGuidUniqueIdCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\CourseSectionEnrollment;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionEnrollmentRequest;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

interface CourseSectionEnrollmentRepositoryInterface
{
    public function getByUniqueIdTermCodeCrn(
        UniqueId $uniqueId,
        TermCode $termCode,
        Crn $crn
    ): CourseSectionEnrollment;

    public function getCollectionByUniqueIdTermCode(
        UniqueId $uniqueId,
        TermCode $termCode
    ): CourseSectionEnrollmentCollection;

    public function getCollectionByTermCodeCrn(
        TermCode $termCode,
        Crn $crn
    ): CourseSectionEnrollmentCollection;

    public function getCollectionByTermCodesCrnsUniqueIds(
        TermCodeCollection $termCodes,
        CrnCollection $crns,
        UniqueIdCollection $uniques,
        Offset $offset = null,
        Limit $limit = null
    ): CourseSectionEnrollmentCollection;

    public function getByCourseSectionGuidAndUniqueId(
        CourseSectionGuid $courseSectionGuid,
        UniqueId $uniqueId
    ): CourseSectionEnrollment;

    public function getCollectionByCourseSectionGuidUniqueIdCollection(
        CourseSectionGuidUniqueIdCollection $collection
    ): CourseSectionEnrollmentCollection;

    public function search(
        SearchCourseSectionEnrollmentRequest $searchRequest
    ): CourseSectionEnrollmentCollection;
}
