<?php
/**
 * Author: liaom
 * Date: 4/26/18
 * Time: 10:48 AM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\BannerIdCollection;
use MiamiOH\Pike\Domain\Collection\PersonCollection;
use MiamiOH\Pike\Domain\Collection\PidmCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\Person;
use MiamiOH\Pike\Domain\ValueObject\BannerId;
use MiamiOH\Pike\Domain\ValueObject\Pidm;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

interface PersonRepositoryInterface
{
    public function getByUniqueId(UniqueId $uniqueId): Person;

    public function getByPidm(Pidm $pidm): Person;

    public function getByBannerId(BannerId $bannerId): Person;

    public function getCollectionByUniqueIdCollection(
        UniqueIdCollection $uniqueIdCollection
    ): PersonCollection;

    public function getCollectionByPidmCollection(
        PidmCollection $pidmCollection
    ): PersonCollection;

    public function getCollectionByBannerIdCollection(
        BannerIdCollection $bannerIdCollection
    ): PersonCollection;
}
