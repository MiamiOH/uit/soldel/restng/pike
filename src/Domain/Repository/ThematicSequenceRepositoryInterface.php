<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\ThematicSequenceCollection;
use MiamiOH\Pike\Domain\Model\ThematicSequence;
use MiamiOH\Pike\Domain\ValueObject\ThematicSequenceCode;

interface ThematicSequenceRepositoryInterface
{
    public function getAll(): ThematicSequenceCollection;


    public function getByCode(ThematicSequenceCode $code): ThematicSequence;
}//end interface
