<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseCollection;
use MiamiOH\Pike\Domain\Collection\CourseGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseGuidTermCodeCollection;
use MiamiOH\Pike\Domain\Model\Course;
use MiamiOH\Pike\Domain\ValueObject\CourseGuid;
use MiamiOH\Pike\Domain\ValueObject\CourseGuidTermCode;
use MiamiOH\Pike\Domain\ValueObject\CourseNumber;
use MiamiOH\Pike\Domain\ValueObject\SubjectCode;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

interface CourseRepositoryInterface
{
    public function getCollectionByCourseGuidTermCodeCollection(
        CourseGuidTermCodeCollection $collection
    ): CourseCollection;
}//end interface
