<?php

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\DepartmentCollection;
use MiamiOH\Pike\Domain\Model\Department;
use MiamiOH\Pike\Domain\ValueObject\DepartmentCode;

interface DepartmentRepositoryInterface
{
    public function getAll(): DepartmentCollection;


    public function getByCode(DepartmentCode $code): Department;
}//end interface
