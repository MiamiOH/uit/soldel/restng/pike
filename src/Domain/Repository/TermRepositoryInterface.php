<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\TermCollection;
use MiamiOH\Pike\Domain\Model\Term;
use MiamiOH\Pike\Domain\ValueObject\TermCode;

/**
 * Interface TermRepositoryInterface
 *
 * @package MiamiOH\Pike\Domain\Repository
 */
interface TermRepositoryInterface
{


    /**
     * Get all terms.
     *
     * @return TermCollection
     */
    public function getAll(): TermCollection;


    /**
     * Get term by term code.
     *
     * @param TermCode $code
     *
     * @return Term
     */
    public function getByCode(TermCode $code): Term;

    /**
     * Get current academic term.
     *
     * @return Term
     */
    public function getCurrentTerm(): Term;


    /**
     * Get next academic term of specified term. Default is current term.
     *
     * @param TermCode|null $termCode
     *
     * @return Term
     */
    public function getNextTerm(TermCode $termCode = null): Term;


    /**
     * Get previous academic term of specified term. Default is current term.
     *
     * @param TermCode|null $termCode
     *
     * @return Term
     */
    public function getPrevTerm(TermCode $termCode = null): Term;


    /**
     * Get term by multiple term codes.
     *
     * @param TermCodeCollection $codes
     *
     * @return TermCollection
     */
    public function getTermCollectionByCodes(
        TermCodeCollection $codes
    ): TermCollection;


    /**
     * Get next number of terms of specified term. Default is current term.
     *
     * @param int $numOfTerms
     *
     * @param TermCode|null $termCode
     *
     * @return TermCollection
     */
    public function getNextTerms(
        int $numOfTerms,
        TermCode $termCode = null
    ): TermCollection;


    /**
     * Get previous number of terms of specified term. Default is current term.
     *
     * @param int $numOfTerms
     *
     * @param TermCode|null $termCode
     *
     * @return TermCollection
     */
    public function getPrevTerms(
        int $numOfTerms,
        TermCode $termCode = null
    ): TermCollection;
}//end interface
