<?php
/**
 * Author: liaom
 * Date: 5/21/18
 * Time: 1:16 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\UpdateGradeRequestCollection;
use MiamiOH\Pike\Domain\Collection\UpdateGradeResponseCollection;

interface UpdateGradeRepositoryInterface
{
    public function bulkUpdate(
        UpdateGradeRequestCollection $requestCollection
    ): UpdateGradeResponseCollection;
}
