<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CourseSectionScheduleCollection;

interface CourseSectionScheduleRepositoryInterface
{
    /**
     * Get a collection of course section schedule by providing
     * a collection of course section id
     *
     * @param CourseSectionGuidCollection $courseSectionGuidCollection
     *
     * @return CourseSectionScheduleCollection
     */
    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $courseSectionGuidCollection
    ): CourseSectionScheduleCollection;
}//end interface
