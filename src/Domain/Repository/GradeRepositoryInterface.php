<?php
/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/7/18
 * Time: 2:56 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\GradeTypeCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

interface GradeRepositoryInterface
{
    public function getCollectionByCourseSectionGuidAndUniqueId(
        CourseSectionGuid $courseSectionGuid,
        UniqueId $uniqueId
    ): GradeCollection;

    public function getCollectionByTermCodesCrnsUniqueIdsTypes(
        TermCodeCollection $termCodes,
        CrnCollection $crns,
        UniqueIdCollection $uniqueIds,
        GradeTypeCollection $types
    ): GradeCollection;

    public function getCollectionByCourseSectionGuidsUniqueIdsTypes(
        CourseSectionGuidCollection $courseSectionGuids,
        UniqueIdCollection $uniqueIds,
        GradeTypeCollection $types
    ): GradeCollection;
}
