<?php
/**
 * Author: liaom
 * Date: 5/16/18
 * Time: 12:14 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Request\SearchCourseSectionRequest;

interface SearchCourseSectionRepositoryInterface
{
    public function search(
        SearchCourseSectionRequest $searchRequest
    ): CourseSectionGuidCollection;
}
