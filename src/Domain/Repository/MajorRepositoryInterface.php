<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\MajorCodeCollection;
use MiamiOH\Pike\Domain\Collection\MajorCollection;
use MiamiOH\Pike\Domain\Model\Major;
use MiamiOH\Pike\Domain\ValueObject\MajorCode;

interface MajorRepositoryInterface
{
    public function getAll(): MajorCollection;


    public function getMajorCollectionByCodes(
        MajorCodeCollection $codes
    ): MajorCollection;


    public function getByCode(MajorCode $code): Major;
}//end interface
