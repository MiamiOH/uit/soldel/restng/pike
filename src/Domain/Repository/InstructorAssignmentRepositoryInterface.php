<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 12/14/17
 * Time: 2:52 PM
 */

namespace MiamiOH\Pike\Domain\Repository;

use MiamiOH\Pike\Domain\Collection\CourseSectionGuidCollection;
use MiamiOH\Pike\Domain\Collection\CrnCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Collection\TermCodeCollection;
use MiamiOH\Pike\Domain\Collection\UniqueIdCollection;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Request\SearchInstructorAssignmentRequest;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\Limit;
use MiamiOH\Pike\Domain\ValueObject\Offset;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;

/**
 * Interface InstructorAssignmentRepositoryInterface
 *
 * @package MiamiOH\Pike\Domain\Repository
 */
interface InstructorAssignmentRepositoryInterface
{


    /**
     * Get a collection of instructor assignment of a specified course section
     *
     * @param TermCode $termCode
     * @param Crn $crn
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByTermCodeCrn(
        TermCode $termCode,
        Crn $crn
    ): InstructorAssignmentCollection;

    /**
     * Get an instructor assignment of a specified course section and instructor's
     * unique id
     *
     * @param UniqueId $uniqueId
     * @param TermCode $termCode
     * @param Crn $crn
     *
     * @return InstructorAssignment
     */
    public function getByUniqueIdTermCodeCrn(
        UniqueId $uniqueId,
        TermCode $termCode,
        Crn $crn
    ): InstructorAssignment;

    /**
     * Get a collection of instructor assignment that a specified instructor has
     * in a specified term
     *
     * @param UniqueId $uniqueId
     * @param TermCode $termCode
     *
     * @return InstructorAssignmentCollection
     */
    public function getCollectionByUniqueIdTermCode(
        UniqueId $uniqueId,
        TermCode $termCode
    ): InstructorAssignmentCollection;

    /**
     * Get an instructor assignment by specified id
     *
     * @param InstructorAssignmentGuid $instructorAssignmentGuid
     *
     * @return InstructorAssignment
     */
    public function getByGuid(
        InstructorAssignmentGuid $instructorAssignmentGuid
    ): InstructorAssignment;

    public function getCollectionByTermCodesCrnsUniqueIds(
        TermCodeCollection $termCodes,
        CrnCollection $crns,
        UniqueIdCollection $uniques,
        Offset $offset = null,
        Limit $limit = null
    ): InstructorAssignmentCollection;

    public function getCollectionByCourseSectionGuidCollection(
        CourseSectionGuidCollection $guidCollection
    ): InstructorAssignmentCollection;

    public function getCollectionByCourseSectionGuidsUniqueIds(
        CourseSectionGuidCollection $guidCollection,
        UniqueIdCollection $uniques
    ): InstructorAssignmentCollection;

    public function search(
        SearchInstructorAssignmentRequest $searchRequest
    ): InstructorAssignmentCollection;
}//end interface
